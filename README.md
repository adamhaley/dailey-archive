# dailey
Dailey Website

##Remote Origin
[https://github.com/AxisStudios/dailey](https://github.com/AxisStudios/dailey)

##Deployment
pushing to Remote Origin repositiory automatically deploys to the sandbox environment.

ie., pushing to `origin dev` deploys to:

[http://dev-dailey.axervices.com](http://dev-dailey.axervices.com)


#To setup front end project:
```
npm install
```

#To enable a local webpack server for local development, first install webpack-dev-server and webpack globally:
```
npm install webpack-dev-server webpack -g
```

#To run the local development environment and view in browser, with a watch that rebuilds and injects automatically, run:
```
npm run start
```
 - and webpack will instruct you to view the FE project locally, ie at http://localhost:8080.
 - npm run start does not generate a bundle.js file, to do so you must run
```
npm run build
```
that must be pushed to the remote server.



##In package.json, this is for server builds:
```
node --max_old_space_size=12288 node_modules/webpack/bin/webpack.js src/main.js
```
