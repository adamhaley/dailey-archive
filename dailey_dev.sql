/*
 Navicat Premium Data Transfer

 Source Server         : axervices
 Source Server Type    : MySQL
 Source Server Version : 50623
 Source Host           : axervices-backend.cxmkn58rekqs.us-west-2.rds.amazonaws.com
 Source Database       : dailey_dev

 Target Server Type    : MySQL
 Target Server Version : 50623
 File Encoding         : utf-8

 Date: 11/18/2016 13:57:28 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `content`
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `section_id` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `data_image_id` int(11) DEFAULT NULL,
  `data_text_id` int(11) DEFAULT NULL,
  `data_video_credit_id` int(11) DEFAULT NULL,
  `data_video_credit_category_id` int(11) DEFAULT NULL,
  `data_video_credit_reel_id` int(11) DEFAULT NULL,
  `data_desktop_id` int(11) DEFAULT NULL,
  `data_download_id` int(11) DEFAULT NULL,
  `data_link_id` int(11) DEFAULT NULL,
  `data_file1_id` int(11) DEFAULT NULL,
  `data_file2_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT '0',
  `hook` varchar(200) DEFAULT NULL,
  `row_state` varchar(55) NOT NULL DEFAULT 'production',
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`id`) USING BTREE,
  KEY `idx2` (`data_text_id`) USING BTREE,
  KEY `idx3` (`data_image_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2067 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `content`
-- ----------------------------
BEGIN;
INSERT INTO `content` VALUES ('2051', null, 'contact.general', null, null, '2051', null, null, null, null, null, null, null, null, null, '0', null, '', '2016-10-12 16:10:08', '2016-11-10 09:19:02'), ('2052', null, 'projects', null, null, '2052', null, null, null, null, null, null, null, null, null, '1', null, '', '2016-10-12 16:35:25', '2016-10-12 16:36:43'), ('2053', null, 'projects.2052', null, null, '2053', null, null, null, null, null, null, null, null, null, '2', null, '', '2016-10-12 21:36:02', '2016-10-12 21:36:02'), ('2054', null, 'projects', null, null, '2054', null, null, null, null, null, null, null, null, null, '3', null, '', '2016-11-04 15:48:59', '2016-11-04 15:48:59'), ('2055', null, 'projects', null, null, '2055', null, null, null, null, null, null, null, null, null, '4', null, '', '2016-11-04 15:49:14', '2016-11-04 15:49:14'), ('2056', null, 'projects', null, null, '2056', null, null, null, null, null, null, null, null, null, '5', null, '', '2016-11-04 15:49:25', '2016-11-04 15:49:25'), ('2057', null, 'projects.2054', null, null, '2057', null, null, null, null, null, null, null, null, null, '6', null, '', '2016-11-04 15:49:53', '2016-11-04 15:49:53'), ('2058', null, 'projects.2055', null, null, '2058', null, null, null, null, null, null, null, null, null, '7', null, '', '2016-11-04 15:50:07', '2016-11-04 15:50:07'), ('2059', null, 'projects.2056', null, '1169', '2059', null, null, null, null, null, null, null, null, null, '8', null, '', '2016-11-04 15:50:28', '2016-11-18 13:56:43'), ('2060', null, 'projects', null, null, '2060', null, null, null, null, null, null, null, null, null, '9', null, '', '2016-11-10 09:43:06', '2016-11-10 09:43:06'), ('2061', null, 'projects.2060', null, '1170', '2061', null, null, null, null, null, null, null, null, null, '10', null, '', '2016-11-10 09:43:58', '2016-11-10 09:43:58'), ('2062', null, 'projects', null, null, '2062', null, null, null, null, null, null, null, null, null, '11', null, '', '2016-11-10 10:00:22', '2016-11-10 10:00:22'), ('2063', null, 'projects', null, null, '2063', null, null, null, null, null, null, null, null, null, '11', null, '', '2016-11-10 10:00:22', '2016-11-10 10:00:22'), ('2064', null, 'projects.2063', null, '1171', '2064', null, null, null, null, null, null, null, null, null, '12', null, '', '2016-11-10 10:07:20', '2016-11-10 10:07:20'), ('2065', null, 'projects.2063', null, '1172', '2065', null, null, null, null, null, null, null, null, null, '13', null, '', '2016-11-10 10:07:40', '2016-11-10 10:07:40'), ('2066', null, 'about', null, '1173', '2066', null, null, null, null, null, null, null, null, null, '14', null, '', '2016-11-18 11:19:24', '2016-11-18 11:19:24');
COMMIT;

-- ----------------------------
--  Table structure for `data_image`
-- ----------------------------
DROP TABLE IF EXISTS `data_image`;
CREATE TABLE `data_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `data_video_credit_id` int(11) DEFAULT NULL,
  `original` varchar(200) DEFAULT NULL,
  `thumb` varchar(200) DEFAULT NULL,
  `thumb1` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `image1` varchar(200) DEFAULT NULL,
  `image2` varchar(200) DEFAULT NULL,
  `image3` varchar(200) DEFAULT NULL,
  `image4` varchar(255) DEFAULT NULL,
  `image5` varchar(255) DEFAULT NULL,
  `image6` varchar(255) DEFAULT NULL,
  `image7` varchar(255) DEFAULT NULL,
  `image8` varchar(255) DEFAULT NULL,
  `image9` varchar(255) DEFAULT NULL,
  `image10` varchar(255) DEFAULT NULL,
  `image11` varchar(255) DEFAULT NULL,
  `image12` varchar(255) DEFAULT NULL,
  `image13` varchar(255) DEFAULT NULL,
  `image14` varchar(255) DEFAULT NULL,
  `original_image` varchar(255) DEFAULT NULL,
  `gallery_image` enum('yes','no') NOT NULL DEFAULT 'no',
  `gallery_name` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `row_state` varchar(55) NOT NULL DEFAULT 'live',
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1174 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `data_image`
-- ----------------------------
BEGIN;
INSERT INTO `data_image` VALUES ('1169', null, null, null, null, null, '/data/files/B5I7rChO.jpeg', '/data/images/B5I7rChO.jpeg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', ''), ('1170', null, null, null, null, null, null, '/data/images/B5I7rChO_1.jpeg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', ''), ('1171', null, null, null, null, null, null, '/data/images/Screen Shot 2016-10-26 at 9.34.54 AM.png', null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', ''), ('1172', null, null, null, null, null, '/data/images/arches_1.jpg', '/data/images/arches.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', ''), ('1173', null, null, null, null, null, '/data/images/Screen Shot 2016-11-18 at 11.18.05 AM.png', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `data_link`
-- ----------------------------
DROP TABLE IF EXISTS `data_link`;
CREATE TABLE `data_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `text` text,
  `url` varchar(200) DEFAULT NULL,
  `open_mode` enum('normal','window','popup','special') NOT NULL DEFAULT 'window',
  `popup_width` int(11) DEFAULT NULL,
  `popup_height` int(11) DEFAULT NULL,
  `bg_color` varchar(255) DEFAULT NULL,
  `special_page` varchar(255) DEFAULT NULL,
  `special_title` varchar(255) DEFAULT NULL,
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `data_text`
-- ----------------------------
DROP TABLE IF EXISTS `data_text`;
CREATE TABLE `data_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `heading` text,
  `text` text,
  `text1` text,
  `text2` text,
  `text3` text,
  `text4` text,
  `text5` text,
  `text6` text,
  `text7` text,
  `text8` text,
  `text9` text,
  `text10` text,
  `text11` text,
  `text12` text,
  `text13` text,
  `text14` text,
  `text15` text,
  `text16` text,
  `text17` text,
  `text18` text,
  `bg_color` varchar(255) DEFAULT NULL,
  `bg_image` varchar(255) DEFAULT NULL,
  `row_state` varchar(55) NOT NULL DEFAULT 'live',
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`id`) USING BTREE,
  FULLTEXT KEY `ft_text` (`text`),
  FULLTEXT KEY `ft_text1` (`text1`),
  FULLTEXT KEY `ft_text2` (`text2`),
  FULLTEXT KEY `ft_text3` (`text3`),
  FULLTEXT KEY `ft_text4` (`text4`),
  FULLTEXT KEY `ft_text5` (`text5`),
  FULLTEXT KEY `ft_text6` (`text6`),
  FULLTEXT KEY `ft_text7` (`text7`),
  FULLTEXT KEY `ft_text8` (`text8`),
  FULLTEXT KEY `ft_text9` (`text9`),
  FULLTEXT KEY `ft_text10` (`text10`),
  FULLTEXT KEY `ft_text11` (`text11`),
  FULLTEXT KEY `ft_text12` (`text12`)
) ENGINE=MyISAM AUTO_INCREMENT=2067 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `data_text`
-- ----------------------------
BEGIN;
INSERT INTO `data_text` VALUES ('2051', null, null, 'GET IN TOUCH WITH USSSS', '8687 Melrose Ave.', null, 'West Hollywood', 'CA', '90069', '(310) 360 - 3100', 'hello@daileyideas.com', null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2052', null, null, 'Honda', 'Motorcycles', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2053', null, null, null, '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">bikes</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2054', null, null, 'Google', 'GO-lang', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2055', null, null, 'Nissan', 'Skyline GT-R', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2056', null, null, 'McDonalds', 'Chicken Nuggets', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2057', null, null, 'video', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">go-lang is another language</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2058', null, null, null, '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">for the wealthy</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2059', null, null, 'video', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">is it really chicken?</span></p>', '/data/files/SAMSUNG_ICONX_SHOPPING-DIR0713.mp4', null, 'https://youtu.be/kSa-TY4oDjU', 'https://vimeo.com/157835117', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2060', null, null, 'Demo client', 'Demo project', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2061', null, null, 'image', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">test</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2062', null, null, 'Test111', 'Test Project2222', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2063', null, null, 'Test', 'Test Project', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2064', null, null, 'image', '<p><span style=\"font-family: Arial,\'Helvetica Neue\',Helvetica,sans-serif; font-size: 14pt; color: #333333;\">Lorem ipsum dolor sit amet, dicam accusam qui et. Ad assum decore tacimates eum, inermis conclusionemque vis ei. Pri unum consetetur elaboraret no, te ius officiis perfecto recusabo, vis ipsum intellegebat ei. Utamur oblique sanctus per eu, vix cu putant alienum. Cum mundi invidunt no, duo ei quem eirmod.<br /><br />Ea duo petentium voluptaria, cu sea brute percipit adversarium. Te iudico primis eum, duo periculis aliquando adolescens te. Vide mentitum duo et. Ut zril eligendi his.<br /><br />Id atqui omnium ornatus mea, an postea viderer mea. Ferri laboramus vituperatoribus ex sea, vim at facete principes. Nobis ubique singulis pri ut, et pro mazim dolores, pri congue epicuri maiestatis no. His an nullam bonorum propriae, pro ad lorem omittam, vix ad indoctum partiendo. Sit an salutatus dignissim, ne mea eius probo graeco, discere dolorum nonumes mel cu.<br /><br />At possit accusam mei, cum ei legere quaestio consequuntur, sit ea nominavi petentium. Per ex posidonium argumentum, mel indoctum accommodare ea, sea an laoreet appetere. Justo ludus no mei. Eu vix odio scripta voluptua. Vix ea omnis scribentur, nullam tamquam rationibus at vix, has omnesque mediocrem ex.<br /><br />Te affert denique usu. Mea ea indoctum disputationi. Porro delicatissimi ex quo. Eu eros posse patrioque pro, brute epicuri convenire ei pro. Per inermis civibus ei, ad nec impedit percipitur.</span></p>\n<p>&nbsp;</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2065', null, null, 'image', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">copy copy copy</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2066', null, null, 'Dailey and Associates', 'ABOUT US', null, '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\"></span>Dailey is a full-service, creative ad agency. We\'re based in Los Angeles where we\'ve been telling memorable brand stories for almost 50 years.</p>\n<div class=\"page\" title=\"Page 1\">\n<div class=\"section\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>We believe a good idea can live anywhere, which is why we engage consumers across all mediums. Whether it&rsquo;s experiential, digital, broadcast, or print, we specialize in smart executions with proven results.</p>\n<p>The only thing we love more than the work is the clients we work with, so give us a call. We can&rsquo;t wait to meet you.</p>\n</div>\n</div>\n</div>\n</div>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `data_video_credit`
-- ----------------------------
DROP TABLE IF EXISTS `data_video_credit`;
CREATE TABLE `data_video_credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `studio` varchar(200) DEFAULT NULL,
  `client` varchar(200) DEFAULT NULL,
  `director` varchar(200) DEFAULT NULL,
  `work_type` varchar(200) DEFAULT NULL,
  `show_in_works` enum('yes','no') DEFAULT 'yes',
  `release` varchar(10) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `link_type` enum('external','internal') NOT NULL DEFAULT 'external',
  `usage` varchar(200) DEFAULT NULL,
  `data_image_id` int(11) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `high_video` varchar(200) DEFAULT NULL,
  `snapshot` varchar(200) DEFAULT NULL,
  `description` text,
  `ord` int(11) NOT NULL DEFAULT '0',
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `data_video_credit_category`
-- ----------------------------
DROP TABLE IF EXISTS `data_video_credit_category`;
CREATE TABLE `data_video_credit_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `site_section_id` int(11) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT '0',
  `row_state` enum('tentative','active','updated','deleted','live','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `import`
-- ----------------------------
DROP TABLE IF EXISTS `import`;
CREATE TABLE `import` (
  `import_id` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` text,
  `SUBTITLE` text,
  `ACTIVE` varchar(4) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `EXTERNAL_URL` text,
  `EDITED` datetime DEFAULT NULL,
  `OVERVIEW` text,
  `CATEGORY` varchar(255) DEFAULT NULL,
  `Related People` text,
  `Related Practices` text,
  `Related File` text,
  `Overriding File` text,
  PRIMARY KEY (`import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `site_section`
-- ----------------------------
DROP TABLE IF EXISTS `site_section`;
CREATE TABLE `site_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT '0',
  `hook` varchar(255) DEFAULT NULL,
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `canonical_link` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `type` enum('generic','form','image_gallery','video_gallery','custom','popup','temporary') NOT NULL DEFAULT 'generic',
  `metadata_id` int(11) DEFAULT NULL,
  `visible` enum('yes','no') NOT NULL DEFAULT 'yes',
  `top` enum('yes','no') NOT NULL DEFAULT 'no',
  `bottom` enum('yes','no') NOT NULL DEFAULT 'no',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subdomain` enum('main','sexy') NOT NULL,
  `page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `site_section_path`
-- ----------------------------
DROP TABLE IF EXISTS `site_section_path`;
CREATE TABLE `site_section_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `path_by_id` text,
  `url` text,
  `path_by_index` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `section_id` (`section_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `sys_admin` tinyint(1) NOT NULL DEFAULT '0',
  `login_enable` tinyint(4) NOT NULL DEFAULT '1',
  `ord` int(11) NOT NULL DEFAULT '0',
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('50', null, 'laura', 'laura', '$2y$10$3.V8PNdnA2H0M0rjTnWL7.GBgfjFFhG4pnHeTY2Bj3lpu1UqyK6X6', 'L.Lehman@bitesizetv.com', '1', '1', '3', '', '2016-09-12 14:08:46', '2016-09-14 10:00:36', '0'), ('49', '55', 'jeffrey', 'jeffrey', '$2y$10$PDGq88G/MwSRry8hMUjoWOIlUXqYxpjVd05uHDHRmyhLBG2UAUTR.', 'jeffrey@axis-studios.com', '1', '1', '2', 'live', '2010-06-08 00:00:00', '2013-05-06 13:38:50', '0');
COMMIT;

-- ----------------------------
--  Table structure for `users_role`
-- ----------------------------
DROP TABLE IF EXISTS `users_role`;
CREATE TABLE `users_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `users_token`
-- ----------------------------
DROP TABLE IF EXISTS `users_token`;
CREATE TABLE `users_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `selector` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`selector`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `users_token`
-- ----------------------------
BEGIN;
INSERT INTO `users_token` VALUES ('120', '49', '', '891a85f31676e0a293e9e4c426cc01f5d5e6775c', '745c42718060459d6916f59649115774f0822d4f'), ('121', '49', '', '9c57dc50dca4966cfbb6f011a96b3e89ee19e3e3', '6ccaf181865d2f0de323faff22ee52dacd1c5e57'), ('122', '49', '', '2cd2e3731c468440f97d0ec8cc281dcbf959bfc1', '9d096f5c61a8139b3e150ce8349bd565f7388200'), ('123', '49', '', '6a0c6deb209d35c67ede7e17c92db5f0eb932ab5', 'e7a81496d19c9a7a940a6b36fde113910a1630dc'), ('124', '49', '', 'fbb344c6eb8c5c5941f52d9e1452d1904258cd76', 'c98453348624ae2acef74b038fd054837de1b4cc'), ('125', '49', '', 'c0e5bb71980da7fc3372cc1867a4cb6f1d57268f', '15e08475e252294b29ba17ac26734fc71c16335a'), ('126', '49', '', '4ef6430ce2885b529446575f9f96d9920b4da3e4', 'dad433dd2508726ec131f50e1a7755d441a894a0'), ('127', '49', '', '44a3923c87bb139d38a8b4837949436ec69b88bd', '60f27237b660d77ca59a531909cbcf78b8731f90'), ('128', '49', '', '334d846b9b795e4ecde3323dfa92a4b91b70e342', 'ab99c85b373ea2c382ac43569c52eec6491be274'), ('129', '49', '', '7809a5cdbeb9bc17fd5da9f68c755c5c22c5ff07', 'd5e710fcef38347fc122ece5c10893f09317c3e2');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
