<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
    include_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/common.php';

	class Home extends CI_Controller {

	    public function index() {

	        $json = [];
            $home = DB::dailey('SELECT dt.text, dt.text1, dt.text2, dt.text3
            FROM content c JOIN data_text dt ON dt.id = c.data_text_id WHERE c.section_id = "home.general"
            ');

            $slides = DB::dailey('SELECT text as "type", text1 as "video", image as "image"
            FROM content c JOIN data_text dt ON dt.id = c.data_text_id
            LEFT JOIN data_image di ON di.id = c.data_image_id
            WHERE c.section_id = "home.slideshow" AND (text2 IS NULL OR text2 = "yes")');

            if (count($home) > 0) {

                $json = [
                    'caption' => $home[0]['text'],
                    'cta' => $home[0]['text1'],
                    'button_text' => $home[0]['text2'],
                    'button_link' => $home[0]['text3'],
                    'slides' => $slides
                ];
            }


            header('Content-type: application/json');
            echo json_encode($json);
	    }
	}
