<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
    include_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/common.php';

	class Careers extends CI_Controller {

	    public function index() {

            $careers = DB::dailey('
                SELECT
                    dt.text,
                    dt.text1,
                    dt.text2,
                    dt.text3,
                    di.image
                FROM
                  content c
                JOIN
                  data_text dt ON dt.id = c.data_text_id
                LEFT JOIN
                  data_image di ON di.id = c.data_image_id
                WHERE
                  c.section_id = "careers"
            ');

            $json = ['Careers' => [
                'image' => '',
                'positions' => []
            ]];

            if (count($careers) > 0) {

                foreach ($careers as $k) {

                    $json['Careers']['positions'][] = [
                        'title' => strip_tags($k['text']),
                        'description' => strip_tags($k['text2']),
                        'excerpt' => strip_tags($k['text1']),
                        'url' => strip_tags($k['text3'])
                    ];
                }
            }

            header('Content-type: application/json');

            echo json_encode($json);
	    }
	}
