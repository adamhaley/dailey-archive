<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
    include_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/common.php';

	class Contact extends CI_Controller {

        public function index() {

            $contact = DB::dailey( '
                select
                    dt.text as "title",
                    dt.text1 as "address1",
                    dt.text2 as "address2",
                    dt.text3 as "city",
                    dt.text4 as "state",
                    dt.text5 as "zip",
                    dt.text6 as "phone",
                    dt.text7 as "email"
                from
                    content c
                join
                    data_text dt on dt.id = c.data_text_id
                where
                    c.section_id = "contact.general"
            ' );

            if (count($contact) == 0) {

                $contact[] = [
                    'title'    => '',
                    'address1' => '',
                    'address2' => '',
                    'city'     => '',
                    'state'    => '',
                    'zip'      => '',
                    'phone'    => '',
                    'email'    => ''
                ];

            } else {

            }

            $data['contact'] = $contact[0];

            header('Content-type: application/json');

            echo json_encode($data['contact']);
        }

        public function save() {

            $keys = array_keys($_POST);

            if (count($keys) > 0) {

                $_temp = json_decode($keys[0], true);

                $_POST = $_temp;
            }

            $tid = DB::dailey('insert into data_text (text, text1, text2, text3, text4, text5) values (:fname, :lname, :email, :phone, :company, :note)', $_POST);

            if ($tid > 0) {

                DB::dailey('insert into content (section_id, data_text_id) values ("contact.inquiries", :tid)', ['tid' => $tid]);

                require_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/swiftmail/swift_required.php';
                $transport = Swift_MailTransport::newInstance();
//                    $transport = Swift_AWSTransport::newInstance('AKIAJJU5EI2JYFCLDRDQ', 'As/5aHUQa3qusAkhHCfikfdPnrlab8AOj07zypuhSwXT');
//                    $transport->setEndpoint('https://email.us-east-1.amazonaws.com/');
//                    $transport->registerPlugin(
//                        new Swift_Events_ResponseReceivedListener( function ( $message, $body ) {
//                            echo sprintf( "Message-ID %s.\n", $body->SendRawEmailResult->MessageId );
//                        })
//                    );

                $html = '<p>First Name: ' . $_POST['fname'] . '</p>';
                $html .= '<p>Last Name: ' . $_POST['lname'] . '</p>';
                $html .= '<p>Email: ' . $_POST['email'] . '</p>';
                $html .= '<p>Phone: ' . $_POST['phone'] . '</p>';
                $html .= '<p>Company: ' . $_POST['company'] . '</p>';
                $html .= '<p>Notes: ' . $_POST['note'] . '</p>';

                $mailer  = Swift_Mailer::newInstance($transport);
                $message = Swift_Message::newInstance()
                    ->setSubject('Confirmation')
                    ->setFrom(array('hello@daileyideas.com' => 'Dailey Ideas'))
                    ->setTo(array('hello@daileyideas.com' => 'Dailey Ideas'))
                    ->setBcc(array('eli@axis-studios.com' => 'Dailey Ideas'))
                    ->setBody($html, 'text/html');

                $result = $mailer->send($message);

                $json['email']  = $result;
                $json['status'] = 200;

            } else {

                $json['status'] = 400;
            }

            header('Content-type: application/json');

            echo json_encode($json);
        }
    }
