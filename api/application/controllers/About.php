<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
    include_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/common.php';

	class About extends CI_Controller {

	    public function index() {

            $about = DB::dailey('SELECT dt.text as "title", dt.text1 as "heading", dt.text3 as "content", dt.text4 as "points", di.image
            FROM content c JOIN data_text dt ON dt.id = c.data_text_id LEFT JOIN data_image di ON di.id = c.data_image_id WHERE c.section_id = "about"
            ');

            header('Content-type: application/json');
            echo json_encode($about);
	    }
	}
