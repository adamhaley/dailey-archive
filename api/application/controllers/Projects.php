<?php

    defined('BASEPATH') OR exit('No direct script access allowed');
    include_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/common.php';

    class Projects extends CI_Controller {

        public function index() {

            $clients = DB::dailey('
                SELECT
                    c.id,
                    dt.text,
                    dt.text1
                FROM
                    content c
                JOIN
                    data_text dt ON dt.id = c.data_text_id
                WHERE
                   c.section_id = "projects"
            ');

            $json = [];

            if (count($clients) > 0) {

                foreach ($clients as $client) {

                    $projects = DB::dailey('
                        SELECT
                            c.id, 
                            dt.text, 
                            dt.text1,
                            dt.text2,
                            dt.text3,
                            dt.text4,
                            dt.text5,
                            dt.text6,
                            dt.text8,
                            dt.text9,
                            dt.text10,
                            di.image,
                            di.image1,
                            di.image2,
                            di.image3,
                            di.image4,
                            di.thumb
                        FROM
                            content c 
                        JOIN data_text dt ON dt.id = c.data_text_id 
                        LEFT JOIN data_image di ON di.id = c.data_image_id 
                        WHERE
                        c.section_id = "projects.' . $client['id'] . '"
                    ' . (isset($_GET['featured']) ? ' and dt.text4 = "yes"' : null));

                    if (count($projects) > 0) {

                        foreach ($projects as $project) {

                            $json[] = [
                                'id' => $project['id'],
                                'clientId' => $client['id'],
                                'client' => $client['text'],
                                'title' => $client['text1'],
                                'heroType' => $project['text'] == null ? '' : $project['text'],
                                'projectHero' => $project['image1'] == null ? '' : $project['image1'],
                                'projectHeroThumbnail' => $project['thumb'] == null ? '' : $project['thumb'],
                                'image' => $project['image'] == null ? '' : $project['image'],
                                'videoSource' => $project['text6'] == null ? '' : $project['text6'],
                                'video' => [
                                    'custom' => [
                                        'poster' => $project['image2'] == null ? '' : $project['image2'],
                                        'mp4'    => $project['text2'] == null ? '' : $project['text2'],
                                        'webm'   => $project['text3'] == null ? '' : $project['text3']
                                    ],
                                    'youtube' => [
                                        'poster' => $project['image3'] == null ? '' : $project['image3'],
                                        'url'    => $project['text4'] == null ? '' : $project['text4']
                                    ],
                                    'vimeo' => [
                                        'poster' => $project['image4'] == null ? '' : $project['image4'],
                                        'url'    => $project['text5'] == null ? '' : $project['text5']
                                    ]
                                ],
                                'heading1' => strip_tags($project['text8']),
                                //'heading2' => strip_tags($project['text9']),
                                //'heading3' => strip_tags($project['text10']),
                                'description' => strip_tags($project['text1'])
                            ];
                        }
                    }
                }
            }

            header('Content-type: application/json');

            echo json_encode($json);
        }

        public function view($id = 0) {

            $json = [];
            $projects = DB::dailey('
                SELECT
                    c.id, 
                    dt.text, 
                    dt.text1,
                    dt.text2,
                    dt.text3,
                    dt.text4,
                    dt.text5,
                    dt.text6,
                    dt.text8,
                    dt.text9,
                    dt.text10,
                    di.image,
                    di.image1,
                    di.image2,
                    di.image3,
                    di.image4,
                    di.thumb,
                    c.section_id
                FROM
                    content c 
                JOIN data_text dt ON dt.id = c.data_text_id 
                LEFT JOIN data_image di ON di.id = c.data_image_id 
                WHERE
                c.id = :id
            ', ['id' => $id]);

            if (count($projects) > 0) {

                $clientId = str_replace('projects.', '', $projects[0]['section_id']);

                $clients = DB::dailey('
                    SELECT
                        c.id,
                        dt.text,
                        dt.text1
                    FROM
                        content c
                    JOIN
                        data_text dt ON dt.id = c.data_text_id
                    WHERE
                       c.section_id = "projects"
                    AND
                       c.id = :id
                ', ['id' => $clientId]);

                foreach ($projects as $project) {

                    $json = [
                        'id' => $project['id'],
                        'clientId' => $clients[0]['id'],
                        'client' => $clients[0]['text'],
                        'title' => $clients[0]['text1'],
                        'heroType' => $project['text'] == null ? '' : $project['text'],
                        'projectHero' => $project['image1'] == null ? '' : $project['image1'],
                        'projectHeroThumbnail' => $project['thumb'] == null ? '' : $project['thumb'],
                        'image' => $project['image'] == null ? '' : $project['image'],
                        'videoSource' => $project['text6'] == null ? '' : $project['text6'],
                        'video' => [
                            'custom'  => [
                                'poster' => $project['image2'] == null ? '' : $project['image2'],
                                'mp4'    => $project['text2'] == null ? '' : $project['text2'],
                                'webm'   => $project['text3'] == null ? '' : $project['text3']
                            ],
                            'youtube' => [
                                'poster' => $project['image3'] == null ? '' : $project['image3'],
                                'url'    => $project['text4'] == null ? '' : $project['text4']
                            ],
                            'vimeo'   => [
                                'poster' => $project['image4'] == null ? '' : $project['image4'],
                                'url'    => $project['text5'] == null ? '' : $project['text5']
                            ]
                        ],
                        'heading1' => strip_tags($project['text8']),
                        //'heading2' => strip_tags($project['text9']),
                        //'heading3' => strip_tags($project['text10']),
                        'description' => strip_tags($project['text1'])
                    ];
                }
            }

            header('Content-type: application/json');

            echo json_encode($json);
        }
    }
