<!DOCTYPE html>
<html>
    <head>
        <title>Contact</title>
    </head>
    <body>
        <fieldset>
            <legend>Contact</legend>
            <ul>
                <li>Title: <?php echo $contact['title']; ?></li>
                <li>Address1: <?php echo $contact['address1']; ?></li>
                <li>Address2: <?php echo $contact['address2']; ?></li>
                <li>City: <?php echo $contact['city']; ?></li>
                <li>State: <?php echo $contact['state']; ?></li>
                <li>Zip: <?php echo $contact['zip']; ?></li>
                <li>Phone: <?php echo $contact['phone']; ?></li>
                <li>Email: <?php echo $contact['email']; ?></li>
            </ul>
        </fieldset>
    </body>
</html>