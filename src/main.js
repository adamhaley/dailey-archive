import Vue from 'vue';
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import ParentComponent from './components/parent/main'
import HomePageComponent from './components/home-page/main'
import AboutPageComponent from './components/about/main'
import ProjectsPageComponent from './components/projects-page/main'
import ProjectDetailComponent from './components/project-detail/main'
import CareerPageComponent from './components/career-page/main'
import ContactPageComponent from './components/contact-page/main'
import StyleGuideComponent from './components/style-guide/main'


import init from './init'

init();

Vue.use(VueRouter)
Vue.use(VueResource)


//common stylesheets
require('./common/css/reset')
require('./common/css/main')



const routes = [
	{ path: '/', component: ParentComponent,
		children: [
			{ path: '', component: HomePageComponent},
			{ path: 'about', component: AboutPageComponent},
			{ path: 'projects', component: ProjectsPageComponent},
			{ path: 'project/:projectid', name: 'project', component: ProjectDetailComponent},
			{ path: 'career', component: CareerPageComponent },
			{ path: 'contact', component: ContactPageComponent },
			{ path: 'style-guide', component: StyleGuideComponent }
		]
	 }
]

const router = new VueRouter({
	mode: 'history',
	routes,
	saveScrollPosition: false,
    transitionOnLoad: true

})

new Vue({
	el: '#main',
	router
})
