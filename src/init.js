import $ from 'jquery'
window.$ = $;
var debounce = require('throttle-debounce/debounce')

const init = function(){

	var resizeHandler = function(e){
		window.matchMedia("(min-width: 768px)").matches && $('#menu-nav').removeClass('sm')
		window.matchMedia("(max-width: 768px)").matches && $('#menu-nav').addClass('sm')

	}		

	$(window).on('resize', debounce(250,resizeHandler))
	$(window).on('orientationchange', debounce(250,resizeHandler))

}

module.exports = init;

