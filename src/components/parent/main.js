import Vue from 'vue'
import template from './main.tpl'
import HeaderComponent from '../header/main'
import SpinnerComponent from '../spinner/main'

require('./main.css')

const ParentComponent = Vue.component('parent-component', {
	template,
	data(){
		return {
			 loading: false,
			 transitionName: 'fade'
		}
	},
	components: {
		'header-component': HeaderComponent,
		'spinner': SpinnerComponent
	},
	created: function(){
		this.$on('data-loading-start',function(){
			this.loading = true
			console.log('loader started')
		})
		this.$on('data-loading-end',function(){
			this.loading = false
			console.log('loader ended')
		})
		this.$on('route-change',function(){

			$('body').removeClass()
			.addClass(this.$route.fullPath.replace('/',''))

			setTimeout(function(){
				window.scrollTo(0, 0)
			},250)
						
		})


	},
	watch: {
		'$route' (to, from) {
			this.$emit('route-change')

			// const toDepth = to.path.split('/').length
			// const fromDepth = from.path.split('/').length
			// this.transitionName = toDepth < fromDepth ? 'transfade-enter' : 'transfade-leave'
		}
	}
})

export default ParentComponent
