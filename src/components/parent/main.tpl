<div>
	<header-component></header-component>
	
	<transition name="fade" transition-mode="out-in">
		<keep-alive>
    		<router-view class="child-view"></router-view>
		</keep-alive>
	</transition>
	
	<spinner v-show="loading"></spinner>
</div>