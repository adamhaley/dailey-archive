import Vue from 'vue'
import template from './main.tpl'
import ProjectListComponent from '../project-list/main'
import TweenMax from 'gsap/src/uncompressed/TweenMax'
import ScrollMagic from 'scrollmagic'

import SpinnerComponent from '../spinner/main'
import $ from 'jquery'

import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import 'ScrollToPlugin'

require('./main.css')

const ProjectPageComponent = Vue.component('project-page-component', {
	template,
	components: {
		'project-list': ProjectListComponent
	},
	props: {
			featured: {
				type: Boolean,
				required: false,
				default: false
			}
	},
	data: function(){
		return {
			projects: [],
			step: 0,
			scenes: [],
			scrollController: null
		}
	},
	created: function(){
		delete this.scrollController
		setTimeout(()=>{
			this.scrollController = new ScrollMagic.Controller()
			this.fetchData()

			if(this.featured === true){
				this.step = -1
			}
		},250)
		
	},
	watch: {
		projects: function(val){
			this.$nextTick(function() {
				this.addScrollMagicScenes()
				this.step = 0
				this.$forceUpdate()
			})
		}
	},
	methods: {
		fetchData: function(){
			this.$parent.$emit('data-loading-start')
			
			var baseUrl = 'http://dailey.dev'
			baseUrl = 'http://master-dailey.axervices.com'
			// baseUrl = 'http://dev-dailey.axervices.com'

			var projectsUrl = baseUrl + '/api/projects'

			var url = this.featured? projectsUrl + '?featured=true' : projectsUrl
			
			return this.$http.get(url)
			.then(function(response){
				this.projects = response.body
				this.$parent.$emit('data-loading-end')
			}.bind(this))
		},
		addScrollMagicScenes: function(){
			var scenes = []
			var vm = this

			this.projects.forEach(function(project, index){

				var triggerElement = "#project-" + project.id
				var tween = TweenMax.fromTo(triggerElement, 0.5, {autoAlpha: 0, top: 20,overwrite: false},{autoAlpha: 1, top: 0,overwrite: false})

				var scene = new ScrollMagic.Scene({
					triggerElement: triggerElement,
					triggerHook: "onEnter",
					reverse: true
				})
				.setTween(tween)

				scene.on("enter", function(e) {
					vm.step = index
				})

				scene.on("leave", function(e) {
					vm.step = index-1
				})

				scenes.push(scene)
			});
			this.scenes = scenes

			this.scrollController.addScene(scenes)
		},
		scrollToScene: function(index){
			var el = this.scenes[index].triggerElement()
			var elID = "#" + el.id

			TweenMax.to(window, .5, {
				scrollTo:  el.offsetTop-50,
				ease: Cubic.easeInOut
			})

		}
	}
})

export default ProjectPageComponent
