<div>
	<div class="project-page page-container">
		<div class="project-list-container">
			<project-list v-for="(project,index) in projects" :project="project" :index="index" :length="projects.length"></project-list>
		</div>
		<transition name="fade" mode="in-out">
			<div class="project-list-nav" v-show="step >= 0">
				<ul>
					<li v-for="(project, index) in projects" v-bind:class="{ selected: step === index }">

						<a @click="scrollToScene(index)" ></a>
					</li>
				</ul>
			</div>
		</transition>
	</div>
</div>
