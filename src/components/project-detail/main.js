import Vue from 'vue'
import template from './main.tpl'
import DownArrowButtonComponent from '../down-arrow-button/main'
var debounce = require('throttle-debounce/debounce')
import plyr from 'plyr'
require('./main.css')

const ProjectDetailComponent = Vue.component('project-detail-component', {
	template,
	props: ['projectid','image','caption','message'],
	components: {
		'down-arrow-button': DownArrowButtonComponent
	},
	data: function(){
		return {
			'project': {
							videoSource: '',//"youtube", "vimeo", or "custom"
							video: {
								custom:{},
								youtube:{},
								vimeo:{}
							}
			},
			videoID: '',
			playing: false,
			playbackProgress: 0
		}
	},
	activated: function(){
		this.fetchData()
		.then(function(){
			this.player = plyr.setup({
				controls:[]
			})

			if(this.project.heroType !== 'video'){
				$('.player',this.$el).remove()
				return 
			}
			this.player[0].on('ready', (e) => {
				var $el = this.$el

				setTimeout(() => {
					var iframeSRC = ''
					switch(this.project.videoSource){
						case 'vimeo':
							iframeSRC = 'https://player.vimeo.com/video/' + this.videoID + '?title=0&byline=0&portrait=0'
							break

						case 'youtube':
							iframeSRC = '//youtube.com/embed/' + this.videoID
							break
					}

					$('iframe',this.$el).attr('src',iframeSRC)

				},100)
				
			})

			this.player[0].on('ended', (e) => {
				this.playing = false
				this.playbackProgress = 0
			})

		}).bind(this)

		$(window).on('resize', debounce(250,this.onResize))
		$(window).on('orientationchange', debounce(250,this.onResize))

	},
	deactivated: function(){
		// console.log('project-detail component deactivated')
		// console.log(this.$el)
		if(this.project.heroType !== 'video'){
			return;
		}
		if(this.player[0] === undefined){
			return
		}
		this.player[0].destroy()
		this.playing = false
		this.playbackProgress = 0
		
		$(window).off('resize', debounce(250,this.onResize))
		$(window).off('orientationchange', debounce(250,this.onResize))

	},
	methods: {
		fetchData: function(){
			this.$parent.$emit('data-loading-start')
			// var url = '/src/data/project.json'
			var baseUrl = 'http://master-dailey.axervices.com'
			// baseUrl = 'http://dev-dailey.axervices.com'

			var url = baseUrl + '/api/projects/view/' +this.$route.params.projectid

			this.project = {}
			return this.$http.get(url)
			.then(function(response){

				this.project = response.body
				if(this.project.video[this.project.videoSource] !== undefined){
					this.videoID = this.project.video[this.project.videoSource].url
				}
	
			}.bind(this))
		},
		scrollToDetails: function(){
			var height = $('.container').offset().top
			$("html, body").animate({ scrollTop: height + "px" })
		},
		toggleVideoPlayback: function(){
			this.playing = !this.playing
			this.player[0].togglePlay()
			if(this.playing){
				this.progressInterval = setInterval(()=>{
					this.updateProgressBar()
				},500)

			}else{
				clearInterval(this.progressInterval)

			}
		},
		updateProgressBar: function(){
			// console.log('in playbackProgress')
			if(this.player === undefined){
				return 0
			}else{
				var player = this.player[0]
				var progress = (player.getCurrentTime() / player.getDuration()) * 100
				this.playbackProgress = progress
			}
		}
	}

})

export default ProjectDetailComponent
