<div class="project-detail page-container">
	<div class="thumbnail">	
		<img v-bind:src="'http://master-dailey.axervices.com' + project.projectHero" alt="" class="hero-image" v-show="project.heroType==='image'">
		<div v-if="project.heroType !=='image'">
			<div class="player"  v-if="project.videoSource !== 'custom'" v-bind:data-type="project.videoSource" v-bind:data-video-id="videoID"></div>
		</div>
	<!-- CUSTOM HTML5 PLAYER -->
		<video v-if="project.videoSource === 'custom'" width="100%" height="100%" controls>
			<source v-bind:src="'http://dev-dailey.axervices.com/' + project.video.custom.mp4" type="video/mp4">
		
			
			Your browser does not support the video tag.
		</video>

	<!-- END CUSTOM HTML5 PLAYER -->


	</div>
	<div class="controls" v-if="project.heroType==='video'">
		<div class="play-pause" @click="toggleVideoPlayback()">
			<div class="pause" v-show="playing">
				<!--PAUSE BUTTON SVG-->
				<svg height="30" width="30" viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg"><path d="M0 832h192V192H0v640zm320-640v640h192V192H320z"/></svg>
				<!--END PAUSE BUTTON SVG-->
			</div>
			<div class="play" v-show="!playing">
				<!--PLAY BUTTON SVG-->
				<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"  viewBox="0 0 1000 1000"><path d="M929.5 480.4L91.3 14.6c-9.7-5.5-18-6.1-24.9-1.9C59.5 17 56 24.5 56 35.5v929.1c0 10.9 3.5 18.5 10.4 22.7 6.9 4.2 15.3 3.6 24.9-1.9l838.2-465.8c9.7-5.5 14.5-12 14.5-19.6 0-7.6-4.8-14.1-14.5-19.6z"/></svg>
				<!--END PLAY BUTTON SVG-->
			</div>
		</div>
		<div class="progress-bar">
			<div class="progress" v-bind:style="'width:' + playbackProgress + '%'"></div>

		</div>
	</div>

		<div class="details-cta" v-on:click="scrollToDetails()">
			<down-arrow-button text="project details" ></down-arrow-button>
		</div>


	<div class="container">
			{{project.client}} 
			<hr></hr>
			<div class="message" v-html="project.title">

			</div>
			<div class="details">
				<h2 class="details-title" v-html="project.heading1"></h2>
				<hr></hr>
				<p class="description" v-html="project.description">
				</p>

			</div>
	</div>
</div>