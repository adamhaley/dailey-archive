import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const SendLinkComponent = Vue.component('send-link-component', {
	template,
	props: ['text','url']
	
})

export default SendLinkComponent
