<div>
	<div :id="'project-' + project.id" class="project-list">
		
		<div class="prev" @click="goPrevious(index)" v-if="index>0">
			<img src="./svg/prev-arrow.svg" alt="">
		</div>
		<div class="project-list">
			 <cta-box 
			 	:projectid="project.id"
				:image="project.image"
				:caption="project.client" 
				:message="project.title" 
				cta="project details"
			>
			</cta-box>
		</div>
		<div class="next" @click="goNext(index)"  v-if="index<length-1">
			<img src="./svg/next-arrow.svg" alt="">
		</div>
		<div class="grid-icon">
			<img src="./svg/grid-icon.svg" alt="">
		</div>
	</div>
</div>