import Vue from 'vue'
import template from './main.tpl'
import CTABoxComponent from '../cta-box/main'

require('./main.css')

const ProjectListComponent = Vue.component('project-list-component', {
	template,
	components: {
		'cta-box': CTABoxComponent
	},
	created: function(){
		if(this.project === undefined){
			return
		}
		var baseUrl = 'http://master-dailey.axervices.com'
		console.log(this.project)
		var source = this.project.videoSource
		// this.project.image = baseUrl + this.project.video[source].poster
		this.project.image = baseUrl + this.project.projectHeroThumbnail

		if(!this.project.image){
			this.project.image = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Image Not Available&w=620&h=430'
		}
	},
	data: function(){
		return {
			id: null
		}
	},
	props: ['project','index','length'],
	methods: {
		goNext: function(index){
			this.$parent.scrollToScene(index+1)
		},
		goPrevious: function(index){
			this.$parent.scrollToScene(index-1)
		}
	}

})

export default ProjectListComponent
