import Vue from 'vue'
import template from './main.tpl'
import CTABoxComponent from '../cta-box/main'

require('./main.css')

const ProjectGridComponent = Vue.component('project-grid-component', {
	template,
	components: {
		'cta-box': CTABoxComponent
	}
})

export default ProjectGridComponent
