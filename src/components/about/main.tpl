<div>
	<div class="about page-container">
		<div class="about-content-container" v-for="(data, index) in pageData"> 
			<div class="about-content">
				<div>
					<h2>{{data.title}}</h2>
					<hr>

					<h1>{{data.heading}}</h1>
					
					<div class="about-content-body">
						<div class="body" v-html="data.content" v-if="data.points"></div>
						<div class="body full" v-html="data.content" v-else></div>
						<div class="points" v-html="data.points" v-if="data.points"></div>
					</div>
				</div>
				
			</div>
			<div class="about-image" v-if="data.image">
				
				<img v-bind:src="baseUrl  + data.image" alt="" />
			</div>
		</div>
	</div>
</div>