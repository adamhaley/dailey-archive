import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const AboutPageComponent = Vue.component('about-page-component', {
	template,
	created: function(){
		this.fetchData()
	},
	data: function(){
		return {
			pageData:[],
			baseUrl: ""
		}
	},
	methods: {
		fetchData: function(){
			this.$parent.$emit('data-loading-start')
			
			var baseUrl = 'http://dailey.dev'
			baseUrl = 'http://master-dailey.axervices.com'
			// baseUrl = 'http://dev-dailey.axervices.com'
			this.baseUrl = baseUrl

			var url = baseUrl + '/api/about'

			return this.$http.get(url)
			.then(function(response){
				this.pageData = response.body
				console.log(this.pageData)

				this.$parent.$emit('data-loading-end')
			}.bind(this))
		}
	}
})

export default AboutPageComponent