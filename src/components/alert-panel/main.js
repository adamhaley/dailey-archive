import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const AlertPanelComponent = Vue.component('alert-panel-component', {
	template,
	props: ['caption','message']
})

export default AlertPanelComponent
