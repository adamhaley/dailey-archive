<div class="alert-panel" >
	<h1>{{caption}}</h1>
	{{message}}

	<div class="close">
		<img src="./svg/x.svg" alt="">
	</div>
</div>