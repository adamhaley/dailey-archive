import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const DownArrowButtonComponent = Vue.component('down-arrow-button-component', {
	template,
	props: ['text']
	
})

export default DownArrowButtonComponent
