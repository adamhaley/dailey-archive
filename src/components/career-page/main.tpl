<div>

	<div class="career-landing page-container">
		<div class="thumbnail">
		</div>
		<div class="container">

			<career-box v-for="(position, index) in careers.positions"
				boxclass="light"
				:caption="position.title"
				:message="position.excerpt"
				:url="position.url"
			></career-box>
			
		</div>
	</div>
</div>
