import Vue from 'vue'
import template from './main.tpl'
import CareerBoxComponent from '../career-box/main'
import HeaderComponent from '../header/main'

import $ from 'jquery'



require('./main.css')

const CareerLandingComponent = Vue.component('career-landing-component', {
	template,
	components: {
		'header-component': HeaderComponent,
		'career-box': CareerBoxComponent
	},
	data: function(){
		return {
			careers: {}
		}
	},
	created: function(){
		this.fetchData()
		// var lastScrollTop = $('.thumbnail').offset().top
		// var difference = 0
		// $(window).scroll(function(){
		// 	var scrollTop = $(this).scrollTop()
		// 	//get difference from last scrollTop
		// 	difference = Math.floor((scrollTop-lastScrollTop))
		// 	console.log(difference)
		// 	// console.log(scrollTop)
		// 	$('.thumbnail').offset({top:lastScrollTop+difference})
		// 	lastScrollTop = $('.thumbnail').offset().top
		// })

	},
	methods: {
		fetchData: function(){
			this.$parent.$emit('data-loading-start')
			
			var baseUrl = 'http://dailey.dev'
			baseUrl = 'http://master-dailey.axervices.com'
			// baseUrl = 'http://dev-dailey.axervices.com'

			var url = baseUrl + '/api/careers'

			return this.$http.get(url)
			.then(function(response){
				this.careers = response.body.Careers

				this.$parent.$emit('data-loading-end')
			}.bind(this))
		}
	}
	
})

export default CareerLandingComponent

