<div class="cta-box" v-bind:class="{ imagebox: image }">
	<div class="value-proposition">
		<div class="thumbnail" v-show="image">
			<img v-bind:src="image" alt="" />
		</div>
		<div class="container">
			{{caption}}
			<hr></hr>
			<div class="message">
				{{message}}
			</div>
		</div>

	</div>
	<div class="call-to-action">
		<div class="container">

			<div v-if="url">
				<a :href="url" target="_new">{{cta}} &rarr;</a>
			</div>
			<div v-else>
				<router-link :to="{ name: 'project', params: { projectid: projectid }}">{{cta}} </router-link>
				&rarr;
			</div>
		</div>
	</div>
	
</div>
