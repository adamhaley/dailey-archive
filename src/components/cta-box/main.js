import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const CTABoxComponent = Vue.component('cta-box-component', {
	template,
	props: ['projectid','image','caption','message','cta','url'],
	mounted: function(){
		var loaded = false
		var $img = $('img',$(this.$el))

		$img.on('load',function(){
			// $img.css({ width: $img[0].naturalWidth + 'px', height: $img[0].naturalHeight + 'px'})
		})
	}

})

export default CTABoxComponent
