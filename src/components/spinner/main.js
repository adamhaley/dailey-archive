import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const SpinnerComponent = Vue.component('spinner-component', {
	template
})

export default SpinnerComponent
