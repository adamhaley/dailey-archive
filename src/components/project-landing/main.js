import Vue from 'vue'
import template from './main.tpl'
import CTABoxComponent from '../cta-box/main'


require('./main.css')

const ProjectLandingComponent = Vue.component('project-landing-component', {
	template,
	components: {
		'cta-box': CTABoxComponent
	}
})

export default ProjectLandingComponent
