<div class="project-landing page-container">
	<div class="prev">
		<img src="./svg/prev-arrow.svg" alt="">
	</div>
	<br>
	<cta-box
		projectid="1"
		image="http://placekitten.com/340/220"
		caption="Honda Powersports"
		message="PROJECT NAME GOES HERE"
		cta="project details"
	></cta-box>
	<br>
	<div class="next">
		<img src="./svg/next-arrow.svg" alt="">
	</div>
	<div class="grid-icon">
		<img src="./svg/grid-icon.svg" alt="">
	</div>

</div>