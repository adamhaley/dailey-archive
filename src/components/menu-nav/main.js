import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const SpinnerComponent = Vue.component('spinner-component', {
	template,
	methods: {
		toggleMenu: function(){
			this.$parent.toggleMenu()
		}
	}
})

export default SpinnerComponent
