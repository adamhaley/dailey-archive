<div>

	<div class="contact page-container">
		<div class="container">
			<div class="caption">{{pageData.title}}</div>
			<br /><br />
			{{pageData.address1}}<br />
			{{pageData.address2}}<br />
			{{pageData.city}}, {{pageData.state}} {{pageData.zip}}

			<br /><br />
			<img src="./svg/phone.svg" class="phone" alt="">{{pageData.phone}}

			<div class="send-link">
				<div class="text"><span><a :href="'mailto:' + pageData.email" target="_new">{{pageData.email}} &rarr;</a></span></div>
			</div>
		</div>
		<div class="thumbnail">

		</div>
		<div class="contact-form-container">
			<form action="">
				<div class="call-to-action"></div>
				<div class="contact-form">
					
						<input type="text" name="fname" title="Please enter your first name" placeholder="First Name" required />
						<input type="text" name="lname" title="Please enter your last name"  placeholder="Last Name" required />
						<input type="email" name="email" title="Please enter your email"  placeholder="Email" required />
						<input type="telephone" name="phone" title="Please enter your phone number" placeholder="Phone (optional)" />
						<input type="text" name="company" title="Please enter your Company"  placeholder="Company (optional)" />
						<textarea name="note" cols="30" title="Please enter your first name"  rows="10" placeholder="Note" required></textarea>

						<input type="submit" style="display:none" name="submitButton">
					
				</div>
			
			<submit-button text="send"></submit-button>
			</form>
		</div>
		<transition name="fade">
			<alert-panel v-show="success" caption="Success!" message="We will get in touch with you soon."></alert-panel>
		</transition>
	</div>
</div>
