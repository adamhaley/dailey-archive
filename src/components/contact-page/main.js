import Vue from 'vue'
import template from './main.tpl'
import SubmitButtonComponent from '../submit-button/main'
import SendLinkComponent from '../send-link/main'
import HeaderComponent from '../header/main'
import CTABoxComponent from '../cta-box/main'
import AlertPanelComponent from '../alert-panel/main'
import TweenMax from 'gsap/src/uncompressed/TweenMax';
import $ from 'jquery'

require('./main.css')

const ContactPageComponent = Vue.component('contact-page-component', {
	template,
	mounted: function(){
		// console.log("mounted..running tween")
		// console.log(this.el)
		setTimeout(function(){
			var tween = TweenMax.to('.contact .contact-form-container', 5, {opacity: 1})
		},250)
		
		this.fetchData()


		var v = this
		//form submit event handler
		$('form').submit(function(e){
			e.preventDefault()
			var form = $(this)
			var payload = {
				fname:$("input[name='fname']").val(),
				lname:$("input[name='lname']").val(),
				email:$("input[name='email']").val(),
				phone:$("input[name='phone']").val(),
				company:$("input[name='company']").val(),
				note:$("textarea[name='note']").val()
			}

			v.processing = true
			v.$http.post('http://master-dailey.axervices.com/api/contact/save', payload,  { headers: {
       			'Content-Type': 'multipart/form-data'
   			}})
   			.then(function(data){
				// console.log("API POST SUCCESSFUL")
				// console.log(data)
				v.processing = false
				v.success = true
				$("input[name='fname']").val('')
				$("input[name='lname']").val('')
				$("input[name='email']").val('')
				$("input[name='phone']").val('')
				$("input[name='company']").val('')
				$("textarea[name='note']").val('')
				
				setTimeout(function(){
					v.success = false
				},4000)
			})
		})

	},
	data: function(){
		return {
			pageData: "",
			processing: false,
			success:false
		}
	},
	components: {
		'header-component': HeaderComponent,
		'submit-button': SubmitButtonComponent,
		'send-link': SendLinkComponent,
		'cta-box': CTABoxComponent,
		'alert-panel': AlertPanelComponent
	},
	methods: {
		fetchData: function(){
			this.$parent.$emit('data-loading-start')
			
			var baseUrl = 'http://dailey.dev'
			baseUrl = 'http://master-dailey.axervices.com'
			// baseUrl = 'http://dev-dailey.axervices.com'

			var url = baseUrl + '/api/contact'
			
			return this.$http.get(url)
			.then(function(response){
				this.pageData = response.body
				this.$parent.$emit('data-loading-end')
			}.bind(this))
		},
		submitForm: function(){
			console.log('IN SUBMIT FORM')
			$('form')[0].submitButton.click()

		}
	}
})

export default ContactPageComponent
