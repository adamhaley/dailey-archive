<div class="career-box-container" v-bind:class="boxclass">
	<div class="container">
		<h2 v-html="caption"></h2>
		<div class="message" v-html="message"></div>
	</div>
	<div @click="goUrl()">
		<send-link text="view details" ></send-link>
	</div>
<div>
