import Vue from 'vue'
import template from './main.tpl'
import SendLinkComponent from '../send-link/main'

require('./main.css')

const CareerBoxComponent = Vue.component('career-box-component', {
	template,
	props: ['caption','message','boxclass','url'],
	components: {
		'send-link': SendLinkComponent
	},
	methods: {
		goUrl: function(){
			console.log(this.caption)
			window.open(this.url,"_new")
		}
	}
})

export default CareerBoxComponent
