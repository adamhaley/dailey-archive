<div>
	<div class="home-page page-container">
		<div class="background-image" v-bind:style="'background-image: url(' + baseUrl + pageData.slides[0].image + ');background-size:cover;'">

			<div class="box-container home-cta-container">
				<cta-box 
				projectid="1"
				image=""
				:caption="pageData.caption" 
				:message="pageData.cta" 
				:cta="pageData.button_text"
				:url="pageData.button_link"
				>
				</cta-box>
			</div>

			<div class="next-arrow" @click="scrollToProjects()">
				<img src="./svg/next-arrow.svg" alt="">
			</div>
		</div>
		<projects-page :featured="true"></projects-page>
		
	</div>
</div>
