import Vue from 'vue'
import template from './main.tpl'
import HeaderComponent from '../header/main'
import CTABoxComponent from '../cta-box/main'
import ProjectListComponent from '../project-list/main'
import ProjectsPageComponent from '../projects-page/main'
import ScrollMagic from 'scrollmagic'
import TweenMax from 'gsap/src/uncompressed/TweenMax'


require('./main.css')


const HomePageComponent = Vue.component('home-page-component', {
	template,
	components: {
		'header-component': HeaderComponent,
		'cta-box': CTABoxComponent,
		'project-list': ProjectListComponent,
		'projects-page': ProjectsPageComponent
	},
	data: function(){
		return {
			projects: [],
			step: -1,
			scenes: [],
			pageData: {},
			baseUrl: ""

		}
	},
	activated: function(){
		this.baseUrl = 'http://master-dailey.axervices.com'
		this.fetchData()
		.then(function(){
			console.log('DATA HAS BEEN FETCHED')
			console.log(this.pageData)
			console.log(this.pageData.slides[0].image)
			this.pageData.backgroundImage = this.pageData.slides[0].image
		})
	},
	methods: {
		fetchData: function(){
			this.$parent.$emit('data-loading-start')

			var baseUrl = 'http://master-dailey.axervices.com'
			// baseUrl = 'http://dev-dailey.axervices.com'

			var url = baseUrl + '/api/home'

			return this.$http.get(url)
			.then(function(response){
				this.pageData = response.body
				console.log("PAGE DATA")
				console.log(this.pageData)
				this.$parent.$emit('data-loading-end')

			}.bind(this))
		},
		scrollToProjects: function(){
			var height = $(".project-page").offset().top
			$("html, body").animate({ scrollTop: height + "px" })

		}
	}
})

export default HomePageComponent
