import Vue from 'vue'
import template from './main.tpl'
import MenuNavComponent from '../menu-nav/main'

require('./main.css')

const HeaderComponent = Vue.component('header-component', {
	template,
	components: {
		'menu-nav': MenuNavComponent
	},
	data: function(){
		return {
			menuOpen: false
		}
	},
	created: function(){
		this.$parent.$on('route-change', this.closeMenu)
	},
	methods: {
		toggleMenu: function(){
			this.menuOpen = !this.menuOpen
		},
		closeMenu: function(){
			this.menuOpen = false
		}
	}
	

})

export default HeaderComponent
