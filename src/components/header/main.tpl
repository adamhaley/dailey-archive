<div id="header" v-bind:class="{ open: menuOpen }">
	<div class="logo">
		<!-- LOGO SVG -->
		<router-link to="/"><img src="/svg/logo.svg"></router-link>
		<!-- END LOGO SVG -->
	</div>
	<transition name="slide">
		<menu-nav v-show="menuOpen"></menu-nav>
	</transition>

    <div class="menu-button" v-on:click="toggleMenu()" >
    	<div class="button-container">
	    	<span></span>
			<span></span>
			<span></span>
		</div>
    </div>
</div>
