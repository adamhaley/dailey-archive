<div>
	<h2>Header:</h2>

	<h2>Header/Close Icon</h2>
	<div style="text-align:center">
		<img src="./svg/close-icon.svg" alt="">
	</div>
	<h2>Spinner:</h2>
	<div>
		<spinner></spinner>
	</div>
	<h2>CTA Box:</h2>
	<div>
		<cta-box 
			image=""
			caption="Dailey and Associates" 
			message="Value Propositon Here" 
			cta="see how we did it"
		>
		</cta-box>
	</div>

	<h2>Menu Nav:</h2>
	<div>
		<!-- <menu-nav></menu-nav> -->
	</div>
	<h2>Project List:</h2>
	<div>
<!-- 		<project-list></project-list> -->
	</div>

	<h2>Project Landing:</h2>
	<div>
		<project-landing></project-landing>
	</div>
	
	<h2>Project Detail:</h2>
	<div>
		<project-detail caption="Dole Fruit Bowls" message="Project Name Goes Here"></project-detail>	
	</div>
	<h2>Career Landing:</h2>
	<div>
		<career-landing></career-landing>
	</div>
	<br />
	<h2>Contact:</h2>
	<div>
		<contact></contact>
	</div>
	<h2>Loading Animation:</h2>
	<div class="loader loader--style8">
		<!-- from http://codepen.io/aurer/pen/jEGbA -->
		<img src="./svg/loading.svg" alt="">
	</div>
	<br />
	<h2>Alert Panel:</h2>
	<div>
		<alert-panel caption="Success!" message="We will get in touch with you soon."></alert-panel>
	</div>


	<h1>{{message}}</h1>
</div>
