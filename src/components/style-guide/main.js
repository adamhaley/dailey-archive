import Vue from 'vue'
import template from './main.tpl'
import HeaderComponent from '../header/main'
import SpinnerComponent from '../spinner/main'
import CTABoxComponent from '../cta-box/main'
import MenuNavComponent from '../menu-nav/main'
import ProjectListComponent from '../project-list/main'
import ProjectLandingComponent from '../project-landing/main'
import ProjectDetailComponent from '../project-detail/main'
import CareerLandingComponent from '../career-page/main'
import ContactComponent from '../contact-page/main'
import AlertPanelComponent from '../alert-panel/main'

require('./reset.css')
require('./main.css')

const MainLayoutComponent = Vue.component('main-layout-component', {

	template,
	data(){
		return {
			message: ""
		}

	},
    components: {
        'header-component': HeaderComponent,
        'spinner': SpinnerComponent,
        'menu-nav': MenuNavComponent,
        'project-list': ProjectListComponent,
        'project-landing': ProjectLandingComponent,
        'cta-box': CTABoxComponent,
        'menu-nav': MenuNavComponent,
        'project-list': ProjectListComponent,
        'project-detail': ProjectDetailComponent,
        'career-landing': CareerLandingComponent,
        'contact': ContactComponent,
        'alert-panel': AlertPanelComponent

    }
})

export default MainLayoutComponent
