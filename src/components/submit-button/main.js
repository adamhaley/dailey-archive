import Vue from 'vue'
import template from './main.tpl'

require('./main.css')

const SubmitButtonComponent = Vue.component('submit-button-component', {
	template,
	props: ['text'],
	computed: {
		processing: function(){
			return this.$parent.processing
		}
	},
	methods: {
		submitForm: function(){
			this.$parent.submitForm()
			
		}
	}
	
})

export default SubmitButtonComponent
