<div class="submit-button" @click="submitForm()">

	<img class="processing" src="./svg/loading.svg" v-if="processing">

	<span v-if="!processing">
	{{text}} &rarr;
	</span>
</div>
