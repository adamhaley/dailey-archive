# Axistools - CMS

Highly configurable CMS.

## Installation

Axistools is actually a Client-Side application. But you may want to install the Server-Side application as well.

### Install Client-Side

Let's assume that we want to install the application under the `axis` folder. In that case we have to execute the following commands:

```bash
# clone the client-side project
$ git clone https://github.com/AxisStudios/axistools axis

# copy the `config-sample` directory to `config` and edit the `config/general.js` file
$ cd axis
$ cp -r config-sample config

# install the JavaScript dependencies
$ bower install
```

### Install Server-Side (optional)

Assuming that we have installed the client-side application under the `axis` folder, we should execute the following commands:

```bash
# enter into the application directory
$ cd axis

# clone the server-side project into the server directory
$ git clone https://github.com/AxisStudios/axistools-server server

# copy the `config-sample` directory to `config` and edit the `config/general.php` file
$ cd server
$ cp -r config-sample config
```

Finally test the application from your browser:  
http://www.yourdomain.com/axistools
