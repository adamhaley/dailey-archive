(function ($) {

    /**
     * Available methods.
     * @var {Object} of functions
     */
    var _methods = {
        /**
         * Creates menu.
         *
         * @param {Document} xml XML Document
         *
         * @return {jQuery}
         */
        'create': function (xml) {
            var items = $('menu > item[align]', xml);
            var menu = document.createElement('UL');
            menu.classList.add('navigation');

            // appends left menu items
            items.each(function () {
                var item = $(this);
                if (item.attr('align') != 'right') {
                    makeSubmenu(menu, this);
                }
            });
            
            // appends right menu items
            $(items.get().reverse()).each(function () {
                var item = $(this);
                if (item.attr('align') == 'right') {
                    makeSubmenu(menu, this);
                }
            });

            return $(menu);
        }
    };

    /**
     * Creates the submenu.
     *
     * @param {HTMLUListElement} menu Menu or submenu
     * @param {Element}          item Item node
     *
     * @return {Void}
     */
    function makeSubmenu(menu, item) {
        // creates Anchor element
        var a = document.createElement('A');
        a.text = item.getAttribute('title');
        if (item.getAttribute('is_empty') == 'true') {
            a.classList.add('disabled');
            a.addEventListener('click', function (event) {
                event.preventDefault();
                this.blur();
                return false;
            });
        }
        
        // creates List Item element
        var li = document.createElement('LI');
        li.appendChild(a);
        if (item.getAttribute('align') == 'right') {
            li.classList.add('right');
        }
        
        var subItems = $.grep(item.childNodes, function (node) {
            return node instanceof Element;
        });
        if (subItems.length > 0) {
            // submenu entry
            a.classList.add('submenu');
            a.addEventListener('click', function (event) {
                event.preventDefault();
                this.blur();
                return false;
            });
            
            // appends submenu entries
            var ul = document.createElement('UL');
            var len = subItems.length;
            for (var i = 0; i < len; i++) {
                makeSubmenu(ul, subItems[i]);
            }
            li.appendChild(ul);
        } else {
            a.setAttribute('href', '?section=' + $.spUri('encode', item.getAttribute('id')));
        }

        menu.appendChild(li);
    }

    /**
     * Main method.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.menu = $.fn.menu = function (methodName) {
        var method = _methods[methodName];

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        // calls method, passing all arguments except the first one
        return method.apply(this, Array.prototype.slice.call(arguments, 1));
    };
})(jQuery);
