$.require(
    [
        'jquery.axis',
        'jquery.sp-request',
        'jquery.sp-uri',
        'jquery.sp-text',
        'jquery.sp-html',
        'jquery.sp-modal',
        'jquery.menu',
        'jquery.section-notes',
        'jquery.table',
        'jquery.table-navigator'
    ],
    function () {
        /**
         * Help documentation.
         * @var {String}
         */
        var _help = '';
        
        /**
         * Login controller.
         * @var {String}
         */
        var _loginController = 'login.php';

        /**
         * Main function.
         *
         * @return {Void}
         */
        function main() {
            var section = $.spRequest('get', 'section', '');
            
            // removes the preloader
            $('.main').removeClass('preloader');
            
            centerContainer();

            $.axis(
                'get', 'home.php', {section: section, disableModalMessage: 'expired_session'}
            ).done(function (xml) {
                make(xml);
            }).fail(function (data) {
                var redirect = location.pathname + location.search;
                var message = $('root > error > message', data).text();

                // redirects only when the session has expired
                if (message.indexOf('Your session has expired') >= 0) {
                    $.axis('redirect', 'login.html?redirect=' + $.spUri('encode', redirect));
                }
            });
        }
        
        /**
         * Centers the main container horizontally.
         * 
         * @return {Void}
         */
        function centerContainer() {
            var win = $(window);
            var main = $('.main');
            var winWidth = win.width() / $.axis('zoom');
            var mainWidth = Math.max(960, 0.95 * winWidth);
            var marginLeft = Math.max(0, (winWidth - mainWidth) / 2);

            main.width(mainWidth).css('margin-left', marginLeft);
            $(window).one('resize', centerContainer);
        }

        /**
         * Makes user interface.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function make(xml) {
            var title = $('title', xml).text();
            var footerText = $('footer-text', xml).text();
            var refreshMenu = $('refresh', xml).text() == 'true';
            var externalModuleSource = $('external-module-source', xml).text();

            _help = $('help', xml).text();

            // title and footer text
            $(document).attr('title', title);
            $('#footer').html(footerText);

            makeBackground(xml);
            makeLogo(xml);
            makeMainMenu(xml);
            makeSectionNotes(xml);
            makeTableList(xml);
            makeActions();
            update();

            if (!$.spText('empty', externalModuleSource)) {
                var ui = $.axis('window', externalModuleSource);

                ui.on('close', function () {
                    if (refreshMenu) {
                        $(window).trigger('refresh');
                    }
                });
            }
        }

        /**
         * Properly aligns the menu on the right.
         *
         * @return {Void}
         */
        function fixRightMenu() {
            var entries = $('.navigation > li.right');

            $.each(entries, function () {
                var entry = $(this);
                var submenu = $('> ul', entry);

                if (submenu.length > 0) {
                    submenu.css('left', entry.width() - submenu.width() - 1);
                    fixRightSubmenu(submenu);
                }
            });
        }

        /**
         * Properly aligns the submenu on the right.
         *
         * @param {jQuery} menu Menu entry
         *
         * @return {Void}
         */
        function fixRightSubmenu(menu) {
            var entries = $('> li', menu);

            $.each(entries, function () {
                var entry = $(this);
                var submenu = $('> ul', entry);

                if (submenu.length > 0) {
                    var width = submenu.width();
                    submenu.css('left', -width - 1).width(width);
                }
            });
        }

        /**
         * Makes actions.
         *
         * @return {Void}
         */
        function makeActions() {
            $('#help').click(function () {
                if (!$.spText('empty', _help)) {
                    // opens the help docs in a new tab/window
                    window.open($.axis('absPath', _help));
                }

                this.blur();
                return false;
            });

            $('#logout').click(function () {
                $.axis('post', _loginController, {action: 'logout'}).done(function () {
                    $.axis('redirect', 'login.html');
                });
                this.blur();
                return false;
            });

            $('#bug-report').click(function () {
                var msg = 'Please specify when and where the error happens. For example:\n\n'
                    + 'The error happens when I try to update a record of the table AnyTable, '
                    + 'section AnySection.';
                $.axisPrompt(
                    'BUG REPORT',
                    [
                        {label: 'Summary', width: '300pt'},
                        {label: 'Description', type: 'textarea', value: msg, width: '300pt'}
                    ],
                    function (summary, description) {
                        if ($.spText('empty', description)) {
                            $.axis('error', 'Description is required');
                        }

                        $.axis(
                            'post',
                            'bug-report.php',
                            {summary: summary, description: description}
                        ).done(function (xml) {
                            $.axis('alert', $('message', xml).text());
                        });

                        this.remove();
                    }
                );

                this.blur();
                return false;
            });

            $(window).on('refresh', function () {
                var section = $.spRequest('get', 'section', '');

                $.axis('get', 'home.php', {section: section, 'compile-sitemap': true})
                    .done(function (xml) {
                        $('.navigation').remove();
                        makeMainMenu(xml);
                    });
            });
        }

        /**
         * Updates user interface.
         *
         * @return {Void}
         */
        function update() {
            var win = $(window);
            var main = $('.main');
            var mainWidth = main.width();

            $('#help').toggleClass('disabled', $.spText('empty', _help));

            win.one('resize', function () {
                update();
            });

            // resizes elements
            $('.section-notes').width(mainWidth - 35);
            $('.table').width(mainWidth - 35);
        }

        /**
         * Sets the background image.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function makeBackground(xml) {
            var background = $('background', xml);
            var image = background.attr('image');
            var color = background.attr('color');
            
            $('html').css(
                'background',
                color + ' url(' + $.spHtml('encode', image) + ') repeat left top'
            );
        }

        /**
         * Sets the logo image.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function makeLogo(xml) {
            var image = $('logo', xml).attr('image');
            var logo = $('.main .header .logo');

            logo.append('<img src="' + $.spHtml('encode', image) + '" alt="" />');
        }

        /**
         * Creates the navigation menu.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function makeMainMenu(xml) {
            var header = $('.main .header');
            var menu = $.menu('create', xml);

            header.append(menu);
            fixRightMenu();
        }

        /**
         * Creates the section notes.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function makeSectionNotes(xml) {
            var content = $('.content');
            var notes = $('.section-notes');

            notes.sectionNotes('update', xml);
        }

        /**
         * Creates the list of the tables.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function makeTableList(xml) {
            var numTables = parseInt($('num_tables', xml).text(), 10);

            appendTables(numTables);
        }

        /**
         * Appends the tables sequentially.
         *
         * @param {Number} numTables Number of tables
         * @param {Number} position  Table position (default is 0)
         *
         * @return {Void}
         */
        function appendTables(numTables, position) {
            var section = $.spRequest('get', 'section', '');
            var content = $('.content');

            if (position === undefined) {
                position = 0;
            }

            if (position < numTables) {
                $(content).table('append', section, position, function () {
                    appendTables(numTables, position + 1);
                });
            }

            update();
        }

        main();
    }
);
