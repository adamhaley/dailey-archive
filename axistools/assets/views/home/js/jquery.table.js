(function ($) {

    /**
     * Table class.
     *
     * @param {String}   section  Section name
     * @param {Number}   tablePos Table position
     * @param {Function} onLoad   On 'load' listener
     *
     * @return {Void}
     */
    function Table(section, tablePos, onLoad) {
        var self = this;
        
        /**
         * Controller.
         * @var {String}
         */
        this._controller = 'list.php';
        
        /**
         * Thumbnail controller.
         * @var {String}
         */
        this._thumbController = $.spText('rtrim', SERVER_URI, '/') + '/image-thumb.php';

        /**
         * Node instance.
         * @var {jQuery}
         */
        this._target = null;

        /**
         * Section name.
         * @var {String}
         */
        this._section = section;

        /**
         * Table position.
         * @var {Number}
         */
        this._tablePos = tablePos;
        
        /**
         * Filter.
         * @var {Object}
         */
        this._filter = {};
        
        /**
         * Order.
         * @var {Array.<{index: column index, asc: 'true'|'false'|empty}>}
         */
        this._order = [];
        
        /**
         * Has the order changed?
         * @var {Boolean}
         */
        this._orderHasChanged = false;

        /**
         * Page number.
         * @var {Number}
         */
        this._pageNumber = 0;

        /**
         * Number of visible rows.
         * @var {Number}
         */
        this._numRows = 0;

        /**
         * Maximum number or rows.
         * @var {Number}
         */
        this._maxRows = 0;

        /**
         * Is the table sortable?
         * @var {Boolean}
         */
        this._isSortable = false;
        
        /**
         * Are insertions allowed?
         * @var {Boolean}
         */
        this._isInsertable = true;
        
        /**
         * Are editions allowed?
         * @var {Boolean}
         */
        this._isEditable = true;
        
        /**
         * Are deletions allowed?
         * @var {Boolean}
         */
        this._isDeletable = true;

        /**
         * External module editor.
         * @var {String}
         */
        this._externalModule = '';
        
        /**
         * Before delete trigger.
         * @var {String}
         */
        this._beforeDelete = '';
        
        /**
         * After delete trigger.
         * @var {String}
         */
        this._afterDelete = '';
        
        /**
         * Refreshes main menu?
         * @var {Boolean}
         */
        this._refreshMenu = false;

        $.axis(
            'get', this._controller, {section: this._section, table: this._tablePos}
        ).done(function (xml) {
            self._refreshMenu = $('root > refresh', xml).text() == 'true';
            self._beforeDelete = $('root > triggers > before-delete', xml).text();
            self._afterDelete = $('root > triggers > after-delete', xml).text();
            self._make(xml);
            $.proxy(onLoad, self)();
        });
    }

    /**
     * Appends the table node to the container.
     *
     * @param {jQuery} container Container
     *
     * @return {jQuery}
     */
    Table.prototype.appendTo = function (container) {
        this._target.appendTo(container);
    }

    /**
     * Makes the table node.
     *
     * @param {Document} xml XML Document
     *
     * @return {jQuery}
     */
    Table.prototype._make = function (xml) {
        this._target = $(
            $('<div class="table" />').append(
                this._makeHeader(xml),
                this._makeFilter(xml),
                this._makeNotes(xml),
                this._makeBody(xml),
                this._makeFooter(xml)
            )
        );

        this._update(xml);
    };

    /**
     * Makes the table header.
     *
     * @param {Document} xml XML Document
     *
     * @return {jQuery}
     */
    Table.prototype._makeHeader = function (xml) {
        var self = this;
        var title = $('title', xml).text();
        var header = $('<div class="head" />').append(
            $('<h3 />').text(title).attr('title', title),
            '<div class="buttons">' +
                '<a class="order-list" href="#">Order List</a>' +
                '<a class="filter" href="#">Filter</a>' +
                '<a class="create last" href="#">Create New Item</a>' +
            '</div>'
        );

        $('.filter', header).click($.proxy(this._onFilterClick, this));
        $('.create', header).click($.proxy(this._onCreateRowClick, this));
        $('.order-list', header).click($.proxy(this._onOrderListClick, this));
        return header;
    }
    
    /**
     * Makes the filter notes.
     * 
     * @param {Document} xml XML Document
     * 
     * @return {jQuery}
     */
    Table.prototype._makeFilter = function (xml) {
        var notes = $('<div class="filter notes open" />').append(
            '<a class="toggle" href="#">Filter</a>',
            '<a class="clear" href="#" title="Clear filter"><span>Clear filter</span></a>',
            $('<div class="text" />').append(
                $('<div class="inner" />')
            )
        );
        
        // toggle button
        var toggle = $('.toggle', notes);
        toggle.click($.proxy(this._onToggleNoteClick, this));
        
        // actions
        notes.on('click', 'a.remove', $.proxy(this._onFilterRemoveItem, this));
        notes.on('click', 'a.clear', $.proxy(this._onFilterClear, this));
        
        return notes;
    };

    /**
     * Makes the table notes.
     *
     * @param {Document} xml XML Document
     *
     * @return {jQuery}
     */
    Table.prototype._makeNotes = function (xml) {
        var text = $('notes', xml).text();
        var notes = $('<div class="notes" />').append(
            '<a class="toggle" href="#">Notes</a>',
            $('<div class="text" />').append(
                $('<div class="inner" />').text(text)
            )
        );
        var toggle = $('.toggle', notes);
        var notesArea = $('.text', notes);
        var notesAreaHeight = undefined;

        // opens or closes the notes
        toggle.click($.proxy(this._onToggleNoteClick, this));

        // shows the notes only if the text is not empty
        if (!$.spText('empty', text)) {
            notes.show();
        }

        return notes;
    }

    /**
     * Makes the table rows.
     *
     * @param {Document} xml XML Document
     *
     * @return {jQuery}
     */
    Table.prototype._makeBody = function (xml) {
        var table = $('<table class="rows" />').append(
            $('<thead />').append(
                $('<tr />').append(function () {
                    var target = $(this);
                    var fields = $('column', xml);
                    var len = fields.length;

                    // appends columns
                    target.append(
                        $('<th class="first clear" title="Clear sorting" />').append(
                            $('<a />').text(' ')
                        )
                    )
                    $(fields).each(function (index) {
                        var item = $(this);
                        var itemTitle = item.attr('title');
                        var isSortable = item.attr('sortable') == 'true';
                        
                        target.append(
                            $('<th />')
                                .data('index', index)
                                .toggleClass('sortable', isSortable)
                                .attr('title', isSortable? 'Sort by ' + itemTitle: itemTitle)
                                .append(
                                    $('<a />')
                                        .append($('<span />').text(item.attr('title')))
                                )
                        );
                    });
                })
            ),
            '<tbody />'
        );
        
        // actions
        $('th.sortable', table).click($.proxy(this._onColumnClick, this));
        $('th.clear', table).click($.proxy(this._onColumnClearClick, this));

        return table;
    }

    /**
     * Makes table footer.
     *
     * @param {Document} xml XML Document
     *
     * @return {jQuery}
     */
    Table.prototype._makeFooter = function (xml) {
        var footer = $(
            '<div class="foot">' +
                '<a class="delete-all" href="#" title="Delete All Items">' +
                    '<span>Delete All Items</span>' +
                '</a>' +
            '</div>'
        );


        // appends the navigator
        var numPages = parseInt($('num-pages', xml).text(), 10);
        $(footer).tableNavigator('append', numPages);

        // actions
        $('.navigator', footer).tableNavigator('onChange', $.proxy(this._onNavigatorChange, this));
        $('.delete-all', footer).click($.proxy(this._onDeleteAllClick, this));

        return footer;
    }

    Table.prototype._onNavigatorChange = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        var pageNumber = target.tableNavigator('getPageNumber');

        $.axis(
            'get',
            this._controller,
            {
                section: this._section,
                table: this._tablePos,
                page: pageNumber,
                filter: JSON.stringify(this._filter),
                order: JSON.stringify(this._order)
            }
        ).done(function (xml) {
            self._update(xml);
        });
    };

    /**
     * Updates the table.
     *
     * @param {Document} xml XML Document
     *
     * @return {Void}
     */
    Table.prototype._update = function (xml) {
        var self = this;
        var columns = $('column', xml);
        var rows = $('row', xml);
        var tableRows = $('table.rows tbody', this._target);

        // init variables
        this._numRows = parseInt($('num-rows', xml).text());
        this._maxRows = parseInt($('max-rows', xml).text());
        this._isSortable = $('sortable', xml).text() == 'true';
        this._isInsertable = $('allow-insert', xml).text() == 'true';
        this._isEditable = $('allow-edit', xml).text() == 'true';
        this._isDeletable = $('allow-delete', xml).text() == 'true';
        this._section = $('section', xml).text();
        this._tablePos = parseInt($('position', xml).text(), 10);
        this._pageNumber = parseInt($('page', xml).text(), 10);
        this._externalModule = $('external-module', xml).text();

        // table buttons
        $('.create', self._target).toggleClass(
            'disabled', (this._maxRows > 0 && this._maxRows < this._numRows + 1) || !this._isInsertable
        );
        $('.order-list', self._target).toggleClass('disabled', rows.length < 2 || !this._isSortable);
        $('a.delete-all', this._target)
            .attr('title', this._isDeletable? 'Delete All Items': '')
            .toggleClass('disabled', !this._isDeletable);
        
        // enables/disables filter button
        var filtrableColumns = $('column[filtrable=true]', xml);
        $('.filter', this._target).toggleClass('disabled', filtrableColumns.length == 0);
        
        // updates filter notes
        this._updateFilter(xml);
        this._updateColumns(xml);

        // clears and creates the rows
        tableRows.empty();
        rows.each(function () {
            var row = $(this);
            var id = row.attr('id');
            var items = $('item', row);

            // edit button
            var tr = $('<tr />').append(
                $('<td class="edit" />').toggleClass('disabled', !self._isEditable).append(
                    $('<a href="#"><span>Edit</span></a>').attr(
                        'title', self._isEditable? 'Edit Row': ''
                    )
                )
            );

            // appends cells
            items.each(function (index) {
                var column = columns.eq(index);
                var cell = $(this);
                var text = cell.text();

                tr.append(
                    $('<td />').append(function () {
                        var elem = elem = $('<div />').addClass('cell-content');
                        var contentElem = elem;

                        if (index == columns.length -1) {
                            elem = $('<div class="cell-content" />').append(
                                $('<div />'),
                                $('<div class="button up" />').append(
                                    $('<a href="#" />').attr('title', 'Move Up in List').append(
                                        $('<span />').text('Up')
                                    )
                                ),
                                $('<div class="button down" />').append(
                                    $('<a href="#" />').attr('title', 'Move Down in List').append(
                                        $('<span />').text('Down')
                                    )
                                ),
                                $('<div class="button delete" />')
                                    .toggleClass('disabled', !self._isDeletable)
                                    .append(
                                        $('<a href="#" />').attr(
                                            'title', self._isDeletable? 'Delete Row': ''
                                        ).append($('<span />').text('Delete'))
                                    )
                            );
                            contentElem = $('div:first-child', elem);
                        }
                        
                        // appends contents
                        if (column.attr('type') == 'image') {
                            if (!$.spText('empty', text)) {
                                var src = $.axis(
                                    'absPath',
                                    self._thumbController
                                    + '?path=' + $.spUri('encode', text)
                                    + '&height=53'
                                );

                                contentElem.append($('<img />').attr('src', src));
                            }
                        } else {
                            contentElem.text($.spText('truncate', $.spHtml('plainText', text), 450));
                        }

                        return elem;
                    })
                );
            });

            // saves the id
            tr.data('id', id);

            // actions
            $('td.edit a', tr).click($.proxy(self._onEditRowClick, self));
            $('div.button.delete a', tr).click($.proxy(self._onDeleteRowClick, self));

            if (self._numRows > 1 && self._isSortable) {
                // enable up/down buttons
                $('div.button.up a', tr).click($.proxy(self._onUpRowClick, self));
                $('div.button.down a', tr).click($.proxy(self._onDownRowClick, self));
            } else {
                // disable up/down buttons
                var buttons = $('div.button.up, div.button.down', tr);
                buttons.addClass('disabled');
                $('a', buttons).attr('title', '');
                buttons.click(function () {
                    this.blur();
                    return false;
                });
            }

            tableRows.append(tr);
        });

        // updates the navigator
        var numberOfPages = parseInt($('num-pages', xml).text(), 10);
        var pageNumber = parseInt($('page', xml).text(), 10);
        var navigator = $('.navigator', this._target);
        navigator.tableNavigator('setNumberOfPages', numberOfPages);
        navigator.tableNavigator('setPageNumber', pageNumber);
    }
    
    /**
     * Updates the filter note.
     * 
     * @param {Document} xml XML document
     * 
     * @return {Void}
     */
    Table.prototype._updateFilter = function (xml) {
        var filters = $('root > filter > field', xml);
        var filter = $('div.filter.notes', this._target).toggle(filters.length > 0);
        $('div.text div.inner', filter)
            .empty()
            .append(
                $('<table border="0" cellpadding="0" cellspacing="0" />')
                    .append(function () {
                        var target = $(this);
                        $.each(filters, function () {
                            var filter = $(this);
                            target.append(
                                $('<tr />').append(
                                    $('<td />').text(filter.attr('title') + ':'),
                                    $('<td />').text(filter.text()),
                                    $('<td />').append(
                                        $('<a class="remove" href="#" />')
                                            .attr('title', 'Remove filter')
                                            .append('<span>Remove Filter</span>')
                                            .data('position', filter.attr('position'))
                                    )
                                )
                            )
                        });
                    })
            );
        
        $('div.text', filter).css('height', 'auto').removeData('height');
    };
    
    /**
     * Updates columns orders.
     * 
     * @param {Document} xml XML document
     * 
     * @return {Void}
     */
    Table.prototype._updateColumns = function (xml) {
        var self = this;
        
        // init order columns
        this._order = [];
        var order = $('order > field', xml);
        $.each(order, function () {
            var item = $(this);
            self._order.push({
                index: parseInt(item.attr('index')),
                asc: item.attr('asc')
            });
        });
        
        $('thead > tr > th.first', this._target)
            .toggleClass('clear', this._orderHasChanged)
            .attr('title', this._orderHasChanged? 'Clear sorting': '');
        
        // updates column orders
        var columns = $('thead > tr > th', this._target);
        columns.removeClass('asc desc');
        $.each(this._order, function () {
            var item = this;
            
            // searches a column by position
            var column = null;
            $.each(columns, function () {
                column = $(this);
                var columnPosition = column.data('index');
                if (columnPosition !== undefined && columnPosition == item.index) {
                    return false;
                }
            });
            
            column.addClass(item.asc == 'true'? 'asc': 'desc');
            return false;
        });
    };

    Table.prototype._onOrderListClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);

        try {
            if (this._numRows > 1) {
                if (!this._isSortable) {
                    target.blur();
                    return false;
                }

                var ui = $.axis(
                    'window', 'order-list.html', {section: this._section, table: this._tablePos}
                );
                ui.on('done', function (event) {
                    // updates the table
                    $.axis(
                        'get',
                        self._controller,
                        {
                            section: self._section,
                            table: self._tablePos,
                            filter: JSON.stringify(self._filter),
                            order: JSON.stringify(self._order)
                        }
                    ).done(function (xml) {
                        self._update(xml);
                    });
                });
            }
        } finally {
            target.blur();
            return false;
        }
    };
    
    /**
     * Filters records.
     * 
     * @param {Object} event Event
     * 
     * @return {Boolean}
     */
    Table.prototype._onFilterClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        
        if (!$('.filter', this._target).hasClass('disabled')) {
            var ui = $.axis(
                'window',
                'filter.html',
                {section: this._section, table: this._tablePos, filter: this._filter}
            );
            ui.on('done', function () {
                self._filter = ui.data('filter');
                
                // removes the interface and refreshes the table
                $.axis(
                    'get',
                    self._controller,
                    {
                        section: self._section,
                        table: self._tablePos,
                        filter: JSON.stringify(self._filter),
                        order: JSON.stringify(self._order)
                    }
                ).always(function () {
                    ui.remove();
                }).done(function (xml) {
                    self._update(xml);
                });
            });
        }
        
        target.blur();
        return false;
    };
    
    /**
     * Removes a filter.
     * 
     * @param {Object} event Event
     * 
     * @return {Void}
     */
    Table.prototype._onFilterRemoveItem = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        var tr = target.closest('tr');
        
        // deletes a filter and updates the interface
        delete this._filter[target.data('position')];
        $.axis(
            'get',
            this._controller,
            {
                section: this._section,
                table: this._tablePos,
                filter: JSON.stringify(this._filter),
                order: JSON.stringify(this._order)
            }
        ).done(function (xml) {
            self._update(xml);
        });
        
        target.blur();
        return false;
    };
    
    /**
     * Clears the filter.
     * 
     * @param {Object} event Event
     * 
     * @return {Boolean}
     */
    Table.prototype._onFilterClear = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        
        // clears the filter and updates the interface
        this._filter = {};
        $.axis(
            'get',
            this._controller,
            {
                section: this._section,
                table: this._tablePos,
                filter: JSON.stringify(this._filter),
                order: JSON.stringify(this._order)
            }
        ).done(function (xml) {
            self._update(xml);
        });
        
        target.blur();
        return false;
    };
    
    /**
     * Clears the current sorting.
     * 
     * @param {Object} event Event
     * 
     * @return {Boolean}
     */
    Table.prototype._onColumnClearClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        
        this._order = [];
        $.axis(
            'get',
            this._controller,
            {
                section: this._section,
                table: this._tablePos,
                filter: JSON.stringify(this._filter),
                order: JSON.stringify(this._order)
            }
        ).done(function (xml) {
            self._orderHasChanged = false;
            self._update(xml);
        });
        
        target.blur();
        return false;
    };
    
    /**
     * Sorts the result by a column.
     * 
     * @param {Object} event Event
     * 
     * @return {Boolean}
     */
    Table.prototype._onColumnClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        var columnPos = target.data('index');
        
        // searches order by position
        var item = {index: columnPos, asc: null};
        $.each(this._order, function (index) {
            if (this.index == columnPos) {
                item = this;
                return false;
            }
        });
        
        // removes the item and prepends it
        var pos = this._order.indexOf(item);
        if (pos > -1) {
            this._order.splice(pos, 1);
        }
        this._order.unshift(item);
        
        // changes the order direction for the first column
        if (pos > 0) {
            item.asc = null;
        } else
        if (item.asc !== null) {
            item.asc = item.asc == 'true'? 'false': 'true';
        }
        
        // removes the interface and refreshes the table
        $.axis(
            'get',
            this._controller,
            {
                section: this._section,
                table: this._tablePos,
                filter: JSON.stringify(this._filter),
                order: JSON.stringify(this._order)
            }
        ).done(function (xml) {
            self._orderHasChanged = true;
            self._update(xml);
        });
        
        target.blur();
        return false;
    };

    /**
     * Creates a new row.
     *
     * @param {Object} event Event
     *
     * @return {Boolean}
     */
    Table.prototype._onCreateRowClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        
        if (target.hasClass('disabled')) {
            target.blur();
            return false;
        }

        try {
            var editor = $.spText('ifEmpty', this._externalModule, 'edit.html');
            var ui = $.axis(
                'window', editor, {section: this._section, table: this._tablePos, filter: this._filter}
            );
            ui.on('done', function () {
                var searchId = ui.data('id');

                // removes the interface and refreshes the table
                $.axis(
                    'get',
                    self._controller,
                    {
                        section: self._section,
                        table: self._tablePos,
                        search_id: searchId,
                        filter: JSON.stringify(self._filter),
                        order: JSON.stringify(self._order)
                    }
                ).always(function () {
                    ui.remove();
                }).done(function (xml) {
                    self._update(xml);
                    
                    // tells the application to refresh the main menu
                    if (self._refreshMenu) {
                        $(window).trigger('refresh');
                    }
                });
            });
        } finally {
            target.blur();
            return false;
        }
    };

    /**
     * Edits a row.
     *
     * @param {Object} event Event
     *
     * @return {Boolean}
     */
    Table.prototype._onEditRowClick = function(event) {
        var self = this;
        var target = $(event.currentTarget);
        var tr = target.closest('tr');
        var td = target.closest('td');
        var id = tr.data('id');
        
        if (td.hasClass('disabled')) {
            target.blur();
            return false;
        }

        var editor = $.spText('ifEmpty', this._externalModule, 'edit.html');
        var ui = $.axis(
            'window',
            editor,
            {section: this._section, table: this._tablePos, id: id, filter: this._filter}
        );

        // removes the interfaces and refreshes the table
        ui.on('done', function () {
            var searchId = ui.data('id');

            // removes the interface and refreshes the table
            $.axis(
                'get',
                self._controller,
                {
                    section: self._section,
                    table: self._tablePos,
                    search_id: searchId,
                    filter: JSON.stringify(self._filter),
                    order: JSON.stringify(self._order)
                }
            ).always(function () {
                ui.remove();
            }).done(function (xml) {
                self._update(xml);

                // tells the application to refresh the main menu
                if (self._refreshMenu) {
                    $(window).trigger('refresh');
                }
            });
        });

        target.blur();
        return false;
    }

    /**
     * Deletes a row.
     *
     * @return {Boolean}
     */
    Table.prototype._onDeleteRowClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        var div = target.closest('div');
        var tr = target.closest('tr');
        var id = tr.data('id');
        
        if (div.hasClass('disabled')) {
            target.blur();
            return false;
        }

        $.axis(
            'confirm',
            'Are you sure you want to delete this item?',
            function () {
                self._deleteRow(id).done(function () {
                    // tells the application to refresh the main menu
                    if (self._refreshMenu) {
                        $(window).trigger('refresh');
                    }
                });
            }
        );

        target.blur();
        return false;
    }

    /**
     * Deletes ALL rows.
     *
     * @return {Boolean}
     */
    Table.prototype._onDeleteAllClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        
        if (target.hasClass('disabled')) {
            target.blur();
            return false;
        }

        $.axis(
            'confirm',
            'YOU ARE ABOUT TO DELETE ALL ITEMS!\n' +
            'THIS OPERATION IS NOT REVERSIBLE.\n\nAre you sure?',
            function () {
                self._deleteAllRows();
            }
        );

        target.blur();
        return false;
    };

    /**
     * Moves the record one position up.
     *
     * @return {Boolean}
     */
    Table.prototype._onUpRowClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        var tr = target.closest('tr');
        var id = tr.data('id');

        $.axis(
            'post',
            this._controller,
            {
                action: 'up',
                section: self._section,
                table: self._tablePos,
                id: id,
                filter: JSON.stringify(self._filter),
                order: JSON.stringify(self._order)
            }
        ).done(function (xml) {
            self._update(xml);
        });

        target.blur();
        return false;
    }

    /**
     * Moves the record one position down.
     *
     * @return {Boolean}
     */
    Table.prototype._onDownRowClick = function (event) {
        var self = this;
        var target = $(event.currentTarget);
        var tr = target.closest('tr');
        var id = tr.data('id');

        $.axis(
            'post',
            this._controller,
            {
                action: 'down',
                section: self._section,
                table: self._tablePos,
                id: id,
                filter: JSON.stringify(self._filter),
                order: JSON.stringify(self._order)
            }
        ).done(function (xml) {
            self._update(xml);
        });

        target.blur();
        return false;
    }

    /**
     * Closes or opens the table notes.
     *
     * @return {Boolean}
     */
    Table.prototype._onToggleNoteClick = function (event) {
        var target = $(event.currentTarget);
        var notes = target.closest('.notes');
        var toggle = $('.toggle', notes);
        var notesArea = $('.text', notes);
        
        // opens the notes
        if (typeof notesArea.data('height') == 'undefined') {
            notesArea.data('height', notesArea.height());
        }

        if (notes.hasClass('open')) {
            // closes the notes
            notesArea.animate({'height': 0}, 250, function () {
                notes.removeClass('open');
            });
        } else {
            notesArea.height(0).show().animate(
                {'height': notesArea.data('height')},
                250,
                function () {
                    notes.addClass('open');
                }
            );
        }

        target.blur();
        return false;
    }
    
    /**
     * Deletes a record.
     * 
     * @param {String} id Record ID
     * 
     * @return {jQuery.Promise}
     */
    Table.prototype._deleteRow = function (id) {
        var self = this;
        var ret = $.Deferred();
        
        var d1 = $.spText('empty', self._beforeDelete)
            ? $.Deferred().resolve().promise()
            : $.spModal('post', self._beforeDelete, {id: id});
        d1.done(function () {
            $.axis(
                'post',
                self._controller,
                {
                    action: 'delete',
                    section: self._section,
                    table: self._tablePos,
                    page: self._pageNumber,
                    id: id,
                    filter: JSON.stringify(self._filter),
                    order: JSON.stringify(self._order)
                }
            ).done(function (xml) {
                var d2 = $.spText('empty', self._afterDelete)
                    ? $.Deferred().resolve().promise()
                    : $.spModal('post', self._afterDelete, {id: id});
                d2.always(function () { // why always?
                    self._update(xml);
                    ret.resolve();
                });
            });
        });
        
        return ret.promise();
    };
    
    /**
     * Deletes ALL rows.
     * 
     * @return {Void}
     */
    Table.prototype._deleteAllRows = function () {
        var self = this;
        var firstRow = $('table.rows > tbody > tr:first', self._target);
        
        if (firstRow.length > 0) {
            var id = firstRow.data('id');
            self._deleteRow(id).done(function () {
                self._deleteAllRows();
            });
        } else
        if (self._refreshMenu) {
            // tells the application to refresh the main menu
            $(window).trigger('refresh');
        }
    };

    var methods = {
        /**
         * Appends a table to the container.
         *
         * @param {String}   section  Section name
         * @param {Number}   tablePos Table position
         * @param {Function} onReady  Callback function
         *
         * @return {jQuery}
         */
        'append': function (section, tablePos, onLoad) {
            return this.each(function () {
                var container = this;

                new Table(section, tablePos, function () {
                    this.appendTo(container);
                    $.proxy(onLoad, container)();
                });
            });
        }
    };

    /**
     * Main method.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.fn.table = function (methodName, options) {
        var method = methods[methodName];

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, Array.prototype.slice.call(arguments, 1));
    };
})(jQuery);
