/**
 * Extends the 'require' configuration file.
 */
$.require('config', $.extend(true, {}, $.require('config'), {
    libraries: {
        'jquery.menu': {
            sources: ['assets/views/home/js/jquery.menu.js'],
            requires: ['jquery.sp-uri']
        },
        'jquery.section-notes': {
            sources: ['assets/views/home/js/jquery.section-notes.js'],
            requires: ['jquery.sp-text']
        },
        'jquery.table': {
            sources: ['assets/views/home/js/jquery.table.js'],
            requires: ['jquery.sp-text', 'jquery.sp-uri', 'jquery.sp-html', 'jquery.axis']
        },
        'jquery.table-navigator': 'assets/views/home/js/jquery.table-navigator.js'
    }
}));
