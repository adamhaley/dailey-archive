(function ($) {
    var namespace = 'tableNavigator';

    /**
     * TableNavigator class.
     *
     * @return {Void}
     */
    function TableNavigator(numberOfPages) {
        /**
         * Target node.
         * @var {jQuery}
         */
        this._target = null;

        /**
         * Current page number.
         * @var {Number}
         */
        this._pageNumber = 0;

        /**
         * Number of pages.
         * @var {Number}
         */
        this._numberOfPages = numberOfPages;

        this._make();
    }

    /**
     * Makes the navigator.
     *
     * @return {Void}
     */
    TableNavigator.prototype._make = function () {
        this._target = $(
            '<div class="navigator">' +
                '<ul class="buttons">' +
                    '<li><a class="prev disabled" href="#">Prev</a></li>' +
                    '<li>' +
                        '<a class="current" href="#">' +
                            'Current Page<span class="page">&nbsp;</span>' +
                        '</a>' +
                    '</li>' +
                    '<li><a class="next" href="#">Next</a></li>' +
                '</ul>' +
                '<div class="pages"></div>' +
            '</div>'
        );

        // actions
        $('.current', this._target).click($.proxy(this._onCurrentPageClick, this));
        $('.prev', this._target).click($.proxy(this._onPrevClick, this));
        $('.next', this._target).click($.proxy(this._onNextClick, this));

        // updates navigator and saves instance
        this._update();
        this._saveCurrentInstance();
    };

    /**
     * Updates the navigator.
     *
     * @return {Void}
     */
    TableNavigator.prototype._update = function (target) {
        var self = this;
        var pages = $('.pages', this._target);

        // page info
        $('.page', this._target).text((this._pageNumber + 1) + ' OF ' + this._numberOfPages);

        // clears the page and appends the links
        pages.empty().append(
            $('<ul />').append(function () {
                var target = $(this);

                for (var i = 0; i < self._numberOfPages; i++) {
                    target.append(
                        $('<li />').append(
                            $('<a href="#" />').text(i + 1).toggleClass(
                                'selected', i == self._pageNumber
                            ).data('pageNumber', i)
                        )
                    );
                }
            })
        );

        // actions
        $('a', pages).click($.proxy(this._onPageClick, this));

        // shows the navigator if there are more than one page
        this._target.toggle(this._numberOfPages > 0);
        $('.prev', this._target).toggleClass('disabled', this._pageNumber == 0);
        $('.next', this._target).toggleClass(
            'disabled', this._pageNumber == this._numberOfPages - 1
        );
    };

    /**
     * Gets object instance from a given node.
     *
     * @param {Object} target Target node
     *
     * @return {Object}
     */
    TableNavigator.getInstance = function (target) {
        return $(target).data(namespace);
    };

    /**
     * Gets node target.
     *
     * @return {jQuery}
     */
    TableNavigator.prototype.getTarget = function () {
        return this._target;
    };

    /**
     * Fires an event.
     *
     * @param {String} eventType Event type
     *
     * @return {Void}
     */
    TableNavigator.prototype.trigger = function (eventType) {
        this._target.trigger(namespace + '.' + eventType);
    };

    /**
     * Attaches an event listener.
     *
     * @param {String}   eventType Event type
     * @param {Function} listener  Event listener
     *
     * @return {Void}
     */
    TableNavigator.prototype.on = function (eventType, listener) {
        this._target.on(namespace + '.' + eventType, listener);
    };

    /**
     * Gets the page number.
     *
     * @return {Number}
     */
    TableNavigator.prototype.getPageNumber = function () {
        return this._pageNumber;
    };

    /**
     * Sets the page number.
     *
     * @param {Number} pageNumber Page number
     *
     * @return {Void}
     */
    TableNavigator.prototype.setPageNumber = function (pageNumber) {
        this._pageNumber = pageNumber;
        this._update();
    };

    /**
     * Gets the number of pages.
     *
     * @return {Number}
     */
    TableNavigator.prototype.getNumberOfPages = function () {
        return this._numberOfPages;
    };

    /**
     * Sets the number of pages.
     *
     * @param {Number} numberOfPages Number of pages
     *
     * @return {Void}
     */
    TableNavigator.prototype.setNumberOfPages = function (numberOfPages) {
        this._numberOfPages = numberOfPages;
    };

    /**
     * Save the current instance on the node.
     *
     * @param {Object} obj Object instance
     *
     * @return {Void}
     */
    TableNavigator.prototype._saveCurrentInstance = function () {
        this._target.data(namespace, this);
    }

    /**
     * Shows or hides the list of pages.
     *
     * @param {Boolean} showOrHide Shows or hides the pages (not required)
     *
     * @return {Void}
     */
    TableNavigator.prototype._toggle = function (showOrHide) {
        $('.pages', this._target).toggle(showOrHide);
        $('.current', this._target).toggleClass('open', showOrHide);
    };

    /**
     * Closes the list of pages.
     *
     * @return {Void}
     */
    TableNavigator.prototype._close = function () {
        this._toggle(false);
    };

    /**
     * Goes to a specific page.
     *
     * @param {Object} event Event
     *
     * @return {Boolean}
     */
    TableNavigator.prototype._onPageClick = function (event) {
        var target = $(event.currentTarget);
        var pageNumber = target.data('pageNumber');

        this.setPageNumber(pageNumber);
        this._close();
        this.trigger('change');

        target.blur();
        return false;
    };

    /**
     * Shows or hides the list of pages.
     *
     * @return {Boolean}
     */
    TableNavigator.prototype._onCurrentPageClick = function (event) {
        var target = $(event.currentTarget);

        this._toggle();
        target.blur();
        return false;
    };

    /**
     * Goes to the previous page.
     *
     * @return {Boolean}
     */
    TableNavigator.prototype._onPrevClick = function (event) {
        var target = $(event.currentTarget);

        if (!target.hasClass('disabled')) {
            this.setPageNumber(this.getPageNumber() - 1);
            this._close();
            this.trigger('change');
        }

        target.blur();
        return false;
    };

    /**
     * Goes to the next page.
     *
     * @return {Boolean}
     */
    TableNavigator.prototype._onNextClick = function (event) {
        var target = $(event.currentTarget);

        if (!target.hasClass('disabled')) {
            this.setPageNumber(this.getPageNumber() + 1);
            this._close();
            this.trigger('change');
        }

        target.blur();
        return false;
    };

    var methods = {
        /**
         * Appends a new table navigator.
         *
         * @param {Number} numberOfPages Number of pages
         *
         * @return {jQuery}
         */
        'append': function (numberOfPages) {
            return this.each(function () {
                var container = $(this);
                var nav = new TableNavigator(numberOfPages);

                container.append(nav.getTarget());
            });
        },

        /**
         * Gets the page number.
         *
         * @return {Number}
         */
        'getPageNumber': function () {
            var obj = TableNavigator.getInstance($(this));
            return obj.getPageNumber();
        },

        /**
         * Sets the page number.
         *
         * @param {Number} pageNumber Page number
         *
         * @return {jQuery}
         */
        'setPageNumber': function (pageNumber) {
            return this.each(function () {
                var target = $(this);
                var obj = TableNavigator.getInstance(target);

                obj.setPageNumber(pageNumber);
            });
        },

        /**
         * Gets the number of pages.
         *
         * @return {Number}
         */
        'getNumberOfPages': function () {
            var obj = TableNavigator.getInstance($(this));
            return obj.getNumberOfPages();
        },

        /**
         * Sets the number of pages.
         *
         * @param {Number} numberOfPages Number of pages
         *
         * @return {jQuery}
         */
        'setNumberOfPages': function (numberOfPages) {
            return this.each(function () {
                var target = $(this);
                var obj = TableNavigator.getInstance(target);

                obj.setNumberOfPages(numberOfPages);
            });
        },

        /**
         * Handles 'change' events.
         *
         * @param {Function} onChange On 'change' event listener
         *
         * @return {jQuery}
         */
        'onChange': function (onChange) {
            return this.each(function () {
                var target = $(this);
                var obj = TableNavigator.getInstance(target);

                obj.on('change', $.proxy(onChange, this));
            });
        }
    };

    $.fn[namespace] = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
