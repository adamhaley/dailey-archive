(function ($) {

    /**
     * Available methods.
     * @var {Object} of functions
     */
    var _methods = {
        /**
         * Creates a section note.
         *
         * @param {Document} xml XML Document
         *
         * @return {jQuery}
         */
        'update': function (xml) {
            var text = $('notes:first', xml).text();

            return this.each(function () {
                var notes = $(this);

                $('.text', notes).text(text);
                notes.toggle(!$.spText('empty', text));
            });
        }
     };

    /**
     * Main method.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.fn.sectionNotes = function (methodName) {
        var method = _methods[methodName];

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        // calls method, passing all arguments except the first one
        return method.apply(this, Array.prototype.slice.call(arguments, 1));
    };
})(jQuery);
