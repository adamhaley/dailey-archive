(function ($) {

    /**
     * Class SelectField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      obj      Field value
     */
    function TimeField(target, title, required, obj) {
        /**
         * Current field.
         * @var {jQuery}
         */
        this._target = TimeField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Title.
         * @var {String}
         */
        this._title = title;
        
        /**
         * Required field.
         * @var {Boolean}
         */
        this._required = required;

        /**
         * Date.
         * @var {Date}
         */
        this._value = $.spText('ifEmpty', obj.value, '');
        
        /**
         * Original date.
         * @var {Date}
         */
        this._originalValue = this._value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    TimeField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    TimeField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };
    
    /**
     * Gets the field value.
     *
     * @return {String}
     */
    TimeField.prototype.getValue = function () {
        var year = $.trim($('input:nth-child(1)', this._target).val());
        var month = $.trim($('input:nth-child(2)', this._target).val());
        var day = $.trim($('input:nth-child(3)', this._target).val());
        
        return $.spText('concat', ':', year, month, day);
    };
    
    /**
     * Sets the field value.
     * 
     * @param {String} value Value
     * 
     * @return {Void}
     */
    TimeField.prototype.setValue = function (value) {
        this._value = value;
        this._update();
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    TimeField.prototype.reset = function () {
        this._value = this._originalValue;
        this._update();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    TimeField.prototype.isEmpty = function () {
        return $.spText('empty', this.getValue());
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    TimeField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TimeField.prototype._make = function () {
        var self = this;
        
        this._target.addClass('axis-field-date').append(
            $('<label />').toggleClass('required', self._required).text(this._title),
            $('<div />').append(
                $('<input type="text" placeholder="HH (24)" />').data('max-digits', 2),
                $('<input type="text" placeholder="MM" />').data('max-digits', 2),
                $('<input type="text" placeholder="SS" />').data('max-digits', 2)
            )
        );
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });
        
        // only numbers allowed
        $('input', this._target).on('input', function () {
            var target = $(this);
            var value = target.val();
            var maxDigits = target.data('max-digits');
            
            target.val(value.replace(/[^0-9]*/gi, '').substr(0, maxDigits).toUpperCase());
            self._target.trigger('input-change');
        });
        
        // normalizes input
        $('input', this._target).on('change', function () {
            var target = $(this);
            var value = $.trim(target.val());
            var maxDigits = target.data('max-digits');
            
            if (!$.spText('empty', value)) {
                target.val($.spText('normalize', value, maxDigits));
            }
        });
        
        this._update();
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    TimeField.prototype._update = function () {
        var info = this._value.split(':');
        var year = $.spText('ifEmpty', info[0], '');
        var month = $.spText('ifEmpty', info[1], '');
        var day = $.spText('ifEmpty', info[2], '');
        
        $('input:nth-child(1)', this._target).val(year);
        $('input:nth-child(2)', this._target).val(month);
        $('input:nth-child(3)', this._target).val(day);
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}               title       Field title
         * @param {Boolean}              required    Is the field required
         * @param {String}               value       Field value
         *
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new TimeField(this, title, required, value);
            });
        },

        /**
         * Gets or sets the value.
         * 
         * @param {mixed} value Value (not required)
         *
         * @return {jQuery|String}
         */
        'value': function (value) {
            if (value === undefined) {
                var ret = '';

                this.each(function () {
                    var input = TimeField.getInstance(this);
                    ret = input.getValue();
                });

                return ret;
            } else {
                return this.each(function () {
                    var input = TimeField.getInstance(this);
                    input.setValue(value);
                });
            }
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = TimeField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = TimeField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = TimeField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldTime = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
