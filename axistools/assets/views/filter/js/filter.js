$.require(
    [
        'jquery.axis',
        'jquery.axis-field',
        'jquery.sp-modal',
        'jquery.sp-text',
        // fields
        'checkboxgroup',
        'date',
        'select',
        'text',
        'time'
    ],
    function () {
        /**
         * Section name.
         * @var {String}
         */
        var _sectionName = '';

        /**
         * Table position.
         * @var {Number}
         */
        var _tablePos = 0;
        
        /**
         * Filter object.
         * @var {Object}
         */
        var _filter = {};

        /**
         * Title.
         * @var {String}
         */
        var _title = '';

        /**
         * Fields.
         * @var {Array of jQuery}
         */
        var _fields = [];

        /**
         * Have any of the input changed?
         * @var {Boolean}
         */
        var _hasChanged = false;

        /**
         * Main function.
         *
         * @return {Void}
         */
        function main() {
            var self = $.axis('self');
            _sectionName = self.data('section');
            _tablePos = parseInt(self.data('table'), 0);
            _filter = self.data('filter');
            
            // removes the preloader
            $('.jquery-modal-ui').removeClass('preloader');

            $('.close').click(function () {
                if (_hasChanged) {
                    $.axis('confirm', 'Do you want to discard changes?', function () {
                        self.remove();
                    });
                } else {
                    self.remove();
                }

                this.blur();
                return false;
            });

            $.axis(
                'get', 'edit.php', {'section': _sectionName, 'table': _tablePos}
            ).done(function (xml) {
                _title = $('root > title', xml).text();
                _fields = $('root > fields > item', xml);
                make();
            }).always(function () {
                $('.jquery-modal-ui').css('opacity', 1);
            });
        }

        /**
         * Creates the interface.
         *
         * @return {Void}
         */
        function make() {
            $('#title').attr('title', 'Filter Record' + _title).text('FILTER RECORDS: ' + _title);
            
            appendFields(function () {
                // sets the focus on the first focusable input (if proceed)
                $('.fields .axis-field:first input:text').select().focus();
                makeActions();
                $.axis('center', $('.jquery-modal-ui'));
            });

            // checks if any field has changed
            $('.axis-field').on('input-change', function () {
                _hasChanged = true;
                updateFields();
            });
            
            update();
        }
        
        /**
         * Makes actions.
         *
         * @return {Void}
         */
        function makeActions() {
            var self = $.axis('self');
            
            $('.reset').click(function () {
                var target = $(this);
                
                $('.axis-field').axisField('reset');
                _hasChanged = false;
                
                target.blur();
                return false;
            });
            
            $('.submit').click(function () {
                var target = $(this);
                var data = collectData();
                
                // returns the filter and 'done'
                self.data('filter', data);
                self.trigger('done');

                target.blur();
                return false;
            });
        }

        /**
         * Appends fields to the container.
         *
         * @return {Void}
         */
        function appendFields(onLoad) {
                        
            function append(i) {
                if (i < _fields.length) {
                    var field = $(_fields[i]);
                    var fieldIndex = field.index();
                    var fieldType = field.attr('type');
                    var fieldTitle = field.attr('title');
                    var fieldRequired = field.attr('required') == 'true';
                    var fieldValue = JSON.parse(field.attr('value'));
                    var fieldName = field.attr('name');
                    var fieldLinkName = field.attr('field-name');
                    var fieldLinkValues = JSON.parse(field.attr('field-values'));
                    var filterValue = _filter[i] !== undefined? JSON.parse(_filter[i]): '';

                    if (field.attr('filtrable') == 'true') {
                        $('.fields').axisField(
                            'append',
                            fieldType,
                            fieldTitle,
                            fieldRequired,
                            fieldValue,
                            fieldName,
                            fieldLinkName,
                            fieldLinkValues,
                            {},
                            function (field) {
                                field
                                    .axisField('value', filterValue)
                                    .data('index', fieldIndex);
                                append(i + 1);
                            }
                        );
                    } else {
                        append(i + 1);
                    }
                } else {
                    $.proxy(onLoad, this)();
                }
            }

            append(0);
        }
        
        /**
         * Updates fields.
         * 
         * @return {Void}
         */
        function updateFields() {
            var fields = $('.axis-field');
            
            fields.each(function () {
                var target = $(this);
                var targetName = $.trim(target.axisField('name'));
                var targetValue = JSON.parse(target.axisField('value'));
                
                if (!$.spText('empty', targetName)) {
                    fields.each(function () {
                        var field = $(this);
                        var linkName = $.trim(field.axisField('linkName'));
                        var linkValues = field.axisField('linkValues');
                        
                        if (!$.spText('empty', linkName) && targetName == linkName) {
                            field.toggle($.inArray(targetValue, linkValues) > -1);
                        }
                    });
                }
            });
        }
        
        /**
         * Updates user interface.
         * 
         * @return {Void}
         */
        function update() {
            updateFields();
        }
        
        /**
         * Collects data.
         * 
         * @return {Object}
         */
        function collectData() {
            var ret = {};
            
            // collects data
            $('.axis-field').each(function () {
                var target = $(this);

                if (target.is(':visible') && !target.axisField('isEmpty')) {
                    ret[target.data('index')] = target.axisField('value');
                }
            });
            
            return ret;
        }

        main();
    }
);
