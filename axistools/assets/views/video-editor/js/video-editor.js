$.require(
    [
        'jquery.sp-text',
        'jquery.sp-modal-progress',
        'jquery.sp-timer',
        'jquery.axis',
        'jquery.snapshot-editor',
        'jquery.sp-video-player'
    ],
    function () {
        /**
         * Has the video changed?
         * @var {Boolean}
         */
        var _hasChanged = false;

        /**
         * Video to play.
         * @var {{src: String, width: Number, height: Number, preset: String}}
         */
        var _video = {};

        /**
         * Poster.
         * @var {{src: String, width: Number, height: Number}}
         */
        var _poster = {};

        /**
         * Thumbnail.
         * @var {{src: String, width: Number, height: Number}}
         */
        var _thumbnail = {};

        /**
         * Extra videos.
         * @var {Array of {src: String, width: Number, height: Number, preset: String}}
         */
        var _extraVideos = [];

        /**
         * Upload folder path.
         * @var {String}
         */
        var _uploadDir = '';

        /**
         * Original video source.
         * @var {String}
         */
        var _originalVideoSource = '';

        /**
         * Interface width.
         * @var {Number}
         */
        var _width = 0;

        /**
         * Interface height.
         * @var {Number}
         */
        var _height = 0;

        /**
         * Main function.
         *
         * @return void
         */
        function main() {
            var self = $.axis('self');
            _video = $.extend(true, {}, self.data('video'));
            _poster = $.extend(true, {}, self.data('poster'));
            _thumbnail = $.extend(true, {}, self.data('thumbnail'));

            $('#close').click(function () {
                var self = $.axis('self');

                if (_hasChanged) {
                    $.axis('confirm', 'Do you want to discard changes?', function () {
                        $.axis('garbage', 'purge', function () {
                            self.remove();
                        });
                    });
                } else {
                    $.axis('garbage', 'purge', function () {
                        self.remove();
                    });
                }

                this.blur();
                return false;
            });

            var extraVideos = self.data('extraVideos');
            $.each(extraVideos, function () {
                _extraVideos.push($.extend(true, {}, this));
            });

            $.axis('get', 'video-editor.php').done(function (xml) {
                _width = parseInt($('width', xml).text(), 10);
                _height = parseInt($('height', xml).text(), 10);
                _uploadDir = $('upload-dir', xml).text();

                make();
            }).always(function () {
                $('.jquery-modal-ui').css('opacity', 1);
            });
        }

        /**
         * Makes the user interface.
         *
         * @return {Void}
         */
        function make() {
            // resizes and centers user interface
            $('.video-editor')
                .width(_width)
                .height(_height)
                .css('left', ($(window).width() - _width) / 2);

            makeActions();
            makeTooltips();
            makeResizable();
            makeVideoPlayer();

            if (!$.spText('empty', _poster.src) && !$.spText('empty', _thumbnail.src)) {
                addSnapshot(_poster.src, _thumbnail.src);
            }

            if (!$.spText('empty', _video.src)) {
                playVideo();
            }

            updateButtons();
            $.axis('center', $('.jquery-modal-ui'));
        }

        /**
         * Makes the video player.
         *
         * @return {Void}
         */
        function makeVideoPlayer() {
            var player = $('#player');
            var playButton = $('#play');

            player.spVideoPlayer({
                controls: false,
                onLoad: function () {
                    updateButtons();
                },
                onPlay: function () {
                    playButton.addClass('pause');
                },
                onStop: function () {
                    playButton.removeClass('pause');
                }
            });

            var interval = setInterval(function () {
                var time = player.spVideoPlayer('time');

                $('#current-time').text(getFormatTime(time));
            }, 1000);
        }

        /**
         * Makes the actions.
         *
         * @return {Void}
         */
        function makeActions() {
            // prevents clicking on disable buttons
            $('#rewind, #prev, #play, #next, #fast-forward, #snapshot').click(function (event) {
                var target = $(this);

                if (target.hasClass('disabled')) {
                    event.stopImmediatePropagation();
                }

                this.blur();
                return false;
            });

            $('#upload-video').change(function () {
                $('#player').spVideoPlayer('pause');

                $.axis(
                    'upload',
                    'file-upload.php',
                    {file: this, dir: _uploadDir}
                ).done(function (xml) {
                    var src = $('path', xml).text();
                    _originalVideoSource = $('path', xml).text();

                    $.axis('garbage', 'purge', function () {
                        $.axis('garbage', 'add', _originalVideoSource);

                        convertVideo(
                            _originalVideoSource,
                            _video.preset,
                            _video.width,
                            _video.height,
                            function (output) {
                                $.axis('garbage', 'add', output);
                                _video.src = output;
                                playVideo();
                                _hasChanged = true;
                            }
                        );
                    });
                });

                $(this).val('');
            });

            $('#rewind').click(function () {
                $('#player').spVideoPlayer('time', 0);

                this.blur();
                return false;
            });

            $('#prev').click(function () {
                var player = $('#player');

                // rewinds the video player 0.5 seconds
                player.spVideoPlayer('time', player.spVideoPlayer('time') - 0.5);
                player.spVideoPlayer('pause');

                this.blur();
                return false;
            });

            $('#play').click(function () {
                var target = $(this);

                $('#player').spVideoPlayer('toggle');

                target.blur();
                return false;
            });

            $('#next').click(function () {
                var player = $('#player');

                // advances the video player 0.5 seconds
                player.spVideoPlayer('time', player.spVideoPlayer('time') + 0.5);
                player.spVideoPlayer('pause');

                this.blur();
                return false;
            });

            $('#fast-forward').mousedown(function () {
                var target = $(this);

                $('#player').spVideoPlayer('speed', 10);
            })
            $(document).on('dragend mouseup', function () {
                $('#player').spVideoPlayer('speed', 1);
            });

            $('#snapshot').click(function () {
                var target = $(this);
                var player = $('#player');

                player.spVideoPlayer('pause');

                $.axis(
                    'post',
                    'video-editor-snapshot.php',
                    {
                        src: _video.src,
                        time: player.spVideoPlayer('time'),
                        'poster-width': _poster.width,
                        'poster-height': _poster.height,
                        'thumbnail-width': _thumbnail.width,
                        'thumbnail-height': _thumbnail.height
                    }
                ).done(function (xml) {
                    var poster = $('poster', xml).text();
                    var thumbnail = $('thumbnail', xml).text();

                    $.axis('garbage', 'add', [poster, thumbnail]);
                    addSnapshot(poster, thumbnail);
                    _hasChanged = true;
                });

                target.blur();
                return false;
            });

            $('#email').click(function () {
                $.axis(
                    'prompt',
                    'Email the Snapshots',
                    ['Subject', 'Email', {label: 'Comments', type: 'textarea'}],
                    function () {
                        this._remove();
                    }
                );

                this.blur();
                return false;
            });

            $('#done').click(function () {
                // saves the data and fires a 'done' event
                function save() {
                    var self = $.axis('self');

                    // gets the selected thumbnail and poster
                    $('.snapshots td').each(function () {
                        var target = $(this);

                        if (target.snapshotEditor('selected')) {
                            _thumbnail.src = target.snapshotEditor('thumbnail');
                            _poster.src = target.snapshotEditor('poster');
                        }
                    });

                    self.data('video', _video);
                    self.data('poster', _poster);
                    self.data('thumbnail', _thumbnail);
                    self.data('extraVideos', _extraVideos);
                    self.trigger('done');
                }

                $('#player').spVideoPlayer('pause');
                
                if (!$.spText('empty', _originalVideoSource)) {
                    // converts videos sequentially
                    function convert(i) {
                        if (i < _extraVideos.length) {
                            var item = _extraVideos[i];

                            convertVideo(
                                _originalVideoSource,
                                item.preset,
                                item.width,
                                item.height,
                                function (output) {
                                    $.axis('garbage', 'add', output);
                                    item.src = output;
                                    convert(i + 1);
                                }
                            );
                        } else {
                            // removes original video and closes
                            $.axis(
                                'post',
                                'file-delete.php',
                                {files: [_originalVideoSource]}
                            ).done(function () {
                                save();
                            });
                        }
                    }

                    convert(0);
                } else {
                    save();
                }

                this.blur();
                return false;
            });
        }

        /**
         * Prepares tooltip buttons.
         *
         * @return {Void}
         */
        function makeTooltips() {
            var tooltip = $('.foot .tooltip');

            $(  '.foot .buttons a, .foot .buttons label, #email').each(function () {
                var target = $(this);

                // removes default tooltip
                target.data('tooltip', target.attr('title'));
                target.removeAttr('title');

                // show tooltip on mouse enter
                target.mouseenter(function () {
                    if (target.hasClass('disabled')) {
                        return;
                    }

                    tooltip.text(target.data('tooltip'));
                });

                // remove tooltip on mouse leave
                target.mouseleave(function () {
                    if (target.hasClass('disabled')) {
                        return;
                    }

                    tooltip.text('');
                });
            });
        }

        /**
         * Makes the video player resizable.
         *
         * @return {Void}
         */
        function makeResizable() {
            $('.video-editor').resizable({
                ghost: true,
                stop: updateVideoHeight
            });
        }

        /**
         * Updates buttons states.
         *
         * @return {Void}
         */
        function updateButtons() {
            // disable buttons if the video is not available
            $('#rewind, #prev, #play, #next, #fast-forward, #snapshot').toggleClass(
                'disabled', $.spText('empty', _video.src)
            );
        }

        /**
         * Updates the video player size to the size of the user interface.
         *
         * @return {Void}
         */
        function updateVideoHeight() {
            var footTop = $('.foot').offset().top;
            var playerTop = $('.player-container').offset().top;

            $('.player-container').height(footTop - playerTop - 35);
            $('#player').height($('.player-container').height());
        }

        /**
         * Normalizes a string by filling characters on the right.
         *
         * @param {String} str    An string
         * @param {String} char   A character
         * @param {Number} digits Number of digits
         *
         * @return {String}
         */
        function fill(str, char, digits) {
            return (Array(digits + 1).join(char) + str).substr(-digits);
        }

        /**
         * Gets a formatted time (hh:ss:mm)
         *
         * @return {String}
         */
        function getFormatTime(time) {
            var units = [60, 60];
            var len = units.length;
            var info = [];

            for (var i = 0; i < len; i++) {
                var unit = units[i];
                info.splice(0, 0, fill(Math.round(time % unit), '0', 2));
                time = Math.floor(time / unit);
            }

            info.splice(0, 0, fill(time, '0', 2));
            return info.join(':');
        }

        /**
         * Plays the current video.
         *
         * @return {Void}
         */
        function playVideo() {
            $.axis('get', 'video-editor-info.php', {src: _video.src}).done(function (xml) {
                var width = $('width', xml).text();
                var height = $('height', xml).text();
                var size = $('human-size', xml).text();
                var fps = $('frames-per-second', xml).text();

                // updates video info
                $('#screen-size').text(width + ' x ' + height);
                $('#file-size').text(size);
                $('#frame-rate').text(fps + 'fps');

                // shows the video controls and plays the video
                $('#player')
                    .spVideoPlayer('controls', true)
                    .spVideoPlayer('play', '/' + $.spText('ltrim', _video.src, '/'));
            });
        }

        /**
         * Appends a snapshot.
         *
         * @param {String} poster    Poster
         * @param {String} thumbnail Thumbnail
         *
         * @return {Void}
         */
        function addSnapshot(poster, thumbnail) {
            var snapshots = $('.snapshots');
            var container = $('table tr', snapshots);

            // appends a snapshot viewer
            container.snapshotEditor(
                'append',
                poster,
                thumbnail,
                _poster.width,
                _poster.height,
                _thumbnail.width,
                _thumbnail.height
            );

            // moves the scroll to the right
            snapshots.stop().animate({scrollLeft: snapshots.prop('scrollWidth')}, 800);

            // selects the last snapshot
            $('td:last', snapshots).snapshotEditor('selected', true);
        }

        /**
         * Converts a video.
         *
         * @param {String}   input   Video input path
         * @param {String}   preset  Video preset
         * @param {Number}   width   Video width
         * @param {Number}   height  Video height
         * @param {Function} onReady Called when the video conversion has finished
         *
         * @return {Void}
         */
        function convertVideo(input, preset, width, height, onReady) {
            var progress = $.spModalProgress(
                preset + ' conversion', 'This process may take several minutes.'
            );
            progress.onAbort(function () {
                request.abort();
                timer.stop();
                progress.close();
            });

            var timer = $.spTimer(0, function () {
                this.setDelay(1000);
                var x = this.getCount();
                var y = x / (10 + x);
                progress.setProgress(y);
            }).start();

            var self = this;
            var url = $.spText('rtrim', SERVER_URI, '/') + '/video-editor-converter.php';
            var request = $.post(
                url,
                {src:input, preset: preset, width: width, height: height},
                'text/xml'
            ).always(function () {
                timer.stop();
                progress.close();
            }).done(function (xml) {
                var error = $('root > error > message', xml).text();
                if (error.length > 0) {
                    $.axis('error', error);
                }

                var output = $('video', xml).text();
                onReady(output);
            }).fail(function (xhr, status) {
                $.axis('error', xhr.status + ': ' + xhr.statusText);
            });
        }

        main();
    }
);
