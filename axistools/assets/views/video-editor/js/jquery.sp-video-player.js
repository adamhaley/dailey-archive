(function () {
    /**
     * VideoPlayer class.
     *
     * @param {HTMLElement} target Target
     */
    function VideoPlayer(target) {
        this._target = VideoPlayer.setInstance(target, this);
        this._make();
    }

    /**
     * Video sources.
     * @var {Array.{src: String, type: String}}
     */
    VideoPlayer.prototype._sources = [];

    /**
     * Video poster.
     * @var {String}
     */
    VideoPlayer.prototype._poster = '';

    /**
     * Auto-play mode.
     * @var {Boolean}
     */
    VideoPlayer.prototype._isAutoPlay = false;

    /**
     * Does the video player has controls?
     * @var {Boolean}
     */
    VideoPlayer.prototype._hasControls = true;

    /**
     * Video width. By default, the video width is 100%;
     * @var {Number}
     */
    VideoPlayer.prototype._width = NaN;

    /**
     * Video width. By default, the video width is 100%;
     * @var {Number}
     */
    VideoPlayer.prototype._height = NaN;

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @static
     * @return {jQuery}
     */
    VideoPlayer.getInstance = function (target) {
        return $(target).data('__spVideoPlayerInstance__');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @static
     * @return {jQuery}
     */
    VideoPlayer.setInstance = function (target, instance) {
        return $(target).data('__spVideoPlayerInstance__', instance);
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    VideoPlayer.prototype._make = function () {
        var player = $('<video />').css({width: '100%', height: '100%'});
        this._target.append(player);

        // gets the player element
        this._player = $('video', this._target)[0];

        // updates
        this.setAutoPlay(this._isAutoPlay);
        this.setControls(this._hasControls);
    };

    /**
     * Plays a video.
     *
     * @param {String|Array.<String>|Array.{src: String, [type: String]}} sources Video paths (not
     *                                                                    required)
     * @return {Void}
     */
    VideoPlayer.prototype.play = function (sources) {
        if (sources !== undefined) {
            this.setSources(sources);
        }

        this._player.play();
    };

    /**
     * Pauses a video.
     *
     * @return {Void}
     */
    VideoPlayer.prototype.pause = function () {
        this._player.pause();
    };

    /**
     * Gets the video sources.
     *
     * @return {Array.{src: String, type: String}
     */
    VideoPlayer.prototype.getSources = function () {
        return this._sources;
    };

    /**
     * Sets the video sources.
     *
     * @param {String|Array.<String>|Array.{src: String, [type: String]}} value Video paths
     */
    VideoPlayer.prototype.setSources = function (value) {
        var self = this;
        this._sources = [];

        if ($.type(value) == 'string') {
            value = [value];
        }

        $.each(value, function (index, value) {
            var src = '';
            var type = '';

            if ($.type(value) == 'string') {
                src = value;

                var pos = src.lastIndexOf('.');
                if (pos >= 0) {
                    type = 'video/' + value.substring(pos + 1).toLowerCase();
                }
            } else {
                src = value['src'];
                type = value['type'];
            }

            self._sources.push({src: src, type: type});
        });

        $('source', this._player).remove();
        $.each(this._sources, function (source) {
            var source = $('<source />').attr({'src': this['src'], 'type': this['type']});
            $('video', this._target).append(source);
        });
    };

    /**
     * Gets the video poster.
     *
     * @return {String}
     */
    VideoPlayer.prototype.getPoster = function () {
        return this._poster;
    };

    /**
     * Sets the video poster.
     *
     * @param {String} value Path to video
     * @return {Void}
     */
    VideoPlayer.prototype.setPoster = function (value) {
        this._poster = value;
        this._player.poster = this._poster;
    };

    /**
     * Gets video width.
     *
     * This function may return `NaN` if the width hasn't been defined previously.
     *
     * @return {Number}
     */
    VideoPlayer.prototype.getWidth = function () {
        return this._width;
    };

    /**
     * Sets video width.
     *
     * @param {Number} value Width
     * @return {Void}
     */
    VideoPlayer.prototype.setWidth = function (value) {
        this._width = value;

        if (!isNaN(this._width)) {
            this._target.width(this._width);
        }
    };

    /**
     * Gets video height.
     *
     * This function may return `NaN` if the height hasn't been defined previously.
     *
     * @return {Number}
     */
    VideoPlayer.prototype.getHeight = function () {
        return this._height;
    };

    /**
     * Sets video height.
     *
     * @param {Number} value height
     * @return {Void}
     */
    VideoPlayer.prototype.setHeight = function (value) {
        this._height = value;

        if (!isNaN(this._height)) {
            this._target.height(this._height);
        }
    };

    /**
     * Is the video player immediately?
     *
     * @return {Boolean}
     */
    VideoPlayer.prototype.isAutoPlay = function () {
        return this._isAutoPlay;
    };

    /**
     * Is the video player pased?
     *
     * @return {Boolean}
     */
    VideoPlayer.prototype.isPaused = function () {
        return this._player.paused;
    };

    /**
     * Sets the auto-play mode.
     *
     * @param {Boolean} value
     * @return {Void}
     */
    VideoPlayer.prototype.setAutoPlay = function (value) {
        this._isAutoPlay = value;
        this._player.autoplay = this._isAutoPlay;
    };

    /**
     * Has the video player controls?
     *
     * @return {Boolean}
     */
    VideoPlayer.prototype.hasControls = function () {
        return this._hasControls;
    };

    /**
     * Sets the video player controls status.
     *
     * @param {Boolean} value Has controls?
     * @return {Void}
     */
    VideoPlayer.prototype.setControls = function (value) {
        this._hasControls = value;
        this._player.controls = this._hasControls;
    };

    /**
     * Gets the current time.
     *
     * @return {Number}
     */
    VideoPlayer.prototype.getTime = function () {
        return this._player.currentTime;
    };

    /**
     * Sets the current time.
     *
     * @param {Number} value;
     * @return {Void}
     */
    VideoPlayer.prototype.setTime = function (value) {
        this._player.currentTime = value;
    };

    /**
     * Gets the playback speed.
     *
     * Normal speed is '1'. Forward speed is greater than 0. Backward speed is lower than 0.
     *
     * @return {Number}
     */
    VideoPlayer.prototype.getSpeed = function () {
        return this._player.playbackRate;
    };

    /**
     * Sets the playback speed.
     *
     * Normal speed is '1'. Forward speed is greater than 0. Backward speed is lower than 0.
     *
     * @param {Number} value Playback speed
     * @return {Void}
     */
    VideoPlayer.prototype.setSpeed = function (value) {
        this._player.playbackRate = value;
    };

    /**
     * This method is fired when the video can be played.
     *
     * @param {Function} handler Event handler
     * @return {Void}
     */
    VideoPlayer.prototype.onLoad = function (handler) {
        this.on('canplay', handler);
    };

    /**
     * This method is fired when the video is playing.
     *
     * @param {Function} handler Event handler
     * @return {Void}
     */
    VideoPlayer.prototype.onPlay = function (handler) {
        this.on('play', handler);
    };

    /**
     * This method is fired when the video is stopped for any reason. For example, the user
     * has paused the video.
     *
     * @param {Function} handler Event handler
     * @return {Void}
     */
    VideoPlayer.prototype.onStop = function (handler) {
        this.on('pause', handler);
    };

    /**
     * This method is fired when the browser can't play the video.
     *
     * @param {Function} handler Event handler
     * @return {Void}
     */
    VideoPlayer.prototype.onAbort = function (handler) {
    };

    /**
     * Adds an event handler.
     *
     * @param {String}   event   Event type
     * @param {Function} handler Handler
     * @return {Void}
     */
    VideoPlayer.prototype.on = function (event, handler) {
        this._player.addEventListener(event, handler);
    };

    /**
     * Searches a method with prefix.
     *
     * @param {VideoPlayer} instance   Video player object
     * @param {String}      methodName Method name
     * @param {String}      prefixes   Method prefixes
     * @return {?Function}
     */
    function searchMethod(instance, methodName, prefixes) {
        var ret = null;

        $.each(prefixes, function (index, prefix) {
            var method = prefix + methodName[0].toUpperCase() + methodName.substring(1);

            if ($.type(instance[method]) == 'function') {
                ret = instance[method];
                return false;
            }
        });

        return ret;
    }

    /**
     * Searches 'getter' method.
     *
     * @param {VideoPlayer} instance   Video player object
     * @param {String}      methodName Method name
     * @return {?Function}
     */
    function searchGetterMethod(instance, methodName) {
        return searchMethod(instance, methodName, ['get', 'is']);
    }

    /**
     * Searches 'setter' method.
     *
     * @param {VideoPlayer} instance   Video player object
     * @param {String}      methodName Method name
     * @return {?Function}
     */
    function searchSetterMethod(instance, methodName) {
        return searchMethod(instance, methodName, ['set']);
    }

    var methods = {
        /**
         * Initialize the video player.
         *
         * @param {Object} options Options (not required)
         * @return {jQuery}
         */
        'init': function (options) {
            return this.each(function () {
                var vp = new VideoPlayer(this);

                // calls methods individually
                $.each(options, function (methodName, value) {
                    var method = vp[methodName];

                    if ($.type(method) != 'function') {
                        method = searchSetterMethod(vp, methodName);

                        if (method == null) {
                            $.error('Invalid property: ' + methodName);
                        }
                    }

                    method.call(vp, value);
                });
            });
        },

        /**
         * Calls a specified method.
         *
         * @param {String} methodName Method name
         * @return {Mixed}
         */
        'call': function (methodName) {
            var ret = this;
            var args = Array.prototype.slice.call(arguments, 1);

            this.each(function () {
                var vp = VideoPlayer.getInstance(this);
                var method = null;

                // calls a getter and returns
                if (args.length == 0) {
                    method = searchGetterMethod(vp, methodName);
                    if (method != null) {
                        ret = method.call(vp);
                        return false;
                    }
                }

                // calls a method
                method = vp[methodName];
                if ($.type(method) != 'function') {
                    method = searchSetterMethod(vp, methodName);
                    if (method == null) {
                        $.error('Invalid method: ' + methodName);
                    }
                }
                method.apply(vp, args);
            });

            return ret;
        },

        /**
         * Plays or pauses a video.
         *
         * @return {jQuery}
         */
        'toggle': function () {
            return this.each(function () {
                var vp = VideoPlayer.getInstance(this);

                if (vp.isPaused()) {
                    vp.play();
                } else {
                    vp.pause();
                }
            });
        }
    };

    /**
     * Defines the plugin.
     *
     * @param {String|Object} param A plain object or a method name
     * @return {jQuery}
     */
    $.fn.spVideoPlayer = function (param) {
        var args = arguments;
        var method = methods['init'];

        if (!$.isPlainObject(param) && $.type(param) != 'string') {
            $.error('The first parameter must be a plain-object or a string');
        }

        if ($.type(param) == 'string') {
            method = methods['call'];

            if (methods[param] !== undefined) {
                args = Array.prototype.slice.call(arguments, 1);
                method = methods[param];
            }
        }

        return method.apply(this, args);
    };
})(jQuery);
