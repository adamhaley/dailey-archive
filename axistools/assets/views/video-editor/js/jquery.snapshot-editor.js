(function ($) {

    /**
     * Creates an instance.
     *
     * @param {String}   poster    Poster path
     * @param {String}   thumbnail Thumbnail path
     * @param {Number}   posterWidth Poster width
     * @param {Number}   posterHeight Poster height
     * @param {Number}   thumbnailWidth Thumbnail width
     * @param {Number}   thumbnailHeight Thumbnail height
     */
    function SnapshotEditor(
        poster,
        thumbnail,
        posterWidth,
        posterHeight,
        thumbnailWidth,
        thumbnailHeight
    ) {
        /**
         * Target.
         * @var {jQuery}
         */
        this._target = null;

        /**
         * Poster path.
         * @var {String}
         */
        this._poster = poster;

        /**
         * Thumbnail path.
         * @var {String}
         */
        this._thumbnail = thumbnail;

        /**
         * Poster width.
         * @var {Number}
         */
        this._posterWidth = posterWidth;

        /**
         * Poster height.
         * @var {Number}
         */
        this._posterHeight = posterHeight;

        /**
         * Thumbnail width.
         * @var {Number}
         */
        this._thumbnailWidth = thumbnailWidth;

        /**
         * Thumbnail height.
         * @var {Number}
         */
        this._thumbnailHeight = thumbnailHeight;

        this._make();
    }

    /**
     * Namespace.
     * @var {String}
     */
    SnapshotEditor._namespace = 'spSnapshotEditor';

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {?Object}
     */
    SnapshotEditor.getInstance = function (target) {
        return $(target).data('snapshot:instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target   Target
     * @param {Object}      instance Instance
     *
     * @return {jQuery}
     */
    SnapshotEditor.setInstance = function (target, instance) {
        return $(target).data('snapshot:instance', instance);
    };

    /**
     * Appends the snapshot to the container.
     *
     * @param {jQuery} container Container
     *
     * @return {Void}
     */
    SnapshotEditor.prototype.appendTo = function (container) {
        container.append(this._target);
    };

    /**
     * Is the snapshot selected?
     *
     * @return {Boolean}
     */
    SnapshotEditor.prototype.isSelected = function () {
        return $('input', this._target).prop('checked');
    };

    /**
     * Selects the snapshot
     *
     * @param {Boolean} value Is selected?
     *
     * @return {Void}
     */
    SnapshotEditor.prototype.setSelected = function (value) {
        var input = $('input', this._target);

        if (input.prop('checked') != value) {
            input.prop('checked', value).trigger('change');
        }
    };

    /**
     * Gets the thumbnail.
     *
     * @return {String}
     */
    SnapshotEditor.prototype.getThumbnail = function () {
        return this._thumbnail;
    };

    /**
     * Gets the poster.
     *
     * @return {String}
     */
    SnapshotEditor.prototype.getPoster = function () {
        return this._poster;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    SnapshotEditor.prototype._make = function () {
        var self = this;
        var inputId = $.spText('uniqid', 'input_');

        this._target = $(
            '<td>' +
                '<div class="image-container">' +
                    '<div class="image"></div>' +
                '</div>' +
                '<div class="buttons">' +
                    '<ul>' +
                        '<li title="Edit Thumbnail">' +
                            '<a href="#" class="edit-thumbnail"><span>Edit Thumbnail</span></a>' +
                        '</li>' +
                        '<li title="Edit Poster">' +
                            '<a href="#" class="edit-image"><span>Edit Poster</span></a>' +
                        '</li>' +
                        '<li title="Delete Snapshot">' +
                            '<a class="delete" href="#"><span>Delete snapshot</span></a>' +
                        '</li>' +
                    '</ul>' +
                '</div>' +
                '<div class="use-image">' +
                    '<input type="radio" name="use-image" id="' + inputId + '" value="true" />' +
                    '<label for="' + inputId + '">Use Image</label>' +
                '</div>' +
            '</td>'
        );

        // shows poster
        $('.image', this._target).css('background-image', 'url(' +  this._poster +')');

        this._makeActions();
        SnapshotEditor.setInstance(this._target, this);
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    SnapshotEditor.prototype._makeActions = function () {
        var self = this;

        $('.edit-thumbnail', this._target).click(function () {
            var target = $(this);

            var ui = $.axis('window', 'image-editor.html', {
                image: self._thumbnail,
                cropWidth: self._thumbnailWidth,
                cropHeight: self._thumbnailHeight
            });

            ui.on('done', function () {
                self._thumbnail = ui.data('value');
                ui.remove();
            });

            target.blur();
            return false;
        });

        $('.edit-image', this._target).click(function () {
            var target = $(this);

            var ui = $.axis('window', 'image-editor.html', {
                image: self._poster,
                cropWidth: self._posterWidth,
                cropHeight: self._posterHeight
            });

            ui.on('done', function () {
                self._poster = ui.data('value');
                ui.remove();
            });

            target.blur();
            return false;
        });

        $('.delete', this._target).click(function () {
            var target = $(this);

            self._target.remove();

            // if checked, marks the last snapshot
            if (self.isSelected()) {
                $('.snapshots td:last-child').snapshotEditor('selected', true);
            }

            target.blur();
            return false;
        });
    };

    var methods = {
        /**
         * Appends a snapshot to the container.
         *
         * @param {String}   poster    Poster path
         * @param {String}   thumbnail Thumbnail path
         * @param {Number}   posterWidth Poster width
         * @param {Number}   posterHeight Poster height
         * @param {Number}   thumbnailWidth Thumbnail width
         * @param {Number}   thumbnailHeight Thumbnail height
         *
         * @return {jQuery}
         */
        'append': function (
            poster, thumbnail, posterWidth, posterHeight, thumbnailWidth, thumbnailHeight
        ) {
            return this.each(function () {
                var container = $(this);
                var snapshot = new SnapshotEditor(
                    poster,
                    thumbnail,
                    posterWidth,
                    posterHeight,
                    thumbnailWidth,
                    thumbnailHeight
                );
                snapshot.appendTo(container);
            });
        },

        /**
         * Gets or sets the 'selected' status.
         *
         * @param {Boolean} value (not required)
         *
         * @return {jQuery|Boolean}
         */
        'selected': function (value) {
            if (arguments.length > 0) {
                // sets a value
                return this.each(function () {
                    var snapshot = SnapshotEditor.getInstance(this);
                    snapshot.setSelected(value);
                });
            } else {
                // gets a value
                var ret = false;

                this.each(function () {
                    var snapshot = SnapshotEditor.getInstance(this);
                    ret = snapshot.isSelected();
                });

                return ret;
            }
        },

        /**
         * Gets the thumbnail.
         *
         * @return {String}
         */
        'thumbnail': function () {
            var ret = '';

            this.each(function () {
                var snapshot = SnapshotEditor.getInstance(this);

                ret = snapshot.getThumbnail();
                return false;
            });

            return ret;
        },

        /**
         * Gets the poster.
         *
         * @return {String}
         */
        'poster': function () {
            var ret = '';

            this.each(function () {
                var snapshot = SnapshotEditor.getInstance(this);

                ret = snapshot.getPoster();
                return false;
            });

            return ret;
        }
    };

    /**
     * Main method.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.fn.snapshotEditor = function (methodName) {
        var method = methods[methodName];

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, Array.prototype.slice.call(arguments, 1));
    };
})(jQuery);
