/**
 * Extends the 'require' configuration file.
 */
$.require('config', $.extend(true, {}, $.require('config'), {
    libraries: {
        'jquery.snapshot-editor': {
            sources: ['assets/views/video-editor/js/jquery.snapshot-editor.js'],
            requires: ['jquery.sp-text', 'jquery.axis']
        },
        'jquery.sp-video-player': 'assets/views/video-editor/js/jquery.sp-video-player.js'
    }
}));
