$.require(
    [
        'kinetic',
        'jquery.sp-text',
        'jquery.sp-request',
        'jquery.axis',
        'jquery.axis-color-picker',
        'jquery.transformtool'
    ],
    function () {
        /**
         * Image editor configuration.
         * @var {Object}
         */
        var _config = {
            // transform tool
            transformTool: {
                handlerRadius: 5,
                handlerStrokeWidth: 2,
                borderStrokeWidth: 2,
                rotateDistance: 35
            },

            // zoom
            zoom: {
                min: 0.1,
                max: 2,
                inc: 0.1
            },

            // crop
            crop: {
                width: 2,
                length: 10,
                opacity: 0.5,
                lightColor: 'white',
                darkColor: 'black'
            }
        };

        /**
         * Has the image changed?
         * var {Booolean}
         */
        var _hasChanged = false;

        /**
         * Interface width.
         * @var {Number}
         */
        var _width = 0;

        /**
         * Interface height.
         * @var {Number}
         */
        var _height = 0;

        /**
         * The stage.
         * @var {Kinetic.Stage}
         */
        var _stage = null;

        /**
         * Image layer.
         * @var {Kinetic.Layer}
         */
        var _imageLayer = null;

        /**
         * Crop layer.
         * @var {Kinetic.Layer}
         */
        var _cropLayer = null;

        /**
         * The image.
         * @var {Kinetic.Image}
         */
        var _image = null;
        
        /**
         * Available watermark sizes in pixels.
         * @var {Array.<Number>}
         */
        var _watermarkSizes = [8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 32, 36, 48, 72];
        
        /**
         * The watermark text.
         * @var {Kinetic.Text}
         */
        var _watermark = null;
        var _watermarkText = '';
        var _watermarkFontName = 'WatermarkFont';
        var _watermarkFontFile = '';
        var _watermarkFontSize = 0;
        var _watermarkColor = '';
        var _watermarkOpacity = 0; // a number between 0 and 1
        var _watermarkHAlign = ''; // left, center or right
        var _watermarkVAlign = ''; // top, center or bottom
        var _watermarkHPadding = ''; // positive decimal or percentage
        var _watermarkVPadding = ''; // positive decimal or percentage

        /**
         * Image source.
         * @var {string}
         */
        var _src = '';
        
        /**
         * Has original image?
         * @var {boolean}
         */
        var _hasOriginalSrc = false;
        
        /**
         * Original image source.
         * @var {string}
         */
        var _originalSrc = '';

        /**
         * Previous uploaded image.
         * @vara {string}
         */
        var _prevSrc = '';

        /**
         * Upload folder.
         * @var {string}
         */
        var _uploadFolder = '';

        /**
         * Crop width.
         * @var {Number}
         */
        var _cropWidth = 0;

        /**
         * Crop height.
         * @var {Number}
         */
        var _cropHeight = 0;

        /**
         * Zoom.
         * @var {Number}
         */
        var _zoom = 1;

        /**
         * Main function.
         */
        function main() {
            var self = $.axis('self');

            $('.close').click(function () {
                if (_hasChanged) {
                    $.axis('confirm', 'Do you want to discard changes?', function () {
                        close();
                    });
                } else {
                    close();
                }

                this.blur();
                return false;
            });

            // image source
            _src = self.data('image');
            if (!$.spText('empty', _src)) {
                _src = '/' + $.spText('ltrim', _src, '/');
            }
            
            // original image source
            _hasOriginalSrc = self.data('hasOriginalImage');
            _originalSrc = self.data('originalImage');
            if (!$.spText('empty', _originalSrc)) {
                _originalSrc = '/' + $.spText('ltrim', _originalSrc, '/');
            }

            $.axis('get', 'image-editor.php', {src: _src}).done(function (xml) {
                _uploadFolder = $('directory', xml).text();
                _width = parseInt($('width', xml).text(), 10);
                _height = parseInt($('height', xml).text(), 10);
                
                // watermark
                var watermarkText = self.data('watermark');
                _watermarkText = watermarkText === null || watermarkText === undefined
                    ? $('watermark > text', xml).text()
                    : '' + self.data('watermark');
                _watermarkFontFile = $('watermark > font-file', xml).text();
                _watermarkFontSize = parseInt($('watermark > font-size', xml).text(), 10);
                _watermarkColor = getHexColor($('watermark > color', xml).text());
                _watermarkOpacity = parseFloat($('watermark > opacity', xml).text(), 10);
                _watermarkHAlign = $('watermark > align > horizontal', xml).text();
                _watermarkVAlign = $('watermark > align > vertical', xml).text();
                _watermarkHPadding = $('watermark > padding > horizontal', xml).text();
                _watermarkVPadding = $('watermark > padding > vertical', xml).text();
                
                // loads font-file dynamically
                if (!$.spText('empty', _watermarkFontFile)) {
                    $('head').append(
                        '<style type="text/css">\n' +
                            '@font-face {\n' +
                                'font-family: \'' + _watermarkFontName + '\';\n' +
                                'src: url(' + encodeURI(_watermarkFontFile) + ');\n' +
                            '}\n' +
                        '</style>'
                    );
                    $('#canvas').css('font-family', _watermarkFontName);
                }

                // crop width
                _cropWidth = parseInt(self.data('cropWidth'), 10);
                if (isNaN(_cropWidth)) {
                    _cropWidth = 0;
                }

                // crop height
                _cropHeight = parseInt(self.data('cropHeight'), 10);
                if (isNaN(_cropHeight)) {
                    _cropHeight = 0;
                }
                
                make();
                update();

                if (!$.spText('empty', _src)) {
                    loadImage(_src);
                }
            }).always(function () {
                $('.jquery-modal-ui').css('opacity', 1);
            });
        }

        /**
         * Makes UI.
         *
         * @return {Void}
         */
        function make(imageSrc) {
            // resizes and centers user interface
            $('.image-editor')
                .width(_width)
                .height(_height)
                .css('left', ($(window).width() - _width) / 2);

            $.axis('center', $('.jquery-modal-ui'));
            makeTooltips();
            makeWatermarkControls();
            makeCanvas();
            makeResizable();
            makeActions();
        }

        /**
         * Makes tooltips.
         *
         * @return {Void}
         */
        function makeTooltips() {
            var tooltip = $('.head .tooltip');

            $('.head .buttons a, .head .buttons div').each(function () {
                var target = $(this);

                // remove default tooltip
                target.data('tooltip', target.attr('title'));
                target.removeAttr('title');

                // show tooltip on mouse enter
                target.mouseenter(function () {
                    if (target.hasClass('disabled')) {
                        return;
                    }

                    tooltip.text(target.data('tooltip'));
                });

                // remove tooltip on mouse leave
                target.mouseleave(function () {
                    if (target.hasClass('disabled')) {
                        return;
                    }

                    tooltip.text('');
                });
            });
        }
        
        /**
         * Makes footer.
         *
         * @return {Void}
         */
        function makeWatermarkControls() {
            // appends _watermarkFontSize if not included in available sizes
            var sizes = _watermarkSizes.slice();
            if ($.inArray(_watermarkFontSize, _watermarkSizes) < 0) {
                var pos = -1;
                $.each(sizes, function (index, size) {
                    if (size > _watermarkFontSize) {
                        pos = index;
                        return false;
                    }
                });
                if (pos < 0) {
                    sizes.push(_watermarkFontSize);
                } else {
                    sizes.splice(pos, 0, _watermarkFontSize);
                }
            }
            
            $('#watermark-text').val(_watermarkText);
            
            $('#watermark-size')
                .empty()
                .append(function () {
                    var target = $(this);
                    $.each(sizes, function (index, size) {
                        target.append(
                            $('<option />').attr('value', size).text(size + ' px')
                        );
                    });
                })
                .val(_watermarkFontSize);
            
            $('#watermark-color')
                .axisColorPicker('init')
                .axisColorPicker('color', _watermarkColor)
                .axisColorPicker('change', function () {
                    var target = $(this);
                    var color = target.axisColorPicker('color');

                    $('.color', target).css('background-color', color);
                })
                .mousedown(function () {
                    var target = $(this);
                    target.axisColorPicker('toggle');
                })
            $('#watermark-color .color').css('background-color', _watermarkColor);
            
            $("#watermark-opacity")
                .slider({
                    orientation: "horizontal",
                    range: "min",
                    min: 0,
                    max: 100,
                    value: 100 * _watermarkOpacity,
                    slide: function( event, ui ) {
                        var target = $(this);
                        target.data('value', ui.value / 100);
                        $(this).trigger('change');
                    }
                })
                .data('value', _watermarkOpacity);
        }

        /**
         * Makes actions.
         *
         * @return {Void}
         */
        function makeActions() {
            // updates watermark when the the controls change
            $('#watermark-text').on('input', function () {
                updateWatermark();
            });
            $('#watermark-color').axisColorPicker('change', function () {
                updateWatermark();
            });
            $('#watermark-opacity, #watermark-size').on('change', function () {
                updateWatermark();
            });
            
            $('#download').click(function () {
                var target = $(this).blur();

                if (!target.hasClass('disabled')) {
                    // forces to download the image
                    var basename = _src.split('/').pop();
                    $('#download').attr({href: _src, download: basename});
                } else {
                    return false;
                }
            });

            $('#revert').click(function () {
                var target = $(this).blur();

                if (!target.hasClass('disabled')) {
                    revert();
                }

                return false;
            });

            $('#upload').change(function () {
                $.axis(
                    'upload',
                    'file-upload.php',
                    {file: this, dir: _uploadFolder}
                ).done(function (xml) {
                    var src = $('path', xml).text();

                    if (!$.spText('empty', _prevSrc)) {
                        // deletes the previous uploaded image and loads the new one
                        $.axis(
                            'post',
                            'file-delete.php',
                            {files: [_prevSrc]}
                        ).always(function (xml) {
                            loadImage(src, function () {
                                _originalSrc = '';
                                _watermark.setVisible(true);
                                _prevSrc = src;
                                _hasChanged = true;
                                updateWatermarkPosition();
                                updateWatermark();
                            });
                        });
                    } else {
                        loadImage(src, function () {
                            _originalSrc = '';
                            _watermark.setVisible(true);
                            _prevSrc = src;
                            _hasChanged = true;
                            updateWatermarkPosition();
                            updateWatermark();
                        });
                    }
                });

                $(this).val('');
            });

            $('#transform').click(function () {
                var target = $(this).blur();

                if (!target.hasClass('disabled')) {
                    // shows or hides the transform tool
                    target.toggleClass('selected');
                    $(_image).transformTool(target.hasClass('selected')? 'show': 'hide');
                    _hasChanged = true;
                }

                return false;
            });

            $('#scale').click(function () {
                var target = $(this).blur();

                if (!target.hasClass('disabled')) {
                    autoScale();
                    _hasChanged = true;
                }

                return false;
            });

            $('#zoom').on('input', function () {
                var target = $(this);
                var value = target.val();

                value = value.replace(/[^0-9]*/gi, '');
                target.val(value);
            }).focusout(function () {
                var target = $(this);
                var value = parseInt(target.val(), 10);

                if (!isNaN(value)) {
                    setZoom(value / 100);
                }
            }).keydown(function (event) {
                if (event.which == 13) {
                    $(this).trigger('focusout');
                }
            });

            $('#zoom-in').click(function () {
                var target = $(this);

                setZoom(_zoom + _config.zoom.inc);
                target.blur();
                return false;
            });

            $('#zoom-out').click(function () {
                var target = $(this);

                setZoom(_zoom - _config.zoom.inc);
                target.blur();
                return false;
            });

            $('#zoom-fit').click(function () {
                var target = $(this).blur();

                if (!target.hasClass('disabled')) {
                    autoZoom();
                }

                return false;
            });

            $("#submit").click(function () {
                var target = $(this).blur();
                var self = $.axis('self');

                if (!target.hasClass('disabled')) {
                    if (_hasChanged) {
                        // saves and closes
                        var imageX = $(_image).transformTool('x');
                        var imageY = $(_image).transformTool('y');
                        var imageWidth = $(_image).transformTool('width');
                        var imageHeight = $(_image).transformTool('height');
                        var imageRotation = $(_image).transformTool('rotation');
                        var preserveImage = $('#preserve-image').is(':checked')? 'true' : 'false';
                        var preserveTransparency = $('#preserve-transparency').is(':checked')
                            ? 'true'
                            : 'false';

                        if (preserveImage == 'true') {
                            // no image transformations
                            self.data('value', _src);
                            self.data('originalImage', _originalSrc);
                            self.trigger('done');
                        } else {
                            preserveImage = _hasOriginalSrc && $.spText('empty', _originalSrc)
                                ? 'true'
                                : 'false';
                            
                            // applies image transformations
                            $.axis(
                                'post',
                                'image-editor.php',
                                {
                                    src: _src,
                                    x: imageX,
                                    y: imageY,
                                    width: imageWidth,
                                    height: imageHeight,
                                    crop_width: _cropWidth,
                                    crop_height: _cropHeight,
                                    rotation: imageRotation,
                                    preserve_image: preserveImage,
                                    preserve_transparency: preserveTransparency
                                }
                            ).done(function (xml) {
                                _originalSrc = _src;
                                _src = $('src', xml).text();
                                
                                // applies watermark
                                var watermarkText = $.trim(_watermark.getText());
                                if (_watermark.isVisible() && !$.spText('empty', watermarkText)) {
                                    var watermarkX = _cropWidth / 2 + _watermark.getX();
                                    var watermarkY = _cropHeight / 2 + _watermark.getY();
                                    var watermarkSize = _watermark.getFontSize();
                                    var watermarkColor = getHexColor(_watermark.getFill());
                                    var watermarkOpacity = _watermark.getOpacity();
                                    
                                    // calculates the left-top corner of the imaginary box
                                    // that contains the picture
                                    var boxX = Number.POSITIVE_INFINITY;
                                    var boxY = Number.POSITIVE_INFINITY;
                                    var handlers = $(_image).transformTool('handlers');
                                    $.each(handlers, function () {
                                        var pos = this.getAbsolutePosition();
                                        boxX = Math.min(boxX, pos.x);
                                        boxY = Math.min(boxY, pos.y);
                                    });
                                    
                                    // fixes watermark position when there's no crop
                                    var pos = _watermark.getAbsolutePosition();
                                    if (_cropWidth == 0) {
                                        watermarkX = pos.x - boxX;
                                    }
                                    if (_cropHeight == 0) {
                                        watermarkY = pos.y - boxY;
                                    }
                                    
                                    $.axis(
                                        'post',
                                        'watermark.php',
                                        {
                                            src: _src,
                                            text: watermarkText,
                                            x: watermarkX,
                                            y: watermarkY,
                                            font: $.axis('absPath', _watermarkFontFile),
                                            size: watermarkSize,
                                            color: watermarkColor,
                                            opacity: watermarkOpacity
                                        }
                                    ).done(function () {
                                        self.data('value', _src);
                                        self.data('originalImage', _originalSrc);
                                        self.trigger('done');
                                    });
                                } else {
                                    self.data('value', _src);
                                    self.data('originalImage', _originalSrc);
                                    self.trigger('done');
                                }
                            });
                        }
                    } else {
                        // closes
                        self.data('value', _src);
                        self.data('originalImage', _originalSrc);
                        self.trigger('done');
                    }
                }

                return false;
            });
        }

        /**
         * Makes the image canvas.
         *
         * @return {Void}
         */
        function makeCanvas() {
            // makes the stage
            var target = $('.image-editor');
            var stageWidth = target.width() - 12;
            var stageHeight = target.height() - 125;
            _stage = new Kinetic.Stage({
                container: 'canvas',
                width: stageWidth,
                height: stageHeight,
                x: stageWidth / 2,
                y: stageHeight / 2
            });
            
            // makes watermark
            _watermark = new Kinetic.Text({
                opacity: _watermarkOpacity,
                text: _watermarkText,
                fontFamily: _watermarkFontName,
                fontSize: _watermarkFontSize,
                fill: _watermarkColor,
                align: _watermarkHAlign,
                visible: false,
                draggable: true
            });

            // creates the image layer
            _imageLayer = new Kinetic.Layer();
            _image = new Kinetic.Image();
            _imageLayer.add(_image);
            _imageLayer.add(_watermark);
            _stage.add(_imageLayer);

            // creates the crop layer
            _cropLayer = new Kinetic.Layer();
            var crop = new Kinetic.Shape({
                opacity: _config.crop.opacity,
                drawFunc: function (context) {
                    if (_cropWidth > 0 || _cropHeight > 0) {
                        for (var k = 0; k < 2; k++) {
                            var length = _config.crop.length / _zoom;
                            var strokeWidth = _config.crop.width / _zoom;
                            var strokeColor = k > 0? _config.crop.darkColor : _config.crop.lightColor;
                            var w = _cropWidth - 2 * strokeWidth * k;
                            var h = _cropHeight - 2 * strokeWidth * k;
                            var x0 = -w / 2;
                            var y0 = -h / 2;

                            // draws the corners
                            this.setStrokeWidth(strokeWidth);
                            this.setStroke(strokeColor);
                            context.beginPath();
                            for (var i = 0; i < 2; i++) {
                                for (var j = 0; j < 2; j++) {
                                    var p = {x: x0 + i * w, y: y0 + j * h};
                                    context.moveTo(p.x, p.y);
                                    context.lineTo(p.x + length * (1 - 2 * i), p.y);
                                    context.moveTo(p.x, p.y);
                                    context.lineTo(p.x, p.y + length * (1 - 2 * j));
                                }
                            }
                            context.closePath();
                            context.strokeShape(this);
                        }
                    }
                }
            });
            _cropLayer.add(crop);
            _cropLayer.setListening(false);
            _stage.add(_cropLayer);
        }

        /**
         * Makes the UI resizable.
         *
         * @return {Void}
         */
        function makeResizable() {
            $('.image-editor').resizable({
                ghost: true,
                stop: function () {
                    var target = $(this);
                    var w = target.width() - 12;
                    var h = target.height() - 125;

                    _stage.setSize(w, h);
                    _stage.setPosition(w / 2, h / 2);
                    _stage.draw();
                }
            });
        }

        /**
         * Updates UI.
         *
         * @return {Void}
         */
        function update() {
            updateButtons();
            updateImageInfo();
            updateWatermark();

            // updates input zoom
            $('#zoom').val((100 * _zoom).toFixed(0));

            // scales transform tool handles
            $(_image).transformTool(
                'handler-radius', _config.transformTool.handlerRadius / _zoom
            );
            $(_image).transformTool(
                'handler-stroke-width', _config.transformTool.handlerStrokeWidth / _zoom
            );
            $(_image).transformTool(
                'border-stroke-width', _config.transformTool.borderStrokeWidth / _zoom
            );
            $(_image).transformTool(
                'rotate-distance', _config.transformTool.rotateDistance / _zoom
            );

            // scales and redraws the stage
            if (_stage !== null) {
                _stage.setScale(_zoom);
                _stage.draw();
            }
        }

        /**
         * Updates the states of the bottons.
         *
         * @return {Void}
         */
        function updateButtons() {
            var isLoaded = isImageLoaded();

            $('#transform').toggleClass('selected', $(_image).transformTool('visible'));
            $('#download').toggleClass('disabled', !isLoaded);
            $('#revert').toggleClass('disabled', !isLoaded);
            $('#transform').toggleClass('disabled', !isLoaded);
            $('#scale').toggleClass('disabled', !(isLoaded && (_cropWidth > 0 || _cropHeight > 0)));
            $('#zoom-fit').toggleClass('disabled', !isLoaded);
            $('#submit').toggleClass('disabled', !isLoaded);
        }

        /**
         * Updates the image info.
         *
         * @return {Void}
         */
        function updateImageInfo() {
            var img = getImage();

            // original image size
            if (img !== undefined) {
                $('#original-image-width').text(getOriginalImageWidth());
                $('#original-image-height').text(getOriginalImageHeight());
            }

            // crop size
            $('#crop-width').text(_cropWidth > 0? _cropWidth : 'Unlimited');
            $('#crop-height').text(_cropHeight > 0? _cropHeight: 'Unlimited');

            // current size
            if (img !== undefined) {
                $('#image-width').text(Math.round(_image.getWidth()));
                $('#image-height').text(Math.round(_image.getHeight()));
            }
        }
        
        /**
         * Updates watermark position.
         * 
         * @return {Void}
         */
        function updateWatermarkPosition() {
            var text = $('#watermark-text').val();
            
            // crop width
            var cropWidth = _cropWidth;
            if (cropWidth == 0) {
                cropWidth = $(_image).transformTool('width');
            }
            
            // crop height
            var cropHeight = _cropHeight;
            if (cropHeight == 0) {
                cropHeight = $(_image).transformTool('height');
            }
            
            // horizontal padding
            var hPadding = parseInt(_watermarkHPadding, 10);
            if (_watermarkHPadding.match(/^\d+%$/) !== null) {
                hPadding = cropWidth * hPadding / 100;
            }
            
            // vertical padding
            var vPadding = parseInt(_watermarkVPadding, 10);
            if (_watermarkVPadding.match(/^\d+%$/) !== null) {
                vPadding = cropHeight * vPadding / 100;
            }
            
            // aligns watermark horizontally
            var textWidth = getTextWidth(_watermarkFontName, _watermarkFontSize, text);
            if (_watermarkHAlign == 'left') {
                _watermark.setX(-cropWidth / 2 + hPadding);
            } else
            if (_watermarkHAlign == 'center') {
                _watermark.setX(-textWidth / 2);
            } else
            if (_watermarkHAlign == 'right') {
                _watermark.setX(cropWidth / 2 - textWidth - hPadding);
            }
            
            // aligns watermark vertically
            if (_watermarkVAlign == 'top') {
                _watermark.setY(-cropHeight / 2 + vPadding);
            } else
            if (_watermarkVAlign == 'center') {
                _watermark.setY(-_watermark.getTextHeight() / 2);
            } else
            if (_watermarkVAlign == 'bottom') {
                _watermark.setY(cropHeight / 2 - _watermark.getTextHeight() - vPadding);
            }
        }
        
        /**
         * Updates watermark.
         * 
         * @return {Void}
         */
        function updateWatermark() {
            var text = $('#watermark-text').val();
            var color = $('#watermark-color').axisColorPicker('color');
            var size = $('#watermark-size').val();
            var opacity = $('#watermark-opacity').data('value');
            
            // hides or shows the watermark controls
            $('.watermark').toggle(_watermark.isVisible());
            
            _watermark.setText(text);
            _watermark.setFill(color);
            _watermark.setFontSize(size);
            _watermark.setOpacity(opacity);
            _watermark.moveToTop();
            _stage.draw();
        }

        /**
         * Closes the user interface.
         *
         * @return {Void}
         */
        function close() {
            var self = $.axis('self');

            if (!$.spText('empty', _prevSrc)) {
                // removes the previous uploaded image and removes the interface
                $.axis('post', 'file-delete.php', {files: [_prevSrc]}).always(function (xml) {
                    _prevSrc = '';
                    self.remove();
                });
            } else {
                self.remove();
            }
        }

        /**
         * Loads an image.
         *
         * @param String src Image source
         * @param Function onLoad 'load' event handler
         * @return {Void}
         */
        function loadImage(src, onLoad) {
            var img = new Image();
            var imgOnLoad = null;
            var loading = $.axis('loading', 'Loading image...', function () {
                img.removeEventListener('load', imgOnLoad);
                $.error('Image loading aborted');
            });


            img.addEventListener('error', function () {
                loading.remove();
                $.axis('error', 'The image could not be loaded or is invalid:\n' + src);
            });

            img.addEventListener('load', imgOnLoad = function () {
                loading.remove();

                // sets image
                _zoom = 1;
                _src = src;
                _image.setImage(img);
                $(_image).transformTool({
                    'handler-radius': _config.transformTool.handlerRadius,
                    'handler-stroke-width': _config.transformTool.handlerStrokeWidth,
                    'border-stroke-width': _config.transformTool.borderStrokeWidth,
                    'rotate-distance': _config.transformTool.rotateDistance,
                    x: 0,
                    y: 0,
                    width: img.width,
                    height: img.height,
                    rotation: 0,
                    onResize: updateImageInfo
                }, function () {
                    // autozoom
                    var pad = 0.95;
                    if (img.width > pad * _stage.getWidth() || img.height > pad * _stage.getHeight()) {
                        autoZoom();
                    }

                    update();

                    if (onLoad !== undefined) {
                        $.proxy(onLoad, this)();
                    }
                });
            });

            img.src = src + '?rnd=' + $.spText('uniqid');
        }

        /**
         * Reverts the changes.
         *
         * @return {Void}
         */
        function revert() {
            if ($.spText('empty', _originalSrc)) {
                // reverts last changes
                $(_image).transformTool({
                    x: 0,
                    y: 0,
                    width: getOriginalImageWidth(),
                    height: getOriginalImageHeight(),
                    rotation: 0
                }, function () {
                    update();
                });
            } else {
                // loads original image
                loadImage(_originalSrc, function () {
                    _originalSrc = '';
                    _watermark.setVisible(true);
                    _hasChanged = true;
                    updateWatermarkPosition();
                    updateWatermark();
                });
            }
        }

        /**
         * Zooms the image to fit the visible area.
         *
         * @return {Void}
         */
        function autoZoom() {
            var pad = 0.95;
            var w0 = pad * _stage.getWidth();
            var h0 = pad * _stage.getHeight();
            var w1 = _image.getWidth();
            var h1 = _image.getHeight();
            var a0 = h0 / w0;
            var a1 = h1 / w1;

            setZoom(a0 < a1? h0 / h1 : w0 / w1);
        }

        /**
         * Scales the image to fit the crop area.
         *
         * @return {Void}
         */
        function autoScale() {
            var w0 = _cropWidth + 5;
            var h0 = _cropHeight + 5;
            var w1 = getOriginalImageWidth();
            var h1 = getOriginalImageHeight();
            var w2 = 0;
            var h2 = 0;
            var a0 = h0 / w0;
            var a1 = h1 / w1;

            if (a0 < a1) {
                w2 = w0;
                h2 = w0 * a1;
            } else {
                w2 = h0 / a1;
                h2 = h0;
            }

            $(_image).transformTool({
                x: 0,
                y: 0,
                width: w2,
                height: h2,
                rotation: 0
            }, function () {
                update();
            });
        }

        /**
         * Gets the image zoom.
         *
         * @return {Number}
         */
        function getZoom() {
            return _zoom;
        }

        /**
         * Sets the image zoom.
         *
         * @return {Void}
         */
        function setZoom(value) {
            _zoom = Math.max(_config.zoom.min, Math.min(_config.zoom.max, value));
            update();
        }

        /**
         * Gets image entity.
         *
         * @return {Image}
         */
        function getImage() {
            return _image !== null? _image.getImage() : undefined;
        }

        /**
         * Is the image loaded?
         *
         * @return {Boolean}
         */
        function isImageLoaded() {
            return $.type(getImage()) == 'object';
        }

        /**
         * Gets the original image width.
         *
         * @return {Number}
         */
        function getOriginalImageWidth() {
            var img = getImage();
            return $.type(getImage()) == 'object'? img.width : 0;
        }

        /**
         * Gets the original image height.
         *
         * @return {Number}
         */
        function getOriginalImageHeight() {
            var img = getImage();
            return $.type(getImage()) == 'object'? img.height : 0;
        }
        
        /**
         * Gets the hexadecimal representation of a CSS color.
         * 
         * See http://stackoverflow.com/a/1573154/1704895
         * 
         * @param {String} color CSS color
         * 
         * @return {String}
         */
        function getHexColor(color) {
            var a = document.createElement('div');
            a.style.color = color;
            var cols = window.getComputedStyle(
                document.body.appendChild(a)
            ).color.match(/\d+/g).map(function(a){
                return parseInt(a,10);
            });
            document.body.removeChild(a);
            return (cols.length >= 3)
                ? '#' + (((1 << 24) + (cols[0] << 16) + (cols[1] << 8) + cols[2]).toString(16).substr(1))
                : false;
        }
        
        /**
         * Gets text width.
         * 
         * See http://stackoverflow.com/a/17284889/1704895
         * 
         * @param {String} fontFamily Font family
         * @param {Number} fontSize   Font size
         * @param {String} text       Text
         * 
         * @return {Number}
         */
        function getTextWidth(fontFamily, fontSize, text) {
            var canvas = _imageLayer.getCanvas();
            var ctx=canvas.getContext("2d")._context;
            var font = [fontSize + 'px', fontFamily].join(' ');
            ctx.font = font;
            var measure = ctx.measureText(text);
            return(measure.width);
        }

        main();
    }
);
