/**
 * Extends the 'require' configuration file.
 */
$.require('config', $.extend(true, {}, $.require('config'), {
    libraries: {
        'kinetic': 'assets/views/image-editor/js/kinetic-v4.7.4.min.js',
        'jquery.transformtool': {
            sources: ['assets/views/image-editor/js/jquery.transformtool-1.0.2.js'],
            requires: ['jquery.sp-timer']
        }
    }
}));
