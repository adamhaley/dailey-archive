(function ($) {
    /**
     * Interface width.
     * @var {Number}
     */
    var _width = 0;

    /**
     * Interface height.
     * @var {Number}
     */
    var _height = 0;

    /**
     * Has the text changed?
     * @var {Boolean}
     */
    var _hasChanged = false;

    /**
     * Makes the UI.
     *
     * @return {Void}
     */
    function make() {
        var ui = $('.textarea-editor');
        var textArea = $('#textarea');
        var self = $.axis('self');
        var value = self.data('value');

        // populates textarea
        textArea.val(value);

        // sets size
        textArea.width(_width).height(_height);

        // autofocus
        textArea.focus();

        // has the text changed?
        textArea.on('input', function () {
            _hasChanged = true;
        });

        // centers the interface horizontally
        ui.css('left', ($(window).width() - textArea.width()) / 2);

        $.axis('center', $('.jquery-modal-ui'));
        makeActions();
    }

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    function makeActions() {
        $('#done').click(function () {
            var self = $.axis('self');

            self.data('value', $('#textarea').val());
            self.trigger('done');
            return false;
        });
    }

    $.plainTextEditor = function () {
        $('#close').click(function () {
            var self = $.axis('self');

            if (_hasChanged) {
                $.axis('confirm', 'Do you want to discard changes?', function () {
                    self.remove();
                });
            } else {
                self.remove();
            }

            return false;
        });

        $.axis('get', 'textarea-editor.php').done(function (xml) {
            // interface size
            _width = parseInt($('width', xml).text(), 10);
            _height = parseInt($('height', xml).text(), 10);


            make();
        }).always(function () {
            $('.jquery-modal-ui').css('opacity', 1);
        });
    };
})(jQuery);
