$.require(['jquery.axis', 'jquery.plain-text-editor', 'jquery.rich-text-editor'], function () {
    var self = $.axis('self');
    var isPlainText = self.data('isPlainText') !== undefined? self.data('isPlainText').valueOf(): false;

    // prepares the interface for rich or plain text
    if (isPlainText) {
        $.plainTextEditor();
    } else {
        $.richTextEditor();
    }
});
