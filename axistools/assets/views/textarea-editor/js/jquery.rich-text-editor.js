(function ($) {
    /**
     * External TinyMCE plugins.
     * @var {String}
     */
    var _tinyMcePlugins = {
        'email': 'assets/views/textarea-editor/js/tinymce/plugins/email/plugin.min.js'
    };
    
    /**
     * Available fonts.
     * @var {Array.<{name: String, value: String, sources: Array.<String>}>}
     */
    var _availFonts = [
        {name: 'Andale Mono', value: 'andale mono,times', sources: []},
        {name: 'Arial', value: 'arial,helvetica,sans-serif', sources: []},
        {name: 'Arial Black', value: 'arial black,avant garde', sources: []},
        {name: 'Book Antiqua', value: 'book antiqua,palatino', sources: []},
        {name: 'Comic Sans MS', value: 'comic sans ms,sans-serif', sources: []},
        {name: 'Courier New', value: 'courier new,courier', sources: []},
        {name: 'Georgia', value: 'georgia,palatino', sources: []},
        {name: 'Helvetica', value: 'helvetica', sources: []},
        {name: 'Impact', value: 'impact,chicago', sources: []},
        {name: 'Symbol', value: 'symbol', sources: []},
        {name: 'Tahoma', value: 'tahoma,arial,helvetica,sans-serif', sources: []},
        {name: 'Terminal', value: 'terminal,monaco', sources: []},
        {name: 'Times New Roman', value: 'times new roman,times', sources: []},
        {name: 'Trebuchet MS', value: 'trebuchet ms,geneva', sources: []},
        {name: 'Verdana', value: 'verdana,geneva', sources:[]},
        {name: 'Webdings', value: 'webdings', sources: []},
        {name: 'Wingdings', value: 'wingdings,zapf dingbats', sources: []}
    ];

    /**
     * Available font sizes.
     * @var {Array.<String>}
     */
    var _availFontSizes = ['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt'];

    /**
     * Default font-family.
     * @var {String}
     */
    var _defaultFontFamily = '';

    /**
     * Default font-size.
     * @var {String}
     */
    var _defaultFontSize = '';

    /**
     * Default color.
     * @var {String}
     */
    var _defaultColor = '';

    /**
     * Has the text changed?
     * @var {Boolean}
     */
    var _hasChanged = false;

    /**
     * Interface width.
     * @var {Number}
     */
    var _width = 0;

    /**
     * Interface height.
     * @var {Number}
     */
    var _height = 0;
    
    /**
     * TinyMCE toolbar1 buttons.
     * @var {String}
     */
    var _toolbar1 = '';
    
    /**
     * TinyMCE toolbar2 buttons.
     * @var {String}
     */
    var _toolbar2 = '';

    /**
     * Makes the UI.
     *
     * @return {Void}
     */
    function make() {
        var self = $.axis('self');
        var value = self.data('value') !== undefined? self.data('value').valueOf(): "Lorem Ipsum Dolor";

        // populates textarea
        $('#textarea').val(value);

        // font formats
        var fontFormats = '';
        $.each(_availFonts, function (font) {
            fontFormats = $.spText('concat', ';', fontFormats, this.name + '=' + this.value);
        });

        // loads and uses fonts
        var embeddedFonts = 'assets/views/textarea-editor/css/embedded_fonts.php' +
            '?font_family=' + $.spUri('encode', _defaultFontFamily) +
            '&font_size=' + $.spUri('encode', _defaultFontSize) +
            '&color=' + $.spUri('encode', _defaultColor) +
            '&fonts=' + $.spUri('encode', JSON.stringify(_availFonts));
        $('<link type="text/css" rel="stylesheet" media="all" />')
            .attr('href', embeddedFonts)
            .appendTo('head');

        tinymce.init({
            plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools code'],
            external_plugins: {
                'email': $.axis('absPath', _tinyMcePlugins.email)
            },
            selector: "textarea",
            auto_focus: 'textarea',
            resize: 'both',
            menubar: false,
            width: _width,
            height: _height,
            font_formats: fontFormats,
            fontsize_formats: _availFontSizes.join(' '),
            toolbar_items_size: 'medium',
            toolbar1: _toolbar1,
            toolbar2: _toolbar2,
            content_css: embeddedFonts,
            setup: function (ed) {
                ed.on('init', $.proxy(initTextareaEditor, window, ed));
                ed.on('change', function () {
                    _hasChanged = true;
                });
            }
        });

        makeActions();
    }

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    function makeActions() {
        $('#done').click(function () {
            var self = $.axis('self');
            var value = tinyMCE.get('textarea').getContent();

            self.data('value', value);
            self.trigger('done');
            return false;
        });
    }

    /**
     * Initializes the textarea editor.
     *
     * @param {TinyMCE} Editor instance
     *
     * @return {Void}
     */
    function initTextareaEditor(editor) {
        // sets the min-width
        var ui = $('.textarea-editor');
        var editorContainer = $(editor.getContainer());
        editorContainer.css('min-width', ui.css('min-width'));

        // centers the interface horizontally
        ui.css('left', ($(window).width() - editorContainer.width()) / 2);

        // sets default font styles
        if ($.spText('empty', editor.getContent())) {
            if (!$.spText('empty', _defaultFontFamily)) {
                editor.execCommand('FontName', false, _defaultFontFamily);
            }
            if (!$.spText('empty', _defaultFontSize)) {
                editor.execCommand('FontSize', false, _defaultFontSize);
            }
            if (!$.spText('empty', _defaultColor)) {
                editor.execCommand('ForeColor', false, _defaultColor);
            }
            _hasChanged = false;
        }

        var statusBar = $('.mce-statusbar', editorContainer);
        var path = $('.mce-path', statusBar);
        path.hide();
        
        $.axis('center', $('.jquery-modal-ui'));
        $('.jquery-modal-ui').css('opacity', 1);
    }

    /**
     * Gets the font value by its name.
     *
     * @param {String} name Font name
     *
     * @return {String}
     */
    function getFontValueByName(name) {
        var ret = '';

        $.each(_availFonts, function () {
            if (this.name == name) {
                ret = this.value;
                return false;
            }
        });

        return ret;
    }

    /**
     * Entry point function.
     *
     * @return {Void}
     */
    $.richTextEditor = function () {
        $('#close').click(function () {
            var self = $.axis('self');

            if (_hasChanged) {
                $.axis('confirm', 'Do you want to discard changes?', function () {
                    self.remove();
                });
            } else {
                self.remove();
            }

            return false;
        });

        $('#textarea').hide();
        $.axis('get', 'textarea-editor.php').done(function (xml) {
            $('#textarea').show();

            // field properties
            var self = $.axis('self');
            var fontFamily = self.data('font-family') !== undefined
                ? self.data('font-family').valueOf()
                : "";
            var fontSize = self.data('font-size') !== undefined? self.data('font-size').valueOf(): "";
            var color = self.data('color') !== undefined? self.data('color').valueOf(): "";

            // pupulates _availFonts
            var fonts = $('fonts item', xml);
            if (fonts.length > 0) {
                _availFonts = [];
                fonts.each(function () {
                    var target = $(this);
                    var sources = $('source', target);
                    var font = {value: target.attr('value'), name: target.attr('name'), sources: []};
                    sources.each(function () {
                        font.sources.push($.axis('absPath', $(this).text()));
                    });
                    _availFonts.push(font);
                });
            }

            // populates _availFontSizes
            var sizes = $('sizes item', xml);
            if (sizes.length > 0) {
                _availFontSizes = [];
                sizes.each(function () {
                    var target = $(this);
                    _availFontSizes.push(target.attr('value'));
                });
            }
            
            // toolbar buttons
            _toolbar1 = $('buttons toolbar1', xml).text();
            _toolbar2 = $('buttons toolbar2', xml).text();

            // interface size
            _width = parseInt($('width', xml).text(), 10);
            _height = parseInt($('height', xml).text(), 10);

            // default font styles
            _defaultFontFamily = getFontValueByName(
                $.spText('ifEmpty', fontFamily, $('font-family', xml).text())
            );
            if (!$.spText('empty', fontFamily) && $.spText('empty', _defaultFontFamily)) {
                $.axis('error', 'Font family not found: ' + fontFamily);
            }
            _defaultFontSize = $.spText('ifEmpty', fontSize, $('font-size', xml).text());
            _defaultColor = $.spText('ifEmpty', color, $('color', xml).text());

            make();
        });
    };
})(jQuery);
