/**
 * Extends the 'require' configuration file.
 */
$.require('config', $.extend(true, {}, $.require('config'), {
    libraries: {
        'jquery.plain-text-editor': {
            sources: ['assets/views/textarea-editor/js/jquery.plain-text-editor.js'],
            requires: ['jquery.axis']
        },
        'jquery.rich-text-editor': {
            sources: ['assets/views/textarea-editor/js/jquery.rich-text-editor.js'],
            requires: ['jquery.sp-text', 'jquery.sp-uri', 'jquery.axis']
        }
    }
}));
