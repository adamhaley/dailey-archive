<?php
header("Content-type: text/css; charset=utf-u");

// paramers
$fontFamily = isset($_REQUEST["font_family"]) && strlen($_REQUEST["font_family"]) > 0
    ? $_REQUEST["font_family"]
    : "Times New Roman";
$fontSize = isset($_REQUEST["font_size"]) && strlen($_REQUEST["font_size"]) > 0
    ? $_REQUEST["font_size"]
    : "12pt";
$color = isset($_REQUEST["color"]) && strlen($_REQUEST["color"]) > 0
    ? $_REQUEST["color"]
    : "black";
$fonts = isset($_REQUEST["fonts"]) && strlen($_REQUEST["fonts"]) > 0
    ? json_decode($_REQUEST["fonts"])
    : array();

// embeds fonts
foreach ($fonts as $font) {
    if (count($font->sources) > 0) {
        echo "@font-face {\n";
        echo "    font-family: '" . $font->value . "';\n";

        $str = "";
        foreach ($font->sources as $source) {
            if (strlen($str) > 0) {
                $str .= ", ";
            }
            $str .= "url($source)";
        }
        echo "    src: $str;\n";
        echo "}\n";
    }
}

// body
echo "body {\n";
echo "    font-family: $fontFamily;\n";
echo "    font-size: $fontSize;\n";
echo "    color: $color;\n";
echo "}\n";
