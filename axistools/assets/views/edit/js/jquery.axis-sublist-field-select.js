(function ($) {

    /**
     * Class SelectField.
     *
     * @param {HTMLElement} target  Target
     * @param {String}      title   Field title
     * @param {Object}      options Select options
     * @param {String}      value   Field value
     */
    function SelectField(target, title, options, value) {
        this._target = SelectField.setInstance(target, this);
        this._title = title;
        this._options = options;
        this._value = value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    SelectField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    SelectField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };


    /**
     * Gets the value.
     *
     * @return {String}
     */
    SelectField.prototype.getValue = function () {
        return $('select', this._target).val();
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    SelectField.prototype._make = function () {
        var self = this;

        this._target.addClass('input-select').attr('title', this._title).append(
            $('<select />').append(function () {
                var target = $(this);

                $.each(self._options, function (value, text) {
                    target.append($('<option />').attr('value', this.value).text(this.label));
                });
            }).val(this._value)
        );
        
        this._makeActions();
    };
    
    /**
     * Makes actions.
     *
     * @return {Void}
     */
    SelectField.prototype._makeActions = function () {
        var self = this;

        $('select', this._target).on('input', function () {
            self._target.trigger('input-change');
        });
    };

    var methods = {
        /**
         * Makes the component
         *
         * @param {jQuery} column Column node
         * @param {String} value  Cell value
         *
         * @return {jQuery}
         */
        'make': function (column, value) {
            var title = column.attr('title');
            var defValue = column.attr('default');
            var options = JSON.parse(column.attr('options'));

            if ($.spText('empty', value)) {
                value = defValue;
            }

            return this.each(function () {
                new SelectField(this, title, options, value);
            });
        },

        /**
         * Gets field value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var field = SelectField.getInstance(this);

                ret = field.getValue();
                return false;
            });

            return ret;
        }
    };


    $.fn.axisSublistFieldSelect = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
