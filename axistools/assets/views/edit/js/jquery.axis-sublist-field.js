(function ($) {
    /**
     * Gets plugin name by type.
     *
     * @param {String} type Plugin type
     *
     * @return {String}
     */
    function getPluginName(type) {
        return 'axisSublistField' + $.spText('capitalize', type);
    }

    /**
     * Gets plugin by type.
     *
     * @param {String} type Plugin type
     *
     * @return {Function|undefined}
     */
    function getPlugin(type) {
        return $.fn[getPluginName(type)];
    }

    var methods = {
        /**
         * Appends a sublist field.
         *
         * @param {jQuery} column Column node
         * @param {String} value  Cell value
         *
         * @return {jQuery}
         */
        'append': function (column, value) {
            var type = column.attr('type');

            return this.each(function () {
                var target = $(this);
                var plugin = getPlugin(type);
                var field = $('<div class="field" />').data('__type__', type);

                target.append(field);

                try {
                    $.proxy(plugin, field)('make', column, value);
                } catch (error) {
                    $.axis('error', 'Field not valid: ' + type);
                }
            });
        },

        /**
         * Gets field value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                var plugin = getPlugin(type);

                ret = $.proxy(plugin, target)('value');
                return false;
            });

            return ret;
        }
    };

    /**
     * Plugin.
     *
     * @param {String} methodName Method name
     * @param {jQuery} node       Node
     *
     * @return {*}
     */
    $.fn.axisSublistField = function (methodName, node) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
