(function ($) {

    /**
     * Class TextField.
     *
     * @param {HTMLElement} target Target
     * @param {String}      title  Field title
     * @param {String}      value  Field value
     */
    function TextField(target, title, value) {
        this._target = TextField.setInstance(target, this);
        this._title = title;
        this._value = value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    TextField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    TextField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };


    /**
     * Gets the value.
     *
     * @return {String}
     */
    TextField.prototype.getValue = function () {
        return $('input', this._target).val();
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TextField.prototype._make = function () {
        this._target.addClass('input-text').attr('title', this._title).append(
            $('<input type="text" />').val(this._value)
        );
        
        this._makeActions();
    };
    
    /**
     * Makes actions.
     *
     * @return {Void}
     */
    TextField.prototype._makeActions = function () {
        var self = this;

        $('input', this._target).on('input', function () {
            self._target.trigger('input-change');
        });
    };

    var methods = {
        /**
         * Makes the component
         *
         * @param {jQuery} column Column node
         * @param {String} value  Cell value
         *
         * @return {jQuery}
         */
        'make': function (column, value) {
            var title = column.attr('title');
            var defValue = column.attr('default');

            if ($.spText('empty', value)) {
                value = defValue;
            }

            return this.each(function () {
                new TextField(this, title, value);
            });
        },

        /**
         * Gets field value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var field = TextField.getInstance(this);

                ret = field.getValue();
                return false;
            });

            return ret;
        }
    };


    $.fn.axisSublistFieldText = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
