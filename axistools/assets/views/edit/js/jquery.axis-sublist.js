(function ($) {

    /**
     * Class Sublist.
     *
     * @param {jQuery} node Sublist node
     */
    function Sublist(node) {
        /**
         * Current sublist.
         * @var {jQuery}
         */
        this._target = null;
        this._node = node;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    Sublist.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    Sublist.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Appends the component to the container.
     *
     * @param {jQuery} container Container
     *
     * @return {Void}
     */
    Sublist.prototype.appendTo = function (container) {
        container.append(this._target);
    };

    /**
     * Gets sublist value.
     *
     * @return {Array of Array of String}
     */
    Sublist.prototype.getValue = function () {
        var rows = [];

        $('.results tbody tr', this._target).each(function () {
            var cols = [];

            $('.field', this).each(function () {
               var value = $(this).axisSublistField('value');
               cols.push(value);
            });

            rows.push(cols);
        });

        return rows;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    Sublist.prototype._make = function () {
        var self = this;
        var title = $('title', this._node).text();
        var columns = $('column', this._node);
        var rows = $('row', this._node);

        this._target = $('<div class="sublist" />').append(function () {
            var target = $(this);

            target.append($('<div class="header" />').text(title))

            // appends the 'results' table
            target.append(
                $('<table class="results" />').append(
                    // tbody
                    $('<tbody />').append(function () {
                        var target = $(this);

                        rows.each(function () {
                            var row = $(this);
                            var items = $('item', row);

                            self._makeRow(target, items);
                        });
                    }),

                    // tfoot
                    $('<tfoot />').append(function () {
                        var target = $(this);

                        target.append(
                            $('<tr />').append(function () {
                                var target = $(this);

                                target.append(
                                    '<td><a class="add-row" href="#"><span>Add Row</span></a></td>'
                                );
                                target.append($('<td />').attr('colspan', columns.length + 2).text(' '));
                            })
                        );
                    })
                )
            );
        });

        this._makeActions();
        this._update();

        Sublist.setInstance(this._target, this);
    };

    /**
     * Makes a row.
     *
     * @param {jQuery} target  Table target
     * @param {jQuery} items   Cell nodes (not required)
     *
     * @return {Void}
     */
    Sublist.prototype._makeRow = function (target, items) {
        var self = this;
        var columns = $('column', this._node);

        target.append(
            $('<tr />').append(function () {
                var target = $(this);

                // appends cells
                columns.each(function (i) {
                    var column = $(this);
                    var value = items !== undefined? $(items[i]).text() : '';

                    target.append(
                        $('<td />').axisSublistField('append', column, value)
                    );
                });

                // appends buttons
                target.append(
                    '<td class="button">' +
                        '<a class="up" href="#" title="Move Up in List">' +
                            '<span>Move Up in List</span>' +
                        '</a>' +
                    '</td>'
                );
                target.append(
                    '<td class="button">' +
                        '<a class="down" href="#" title="Move Down in List">' +
                            '<span>Move Down in List</span>' +
                        '</a>' +
                    '</td>'
                );
                target.append(
                    '<td class="button">' +
                        '<a class="delete" href="#" title="Delete Row">' +
                            '<span>Delete Row</span>' +
                        '</a>' +
                    '</td>'
                );

                // button actions
                $('.up', target).click($.proxy(self._onUpClick, self));
                $('.down', target).click($.proxy(self._onDownClick, self));
                $('.delete', target).click($.proxy(self._onDeleteClick, self));
            })
        );
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    Sublist.prototype._makeActions = function () {
        var self = this;

        // appends a new row
        $('.add-row', this._target).click(function () {
            self._makeRow($('.results tbody', self._target));

            // places the focus in the first 'input text' of the last record
            $('.results tbody tr:last-child input[type="text"]', self._target).first().focus();
            
            // the component has changed
            self._target.trigger('input-change');

            self._update();
            this.blur();
            return false;
        });
    };

    /**
     * Updates user interface.
     *
     * @return {Void}
     */
    Sublist.prototype._update = function () {
        var self = this;
        
        $('.results tbody tr td.button a', this._target).removeClass('disabled');

        // marks the first 'up' button as disabled
        $('.results tbody tr:first-child td.button a.up', this._target).addClass('disabled');

        // marks the last 'down' button as disabled
        $('.results tbody tr:last-child td.button a.down', this._target).addClass('disabled');
        
        // fixes column width
        if ($('tr:first td', this._target).length < 5) {
            $('tr td:first-child', self._target).css('width', 'auto');
        }
    };

    /**
     * Moves record up.
     *
     * @return {Void}
     */
    Sublist.prototype._onUpClick = function (event) {
        var target = $(event.currentTarget);
        var currentRow = target.closest('tr');
        var prevRow = currentRow.prev();

        currentRow.insertBefore(prevRow);
        this._target.trigger('input-change');
        this._update();
        target.blur();
        return false;
    };

    /**
     * Moves record down.
     *
     * @return {Void}
     */
    Sublist.prototype._onDownClick = function (event) {
        var target = $(event.currentTarget);
        var currentRow = target.closest('tr');
        var nextRow = currentRow.next();

        currentRow.insertAfter(nextRow);
        this._target.trigger('input-change');
        this._update();
        target.blur();
        return false;
    };

    /**
     * Deletes record.
     *
     * @return {Void}
     */
    Sublist.prototype._onDeleteClick = function (event) {
        var target = $(event.currentTarget);
        var currentRow = target.closest('tr');

        currentRow.remove();
        this._target.trigger('input-change');
        this._update();
        target.blur();
        return false;
    };

    var methods = {
        /**
         * Appends a sublist.
         *
         * @param {jQuery} node Sublist node
         *
         * @return {jQuery}
         */
        'append': function (node) {
            return this.each(function () {
                var container = $(this);
                var sublist = new Sublist(node);

                sublist.appendTo(container);
            });
        },

        /**
         * Gets the sublist value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var sublist = Sublist.getInstance(this);

                ret = JSON.stringify(sublist.getValue());
                return false;
            });

            return ret;
        }
    };

    $.fn.axisSublist = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
