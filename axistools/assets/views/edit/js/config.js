/**
 * Extends the 'require' configuration file.
 */
$.require('config', $.extend(true, {}, $.require('config'), {
    libraries: {
        'jquery.axis-field': {
            sources: ['assets/views/edit/js/jquery.axis-field.js'],
            requires: ['jquery.axis', 'jquery.sp-text']
        },
        'jquery.axis-sublist-field-select': {
            sources: ['assets/views/edit/js/jquery.axis-sublist-field-select.js'],
            requires: ['jquery.sp-text']
        },
        'jquery.axis-sublist-field-text': {
            sources: ['assets/views/edit/js/jquery.axis-sublist-field-text.js'],
            requires: ['jquery.sp-text']
        },
        'jquery.axis-sublist-field': {
            sources: ['assets/views/edit/js/jquery.axis-sublist-field.js'],
            requires: ['jquery.axis', 'jquery.sp-text']
        },
        'jquery.axis-sublist': 'assets/views/edit/js/jquery.axis-sublist.js',
        // fields
        'checkboxgroup': {
            sources: ['assets/views/edit/fields/checkboxgroup/script.js'],
            requires: ['jquery.sp-text']
        },
        'color': {
            sources: ['assets/views/edit/fields/color/script.js'],
            requires: ['jquery.sp-text', 'jquery.axis-color-picker']
        },
        'date': {
            sources: ['assets/views/edit/fields/date/script.js'],
            requires: ['jquery.sp-text', 'jquery.axis-calendar', 'jquery.axis-date']
        },
        'file': {
            sources: ['assets/views/edit/fields/file/script.js'],
            requires: ['jquery.sp-text', 'jquery.axis', 'jquery.axis-tree-navigator']
        },
        'image': {
            sources: ['assets/views/edit/fields/image/script.js'],
            requires: ['jquery.sp-text', 'jquery.sp-uri', 'jquery.axis']
        },
        'password': {
            sources: ['assets/views/edit/fields/password/script.js'],
            requires: ['jquery.sp-text']
        },
        'readonly': {
            sources: ['assets/views/edit/fields/readonly/script.js'],
            requires: ['jquery.sp-text']
        },
        'select': {
            sources: ['assets/views/edit/fields/select/script.js'],
            requires: ['jquery.sp-text']
        },
        'text': 'assets/views/edit/fields/text/script.js',
        'textarea': {
            sources: ['assets/views/edit/fields/textarea/script.js'],
            requires: ['jquery.sp-text', 'jquery.sp-html', 'jquery.axis']
        },
        'time': {
            sources: ['assets/views/edit/fields/time/script.js'],
            requires: ['jquery.sp-text', 'jquery.axis-date', 'jquery.axis-item-selector']
        },
        'video': {
            sources: ['assets/views/edit/fields/video/script.js'],
            requires: ['jquery.sp-text', 'jquery.sp-uri', 'jquery.axis']
        },
    }
}));
