// TODO: perhaps it isn't necessary to load all field scripts
$.require(
    [
        'jquery.axis',
        'jquery.axis-field',
        'jquery.axis-sublist-field-select',
        'jquery.axis-sublist-field-text',
        'jquery.axis-sublist-field',
        'jquery.axis-sublist',
        'jquery.sp-modal',
        'jquery.sp-text',
        // fields
        'checkboxgroup',
        'color',
        'date',
        'file',
        'image',
        'password',
        'readonly',
        'select',
        'text',
        'textarea',
        'time',
        'video'
    ],
    function () {
        /**
         * Section name.
         * @var {String}
         */
        var _sectionName = '';

        /**
         * Table position.
         * @var {Number}
         */
        var _tablePos = 0;

        /**
         * Record id
         * @var {String}
         */
        var _recordId = 0;
        
        /**
         * Filter.
         * @var {Object}
         */
        var _filter = {};

        /**
         * Title.
         * @var {String}
         */
        var _title = '';

        /**
         * Fields.
         * @var {Array of jQuery}
         */
        var _fields = [];

        /**
         * Sublists.
         * @var {Array of jQuery}
         */
        var _sublists = [];

        /**
         * System directories.
         * @var {{file, image, video}}
         */
        var _directories = {};
        
        /**
         * List of available servers.
         * @var {Array of jQuery}
         */
        var _servers = [];
        
        /**
         * Before POST trigger.
         * @var {String}
         */
        var _beforePost = '';
        
        /**
         * After POST trigger.
         * @var {String}
         */
        var _afterPost = '';
        
        /**
         * Previous data (before POST).
         * @param {Object}
         */
        var _oldData = {};
        
        /**
         * Server value.
         * @var {String}
         */
        var _serverValue = '';

        /**
         * Have any of the input changed?
         * @var {Boolean}
         */
        var _hasChanged = false;

        /**
         * Main function.
         *
         * @return {Void}
         */
        function main() {
            var self = $.axis('self');
            _sectionName = self.data('section');
            _tablePos = parseInt(self.data('table'), 0);
            _recordId = $.spText('ifEmpty', self.data('id'), '');
            _filter = self.data('filter');
            
            // removes the preloader
            $('.jquery-modal-ui').removeClass('preloader');

            $('.close').click(function () {
                if (_hasChanged) {
                    $.axis('confirm', 'Do you want to discard changes?', function () {
                        self.remove();
                    });
                } else {
                    self.remove();
                }

                this.blur();
                return false;
            });

            $.axis(
                'get',
                'edit.php',
                {
                    'section': _sectionName,
                    'table': _tablePos,
                    'id': _recordId,
                    'filter': JSON.stringify(_filter)
                }
            ).done(function (xml) {
                _title = $('root > title', xml).text();
                _fields = $('root > fields > item', xml);
                _sublists = $('root > sublist', xml);
                _directories.file = $('directories file', xml).text();
                _directories.image = $('directories image', xml).text();
                _directories.video = $('directories video', xml).text();
                _servers = $('root > servers > option', xml);
                _serverValue = $('root > servers', xml).attr('value');
                _beforePost = $('root > triggers > before-post', xml).text();
                _afterPost = $('root > triggers > after-post', xml).text();

                make();
                _oldData = collectData();
            }).always(function () {
                $('.jquery-modal-ui').css('opacity', 1);
            });
        }

        /**
         * Creates the interface.
         *
         * @return {Void}
         */
        function make() {
            $('#title').attr('title', _title).text(_title);

            appendFields(function () {
                // sets the focus on the first focusable input (if proceed)
                $('.fields .axis-field:first input:text').select().focus();

                appendSublists();
                makeActions();
                $.axis('center', $('.jquery-modal-ui'));
            });

            // checks if any field has changed
            $('.axis-field, .sublist').on('input-change', function () {
                _hasChanged = true;
                updateFields();
            });
            
            update();
        }

        /**
         * Makes actions.
         *
         * @return {Void}
         */
        function makeActions() {
            $('.submit').click(function () {
                var target = $(this);
                var data = collectData();
                
                // saves data and, eventually, calls the 'Before POST' trigger
                if (!$.spText('empty', _beforePost)) {
                    $.spModal('post', _beforePost, {'old': _oldData, 'new': data}).done(function () {
                        save(data);
                    });
                } else {
                    save(data);
                }

                target.blur();
                return false;
            });
        }

        /**
         * Appends fields to the container.
         *
         * @return {Void}
         */
        function appendFields(onLoad) {

            function append(i) {
                if (i < _fields.length) {
                    var field = $(_fields[i]);
                    var fieldType = field.attr('type');
                    var fieldTitle = field.attr('title');
                    var fieldRequired = field.attr('required') == 'true';
                    var fieldValue = JSON.parse(field.attr('value'));
                    var fieldName = field.attr('name');
                    var fieldLinkName = field.attr('field-name');
                    var fieldLinkValues = JSON.parse(field.attr('field-values'));

                    $('.fields').axisField(
                        'append',
                        fieldType,
                        fieldTitle,
                        fieldRequired,
                        fieldValue,
                        fieldName,
                        fieldLinkName,
                        fieldLinkValues,
                        _directories,
                        function () {
                            append(i + 1);
                        }
                    );
                } else {
                    $.proxy(onLoad, this)();
                }
            }

            append(0);
        }

        /**
         * Appends sublists to the container.
         *
         * @return {Void}
         */
        function appendSublists() {
            _sublists.each(function () {
                $('.fields').axisSublist('append', $(this));
            });
        }
        
        /**
         * Updates fields.
         * 
         * @return {Void}
         */
        function updateFields() {
            var fields = $('.axis-field');
            
            fields.each(function () {
                var target = $(this);
                var targetName = $.trim(target.axisField('name'));
                var targetValue = JSON.parse(target.axisField('value'));
                
                if (!$.spText('empty', targetName)) {
                    fields.each(function () {
                        var field = $(this);
                        var linkName = $.trim(field.axisField('linkName'));
                        var linkValues = field.axisField('linkValues');
                        
                        if (!$.spText('empty', linkName) && targetName == linkName) {
                            field.toggle($.inArray(targetValue, linkValues) > -1);
                        }
                    });
                }
            });
        }
        
        /**
         * Updates user interface.
         * 
         * @return {Void}
         */
        function update() {
            // updates live-on-site section
            var liveOnSite = $('.live-on-site').hide();
            if (_servers.length > 0) {
                liveOnSite.show();
                $('#server').empty().append(function () {
                    var target = $(this);
                    _servers.each(function () {
                        var option = $(this);
                        target.append($('<option />').val(option.attr('value')).text(option.text()));
                    });
                }).val(_serverValue);
            }
            
            updateFields();
        }
        
        /**
         * Saves data and, eventually, calls the 'After POST' trigger.
         * 
         * @param {Object} data Data
         * 
         * @return {Void}
         */
        function save(data) {
            $.axis('post', 'edit.php', data).done(function (xml) {
                if (!$.spText('empty', _afterPost)) {
                    data.id = $('id', xml).text();
                    $.spModal('post', _afterPost, {'old': _oldData, 'new': data}).done(function () {
                       done(xml);
                    });
                } else {
                    done(xml);
                }
            });
        }
        
        /**
         * Closes the interface and, eventally, refreshes the main menu.
         * 
         * @return {Void}
         */
        function done(xml) {
            var self = $.axis('self');
            
            self.data('id', $('id', xml).text());
            self.data('refresh', $('refresh', xml).text() == 'true');
            self.trigger('done');
        }
        
        /**
         * Collects data.
         * 
         * @return {Object}
         */
        function collectData() {
            var ret = {
                'section': _sectionName,
                'table': _tablePos,
                'id': _recordId,
                'values': {},
                'sublistValues': [],
                'server': $('#server').val()
            };
            
            // collects data
            $('.axis-field').each(function () {
                var target = $(this);

                if ((target.axisField('hasChanged') || !target.axisField('isEmpty'))
                    && target.is(':visible')
                ) {
                    ret.values[target.index()] = target.axisField('value');
                }
            });

            // collects sublists data
            $('.sublist').each(function () {
                var target = $(this);
                var value = target.axisSublist('value');

                ret.sublistValues.push(value);
            });
            
            return ret;
        }

        main();
    }
);
