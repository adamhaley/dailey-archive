(function ($) {
    /**
     * Gets plugin name by type.
     *
     * @param {String} type Plugin type
     *
     * @return {String}
     */
    function getPluginName(type) {
        return 'axisField' + $.spText('capitalize', type);
    }

    /**
     * Gets plugin by type.
     *
     * @param {String} type Plugin type
     *
     * @return {Function|undefined}
     */
    function getPlugin(type) {
        return $.fn[getPluginName(type)];
    }


    /**
     * Loads script.
     *
     * @param {String}   type   Plugin type
     * @param {Function} onLoad Event handler
     *
     * @return {Void}
     */
    function loadScript(type, onLoad) {
        var script = $.axis(
            'absPath', $.spText('concat', '/', 'fields', type, 'script.js')
        );

        $.getScript(script).done(function () {
            var plugin = getPlugin(type);

            if ($.type(plugin) != 'function') {
                $.axis('error', 'The plugin $.' + getPluginName(type) + ' was not found:\n' + script);
            }

            onLoad();
        }).fail(function (jqXhr, error) {
            var errorTitle = jqXhr.status == 404? 'Script not found' : 'The script contains errors';
            $.axis('error', errorTitle + ':\n' + script);
        });
    }

    /**
     * Loads style.
     *
     * @param {String}   type   Plugin type
     * @param {Function} onLoad Event handler
     *
     * @return {Void}
     */
    function loadStyle(type, onLoad) {
        var styleSheet = $.axis(
            'absPath', $.spText('concat', '/', 'fields', type, 'style.css')
        );

        $.get(styleSheet).done(function (jqXhr) {
            $('<link type="text/css" rel="stylesheet" media="all" />').attr(
                'href', styleSheet
            ).appendTo('head');
            onLoad();
        }).fail(function () {
            $.axis('error', 'Stylesheet not found:\n' + styleSheet);
        });
    }

    /**
     * Loads script and style.
     *
     * @param {String}   type   Plugin type
     * @param {Function} onLoad 'load' event listener
     *
     * @return {Void}
     */
    function loadAssets(type, onLoad) {
        var plugin = getPlugin(type);

        if (plugin === undefined) {
            loadScript(type, function () {
                loadStyle(type, function () {
                    onLoad();
                });
            });
        } else {
            onLoad();
        }
    }

    var methods = {
        /**
         * Appends a field.
         *
         * @param {String}               type        Field type
         * @param {String}               title       Field title
         * @param {Boolean}              required    Is the field required?
         * @param {Object}               value       Field value
         * @param {String}               name        Field Name
         * @param {String}               linkName    Linked field name
         * @param {Array.<String>}       linkValues  Linked field values
         * @param {{file, image, video}} directories System directories
         * @param {Function}             onLoad      Load event handler (not required)
         *
         * @return {jQuery}
         */
        'append': function (
            type,
            title,
            required,
            value,
            name,
            linkName,
            linkValues,
            directories,
            onLoad
        ) {
            return this.each(function () {
                var container = $(this);

                loadAssets(type, function () {
                    var plugin = getPlugin(type);
                    var field = $('<div class="axis-field" />').data({
                        '__type__': type,
                        '__name__': name,
                        '__linkName__': linkName,
                        '__linkValues__': linkValues
                    });

                    container.append(field);

                    try {
                        $.proxy(plugin, field)('make', title, required, value, directories);
                    } catch (error) {
                        $.axis('error', '$.' + getPluginName(type) + ':\n' + error.message);
                    }

                    if (onLoad !== undefined) {
                        $.proxy(onLoad, container)(field);
                    }
                });
            });
        },

        /**
         * Gets or sets the field value.
         * 
         * @param {mixed} value Value (not required)
         *
         * @return {jQuery|String}
         */
        'value': function (value) {
            if (value === undefined) {
                var ret = '';
                var self = this;

                this.each(function () {
                    var target = $(this);
                    var type = target.data('__type__');
                    var plugin = getPlugin(type);

                    try {
                        ret = JSON.stringify($.proxy(plugin, target)('value'));
                    } catch (error) {
                        $.axis('error', '$.' + getPluginName(type) + ':\n' + error.message);
                    }

                    return false;
                });
                
                return ret;
            } else {
                return this.each(function () {
                    var target = $(this);
                    var type = target.data('__type__');
                    var plugin = getPlugin(type);
                    
                    $.proxy(plugin, target)('value', value);
                });
            }
        },
        
        /**
         * Is the current field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                var plugin = getPlugin(type);
                
                ret = $.proxy(plugin, target)('isEmpty');
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                var plugin = getPlugin(type);
                
                ret = $.proxy(plugin, target)('hasChanged');
                return false;
            });
            
            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            var self = this;
            
            return this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                var plugin = getPlugin(type);
                
                $.proxy(plugin, target)('reset');
            });
        },
        
        /**
         * Gets the field name.
         * 
         * @return {String}
         */
        'name': function () {
            var ret = '';
            var self = this;

            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                
                ret = target.data('__name__');
                return false;
            });

            return ret;
        },
        
        /**
         * Gets the linked field name.
         * 
         * @return {String}
         */
        'linkName': function () {
            var ret = '';
            var self = this;

            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                
                ret = target.data('__linkName__');
                return false;
            });

            return ret;
        },
        
        /**
         * Gets the linked field values.
         * 
         * @return {Array.<String>}
         */
        'linkValues': function () {
            var ret = '';
            var self = this;

            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');
                
                ret = target.data('__linkValues__');
                return false;
            });

            return ret;
        },

        /**
         * Is the field readonly?
         *
         * @return {Boolean}
         */
        'readonly': function () {
            var ret = false;

            this.each(function () {
                var target = $(this);
                var type = target.data('__type__');

                ret = (type == 'readonly');
                return false;
            });

            return ret;
        }
    };

    /**
     * Plugin.
     *
     * @param {String} methodName Method name
     * @param {jQuery} node       Node
     *
     * @return {*}
     */
    $.fn.axisField = function (methodName, node) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
