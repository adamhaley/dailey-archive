(function ($) {

    /**
     * Class SelectField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      obj      Field value
     */
    function SelectField(target, title, required, obj) {
        this._target = SelectField.setInstance(target, this);
        this._hasChanged = false;
        this._title = title;
        this._required = required;
        this._value = $.spText('ifEmpty', obj.value, '');
        this._originalValue = this._value;
        this._options = obj.options;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {SelectField}
     * @static
     */
    SelectField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    SelectField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };


    /**
     * Gets the value.
     *
     * @return {Mixed}
     */
    SelectField.prototype.getValue = function () {
        var val = $('select:last-child', this._target).val();
        return $.spText('empty', val)? '': JSON.parse(val);
    };
    
    /**
     * Sets the value.
     * 
     * @param {Mixed} value Value
     * 
     * @return {Void}
     */
    SelectField.prototype.setValue = function (value) {
        this._value = value;
        $('.inner-input', this._target).empty();
        this._update();
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    SelectField.prototype.reset = function () {
        this._value = this._originalValue;
        $('.inner-input', this._target).empty();
        this._update();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    SelectField.prototype.isEmpty = function () {
        var value = '' + this.getValue();
        return $.spText('empty', value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    SelectField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    SelectField.prototype._make = function () {
        var self = this;

        this._target.addClass('axis-field-select').append(
            $('<label />')
                .attr('title', this._title)
                .toggleClass('required', this._required)
                .append($('<span />').text(this._title)),
            $('<div class="input" />').append($('<div class="inner-input" />'))
        );
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        this._update();
    };

    /**
     * Updates the component.
     *
     * @param {Array} options (not required)
     *
     * @return {Void}
     */
    SelectField.prototype._update = function () {
        var self = this;

        function searchAndPrepend(value, options) {
            for (var i in options) {
                var option = options[i];

                if ($.isArray(option.value) && searchAndPrepend(value, option.value)
                    || option.value === value
                ) {
                    $('.inner-input', self._target).prepend(self._makeSelect(options, option.value));
                    return true;
                }
            }

            return false;
        };

        if (!searchAndPrepend(this._value, this._options)) {
            $('.inner-input', this._target).prepend(this._makeSelect(this._options));
        }
    };

    /**
     * Handles 'change' events.
     *
     * @param {Event} event Event
     *
     * @return {Void}
     */
    SelectField.prototype._onSelectChange = function (event) {
        var target = $(event.currentTarget);
        var val = target.val();
        var options = !$.spText('empty', val)? JSON.parse(target.val()): '';

        // removes all "selects" under the current "select"
        target.nextAll().remove();

        // appends a "subselect"
        if ($.isArray(options)) {
            $('.inner-input', this._target).append(this._makeSelect(options));
        }
    };

    /**
     * Makes a select with options
     *
     * @param {Array}  options Select options
     * @param {String} value   Value (default is '')
     *
     * @return {jQuery}
     */
    SelectField.prototype._makeSelect = function (options, value) {
        var self = this;

        if (value === undefined) {
            value = '';
        }

        return $('<select />')
            .append(function () {
                var target = $(this);

                target.append('<option value="" />');
                $.each(options, function () {
                    target.append(
                        $('<option />')
                            .attr('value', JSON.stringify(this.value))
                            .text(this.label)
                    )
                });

                target
                    .val(JSON.stringify(value))
                    .change($.proxy(self._onSelectChange, self));
            })
            .on('input', function () {
                self._target.trigger('input-change');
            });
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new SelectField(this, title, required, value);
            });
        },

        /**
         * Gets or sets the value.
         * 
         * @param {mixed} value Value (not required)
         *
         * @return {jQuery|String}
         */
        'value': function (value) {
            if (value === undefined) {
                var ret = '';

                this.each(function () {
                    var input = SelectField.getInstance(this);
                    ret = input.getValue();
                });

                return ret;
            } else {
                return this.each(function () {
                    var input = SelectField.getInstance(this);
                    input.setValue(value);
                });
            }
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = SelectField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = SelectField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = SelectField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldSelect = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
