(function ($) {

    /**
     * Class TextField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      value    Field value
     */
    function TextField(target, title, required, value) {
        this._target = TextField.setInstance(target, this);
        this._hasChanged = false;
        this._title = title;
        this._required = required;
        this._value = value;

        this._make();
        this.setValue(value);
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    TextField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    TextField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Gets the value.
     *
     * @return {String}
     */
    TextField.prototype.getValue = function () {
        return $('input', this._target).val();
    };

    /**
     * Sets the value.
     *
     * @param {String} value
     *
     * @return {Void}
     */
    TextField.prototype.setValue = function (value) {
        $('input', this._target).val(value);
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    TextField.prototype.reset = function () {
        this.setValue(this._value);
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    TextField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    TextField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TextField.prototype._make = function () {
        this._target.addClass('axis-field-text').append(
            $('<label />').text(this._title).toggleClass('required', this._required),
            $('<div class="input" />').append(
                $('<input type="text" />')
            )
        );

        this._makeActions();
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    TextField.prototype._makeActions = function () {
        var self = this;
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        $('input', this._target).on('input', function () {
            self._target.trigger('input-change');
        });
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new TextField(this, title, required, value);
            });
        },

        /**
         * Gets or sets the value.
         * 
         * @param {mixed} value Value (not required)
         *
         * @return {jQuery|String}
         */
        'value': function (value) {
            if (value === undefined) {
                var ret = '';

                this.each(function () {
                    var input = TextField.getInstance(this);
                    ret = input.getValue();
                });

                return ret;
            } else {
                return this.each(function () {
                    var input = TextField.getInstance(this);
                    input.setValue(value);
                });
            }
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = TextField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = TextField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = TextField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldText = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
