(function ($) {

    /**
     * Class SelectField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      obj      Field value
     */
    function TimeField(target, title, required, obj) {
        /**
         * Current field.
         * @var {jQuery}
         */
        this._target = TimeField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Title.
         * @var {String}
         */
        this._title = title;

        /**
         * Is the field required?
         * @var {Boolean}
         */
        this._required = required;

        /**
         * Date format.
         * @var {String}
         */
        this._format = obj.format;

        /**
         * Date.
         * @var {String}
         */
        this._value = $.axisDate('parse', $.spText('ifEmpty', obj.value, ''));
        
        /**
         * Original date.
         * @var {String}
         */
        this._originalValue = this._value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    TimeField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    TimeField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };
    
    /**
     * Gets the field value.
     *
     * @return {String}
     */
    TimeField.prototype.getValue = function () {
        return $.axisDate('format', 'H:i:s', this._value);
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    TimeField.prototype.reset = function () {
        this._target.empty();
        this._value = this._originalValue;
        this._make();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    TimeField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    TimeField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TimeField.prototype._make = function () {
        this._target.addClass('axis-field-time').append(
            // label
            $('<label />').toggleClass('required', this._required).text(this._title),

            // hours, minutes seconds
            '<a class="hours selector" href="#">Hours</a>',
            '<a class="minutes selector" href="#">Minutes</a>',
            '<a class="seconds selector" href="#">Seconds</a>',

            // text
            '<span class="text">&nbsp;</span>'
        );

        // hours, minutes and seconds selectors
        $('.hours', this._target).axisItemSelector(
            {numCols: 7, numItems: 24, iterator: this._normalize}
        );
        $('.minutes', this._target).axisItemSelector(
            {numCols: 7, numItems: 60, iterator: this._normalize}
        );
        $('.seconds', this._target).axisItemSelector(
            {numCols: 7, numItems: 60, iterator: this._normalize}
        );

        this._makeActions();
        this._update();
    };

    /**
     * Normalizes a number by adding zeros to the left.
     *
     * @param {Number} value
     *
     * @return {String}
     */
    TimeField.prototype._normalize = function (value) {
        return $.spText('normalize', value, 2);
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    TimeField.prototype._makeActions = function () {
        var self = this;
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        // shows or hides the time selector
        $('.selector', this._target).click(function () {
            var target = $(this);

            target.axisItemSelector('toggle');
            this.blur();
            return false;
        });

        // changes hour
        $('.hours', this._target).axisItemSelector('change', function () {
            var target = $(this);
            var hours = target.axisItemSelector('index');
            var minutes = 0;
            var seconds = 0;

            if (self._value !== null) {
                minutes = self._value.getMinutes();
                seconds = self._value.getSeconds();
            }

            self._value = new Date(0, 0, 0, hours, minutes, seconds, 0);
            self._target.trigger('input-change');
            self._update();
        });

        // changes minutes
        $('.minutes', this._target).axisItemSelector('change', function () {
            var target = $(this);
            var hours = 0;
            var minutes = target.axisItemSelector('index');
            var seconds = 0;

            if (self._value !== null) {
                hours = self._value.getHours();
                seconds = self._value.getSeconds();
            }

            self._value = new Date(0, 0, 0, hours, minutes, seconds, 0);
            self._target.trigger('input-change');
            self._update();
        });

        // changes seconds
        $('.seconds', this._target).axisItemSelector('change', function () {
            var target = $(this);
            var hours = 0;
            var minutes = 0;
            var seconds = target.axisItemSelector('index');

            if (self._value !== null) {
                hours = self._value.getHours();
                minutes = self._value.getMinutes();
            }

            self._value = new Date(0, 0, 0, hours, minutes, seconds, 0);
            self._target.trigger('input-change');
            self._update();
        });
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    TimeField.prototype._update = function () {
        var format = 'H:i:s';

        if (this._format == '12h') {
            var suffix = this._value !== null && this._value.getHours() < 12? '\\A\\M' : '\\P\\M';
            format = 'g:i:s '  + suffix;
        }

        $('.text', this._target).text(strDate = $.axisDate('format', format, this._value));
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}               title       Field title
         * @param {Boolean}              required    Is the field required
         * @param {String}               value       Field value
         * @param {{file, image, video}} directories System directories
         * @return {jQuery}
         */
        'make': function (title, required, value, directories) {
            return this.each(function () {
                new TimeField(this, title, required, value, directories);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = TimeField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = TimeField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = TimeField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = TimeField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldTime = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
