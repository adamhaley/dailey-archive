(function ($) {

    /**
     * Class TextareaField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {Object}      value    Field object
     */
    function VideoField(target, title, required, value) {
        /**
         * Target object.
         * @var {HTMLElement}
         */
        this._target = VideoField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Field title.
         * @var {String}
         */
        this._title = title;

        /**
         * Is the field required.
         * @var {Boolean}
         */
        this._required = required;

        /**
         * Video to play.
         * @var {{src: String, width: Number, height: Number, preset: String}}
         */
        this._video = value.video;

        /**
         * Poster.
         * @var {{src:String, width: Number, height: Number}}
         */
        this._poster = value.poster;

        /**
         * Thumbnail.
         * @var {{src:String, width: Number, height: Number}}
         */
        this._thumbnail = value.thumbnail;

        /**
         * Extra videos.
         * @var {Array of {src: String, width: Number, height: Number, preset: String}}
         */
        this._extraVideos = value.extraVideos;
        
        /**
         * Original values.
         */
        this._originalVideo = $.extend(true, {}, this._video);
        this._originalPoster = $.extend(true, {}, this._poster);
        this._originalThumbnail = $.extend(true, {}, this._thumbnail);
        this._originalExtraVideos = $.extend(true, {}, this._extraVideos);

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {ImageField}
     * @static
     */
    VideoField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    VideoField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Gets 'value' object.
     *
     * @return {
     *              {
     *                  video: {src: String, width: Number, height: Number, preset: String},
     *                  poster: {src: String, width: Number, height: Number},
     *                  thumbnail: {src: String, width: Number, height: Number},
     *                  extraVideos: Array of {
     *                      src: String, width: Number, height: Number, preset: String
     *                  }
     *              }
     *          }
     */
    VideoField.prototype.getValue = function () {
        return {
            'video': this._video,
            'poster': this._poster,
            'thumbnail': this._thumbnail,
            'extraVideos': this._extraVideos
        };
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    VideoField.prototype.reset = function () {
        this._video = $.extend(true, {}, this._originalVideo);
        this._poster = $.extend(true, {}, this._originalPoster);
        this._thumbnail = $.extend(true, {}, this._originalThumbnail);
        this._extraVideos = $.extend(true, {}, this._originalExtraVideos);
        this._update();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    VideoField.prototype.isEmpty = function () {
        var value = this.getValue();
        var video = value.video.src;
        var poster = value.poster.src;
        var thumbnail = value.thumbnail.src;
        var extraVideos = value.extraVideos;
        
        return $.spText('empty', video)
            && $.spText('empty', poster)
            && $.spText('empty', thumbnail)
            && extraVideos.length == 0;
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    VideoField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    VideoField.prototype._make = function () {
        var self = this;
        
        this._target.addClass('axis-field-video').append(
            $('<label />').text(this._title).toggleClass('required', this._required),
            $('<a class="edit" href="#" title="Edit Video"><span>Edit Video</span></a>'),
            $('<div class="content"><div class="inner" /></div>'),
            $('<a class="delete" href="#" title="Clear Video"><span>Clear Video</span></a>')
        );
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        // actions
        $('.edit', this._target).click($.proxy(this._editOnClick, this));
        $('.delete', this._target).click($.proxy(this._deleteOnClick, this));

        this._update();
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    VideoField.prototype._update = function () {
        var src = this._thumbnail.src;

        if (!$.spText('empty', src)) {
            var image = $.axis(
                'absPath',
                $.spText('rtrim', SERVER_URI, '/') + '/image-thumb.php?path=' + $.spUri('encode', src) + '&height=53'
            );

            $('.inner', this._target).empty().append($('<img />').attr('src', image));
        } else {
            $('.inner', this._target).empty();
        }
    };

    /**
     * Opens the text editor.
     *
     * @return {Void}
     */
    VideoField.prototype._editOnClick = function () {
        var self = this;

        var ui = $.axis('window', 'video-editor.html', {
            'video': this._video,
            'poster': this._poster,
            'thumbnail': this._thumbnail,
            'extraVideos': this._extraVideos
        });

        ui.on('done', function () {
            self._video = ui.data('video');
            self._poster = ui.data('poster');
            self._thumbnail = ui.data('thumbnail');
            self._extraVideos = ui.data('extraVideos');
            self._target.trigger('input-change');
            self._update();
            ui.remove();
        });

        this._target.blur();
        return false;
    };

    /**
     * Clears the video info.
     *
     * @return {Void}
     */
    VideoField.prototype._deleteOnClick = function () {
        this._video.src = '';
        this._poster.src = '';
        this._thumbnail.src = '';

        $.each(this._extraVideos, function () {
            this.src  ='';
        });

        this._update();
        this._target.trigger('input-change');
        this._target.blur();
        return false;
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new VideoField(this, title, required, value);
            });
        },

        /**
         * Gets the object value.
         *
         * @return {{video: String, poster: String, thumbnail: String, extraVideos: Array.<String>}}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = VideoField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = VideoField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = VideoField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = VideoField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldVideo = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
