(function ($) {

    /**
     * Class ImageField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {Object}      value    Field value
     */
    function ImageField(target, title, required, value) {
        this._target = ImageField.setInstance(target, this);
        this._hasChanged = false;
        this._title = title;
        this._required = required;
        this._cropWidth = parseInt(value.width, 10);
        this._cropHeight = parseInt(value.height, 10);
        this._watermark = value.watermark;
        this._value = $.spText('ifEmpty', value.value, '');
        this._hasOriginalImage = value.hasOriginalImage;
        this._originalImage = $.spText('ifEmpty', value.originalImage, '');
        this.setValue(this._value);

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {ImageField}
     * @static
     */
    ImageField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    ImageField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };
    
    /**
     * Gets original image.
     * 
     * @return {String}
     */
    ImageField.prototype.getOriginalImage = function () {
        return this._originalImage;
    };

    /**
     * Gets the value.
     *
     * @return {String}
     */
    ImageField.prototype.getValue = function () {
        return this._target.data('value');
    };

    /**
     * Sets the value.
     *
     * @param {String} value
     *
     * @return {Void}
     */
    ImageField.prototype.setValue = function (value) {
        this._target.data('value', value);
        this._update();
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    ImageField.prototype.reset = function () {
        this.setValue(this._value);
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    ImageField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    ImageField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    ImageField.prototype._make = function () {
        var self = this;
        
        this._target.addClass('axis-field-image').append(
            $('<label />').text(this._title).toggleClass('required', this._required),
            $('<a class="edit" href="#" title="Edit Image"><span>Edit Image</span></a>'),
            $('<div class="content"><div class="inner" /></div>'),
            $('<a class="delete" href="#" title="Clear Image"><span>Clear Image</span></a>')
        );
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        // actions
        $('.edit', this._target).click($.proxy(this._editOnClick, this));
        $('.delete', this._target).click($.proxy(this._deleteOnClick, this));

        this._update();
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    ImageField.prototype._update = function () {
        var src = this.getValue();

        if (!$.spText('empty', src)) {
            var image = $.axis(
                'absPath',
                $.spText('rtrim', SERVER_URI, '/') + '/image-thumb.php?path=' + $.spUri('encode', src) + '&height=53'
            );

            $('.inner', this._target).empty().append($('<img />').attr('src', image));
        } else {
            $('.inner', this._target).empty();
        }
    };

    /**
     * Opens the text editor.
     *
     * @return {Void}
     */
    ImageField.prototype._editOnClick = function () {
        var self = this;

        var ui = $.axis('window', 'image-editor.html', {
            image: this.getValue(),
            hasOriginalImage: this._hasOriginalImage,
            originalImage: this._originalImage,
            cropWidth: this._cropWidth,
            cropHeight: this._cropHeight,
            watermark: this._watermark
        });

        ui.on('done', function () {
            self.setValue(ui.data('value'));
            self._originalImage = ui.data('originalImage');
            self._target.trigger('input-change');
            ui.remove();
        });

        this._target.blur();
        return false;
    };

    /**
     * Clears the text.
     *
     * @return {Void}
     */
    ImageField.prototype._deleteOnClick = function () {
        this.setValue('');
        this._target.trigger('input-change');
        this._target.blur();
        return false;
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new ImageField(this, title, required, value);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = ImageField.getInstance(this);
                ret = {image: input.getValue(), originalImage: input.getOriginalImage()};
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = ImageField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = ImageField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = ImageField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldImage = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
