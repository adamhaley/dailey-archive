(function ($) {

    /**
     * Class SelectField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      obj      Field value
     */
    function DateField(target, title, required, obj) {
        /**
         * Current field.
         * @var {jQuery}
         */
        this._target = DateField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Title.
         * @var {String}
         */
        this._title = title;

        /**
         * Is the field required?
         * @var {Boolean}
         */
        this._required = required;

        /**
         * Calendar (see $.axisCalendar).
         * @var {Calendar}
         */
        this._calendar = null;

        /**
         * Date format.
         * @var {String}
         */
        this._format = obj.format;

        /**
         * Date.
         * @var {Date}
         */
        this._value = $.axisDate('parse', $.spText('ifEmpty', obj.value, ''));
        
        /**
         * Original value.
         * @var {Date}
         */
        this._originalValue = this._value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    DateField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    DateField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };
    
    /**
     * Gets the field value.
     *
     * @return {String}
     */
    DateField.prototype.getValue = function () {
        return $.axisDate('format', 'Y-m-d', this._value);
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    DateField.prototype.reset = function () {
        this._target.empty();
        this._calendar = null;
        this._value = this._originalValue;
        this._make();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    DateField.prototype.isEmpty = function () {
        return $.spText('empty', this.getValue());
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    DateField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    DateField.prototype._make = function () {
        this._target.addClass('axis-field-date').append(
            $('<label />').toggleClass('required', this._required).text(this._title),
            $('<a href="#" />')
        );

        this._makeActions();
        this._update();
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    DateField.prototype._makeActions = function () {
        var self = this;
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        $('a', this._target).click(function () {
            if (self._calendar == null) {
                self._calendar = $.axisCalendar($(this), 'left-left bottom-top');
            }

            // sets and gets the selected date
            self._calendar.setSelectedDate(self._value);
            $(self._calendar).one('done', function () {
                self._value = self._calendar.getSelectedDate();
                self._update();
                self._target.trigger('input-change');
            });

            this.blur();
            return false;
        });
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    DateField.prototype._update = function () {
        $('a', this._target).text($.axisDate('format', this._format, this._value));
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}               title       Field title
         * @param {Boolean}              required    Is the field required
         * @param {String}               value       Field value
         *
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new DateField(this, title, required, value);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = DateField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = DateField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = DateField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = DateField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldDate = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
