(function ($) {

    /**
     * Class PasswordField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      value    Field value
     */
    function PasswordField(target, title, required, value) {
        this._target = PasswordField.setInstance(target, this);
        this._title = title;
        this._required = required;
        this._hasChanged = false;
        this._value = value;
        this._originalValue = value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {PasswordField}
     * @static
     */
    PasswordField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    PasswordField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Gets the value.
     *
     * @return {Object}
     */
    PasswordField.prototype.getValue = function () {
        return {hasChanged: this._hasChanged, value: $('input', this._target).val()}
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    PasswordField.prototype.reset = function () {
        $('input', this._target).val(this._originalValue);
        this._hasChanged = false;
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    PasswordField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value.value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    PasswordField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    PasswordField.prototype._make = function () {
        this._target.addClass('axis-field-text').append(
            $('<label />').text(this._title).toggleClass('required', this._required),
            $('<div class="input" />').append('<input type="password" />')
        );
        this._makeActions();
        this._update();
    };

    /**
     * Makes the actions.
     *
     * @return {Void}
     */
    PasswordField.prototype._makeActions = function () {
        var self = this;

        $('input', this._target)
            .on('input', function () {
                self._hasChanged = true;
                self._target.trigger('input-change');
            });
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    PasswordField.prototype._update = function () {
        $('input', this._target).val(this._value);
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new PasswordField(this, title, required, value);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = PasswordField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = PasswordField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = PasswordField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = PasswordField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldPassword = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
