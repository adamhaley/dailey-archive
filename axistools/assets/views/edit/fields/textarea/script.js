(function ($) {

    /**
     * Class TextareaField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {Object}      obj      Object
     */
    function TextareaField(target, title, required, obj) {
        this._target = TextareaField.setInstance(target, this);
        this._hasChanged = false;
        this._title = title;
        this._required = required;
        this._isPlainText = obj.isPlainText;
        this._fontFamily = obj.fontFamily;
        this._fontSize = obj.fontSize;
        this._color = obj.color;
        this._value = $.spText('ifEmpty', obj.value, '');

        this._make();
        this.setValue(this._value);
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextareaField}
     * @static
     */
    TextareaField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    TextareaField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Gets the value.
     *
     * @return {String}
     */
    TextareaField.prototype.getValue = function () {
        return this._target.data('value');
    };

    /**
     * Sets the value.
     *
     * @param {String} value
     *
     * @return {Void}
     */
    TextareaField.prototype.setValue = function (value) {
        this._target.data('value', value);
        this._update();
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    TextareaField.prototype.reset = function () {
        this.setValue(this._value);
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    TextareaField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    TextareaField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TextareaField.prototype._make = function () {
        var self = this;
        
        this._target.addClass('axis-field-textarea').append(
            $('<label />').text(this._title).toggleClass('required', this._required),
            $('<a class="edit" href="#" title="Edit Text"><span>Edit Text</span></a>'),
            $('<div class="content"><div class="inner" /></div>'),
            $('<a class="delete" href="#" title="Clear Text"><span>Clear Text</span></a>')
        );
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        // actions
        $('.edit', this._target).click($.proxy(this._editOnClick, this));
        $('.delete', this._target).click($.proxy(this._deleteOnClick, this));

        this._update();
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    TextareaField.prototype._update = function () {
        if (this._isPlainText) {
            $('.inner', this._target).text(this.getValue());
        } else {
            $('.inner', this._target).html($.spHtml('plainText', this.getValue()));
        }
    };

    /**
     * Opens the text editor.
     *
     * @return {Void}
     */
    TextareaField.prototype._editOnClick = function () {
        var self = this;

        var ui = $.axis('window', 'textarea-editor.html', {
            value: this.getValue(),
            isPlainText: this._isPlainText,
            fontFamily: this._fontFamily,
            fontSize: this._fontSize,
            color: this._color
        });

        ui.on('done', function () {
            self.setValue(ui.data('value'));
            self._target.trigger('input-change');
            ui.remove();
        });

        this._target.blur();
        return false;
    };

    /**
     * Clears the text.
     *
     * @return {Void}
     */
    TextareaField.prototype._deleteOnClick = function () {
        this.setValue('');
        this._target.trigger('input-change');
        this._target.blur();
        return false;
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new TextareaField(this, title, required, value);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = TextareaField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = TextareaField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = TextareaField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = TextareaField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldTextarea = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
