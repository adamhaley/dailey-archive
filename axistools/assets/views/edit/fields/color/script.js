(function ($) {

    /**
     * Class ColorField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      value    Field value
     */
    function ColorField(target, title, required, value) {
        /**
         * Current field.
         * @var {jQuery}
         */
        this._target = ColorField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Title.
         * @var {String}
         */
        this._title = title;

        /**
         * Is the field required?
         * @var {Boolean}
         */
        this._required = required;

        /**
         * Value.
         * @var {String}
         */
        this._value = $.spText('empty', value)? '': value;
        
        /**
         * Original value.
         * @var {String}
         */
        this._originalValue = this._value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    ColorField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    ColorField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Gets the value.
     *
     * @return {String}
     */
    ColorField.prototype.getValue = function () {
        return this._value;
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    ColorField.prototype.reset = function () {
        this._target.empty();
        this._value = this._originalValue;
        this._make();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    ColorField.prototype.isEmpty = function () {
        return $.spText('empty', this._value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    ColorField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    ColorField.prototype._make = function () {
        var self = this;

        this._target.addClass('axis-field-color').append(
            // label
            $('<label />').toggleClass('required', this._required).text(this._title),

            // color button
            '<a class="color-btn" href="#" title="Select color">' +
                '<span class="color"><span class="shadow"><span>Selected Color</span></span></span>' +
            '</a>',

            // color text
            '<span class="color-text">&nbsp;</span>'
        );

        // initializes the color picker
        $('.color-btn', this._target).axisColorPicker();

        this._makeActions();
        this._update();
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    ColorField.prototype._makeActions = function () {
        var self = this;
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        $('.color-btn', this._target).axisColorPicker('change', function () {
            var target = $(this);
            var color = target.axisColorPicker('color');

            $('.color', target).css('background-color', color);
            self._value = $.spText('ltrim', color, '#');
            self._update();
            self._target.trigger('input-change');
        }).mousedown(function () {
            var target = $(this);

            target.axisColorPicker('toggle');
        });
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    ColorField.prototype._update = function () {
        var color = !$.spText('empty', this._value)
            ? '#' + $.spText('ltrim', this._value, '#')
            : 'transparent';

        $('.color-text', this._target).text(this._value);
        $('.color-btn', this._target)
            .axisColorPicker('color', color)
            .find('.color').css('background-color', color);
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new ColorField(this, title, required, value);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = ColorField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = ColorField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = ColorField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = ColorField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldColor = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
