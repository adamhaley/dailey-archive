(function ($) {

    /**
     * Class FileField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {Object}      value    Field value
     * @param {{file, image, video}} directories System directories
     */
    function FileField(target, title, required, value, directories) {
        /**
         * Current field.
         * @var {jQuery}
         */
        this._target = FileField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Title.
         * @var {String}
         */
        this._title = title;

        /**
         * Is the field required?
         * @var {Boolean}
         */
        this._required = required;

        /**
         * System directories.
         * @var {{file, image, video}}
         */
        this._directories = directories;

        /**
         * URL.
         * @var {String}
         */
        this._url = value.url;

        /**
         * URL text.
         * @var {String}
         */
        this._text = value.text;
        
        /**
         * Original URL.
         * @var {String}
         */
        this._originalUrl = this._url;
        
        /**
         * Original text.
         * @var {String}
         */
        this._originalText = this._text;

        /**
         * Is the selector visible?
         * @var {Boolean}
         */
        this._showSelector = value.showSelector;

        /**
         * Is the URL text visible?
         * @var {Boolean}
         */
        this._showText = value.showText;

        /**
         * Tree navigator. See $.axisTreeNavigator plugin.
         * @var {TreeNavigator}
         */
        this._treeNavigator;

        this._make();
    }


    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {FileField}
     * @static
     */
    FileField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    FileField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };
    
    /**
     * Gets the field value.
     *
     * @return {{url: String, text: String}}
     */
    FileField.prototype.getValue = function () {
        return {
            url: $('div.url input', this._target).val(),
            text: $('div.text input', this._target).val()
        };
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    FileField.prototype.reset = function () {
        this._url = this._originalUrl;
        this._text = this._originalText;
        this._update();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    FileField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value.url) && $.spText('empty', value.text);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    FileField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the field.
     *
     * @return {Void}
     */
    FileField.prototype._make = function () {
        var self = this;
        var textInputId = $.spText('uniqid', 'input_');
        var urlInputId = $.spText('uniqid', 'input_');

        this._target.addClass('axis-field-file').append(
            $('<div class="header" />').text(self._title),
            $('<div class="body" />').append(function () {
                var target = $(this);

                // header text
                if (self._showText) {
                    target.append(
                        $('<div class="text" />').toggleClass('required', self._required).append(
                            $('<label>Link text</label>').attr('for', textInputId),
                            $('<div class="input" />').append(
                                $('<input type="text" />').attr('id', textInputId)
                            )
                        )
                    );
                }

                // url, upload file and selector
                target.append(function () {
                    var target = $(this);
                    var table = $('<table />').appendTo(target);

                    table.append('<tr />').append(
                        // url
                        $('<td />').append(
                            $('<div class="url" />').toggleClass('required', self._required).append(
                                $('<label>Manual URL</label>').attr('for', urlInputId),
                                $('<div class="input" />').append(
                                    $('<input type="text" />').attr('id', urlInputId)
                                )
                            )
                        ),

                        // upload file button
                        $('<td />').append('or'),
                        $('<td />').append(
                            $('<div class="upload-file" />').append(
                                '<label title="Upload file">' +
                                    '<span>Upload file</span><input id="upload-file" type="file" />' +
                                '</label>'
                            )
                        )
                    );

                    // selector
                    if (self._showSelector) {
                        table.append(
                            $('<td />').append('or'),
                            $('<td />').append(
                                $('<div class="selector" />').append(
                                    '<a href="#" id="tree-navigator">Select an internal link</a>'
                                )
                            )
                        );

                        $('td', table).width('1%');
                        $('div.url', table).width(312);
                        $('div.url div.input', table).width(217);
                    }
                });
            })
        );

        this._makeActions();
        this._update();
    };

    /**
     * Makes the actions.
     *
     * @return {Void}
     */
    FileField.prototype._makeActions = function () {
        var self = this;
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        // uploads a file
        $('#upload-file', this._target).change(function () {
            $.axis('upload', 'file-upload.php', {file: this, dir: self._directories.file}).done(
                function (xml) {
                    var src = $('path', xml).text();

                    $.axis('garbage', 'purge', function () {
                        $.axis('garbage', 'add', src);

                        // file name
                        var pos = src.lastIndexOf('/');
                        $('div.text input', self._target).val(pos > -1? src.substring(pos + 1): src);

                        // file path
                        $('div.url input', self._target).val(src);
                    });
                    self._target.trigger('input-change');
                }
            );

            $(this).val('');
        });

        // opens the tree-navigator interface
        $('#tree-navigator', this._target).mousedown(function (event) {
            if (self._treeNavigator == null) {
                self._treeNavigator = $.axisTreeNavigator($(this), 'right-right bottom-top');
            }

            // updates URL and text
            $(self._treeNavigator).one('done', function () {
                self._url = self._treeNavigator.getSelectedUrl();
                self._text = self._treeNavigator.getSelectedText();
                self._update();
                self._target.trigger('input-change');
            });
        }).click(function () {
            this.blur();
            return false;
        });

        $('input', this._target).on('input', function () {
            self._target.trigger('input-change');
        });
    };

    /**
     * Updates the field.
     *
     * @return {Void}
     */
    FileField.prototype._update = function () {
        $('div.url input', this._target).val(this._url);
        $('div.text input', this._target).val(this._text);
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}               title       Field title
         * @param {Boolean}              required    Is the field required
         * @param {String}               value       Field value
         * @param {{file, image, video}} directories System directories
         * @return {jQuery}
         */
        'make': function (title, required, value, directories) {
            return this.each(function () {
                new FileField(this, title, required, value, directories);
            });
        },

        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            var ret = '';

            this.each(function () {
                var input = FileField.getInstance(this);
                ret = input.getValue();
            });

            return ret;
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = FileField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = FileField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = FileField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldFile = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
