(function ($) {

    /**
     * Class CheckboxgroupField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      obj      Field object
     */
    function CheckboxgroupField(target, title, required, obj) {
        /**
         * Current field.
         * @var {jQuery}
         */
        this._target = CheckboxgroupField.setInstance(target, this);
        
        /**
         * Has the field changed?
         * @var {Boolean}
         */
        this._hasChanged = false;

        /**
         * Title.
         * @var {String}
         */
        this._title = title;

        /**
         * Is the field required?
         * @var {Boolean}
         */
        this._required = required;

        /**
         * Checkbox value.
         * @var {Array}
         */
        this._value = $.spText('ifEmpty', obj.value, []);
        
        /**
         * Original value.
         * @var {Array}
         */
        this._originalValue = this._value;

        /**
         * Options.
         * @var {Array of string}
         */
        this._options = obj.options;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {TextField}
     * @static
     */
    CheckboxgroupField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    CheckboxgroupField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };

    /**
     * Gets the value.
     *
     * @return {Array}
     */
    CheckboxgroupField.prototype.getValue = function () {
        var ret = [];

        $('input', this._target).each(function () {
            var target = $(this);

            if (target.prop('checked')) {
                ret.push(target.val());
            }
        });

        return ret;
    };
    
    /**
     * Sets the value.
     * 
     * @param {Array} value Value
     * 
     * @return {Void}
     */
    CheckboxgroupField.prototype.setValue = function (value) {
        this._value = value;
        this._target.empty();
        this._make();
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    CheckboxgroupField.prototype.reset = function () {
        this._value = this._originalValue;
        this._target.empty();
        this._make();
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    CheckboxgroupField.prototype.isEmpty = function () {
        var value = this.getValue();
        return value.length == 0;
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    CheckboxgroupField.prototype.hasChanged = function () {
        return this._hasChanged;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    CheckboxgroupField.prototype._make = function () {
        var self = this;

        this._target.addClass('axis-field-checkboxgroup').append(
            // label
            $('<div class="label" />').append(
                $('<label />')
                    .text(self._title)
                    .toggleClass('required', self._required)
                    .attr('title', self._title)
            ),

            // checkboxes
            $('<div class="checkboxes" />').append(
                $('<div class="inner" />').append(function () {
                    var target = $(this);

                    for (var i in self._options) {
                        var option = self._options[i];
                        var checked = !(self._value.indexOf(option.value) < 0);
                        var inputId = $.spText('uniqid', 'input_');

                        target.append(
                            $('<div class="checkbox" />').append(
                                $('<input type="checkbox" />')
                                    .val(option.value)
                                    .prop('checked', checked)
                                    .attr('id', inputId),
                                $('<label />').text(option.label)
                                    .attr('for', inputId)
                            )
                        );
                    }
                })
            )
        );

        this._makeActions();
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    CheckboxgroupField.prototype._makeActions = function () {
        var self = this;
        
        this._target.on('input-change', function () {
            self._hasChanged = true;
        });

        $('input', this._target).on('click', function () {
            self._target.trigger('input-change');
        });
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new CheckboxgroupField(this, title, required, value);
            });
        },

        /**
         * Gets or sets the value.
         * 
         * @param {mixed} value Value (not required)
         *
         * @return {jQuery|String}
         */
        'value': function (value) {
            if (value === undefined) {
                var ret = '';

                this.each(function () {
                    var input = CheckboxgroupField.getInstance(this);
                    ret = input.getValue();
                });

                return ret;
            } else {
                return this.each(function () {
                    var input = CheckboxgroupField.getInstance(this);
                    input.setValue(value);
                });
            }
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = CheckboxgroupField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = CheckboxgroupField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = CheckboxgroupField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldCheckboxgroup = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
