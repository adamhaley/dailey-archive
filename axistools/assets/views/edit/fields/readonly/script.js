/**
 * ReadonlyField is an special field. It is used to only show 'readonly' fields in the Record Editor.
 * This field is not editable.
 */
(function ($) {

    /**
     * Class ReadonlyField.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      title    Field title
     * @param {Boolean}     required Is field required?
     * @param {String}      value    Field value
     */
    function ReadonlyField(target, title, required, value) {
        this._target = ReadonlyField.setInstance(target, this);
        this._title = title;
        this._value = value;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {ReadonlyField}
     * @static
     */
    ReadonlyField.getInstance = function (target) {
        return $(target).data('instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {jQuery}
     * @static
     */
    ReadonlyField.setInstance = function (target, instance) {
        return $(target).data('instance', instance);
    };
    
    /**
     * Gets the value.
     *
     * @return {Object}
     */
    ReadonlyField.prototype.getValue = function () {
        return this._value;
    };
    
    /**
     * Resets the component.
     * 
     * @return {Void}
     */
    ReadonlyField.prototype.reset = function () {
        // do nothing
    };
    
    /**
     * Is the field empty?
     * 
     * @return {Boolean}
     */
    ReadonlyField.prototype.isEmpty = function () {
        var value = this.getValue();
        return $.spText('empty', value);
    };
    
    /**
     * Has the field changed?
     * 
     * @return {Boolean}
     */
    ReadonlyField.prototype.hasChanged = function () {
        return false;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    ReadonlyField.prototype._make = function () {
        this._target.addClass('axis-field-readonly').append(
            $('<label />').text(this._title).toggleClass('required', this._required),
            $('<span />').text(this._value)
        );
    };

    var methods = {
        /**
         * Makes the component.
         *
         * @param {String}  title    Field title
         * @param {Boolean} required Is the field required
         * @param {String}  value    Field value
         * @return {jQuery}
         */
        'make': function (title, required, value) {
            return this.each(function () {
                new ReadonlyField(this, title, required, value);
            });
        },
        
        /**
         * Gets the value.
         *
         * @return {String}
         */
        'value': function () {
            return '';
        },
        
        /**
         * Resets the field.
         * 
         * @return {jQuery}
         */
        'reset': function () {
            return this.each(function () {
                var input = ReadonlyField.getInstance(this);
                input.reset();
            });
        },
        
        /**
         * Is the field empty?
         * 
         * @return {Boolean}
         */
        'isEmpty': function () {
            var ret = false;
            
            this.each(function () {
                var input = ReadonlyField.getInstance(this);
                ret = input.isEmpty();
                return false;
            });
            
            return ret;
        },
        
        /**
         * Has the field changed?
         * 
         * @return {Boolean}
         */
        'hasChanged': function () {
            var ret = false;
            
            this.each(function () {
                var input = ReadonlyField.getInstance(this);
                ret = input.hasChanged();
                return false;
            });
            
            return ret;
        }
    };

    $.fn.axisFieldReadonly = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
