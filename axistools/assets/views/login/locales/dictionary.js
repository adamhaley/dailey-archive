var dictionary = {
    en: {
        requestPasswordTitle: 'Site Admin',
        requestPasswordLink: 'Forgot your password?',
        formTitle: 'Login Security Check',
        labelUsername: 'User Name',
        labelPassword: 'Password',
        labelRemember: 'Remember me on this computer'
    }
};
