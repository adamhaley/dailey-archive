/**
 * Extends the 'require' configuration file.
 */
$.require('config', $.extend(true, {}, $.require('config'), {
    libraries: {
        'jquery.axis-preload-images': 'assets/views/login/js/jquery.axis-preload-images.js'
    }
}));
