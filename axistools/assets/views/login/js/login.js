$.require(
    [
        'jquery.sp-text',
        'jquery.sp-request',
        'jquery.sp-html',
        'jquery.axis',
        'jquery.axis-preload-images'
    ],
    function () {
        /**
         * Main function.
         *
         * @return {Void}
         */
        function main() {
            var cssFile = 'assets/common/css/general.css';
            var loginForm = $('#login');
            var submitButton = $('#submit');

            // parameters
            var redirect = $.spRequest('get', 'redirect', './');
            
            // removes the preloader
            $('.main').removeClass('preloader');

            centerContainer();

            // disables the form
            loginForm.submit(function () {return false;});
            submitButton.attr('disabled', 'disabled');

            $.axisPreloadImages(cssFile, function () {
                var username = $('#username');
                var password = $('#password');
                var remember = $('#remember');

                $.axis('get', 'login.php').done(function (xml) {
                    loadBackgroundImage(xml);
                    loadLogoImage(xml);
                    
                    // populates input fields
                    username.val($('username', xml).text());
                    remember.attr('checked', $('remember', xml).text() == 'true');

                    // places the focus in the username or the password
                    if ($.spText('empty', username.val())) {
                        username.focus().select();
                    } else {
                        password.focus().select();
                    }

                    // enables form
                    submitButton.removeAttr('disabled');
                    loginForm.submit(function () {
                        var usernameVal = $.trim(username.val());
                        var passwordVal = $.trim(password.val());
                        var rememberVal = $.trim(
                            remember.is(':checked')? remember.val() : 'false'
                        );
                        var data = {
                            username: usernameVal,
                            password: passwordVal,
                            remember: rememberVal
                        }

                        $.axis('post', 'login.php?compile-sitemap=true', data
                        ).done(function (xml) {
                            $.axis('redirect', redirect);
                        }).fail(function (xml) {
                            password.val('');
                            username.focus().select();
                        });

                        return false;
                    });
                });
            });

            $('#requestPasswordLink').click(function () {
                var target = $(this);

                $.axis('redirect', 'request-password.html');
                target.blur();
                return false;
            });
        }
        
        /**
         * Centers the main container horizontally.
         * 
         * @return {Void}
         */
        function centerContainer() {
            var win = $(window);
            var main = $('.main');
            var winWidth = win.width() / $.axis('zoom');
            var mainWidth = Math.max(960, 0.95 * winWidth);
            var marginLeft = Math.max(0, (winWidth - mainWidth) / 2);

            main.width(mainWidth).css('margin-left', marginLeft);
            $(window).one('resize', centerContainer);
        }

        /**
         * Loads the background image.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function loadBackgroundImage(xml) {
            var background = $('background', xml);
            var image = background.attr('image');
            var color = background.attr('color');
            
            $('html').css(
                'background',
                color + ' url(' + $.spHtml('encode', image) + ') repeat left top'
            );
        }

        /**
         * Loads the logo image.
         *
         * @param {Document} xml XML document
         *
         * @return {Void}
         */
        function loadLogoImage(xml) {
            var image = $('logo', xml).attr('image');
            var logo = $('.main .header .logo');

            logo.append('<img src="' + $.spHtml('encode', image) + '" alt="" />');
        }

        main();
    }
);
