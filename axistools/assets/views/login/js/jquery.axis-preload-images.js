/**
 * <p>jQuery.preloadImages plugin.</p>
 *
 * <p>Requires jQuery >= 1.7</p>
 *
 * @namespace
 * @name      preloadImages
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw.github.com/soloproyectos/common-js-lib/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/common-js-lib
 */
(function ($) {

    /**
     * Loads all images from a CSS file.
     *
     * Example:
     * ```JavaScript
     * $.preloadImages('css/my-style.css', function () {
     *     console.log('Images are loaded');
     * });
     * ```
     *
     * @param {String}   cssFile CSS file path
     * @param {Function} onReady This function is called when all images are loaded
     *
     * @return   {jQuery}
     * @name     preloadImages#main
     * @function
     */
    $.axisPreloadImages = function (cssFile, onReady) {
        var pos = cssFile.lastIndexOf('/');
        var cssFolder = pos < 0? '' : cssFile.substr(0, pos + 1);

        $.get(cssFile, function(data) {
            var matches = data.match(/url\([^)]+/gmi);
            var count = matches.length;

            // loads all images
            var len = matches.length;
            for (var i = 0; i < len; i++) {
                var s = matches[i];
                var pos = s.indexOf('(');
                if (pos >= 0) {
                    var image = cssFolder + s.substr(pos + 1);
                    image = image.replace(/^['"]+/, '').replace(/['"]+$/, '');

                    $('<img />').attr('src', image).load(function() {
                        count--;
                    }).error(function() {
                        count--;
                    });
                }
            }

            // calls the callback function if all images have been loaded
            // or if the process takes more than 15 seconds.
            var t0 = (new Date()).getTime();
            var interval = setInterval(function() {
                var t1 = (new Date()).getTime();
                if ( (count <= 0) || (t1 - t0 > 15000) ) {
                    $.proxy(onReady, window)();
                    clearInterval(interval);
                }
            }, 10);
        });

        return this;
    };
})(jQuery);
