$.require(['jquery.sp-text', 'jquery.sp-html', 'jquery.axis'], function () {
    /**
     * Main function.
     */
    function main() {
        var email = $('#email');
        var questionId = $('#question_id');
        var answer = $('#answer');
        var submitRequest;
        
        // removes the preloader
        $('.main').removeClass('preloader');

        i18n();
        centerContainer();

        $.axis('get', 'request-password.php').done(function (xml) {
            logoImage(xml);
            backgroundImage(xml);
        
            // fills the select field
            var options = $('question', xml);
            options.each(function () {
                var target = $(this);
                var option = $('<option />').val(target.attr('id')).text(target.text());
                questionId.append(option);
            });

            email.focus();

            // handles submit request
            $('#request-form').submit(submitRequest = function () {
                var requestForm = $('#request-form');
                var submitButton = $('#submit');
                var emailVal = $.trim(email.val());
                var questionIdVal = $.trim(questionId.val());
                var answerVal = $.trim(answer.val());

                // required fields
                if ($.spText('empty', emailVal)
                    || $.spText('empty', questionIdVal)
                    || $.spText('empty', answerVal))
                {
                    $.axis('alert', 'All fields are required', function () {
                        email.focus();
                    });
                    return false;
                }

                // disable form
                submitButton.attr('disabled', 'disabled');
                requestForm.off('submit', submitRequest);
                requestForm.submit(disableForm);

                $.axis(
                    'post',
                    'request-password.php',
                    {email: emailVal, question_id: questionIdVal, answer: answerVal}
                ).done(function (xml) {
                    var message = $('message', xml).text();

                    // success
                    $.axis('alert', message, function () {
                        $.axis('redirect', 'login');
                    });
                }).always(function () {
                    // re-enables form
                    requestForm.on('submit', submitRequest);
                    submitButton.removeAttr('disabled');
                });

                return false;
            });

            $('#goBackLink').click(function () {
                var target = $(this);
                $.axis('redirect', 'login.html');
                target.blur();
                return false;
            });
        });
    }
    
    /**
     * Centers the main container horizontally.
     * 
     * @return {Void}
     */
    function centerContainer() {
        var win = $(window);
        var main = $('.main');
        var winWidth = win.width() / $.axis('zoom');
        var mainWidth = Math.max(960, 0.95 * winWidth);
        var marginLeft = Math.max(0, (winWidth - mainWidth) / 2);

        main.width(mainWidth).css('margin-left', marginLeft);
        $(window).one('resize', centerContainer);
    }

    /**
     * Internationalization.
     *
     * @return {Void}
     */
    function i18n() {
        $('#goBackTitle').text('Site Admin');
        $('#goBackLink').text('Go Back');
        $('#formTitle').text('Forgot Password');
        $('#formText').html(
            'Enter your email address and answer the security question.<br />' +
            'Your username and password will be sent to you.'
        );
        $('#labelEmail').text('Email:');
        $('#labelSecurityQuestion').text('Security Question:');
        $('#labelAnswer').text('Security Answer:');
        $('#selectQuestion').text('Select an option');
        $('#submit').val('Submit');
    }

    /**
     * Sets the background image.
     *
     * @param {Document} xml XML document
     *
     * @return {Void}
     */
    function backgroundImage(xml) {
        var background = $('background', xml);
        var image = background.attr('image');
        var color = background.attr('color');
        
        $('html').css(
            'background',
            color + ' url(' + $.spHtml('encode', image) + ') repeat left top'
        );
    }

    /**
     * Sets the logo image.
     *
     * @param {Document} xml XML document
     *
     * @return {Void}
     */
    function logoImage(xml) {
        var image = $('logo', xml).attr('image');
        var logo = $('.main .header .logo');

        logo.append('<img src="' + $.spHtml('encode', image) + '" alt="" />');
    }

    /**
     * Disables the form.
     *
     * @return {Boolean}
     */
    function disableForm() {
        return false;
    }

    main();
});
