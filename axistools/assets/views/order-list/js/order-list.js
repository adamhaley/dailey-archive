$.require(
    [
        'html2canvas',
        'jquery.sp-uri',
        'jquery.sp-dragger',
        'jquery.sp-slider',
        'jquery.axis'
    ],
    function () {
        /**
         * Maximum container height.
         * @var {Number}
         */
        var _maxHeight = 600;

        /**
         * Section name.
         * @var {String}
         */
        var _sectionName = '';

        /**
         * Table position.
         * @var {Number}
         */
        var _tablePos = 0;

        /**
         * Title.
         * @var {String}
         */
        var _title = '';

        /**
         * Columns.
         * @var {Array of jQuery}
         */
        var _columns = [];

        /**
         * Records.
         * @var {Array of jQuery}
         */
        var _rows = [];

        /**
         * Has the rows changed?
         * @var {Boolean}
         */
        var _hasChanged = false;

        function main() {
            var self = $.axis('self');
            var container = $('.list');
            var table = $('table', container);

            _sectionName = self.data('section');
            _tablePos = parseInt(self.data('table'), 10);
            
            // removes the preloader
            $('.jquery-modal-ui').removeClass('preloader');

            // closes the interface
            $('.close').click(function () {
                if (_hasChanged) {
                    $.axis('confirm', 'Do you want to discard changes?', function () {
                        self.remove();
                    });
                } else {
                    self.remove();
                }
                
                this.blur();
                return false;
            });

            $.axis('get', 'order-list.php', {'section': _sectionName, 'table': _tablePos})
                .done(function (xml) {
                    _title = $('title', xml).text();
                    _columns = $('column', xml);
                    _rows = $('row', xml);

                    make();
                })
                .always(function () {
                    $('.jquery-modal-ui').css('opacity', 1);
                });
        }

        /**
         * Makes the interface.
         *
         * @return {Void}
         */
        function make() {
            var container = $('.list');
            var table = $('table', container);

            $('#title').text(_title);

            // disables text selection
            $('.list').spTextSelection('disable');

            // makes the table body
            $(_rows).each(function () {
                var row = $(this);
                var items = $('item', row);
                var tr = $('<tr />').data('id', row.attr('id'));

                items.each(function (index) {
                    var column = _columns.eq(index);
                    var text = $(this).text();

                    tr.append(
                        $('<td />').append(function () {
                            var div = $('<div />').addClass('cell-content');

                            if (column.attr('type') == 'image') {
                                // appends an image
                                if (!$.spText('empty', text)) {
                                    var src = $.axis(
                                        'absPath',
                                        $.spText('rtrim', SERVER_URI, '/') + '/image-thumb.php?path='
                                        + $.spUri('encode', text)
                                        + '&height=53'
                                    );

                                    div.append($('<img />').attr('src', src));
                                }
                            } else {
                                // appends text
                                div.text($.spText('truncate', $.spHtml('plainText', text), 450));
                            }

                            return div;
                        })
                    );
                });

                table.append(tr);
            });

            // max-height
            container.css('max-height', _maxHeight);
            if (container.height() < _maxHeight) {
                container.css('overflow-y', 'hidden');
            }

            // initializes the slider
            container.spSlider({
                type: 'vertical'
            });

            // initializes the dragger for each row (rows are 'draggable')
            $('tr', table).spDragger({
                snapshotOpacity: 0,
                bounds: {
                    x: container.offset().left + 2,
                    y: container.offset().top + 2,
                    width: table.width(),
                    height: table.height()
                }
            })

            $.axis('center', $('.jquery-modal-ui'));
            makeActions();
        }

        /**
         * Makes actions.
         *
         * @return {Void}
         */
        function makeActions() {
            var self = $.axis('self');
            var container = $('.list');
            var table = $('table', container);
            var guide = $('.guide', container);
            var moveRowAfter = false;
            var isDraggerStarted = false;

            // hides or shows the horizontal guide
            container
                .on('mouseleave', function () {
                    guide.hide();
                })
                .on('mouseenter', function () {
                    guide.toggle(isDraggerStarted);
                });

            // handles 'onDrop' event
            $('tr', table).spDragger('onDrop', function (event) {
                var target = $(this);
                var draggedTarget = $(event.draggedTarget);
                var offset = draggedTarget.offset();
                var rowY = offset.top + draggedTarget.height() / 2;

                if (Math.abs(rowY - event.pageY) > draggedTarget.height()) {
                    // moves elements
                    if (moveRowAfter) {
                        draggedTarget.insertAfter(target);
                    } else {
                        draggedTarget.insertBefore(target);
                    }
                    
                    _hasChanged = true;
                }
            });

            // starts the slider
            $('tr', table).on('mousedown', function (event) {
                isDraggerStarted = true;
                guide.show();
                container.spSlider('start');
            });

            // stops the slider
            $(window).on('mouseup', function () {
                isDraggerStarted = false;
                guide.hide();
                container.spSlider('stop');
            });

            // places the horizontal guide
            $('tr', table).on('mousemove', function (event) {
                var target = $(this);
                var tableOffset = table.offset();
                var offset = target.offset();
                var guideTop = offset.top - tableOffset.top;

                moveRowAfter = event.pageY > offset.top + target.height() / 2;
                if (moveRowAfter) {
                    guideTop += target.height() - guide.height();
                }

                guide.css('top', guideTop);
            });
            
            $('.submit').click(function () {
                var target = $(this);
                
                // gets the record ids
                var ids = $.map($('tr', table), function (elem) {
                    return $(elem).data('id');
                });
                
                // rearrange elements
                $.axis(
                    'post',
                    'order-list.php',
                    {
                        'section': _sectionName,
                        'table': _tablePos,
                        'action': 'sort',
                        'ids': ids
                    }
                ).done(function () {
                    self.trigger('done', _hasChanged);
                    self.remove();
                });
                
                target.blur();
                return false;
            });
        }

        main();
    }
);
