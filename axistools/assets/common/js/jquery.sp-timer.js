/**
 * jQuery.spTimer - Timer plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      spTimer
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Timer constructor.
     *
     * Example:
     * ```JavaScript
     * // starts the timer immediately and repeat every second
     * $.spTimer(0, function() {
     *      this.setDelay(1000);
     *      console.log('tick');
     * }).start();
     * ```
     *
     * @param {Number}   delay    Delay in milliseconds
     * @param {Function} callback Callback function
     *
     * @return   {Void}
     * @name     spTimer#Timer
     * @function
     */
    function Timer(delay, callback) {
        /**
         * Delay in milliseconds.
         * @var {Number}
         */
        this._delay = delay;

        /**
         * Callback function.
         * @var {Function}
         */
        this._callback = callback;

        /**
         * The number of times the Timer is going to fire.
         * @var {Number}
         */
        this._maxCount = Number.POSITIVE_INFINITY;

        /**
         * The number of times the Timer has fired.
         * @ar {Number}
         */
        this._count = 0;

        /**
         * The time the Timer has been paused.
         * @var {Number}
         */
        this._pausedTime = 0;

        /**
         * The current time.
         * @var {Number}
         */
        this._currentTime = (new Date()).getTime();

        /**
         * The time elapsed from the last event or since the timer is running.
         * @var {Number}
         */
        this._lastTime = this._currentTime;

        /**
         * The interval id.
         * @var {Number}
         */
        this._interval = null;

        /**
         * The state of the timer: 'stop', 'play' or 'pause'.
         */
        this._state = Timer.STOP;
    }

    /**
     * Timer states.
     */
    Timer.STOP = 'stop';
    Timer.PLAY = 'play';
    Timer.PAUSE = 'pause';

    /**
     * Gets the number of times the Timer has fired.
     *
     * @return  {Number}
     * @name     spTimer#Timer::getCount
     * @function
     */
    Timer.prototype.getCount = function () {
        return this._count;
    };

    /**
     * Gets the delay.
     *
     * @return   {Number}
     * @name     spTimer#Timer::getDelay
     * @function
     */
    Timer.prototype.getDelay = function () {
        return this._delay;
    };

    /**
     * Sets the delay.
     *
     * Example:
     * ```JavaScript
     * // starts immediately and then wait for 1 second
     * var timer = $.spTimer(0, function() {
     *      this.setDelay(1000);
     *      console.log('tick');
     * }).start();
     * ```
     *
     * @param {Number} delay Delay in milliseconds
     *
     * @return   {Timer}
     * @name     spTimer#Timer::setDelay
     * @function
     */
    Timer.prototype.setDelay = function (delay) {
        this._delay = delay;

        if (this.isRunning()) {
            this.play();
        }

        return this;
    };

    /**
     * Gets the state.
     *
     * This function returns any of the following values: 'stop', 'play' or
     * 'pause'.
     *
     * @return   {String}
     * @name     spTimer#getState
     * @function
     */
    Timer.prototype.getState = function () {
        return this._state;
    };

    /**
     * Is the timer paused?
     *
     * @return   {Boolean}
     * @name     spTimer#Timer::isPaused
     * @function
     */
    Timer.prototype.isPaused = function () {
        return this.getState() == Timer.PAUSE;
    };

    /**
     * Is the timer running?
     *
     * @return   {Boolean}
     * @name     spTimer#isRunning
     * @function
     */
    Timer.prototype.isRunning = function () {
        return this.getState() == Timer.PLAY;
    };

    /**
     * Is the timer stopped?
     *
     * @return   {Boolean}
     * @name     spTimer#Timer::isStopped
     * @function
     */
    Timer.prototype.isStopped = function () {
        return this.getState() == Timer.STOP;
    };

    /**
     * Gets the current time, not counting the time it has been paused.
     *
     * @return   {Number}
     * @name     spTimer#Timer::getCurrentTime
     * @function
     */
    Timer.prototype.getCurrentTime = function () {
        return this.isRunning()
            ? (new Date()).getTime() - this._pausedTime
            : this._currentTime;
    };

    /**
     * Gets the remaining time for the next event.
     *
     * @return   {Number}
     * @name     spTimer#Timer::getRemainingTime
     * @function
     */
    Timer.prototype.getRemainingTime = function () {
        var t = this.getElapsedTime();
        var remainingTime = this._delay > 0
            ? Math.ceil((t + 1) / this._delay) * this._delay - t
            : 0;

        return remainingTime;
    };

    /**
     * Gets the elapsed time from the latest event.
     *
     * @return   {Number}
     * @name     spTimer#Timer::getElapsedTime
     * @function
     */
    Timer.prototype.getElapsedTime = function () {
        return this.getCurrentTime() - this._lastTime;
    };

    /**
     * Starts the timer.
     *
     * Example:
     * ```JavaScript
     * $.spTimer(1000, function () {
     *      console.log('tick');
     * }).start();
     * ```
     *
     * @param {Number} n Number of times the Timer is going to fire
     *
     * @return   {Timer}
     * @name     spTimer#Timer::start
     * @function
     */
    Timer.prototype.start = function (n) {
        this._maxCount = (typeof n == 'undefined')? Number.POSITIVE_INFINITY : n;

        this.stop();
        this.play();

        return this;
    };

    /**
     * Stops the timer.
     *
     * Example:
     * ```JavaScript
     * var timer = $.spTimer(1000, function () {
     *      console.log('tick');
     * }).start();
     *
     * // stops the timer
     * $('#stop-button').click(function () {
     *      timer.stop();
     * });
     * ```
     *
     * @return   {Timer}
     * @name     spTimer#Timer::stop
     * @function
     */
    Timer.prototype.stop = function () {
        clearTimeout(this._interval);
        clearInterval(this._interval);

        this._count = 0;
        this._pausedTime = 0;
        this._currentTime = (new Date()).getTime();
        this._lastTime = this._currentTime;
        this._state = Timer.STOP;

        return this;
    };

    /**
     * Plays the timer.
     *
     * If the timer is paused, continue. Otherwise, start.
     *
     * @param {Number} n Number of 'ticks' (not required)
     *
     * @return   {Timer}
     * @name     spTimer#Timer::play
     * @function
     */
    Timer.prototype.play = function (n) {
        var self = this;

        if (this._maxCount > 0) {
            this.pause();
            this._interval = setTimeout(function () {
                self._lastTime = self.getCurrentTime();
                self._count++;
                self._callback.apply(self);

                if (self.isRunning()) {
                    self._maxCount--;
                    if (self._maxCount > 0) {
                        self._interval = setInterval(function () {
                            self._lastTime = self.getCurrentTime();
                            self._count++;
                            self._callback.apply(self);

                            self._maxCount--;
                            if (self._maxCount <= 0) {
                                self.stop();
                            }
                        }, self._delay);
                    } else {
                        self.stop();
                    }
                }
            }, this.getRemainingTime());
            this._pausedTime = (new Date()).getTime() - this._currentTime;
            this._state = Timer.PLAY;
        } else {
            this.stop();
        }

        return this;
    };

    /**
     * Pauses the timer.
     *
     * @return   {Timer}
     * @name     spTimer#Timer::pause
     * @function
     */
    Timer.prototype.pause = function () {
        if (this._state == 'play') {
            clearTimeout(this._interval);
            clearInterval(this._interval);

            this._currentTime = this.getCurrentTime();
            this._state = Timer.PAUSE;
        }

        return this;
    };

    /**
     * Plays or pauses the timer.
     *
     * @return   {Timer}
     * @name     spTimer#Timer::toggle
     * @function
     */
    Timer.prototype.toggle =  function () {
        if (this.isRunning()) {
            this.pause();
        } else {
            this.play();
        }

        return this;
    };

    /**
     * Executes the timer only one time.
     *
     * @return   {Timer}
     * @name     spTimer#Timer::once
     * @function
     */
    Timer.prototype.once = function () {
        return this.start(1);
    };

    /**
     * Creates the plugin.
     *
     * @param {Number}   delay    Delay in milliseconds
     * @param {Function} callback Callback function
     *
     * @return {Timer}
     */
    $.spTimer = function (delay, callback) {
        return new Timer(delay, callback);
    };
})(jQuery);
