/**
 * jQuery.axisPrompt - Axis prompt plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spArgs
 *   3. jQuery.spIntl
 *   4. jQuery.spModal
 *   5. jQuery.spText
 *
 * @namespace
 * @name      axisPrompt
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @copyright AxisGraphics, Inc. All Right Reserved.
 * @license   Proprietary/Closed Source
 */
(function ($) {
    /**
     * Class FieldFactory.
     */
    function FieldFactory() {
    }

    /**
     * Creates a field.
     *
     * @param {jQuery} target Target
     *
     * @return {*}
     */
    FieldFactory.create = function (target) {
        if ($.type(target) == 'string') {
            target = {type: 'text', label: target};
        }

        var type = $.spText('ifEmpty', target.type, 'text');
        var label = $.spText('ifEmpty', target.label, '');
        var value = $.spText('ifEmpty', target.value, '');
        var width = $.spText('ifEmpty', target.width, 240);

        if (type == 'password') {
            ret = new FieldPassword(label, value, width);
        } else
        if (type == 'select') {
            ret = new FieldSelect(label, value, width, target.options);
        } else
        if (type == 'checkbox') {
            ret = new FieldCheckbox(label, value, width, target.options);
        } else
        if (type == 'radio') {
            ret = new FieldRadio(label, value, width, target.options);
        } else
        if (type == 'textarea') {
            ret = new FieldTextarea(label, value, width, target.options);
        } else {
            ret = new FieldText(label, value, width);
        }

        return ret;
    }

    /**
     * Abstract class Field.
     *
     * @param {String}        label Label
     * @param {*}             value Value
     * @param {String|Number} width Field width
     *
     * @abstract
     */
    function Field(label, value, width) {
        if (label !== undefined || value !== undefined || width !== undefined) {
            var args = $.spArgs(arguments, {
                label: 'string',
                value: '*',
                width: 'string|number'
            });

            this.fieldId = 'field' + Math.random().toString(36).substr(2);
            this.label = args.label;
            this.value = args.value;
            this.width = args.width;
        }
    }
    Field.prototype.getInputNode = function () {
        // abstract method
    };
    Field.prototype.getLabelNode = function () {
        return $('<label />').attr('for', this.fieldId).text(this.label);
    };
    Field.prototype.toString = function () {
        var div = $('<div class="axis-prompt-field" />');

        if (!$.spText('empty', this.label)) {
            div.append(this.getLabelNode());
        }

        div.append(this.getInputNode());
        return div[0].outerHTML;
    };

    /**
     * Class FieldText.
     *
     * @param {String}        label Label
     * @param {*}             value Value
     * @param {String|Number} width Field width
     *
     * @extends Field
     */
    function FieldText(label, value, width) {
        Field.call(this, label, value, width);
    }
    FieldText.prototype = new Field();
    FieldText.prototype.getInputNode = function () {
        return $('<input type="text" class="axis-prompt-input" />').attr(
            {id: this.fieldId, value: this.value}
        ).css('width', this.width);
    };

    /**
     * Class FieldTextarea.
     *
     * @param {String}        label Label
     * @param {*}             value Value
     * @param {String|Number} width Field width
     *
     * @extends Field
     */
    function FieldTextarea(label, value, width) {
        Field.call(this, label, value, width);
    }
    FieldTextarea.prototype = new Field();
    FieldTextarea.prototype.getInputNode = function () {
        return $('<textarea cols="80" rows="7" class="axis-prompt-input" />').attr(
            {id: this.fieldId}
        ).css('width', this.width).text(this.value);
    };

    /**
     * Class FieldPassword.
     *
     * @param {String}        label Label
     * @param {*}             value Value
     * @param {String|Number} width Field width
     *
     * @extends Field
     */
    function FieldPassword(label, value, width) {
        Field.call(this, label, value, width);
    }
    FieldPassword.prototype = new Field();
    FieldPassword.prototype.getInputNode = function () {
        return $('<input type="password" class="axis-prompt-input" />').attr(
            {id: this.fieldId, value: this.value}
        ).css('width', this.width);
    };

    /**
     * Class FieldSelect.
     *
     * @param {String}               label   Label
     * @param {*}                    value   Value
     * @param {String|Number}        width   Field width
     * @param {Array.<{value, label}>} options Options
     *
     * @extends Field
     */
    function FieldSelect(label, value, width, options) {
        var args = $.spArgs(arguments, {
            label: 'string',
            value: '*',
            width: 'string|number',
            options: 'object'
        });
        Field.call(this, label, value, width);
        this.options = args.options;
    }
    FieldSelect.prototype = new Field();
    FieldSelect.prototype.getInputNode = function () {
        var target = this;
        var select = $('<select />').attr({id: this.fieldId}).css('width', this.width);

        // appends input
        $.each(this.options, function (value, label) {
            var option = $('<option />').attr('value', value).text(label);
            if (value == target.value) {
                option.attr('selected', 'selected');
            }
            select.append(option);
        });

        return select;
    };

    /**
     * Class FieldCheckbox.
     *
     * @param {String}               label   Label
     * @param {*}                    value   Value
     * @param {String|Number}        width   Field width
     * @param {Array.<{value, label}>} options Options
     *
     * @extends Field
     */
    function FieldCheckbox(label, value, width, options) {
        var args = $.spArgs(arguments, {
            label: 'string',
            value: 'array',
            width: 'string|number',
            options: 'object'
        });
        Field.call(this, label, value, width);
        this.options = args.options;
    }
    FieldCheckbox.prototype = new Field();
    FieldCheckbox.prototype.getLabelNode = function () {
        return '';
    };
    FieldCheckbox.prototype.getInputNode = function () {
        var target = this;
        var fieldset = $('<fieldset class="axis-prompt-input" />').css('width', this.width);

        if (!$.spText('empty', this.label)) {
            fieldset.append($('<legend />').text(this.label));
        }

        $.each(this.options, function (value, label) {
            var inputId = $.spText('uniqid');
            var items = $.grep(target.value, function (v) {
                return v == value;
            });
            var checked = items.length > 0;

            fieldset.append(
                $('<table border="0" cellpadding="0" cellspacing="0" />').append(
                    $('<tr />').append(
                        $('<td />').append(
                            $('<input type="checkbox" />').attr(
                                {checked: checked, name: target.fieldId, id: inputId, value: value}
                            )
                        ),
                        $('<td />').append($('<label />').attr('for', inputId).text(label))
                    )
                )
            );
        });
        return fieldset;
    }

    /**
     * Class FieldRadio.
     *
     * @param {String}               label   Label
     * @param {*}                    value   Value
     * @param {String|Number}        width   Field width
     * @param {Array.<{value, label}>} options Options
     *
     * @extends Field
     */
    function FieldRadio(label, value, width, options) {
        var args = $.spArgs(arguments, {
            label: 'string',
            value: 'scalar',
            width: 'string|number',
            options: 'object'
        });
        Field.call(this, label, value, width);
        this.options = args.options;
    }
    FieldRadio.prototype = new Field();
    FieldRadio.prototype.getLabelNode = function () {
        return '';
    };
    FieldRadio.prototype.getInputNode = function () {
        var target = this;
        var fieldset = $('<fieldset class="axis-prompt-input" />').css('width', this.width);

        if (!$.spText('empty', this.label)) {
            fieldset.append($('<legend />').text(this.label));
        }

        $.each(this.options, function (value, label) {
            var inputId = $.spText('uniqid');
            var checked = target.value == value;

            fieldset.append(
                $('<table border="0" cellpadding="0" cellspacing="0" />').append(
                    $('<tr />').append(
                        $('<td />').append(
                            $('<input type="radio" />').attr(
                                {checked: checked, name: target.fieldId, id: inputId, value: value}
                            )
                        ),
                        $('<td />').append($('<label />').attr('for', inputId).text(label))
                    )
                )
            );
        });
        return fieldset;
    }

    /**
     * Shows a modal prompt dialog.
     *
     * Example 1:
     * ```JavaScript
     * $.axisPrompt('Enter your username', function (username) {
     *      if (!username) {
     *          $.axis('error', 'Username is required');
     *      }
     *      this.remove();
     * });
     * ```
     *
     * Example 2 (multiple fields):
     * ```JavaScript
     * $.axisPrompt(['Username', 'Age'], function (username, age) {
     *      if (!username || !age) {
     *          $.axis('error', 'Username and fields are required');
     *      }
     *      this.remove();
     * });
     * ```
     *
     * Example 3 (full example):
     * ```JavaScript
     * $.axisPrompt(
     *      'Please complete the fields',
     *      [
     *          {label: 'Username', type: 'text'},
     *          {label: 'Password', type: 'password'},
     *          {label: 'Age', type: 'select', options: {f: 'Female', m: 'Male'}, value: 'm'},
     *          {
     *              label: 'Profession',
     *              type: 'checkbox',
     *              options: {1: 'Engineer', 2: 'Lawyer', 3: 'Scientist', 4: 'Other'},
     *              value: [1, 4]
     *          },
     *          {label: 'Salary', type: 'radio', options: {0: 'Low', 1: 'Middle', 2: 'High'}, value: 1}
     *      ],
     *      function (username, password, age, profession, salary) {
     *          if (!username || !password) {
     *              $.axis('error', 'Username and password are required');
     *          }
     *          this.remove();
     *      }
     * );
     * ```
     *
     * @param {String}       title    Prompt title (not required)
     * @param {Array} fields List of fields
     * @param {Function}     onAccept Accept event handler
     *
     * @return   {Void}
     * @name     axisPrompt#main
     * @function
     */
    $.axisPrompt = function (title, fields, onAccept) {
        var args = $.spArgs(arguments, {
            title: {
                type: 'string',
                required: false
            },
            fields: {
                type: 'array|object',
                required: false
            },
            onAccept: 'function'
        });

        if (args.fields === undefined && args.title !== undefined) {
            args.fields = [args.title];
            args.title = '';
        }

        if ($.type(args.fields) == 'object') {
            args.fields = [args.fields];
        }

        if (args.fields.length == 0) {
            $.error('At least one field is required');
        }

        var fieldsNode = $('<div />');
        $.each(args.fields, function () {
            var field = FieldFactory.create(this);

            if ($.spText('empty', args.title) && args.fields.length == 1) {
                args.title = field.label;
                field.label = '';
            }

            fieldsNode.append(field.toString());
        });

        if ($.spText('empty', args.title)) {
            args.title = 'Prompt';
        }

        // creates a modal message
        var message = $.spModal(
            'message',
            args.title,
            fieldsNode.html(),
            {
                html: true,
                align: 'left',
                buttons: {
                    '[Accept]': function () {
                        var params = [];
                        $('.axis-prompt-input', message).each(function () {
                            var target = $(this);
                            var value = $.spText('trim', target.val())

                            if (target.prop('nodeName').toLowerCase() == 'fieldset') {
                                value = [];
                                $('input:checked', target).each(function (v) {
                                    value.push($(this).val());
                                });
                            }

                            params.push(value);
                        });
                        args.onAccept.apply(this, params);
                    },
                    'Cancel': function () {
                        this.remove();
                    }
                }
            }
        );

        $('.jquery-modal-message', message).addClass('axis-prompt');

        // places the focus on the first input
        $('.axis-prompt-input:first', message).focus();

        // accepts on 'Enter' key
        $('.axis-prompt-input', message).keydown(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
                $('input[value=Accept]', message).click();
            }
        });
    };
})(jQuery);
