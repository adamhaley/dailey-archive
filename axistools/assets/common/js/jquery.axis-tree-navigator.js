/**
 * jQuery.axisTreeNavigator - TreeNavigator interface.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      axisTreeNavigator
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @copyright AxisGraphics, Inc. All Right Reserved.
 * @license   Proprietary/Closed Source
 */
(function ($) {

    /**
     * TreeNavigator constructor.
     *
     * The component is aligned against the 'sugar' target using
     * the 'position' argument (see the plugin $.axisSugarPos for more info).
     *
     * Example:
     * ```JavaScript
     * var treeNavigator = $.axisTreeNavigator($('#button'), 'right-right bottom-top');
     * ```
     *
     * @param {jQuery} sugar Sugar target
     * @param {String} position
     *
     * @name     axisTreeNavigator#TreeNavigator
     * @function
     */
    function TreeNavigator(sugar, position) {
        /**
         * Current target.
         * @var {jQuery}
         */
        this._target = null;

        /**
         * Sugar target.
         * @var {jQuery}
         */
        this._sugar = sugar;

        /**
         * 'Sugar' position (see the plugin $.axisSugarPos for more info)
         * @var {String}
         */
        this._position = position;

        /**
         * Total number of columns.
         * @var {Number}
         */
        this._totalColumns = 3;

        /**
         * Number of columns to the left.
         * @var {Number}
         */
        this._leftColumns = 0;

        /**
         * Selected text.
         * @var {String}
         */
        this._selectedText = '';

        /**
         * Selected URL.
         * @var {String}
         */
        this._selectedUrl = '';

        this._make();
    }

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TreeNavigator.prototype._make = function () {
        var self = this;

        $.axis('get', 'tree-navigator.php').done(function (xml) {
            self._target = $('<div class="axis-tree-navigator"></div>').appendTo('body');
            self._target.append(function () {
                var target = $(this);
                var items = $('menu > item', xml);

                // menu
                $.proxy(self._makeMenu, self)(target, items);

                // navigator
                target.append(
                    '<div class="navigator">' +
                        '<a class="prev disabled" id="prev" title="Previous column" href="#">' +
                            '<span>Previous column</span>' +
                        '</a>' +
                        '<a class="next disabled" id="next" title="Next column" href="#">' +
                            '<span>Next column</span>' +
                        '</a>' +
                    '</div>'
                );

                // select button
                target.append('<a class="select-link" id="select-link" href="#">Select This Link</a>');
            });

            self._makeActions();
            self._update();
        });
    };

    /**
     * Makes the menu.
     *
     * @param {jQuery} target Target context
     * @param {jQuery} items  Menu items
     *
     * @return {Void}
     */
    TreeNavigator.prototype._makeMenu = function (target, items) {
        var self = this;

        target.append(
            $('<ul />').append(function () {
                var target = $(this);

                items.each(function () {
                    var item = $(this);

                    target.append(
                        $('<li />').append(function () {
                            var target = $(this);
                            var items = $('> item', item);

                            // appends a link
                            target.append(
                                $('<a />').append(
                                    $('<span />').text(item.attr('title'))
                                ).attr('href', item.attr('url'))
                            );

                            // appends a submenu to the entry
                            if (items.length > 0) {
                                $.proxy(self._makeMenu, self)(target, items);
                            }
                        })
                    );
                })
            })
        );
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    TreeNavigator.prototype._makeActions = function () {
        var self = this;

        // shows or hides the component when the users clicks on the button
        $(this._sugar).mousedown(function () {
            if (self._target.is(':hidden')) {
                // resets the component
                self._setLeftColumns(0);
                $('ul a', self._target).removeClass('selected');
                $('ul ul', self._target).hide();
                self._update(true);

                self._target.show();
            } else {
                self._target.hide();
            }
        });

        // closes the component when the users clicks outside it
        $(window).mousedown(function () {
            if (!self._sugar.is(':hover') && !self._target.is(':hover')) {
                self._target.hide();
            }
        });

        // menu entries actions
        $('ul a').mousedown(function () {
            var target = $(this);
            var entry = target.closest('li');
            var siblings = entry.siblings();

            self._selectedText = target.text();
            self._selectedUrl = target.attr('href');

            // removes the 'selected' class from all anchor siblings except for the current one
            $('a', siblings).removeClass('selected');
            target.addClass('selected');

            // hides all submenu siblings except the current one
            $('ul', siblings).hide();
            $('> ul', entry).show();

            // shows the right columns
            self._setRightColumns(0);
            self._update();
        }).click(function () {
            this.blur();
            return false;
        });

        // moves columns to the right
        $('#prev').click(function () {
            self._setLeftColumns(self._getLeftColumns() - 1);
            self._update();
            this.blur();
            return false;
        });

        // moves columns to the left
        $('#next').click(function () {
            self._setRightColumns(self._getRightColumns() - 1);
            self._update();
            this.blur();
            return false;
        });

        // closes the component and fires a 'done' event
        $('#select-link').click(function () {
            $(self).trigger('done');
            self._target.hide();
            this.blur();
            return false;
        });
    };

    /**
     * Updates the component.
     *
     * @param {Boolean} updateImmediately Updates the component with no animations
     *
     * @return {Void}
     */
    TreeNavigator.prototype._update = function (updateImmediately) {
        var self = this;
        var marginLeft = -113 * parseInt(this._getLeftColumns(), 10);

        // updates position
        $.axisSugarPos(this._sugar, this._target, this._position);

        // updates menu height
        var mainMenu = $('> ul', this._target);
        var menuHeight = 0;
        $('ul ul:visible', this._target).each(function () {
            var target = $(this);
            menuHeight = Math.max(menuHeight, target.height());
        });
        mainMenu.height(menuHeight);

        this._updateNavigatorButtons();
        if (updateImmediately) {
            $('> ul', this._target).css('margin-left', marginLeft);
        } else {
            $('> ul', this._target).animate({'margin-left': marginLeft}, 200, function () {
                self._updateNavigatorButtons();
            });
        }
    };

    /**
     * Updates navigator buttons.
     *
     * @return {Void}
     */
    TreeNavigator.prototype._updateNavigatorButtons = function () {
        $('#next').toggleClass('disabled', !(this._getRightColumns() > 0));
        $('#prev').toggleClass('disabled', !(this._getLeftColumns() > 0));
    };

    /**
     * Gets the number of visible columns.
     *
     * @return {Number}
     */
    TreeNavigator.prototype._getVisibleColumns = function () {
        return $('ul:visible', this._target).length;
    };

    /**
     * Gets the number of hidden columns to the left.
     *
     * @return {Number}
     */
    TreeNavigator.prototype._getLeftColumns = function () {
        return this._leftColumns;
    };

    /**
     * Sets the number of hidden columns to the left.
     *
     * @return {Void}
     */
    TreeNavigator.prototype._setLeftColumns = function (value) {
        this._leftColumns = value;
        this._leftColumns = Math.min(this._getVisibleColumns() - this._totalColumns, this._leftColumns);
        this._leftColumns = Math.max(0, this._leftColumns);
    };

    /**
     * Gets the number of hidden columns to the right.
     *
     * @return {Number}
     */
    TreeNavigator.prototype._getRightColumns = function () {
        return this._getVisibleColumns() - this._totalColumns - this._leftColumns;
    };

    /**
     * Sets the number of hidden columns to the right.
     *
     * @return {Void}
     */
    TreeNavigator.prototype._setRightColumns = function (value) {
        this._setLeftColumns(this._getVisibleColumns() - this._totalColumns - value);
    };

    /**
     * Gets the selected text.
     *
     * @return   {String}
     * @name     axisTreeNavigator#TreeNavigator::getSelectedText
     * @function
     */
    TreeNavigator.prototype.getSelectedText = function () {
        return this._selectedText;
    };

    /**
     * Sets the selected URL.
     *
     * @return {String}
     * @name     axisTreeNavigator#TreeNavigator::getSelectedUrl
     * @function
     */
    TreeNavigator.prototype.getSelectedUrl = function () {
        return this._selectedUrl;
    };

    /**
     * Creates a tree navigator.
     *
     * @param {jQuery} sugarTarget Sugar target
     * @param {String} relativePos Position relative to 'sugar target'
     *
     * @return {TreeNavigator}
     */
    $.axisTreeNavigator = function (sugarTarget, relativePosition) {
        return new TreeNavigator(sugarTarget, relativePosition);
    };
})(jQuery);
