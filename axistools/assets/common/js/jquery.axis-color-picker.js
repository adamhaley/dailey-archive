/**
 * jQuery.axisColorPicker - Color Picker plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spText
 *
 * @namespace
 * @name      axisColorPicker
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    /**
     * Default settings.
     */
    var settings = {
        position: 'bottom left',
        color: 'red'
    };

    /**
     * Constructor.
     *
     * @param {HTMLElement} target   Target
     * @param {String}      position Palette position
     */
    function ColorPicker(target, position) {
        /**
         * Target.
         * @var {jQuery}
         */
        this._target = ColorPicker.setInstance(target, this);

        /**
         * Palette.
         * @var {jQuery}
         */
        this._palette = this._createPalette();

        /**
         * Selected color.
         * @var {String}
         */
        this._color = '#000000';

        /**
         * Palette position.
         * @var {String}
         */
        this._position = position;

        // shows or hides the palette
        $(window).on('blur mousedown', $.proxy(this._windowOnBlurMouseDown, this));

        // palette actions
        $('.axis-color-picker-palette', this._palette).mouseover($.proxy(this._paletteOnMouseOver, this));
        $('.axis-color-picker-palette', this._palette).click($.proxy(this._paletteOnClick, this));
        $('input', this._palette)
            .on('input', $.proxy(this._paletteInputOnChange, this))
            .on('keypress', $.proxy(this._paletteInputOnKeypress, this));

        this._update();
    }

    /**
     * Namespace.
     * @var {String}
     */
    ColorPicker._namespace = 'axisColorPicker';

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {?Object}
     */
    ColorPicker.getInstance = function (target) {
        return ColorPicker._getData(target, 'instance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target   Target
     * @param {Object}      instance Instance
     *
     * @return {jQuery}
     */
    ColorPicker.setInstance = function (target, instance) {
        return ColorPicker._setData(target, 'instance', instance);
    };

    /**
     * Handles events.
     *
     * @param {String}   eventType Event name
     * @param {Function} handler   Event handler
     *
     * @return   {jQuery}
     */
    ColorPicker.prototype.on = function (eventType, handler) {
        return this._target.on(ColorPicker._getNamespaceId(eventType), handler);
    };

    /**
     * Triggers an event.
     *
     * @param {String} eventType       Event type
     * @param {*}      extraParameters Extra parameters
     *
     * @return   {jQuery}
     */
    ColorPicker.prototype.trigger = function (eventType, extraParameters) {
        var args = Array.prototype.slice.call(arguments, 1);

        this._target.trigger(ColorPicker._getNamespaceId(eventType), args);
    };

    /**
     * Gets the color.
     *
     * @return   {String}
     */
    ColorPicker.prototype.getColor = function () {
        return this._color;
    };

    /**
     * Sets the color.
     *
     * @param {Number|String} value Color
     *
     * @return   {Void}
     */
    ColorPicker.prototype.setColor = function (value) {
        if ($.type(value) == 'number') {
            value = ColorPicker._num2hex(value);
        }

        var span = $('<span />').appendTo('body').css('color', value);
        value = ColorPicker._rgb2hex(span.css('color'));
        span.remove();

        this._color = value;
        this._update();
    };

    /**
     * Gets visibility.
     *
     * @return   {Boolean}
     */
    ColorPicker.prototype.isVisible = function () {
        return this._palette.is(':visible');
    };

    /**
     * Sets visibility.
     *
     * @param {Boolean} value Is visible?
     *
     * @return   {Void}
     */
    ColorPicker.prototype.setVisible = function (value) {
        this._palette.toggle(value);
        this._update();
    };

    /**
     * Gets the position.
     *
     * @return   {String}
     */
    ColorPicker.prototype.getPosition = function () {
        return this._position;
    };

    /**
     * Sets the position.
     *
     * @param {String} value Position
     *
     * @return   {Void}
     */
    ColorPicker.prototype.setPosition = function (value) {
        this._position = value;
        this._update();
    };

    /**
     * Gets the ID relative to the current namespace.
     *
     * For example:
     * ```JavaScript
     * console.log(this._getNamespaceId('test')); // prints 'axisColorPicker:test'
     * ```
     *
     * @param {String} attrName Attribute name
     *
     * @return {String}
     */
    ColorPicker._getNamespaceId = function (attrName) {
        return ColorPicker._namespace + ':' + attrName;
    };

    /**
     * Gets data.
     *
     * @param {HTMLElement} target Target
     * @param {String}      attr   Attribute
     *
     * @return {*}
     */
    ColorPicker._getData = function (target, attr) {
        return $(target).data(ColorPicker._getNamespaceId(attr));
    };

    /**
     * Sets data.
     *
     * @param {HTMLElement} target Target
     * @param {String}      attr   Attribute
     * @param {*}           value  Value
     *
     * @return {jQuery}
     */
    ColorPicker._setData = function (target, attr, value) {
        return $(target).data(ColorPicker._getNamespaceId(attr), value);
    };

    /**
     * Converts RGB into hexadecimal color format.
     *
     * @param {String} rgb RGB color format
     *
     * @return {String}
     */
    ColorPicker._rgb2hex = function (rgb) {
        var ret = '000000';
        var matches = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

        if (matches) {
            var red = ('0' + parseInt(matches[1], 10).toString(16)).slice(-2);
            var green = ('0' + parseInt(matches[2], 10).toString(16)).slice(-2);
            var blue = ('0' + parseInt(matches[3], 10).toString(16)).slice(-2);

            ret = red + green + blue;
        }

        return '#' + ret.toUpperCase();
    };

    /**
     * Converts number into hexadecimal color format.
     *
     * @param {Number} num Color number
     *
     * @return {String}
     */
    ColorPicker._num2hex = function (num) {
        return '#' + ('000000' + num.toString(16).toUpperCase()).slice(-6);
    };

    /**
     * Gets a bidimensional matrix of colors.
     *
     * @return {Array}
     */
    ColorPicker._getColorMatrix = function () {
        var numRows = 12;
        var numCols = 20;

        // creates bidimension matrix
        var matrix = new Array(numRows);
        for (var i = 0; i < numRows; i++) {
            matrix[i] = new Array(numCols);
        }

        // fills with colors
        for (var r = 0; r < 2; r++) {
            for (var c = 0; c < 3; c++) {
                for (var i = 0; i < 6; i++) {
                    for (j = 0; j < 6; j++) {
                        var red = 0x33 * (3 * r + c);
                        var green = 0x33 * j;
                        var blue = 0x33 * i;

                        matrix[6 * r + i][6 * c + j] = [red, green, blue];
                    }
                }
            }

            if (r > 0) {
                // basic colors
                var colors = [
                    [0xff, 0, 0], [0, 0xff, 0], [0, 0, 0xff], [0xff, 0xff, 0],
                    [0, 0xff, 0], [0xff, 0, 0xff]
                ];

                for (var i = 0; i < 6; i++) {
                    matrix[6 * r + i][18] = [0, 0, 0];
                    matrix[6 * r + i][19] = colors[i];
                }
            } else {
                // gray colors
                for (var i = 0; i < 6; i++) {
                    matrix[6 * r + i][18] = [0, 0, 0];
                    matrix[6 * r + i][19] = [0x33 * i, 0x33 * i, 0x33 * i];
                }
            }
        }

        return matrix;
    };

    /**
     * Updates the interface.
     *
     * @param {String} color Sample color (not required)
     *
     * @return {Void}
     */
    ColorPicker.prototype._update = function (color) {
        if (color === undefined) {
            color = this._color;
        }

        // updates input and sample color
        $('input', this._palette).val($.spText('trim', color, '#'));
        $('.axis-color-picker-sample-color', this._palette).css('background-color', color);

        if (this.isVisible()) {
            this._updatePosition();
        }
    };

    /**
     * Updates palette position.
     *
     * @return {Void}
     */
    ColorPicker.prototype._updatePosition = function () {
        var aligns = this._position.split(' ');
        var align0 = aligns.length > 0? aligns[0] : 'bottom';
        var align1 = aligns.length > 1? aligns[1] : 'left';
        var offset = this._target.offset();
        var y = offset.top;
        var x = offset.left;
        var paletteWidth = this._palette.outerWidth();
        var paletteHeight = this._palette.outerHeight();
        var targetWidth = this._target.outerWidth();
        var targetHeight = this._target.outerHeight();

        $.each([align0, align1], function (i) {
            if (this == 'top') {
                y = y + paletteHeight * (i - 1);
            } else
            if (this == 'right') {
                x = x + targetWidth - i * paletteWidth;
            } else
            if (this == 'bottom') {
                y = y + targetHeight - i * paletteHeight;
            } else
            if (this == 'left') {
                x = x + paletteWidth * (i - 1);
            }
        });

        this._palette.offset({left: x, top: y});
    };

    /**
     * Hides the color picker.
     *
     * @return {Void}
     */
    ColorPicker.prototype._windowOnBlurMouseDown = function (event) {
        if (!this._target.is(':hover') && !this._palette.is(':hover')) {
            this._palette.hide();
        }
    };

    /**
     * Handles palette's mouseover events.
     *
     * @param {Object} event Event
     *
     * @return {Void}
     */
    ColorPicker.prototype._paletteOnMouseOver = function (event) {
        var target = $(event.target);
        var color = ColorPicker._rgb2hex(target.css('background-color'));

        this._update(color);
    };

    /**
     * Selects a color and hides the palette.
     *
     * @return {Void}
     * @fires  axisColorPicker#change
     */
    ColorPicker.prototype._paletteOnClick = function (event) {
        var target = $(event.target);

        this._color = ColorPicker._rgb2hex(target.css('background-color'));
        this._target.trigger(ColorPicker._getNamespaceId('change'));
        this._palette.hide();
    };

    /**
     * Handles the input's change event.
     *
     * @param {Object} event Event
     *
     * @return {Void}
     */
    ColorPicker.prototype._paletteInputOnChange = function (event) {
        var target = $(event.target);
        var value = target.val();

        // applies mask
        value = value.replace(/[^a-f0-9]*/gi, '').substr(0, 6).toUpperCase();
        target.val(value);

        // updates sample color
        var color = value.length == 6? value: '000000';
        $('.axis-color-picker-sample-color', this._palette).css('background-color', '#' + color);
    };

    /**
     * Handles the input's keypress event.
     *
     * @param {Object} event Event
     *
     * @return {Void}
     */
    ColorPicker.prototype._paletteInputOnKeypress = function (event) {
        var target = $(event.target);
        var value = target.val();

        if (event.which == 13 && $.inArray(value.length, [0, 6]) >= 0) {
            this._color = value;
            this._target.trigger(ColorPicker._getNamespaceId('change'));
            this._palette.hide();
        }
    };

    /**
     * Creates the palette.
     *
     * @return {jQuery}
     */
    ColorPicker.prototype._createPalette = function () {
        var self = this;
        var matrix = ColorPicker._getColorMatrix();

        return $(
            '<div class="axis-color-picker">\n' +
                '<div class="axis-color-picker-heading">\n' +
                    '<table ' +
                        'class="axis-color-picker-sample" ' +
                        'border="0" ' +
                        'cellpadding="0" ' +
                        'cellspacing="0">\n' +
                        '<tr>\n' +
                            '<td>' +
                                '<input class="axis-color-picker-value" type="text" value="000000" />' +
                            '</td>\n' +
                            '<td class="axis-color-picker-sample-color" />\n' +
                        '</tr>\n' +
                    '</table>\n' +
                '</div>\n' +
            '</div>'
        ).append(
            $('<table border="0" cellpadding="0" cellspacing="0" />').addClass(
                'axis-color-picker-palette'
            ).append($.map(matrix, function (row) {
                return $('<tr />').append($.map(row, function (rgb) {
                    return $('<td />').css(
                        'background-color', 'rgb(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ')'
                    );
                }));
            }))
        ).appendTo('body');
    };

    /**
     * Available methods.
     * @var {object}
     */
    var methods = {
        /**
         * Initializes color picker.
         *
         * Example:
         * ```JavaScript
         * $('#target').axisColorPicker({position: 'top', backgroundColor: 'black'});
         * ```
         *
         * @return   {jQuery}
         * @name     axisColorPicker#init
         * @function
         */
        'init': function (options) {
            options = $.extend({}, settings, options);

            return this.each(function () {
                new ColorPicker(this, options.position);
            });
        },

        /**
         * Handlers or fires the 'change' event.
         *
         * The method 'onChange' is fired when the color changes.
         *
         * @param {Function} onChange Event handler (not required)
         *
         * @return   {jQuery}
         * @name     axisColorPicker#change
         * @function
         */
        'change': function (onChange) {
            var args = Array.prototype.slice.call(arguments);
            args.unshift('change');

            return this.each(function () {
                var cp = ColorPicker.getInstance(this);

                if ($.type(onChange) == 'function') {
                    cp.on.apply(cp, args);
                } else {
                    cp.trigger.apply(cp, args);
                }
            });
        },

        /**
         * Sets or gets the color.
         *
         * @param {String|Number} value Color
         *
         * @return   {String|jQuery}
         * @name     axisColorPicker#color
         * @function
         */
        'color': function (value) {
            var ret = this;

            this.each(function () {
                var cp = ColorPicker.getInstance(this);

                if (value === undefined) {
                    ret = cp.getColor();
                } else {
                    cp.setColor(value);
                }
            });

            return ret;
        },

        /**
         * Shows the color picker.
         *
         * @return   {jQuery}
         * @name     axisColorPicker#show
         * @function
         */
        'show': function () {
            return this.each(function () {
                var cp = ColorPicker.getInstance(this);
                cp.setVisible(true);
            });
        },

        /**
         * Hides the color picker.
         *
         * @return   {jQuery}
         * @name     axisColorPicker#hide
         * @function
         */
        'hide': function () {
            return this.each(function () {
                var cp = ColorPicker.getInstance(this);
                cp.setVisible(false);
            });
        },

        /**
         * Shows or hides the color picker.
         *
         * @return   {jQuery}
         * @name     axisColorPicker#toggle
         * @function
         */
        'toggle': function () {
            return this.each(function () {
                var cp = ColorPicker.getInstance(this);
                cp.setVisible(!cp.isVisible());
            });
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.fn.axisColorPicker = function (methodName) {
        var method = methods['init'];
        var args = arguments;

        if ($.type(methodName) == 'string') {
            method = methods[methodName];
            args = Array.prototype.slice.call(arguments, 1);
        }

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
