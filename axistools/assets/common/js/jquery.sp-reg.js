/**
 * jQuery.spUri - Some useful regex functions.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      spReg
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Plugin methods.
     * @var {Object}
     */
    var methods = {
        /**
         * Encodes a regex parameter.
         *
         * For example:
         * ```JavaScript
         * // the characters '(' and ')' have special meaning
         * // in regular expressions, so they must be escaped
         * var str = 'hello (there)';
         * var re = new RegExp('(^|;)[\\s]*' + $.spReg('encode', str) + '=([^;]*)');
         * ```
         *
         * @param {String} param Parameter to be encoded
         *
         * @return   {String}
         * @name     spReg#encode
         * @function
         */
        'encode': function (param) {
            var chars = new RegExp("[.*+?|()\\[\\]{}\\\\]", "g");

            return param.replace(chars, "\\$&");
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.spReg = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
