/**
 * jQuery.spSlider - Slider plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.browser
 *   3. jQuery.spText
 *   4. jQuery.spTimer
 *
 * @namespace
 * @name      spSlider
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    /**
     * Class Slider.
     *
     * @param {HTMLElement} container Container
     */
    function Slider(container) {
        /**
         * Container.
         * @var {jQuery}
         */
        this._container = Slider.saveInstance(container, this);

        /**
         * Slider type.
         * Allowed values are 'vertical', 'horizontal' and 'both'.
         * @var {String}
         */
        this._type = 'all';

        /**
         * Radius length in pixels.
         * The radius represents a circle. The maximum speed is reached at the border.
         * In the center, the velocity is zero.
         * @var {Number}
         */
        this._radius = 100;

        /**
         * The coordinate origin from which the surface moves.
         * This point represents the center of the circle.
         * @var {{x:Number, y:Number}}
         */
        this._origin = {x: 0, y: 0};

        /**
         * Mouse position.
         * @var {{x:Number, y:Number}}
         */
        this._mousePosition = {x: 0, y: 0};

        /**
         * Scroll position.
         * @var {{x:Number, y:Number}}
         */
        this._scroll = {x: this._container.scrollLeft(), y: this._container.scrollTop()};

        /**
         * Container size.
         * @var {{width: Number, y:Number}}
         */
        this._size = {width: this._container.width(), height: this._container.height()};

        /**
         * Maximum speed in pixels.
         * @var {Number}
         */
        this._maxSpeed = 700;

        /**
         * Frames per seconds.
         * How many times per second the slider is updated.
         * @var {Number}
         */
        this._framesPerSecond = 60;

        /**
         * Is the slider running?
         * @var {Boolean}
         */
        this._isRunning = false;

        /**
         * Is the slider in debug mode?
         * @var {Boolean}
         */
        this._isDebug = false;

        /**
         * Debugging object.
         * This object appears when the user activates the 'debugging mode'.
         * @var {jQuery}
         */
        this._debugTarget = null;

        /**
         * A timer to control the movement (see the $.spTimer plugin).
         * @var {Timer}
         */
        this._timer = null;

        /**
         * Ease function.
         * This function controls the movement of the slider.
         * See http://gizma.com/easing/ for mor info.
         * @var {Function}
         */
        this._easeFunction = null;
    }

    /**
     * Retrieves the instance from an HTML element.
     *
     * @param {HTMLElement} element Target
     *
     * @return {Slider}
     */
    Slider.retrieveInstance = function (element) {
        return $(element).data('slider-instance');
    };

    /**
     * Saves an instance into an HTML element.
     *
     * @param {HTMLElement} target   Target
     * @param {Slider}     instance Dragger instance
     *
     * @return {jQuery}
     */
    Slider.saveInstance = function (target, instance) {
        return $(target).data('slider-instance', instance);
    };

    /**
     * Is the slider in debug mode?
     *
     * @return {Boolean}
     */
    Slider.prototype.isDebug = function () {
        return this._isDebug;
    };

    /**
     * Sets debug mode.
     *
     * @param {Boolean} value Debug mode
     *
     * @return {Void}
     */
    Slider.prototype.setDebug = function (value) {
        if (value && $.browser.opera) {
            $.error('The debugger feature is not supported in Opera Browser');
        }

        this._isDebug = value;
    };

    /**
     * Gets slider type.
     *
     * @param {String}
     */
    Slider.prototype.getType = function () {
        return this._type;
    };

    /**
     * Sets slider type.
     *
     * Allowed values are 'horizontal' and 'vertical'.
     *
     * @param {String} value Type
     *
     * @return {Void}
     */
    Slider.prototype.setType = function (value) {
        if ($.inArray(value, ['vertical', 'horizontal', 'all']) < 0) {
            $.error('Invalid type. Allowed values are: vertical, horizontal, all');
        }

        this._type = value;
    };

    /**
     * Gets the radius of the slider.
     *
     * @return {Number} Pixels
     */
    Slider.prototype.getRadious = function () {
        return this._radius;
    };

    /**
     * Sets the radius of the slider.
     *
     * @param {Number} value Radius in pixels
     *
     * @return {Void}
     */
    Slider.prototype.setRadius = function (value) {
        this._radius = value;
    };

    /**
     * Gets the maximum speed of the slider.
     *
     * @return {Number} Pixels
     */
    Slider.prototype.getMaxSpeed = function () {
        return this._maxSpeed;
    };

    /**
     * Sets the maximum speed of the slider.
     *
     * @param {Number} value Speed in pixels
     *
     * @return {Void}
     */
    Slider.prototype.setMaxSpeed = function (value) {
        this._maxSpeed = value;
    };

    /**
     * Gets the 'ease function'.
     *
     * @return {Function}
     */
    Slider.prototype.getEaseFunction = function () {
        return this._easeFunction;
    };

    /**
     * Sets the 'ease function'.
     *
     * See http://gizma.com/easing/ for more info.
     *
     * @param {Function} value Ease function
     *
     * @return {Void}
     */
    Slider.prototype.setEaseFunction = function (value) {
        this._easeFunction = value;
    };

    /**
     * Gets the 'scroll left' of the container.
     *
     * @return {Number}
     */
    Slider.prototype.getScrollLeft = function () {
        return this._scroll.x;
    };

    /**
     * Sets the 'scroll left' of the container.
     *
     * @param {Number} value X coordinate
     *
     * @return {Void}
     */
    Slider.prototype.setScrollLeft = function (value) {
        this._scroll.x = value;
    };

    /**
     * Gets the 'scroll top' of the container.
     *
     * @return {Number}
     */
    Slider.prototype.getScrollTop = function () {
        return this._scroll.y;
    };

    /**
     * Sets the 'scroll top' of the container.
     *
     * @param {Number} value Y coordinate
     *
     * @return {Void}
     */
    Slider.prototype.setScrollTop = function (value) {
        this._scroll.y = value;
    };

    /**
     * Gets frames per second.
     *
     * The 'frames per second' indicates how many times per second the slider is updated.
     *
     * @return {Number}
     */
    Slider.prototype.getFramesPerSecond = function () {
        return this._framesPerSecond;
    };

    /**
     * Sets frames per second.
     *
     * @param {Number} value Frames per second
     *
     * @return {Void}
     */
    Slider.prototype.setFramesPerSecond = function (value) {
        this._framesPerSecond = value;
    };

    /**
     * Starts the slider.
     *
     * @return {Void}
     */
    Slider.prototype.start = function () {
        var self = this;

        this.stop();

        $(this._container).one('mousemove', function (event) {
            self._play(event.pageX, event.pageY);
        });
    };

    /**
     * Stops the slider.
     *
     * @return {Void}
     */
    Slider.prototype.stop = function () {
        this._timer.stop();

        if (this._isDebug) {
            this._debugTarget.hide();
        }

        this._isRunning = false;
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    Slider.prototype.make = function () {
        //this._container.css('overflow', 'hidden');

        // by default the 'ease function' is 'easeInOutQuint'
        // see http://gizma.com/easing/ for mor info.
        this._easeFunction = function (t, b, c, d) {
            t /= d/2;
            if (t < 1) return c/2*t*t*t*t*t + b;
            t -= 2;
            return c/2*(t*t*t*t*t + 2) + b;
        };

        this._timer = $.spTimer(
            Math.round(1000 / this._framesPerSecond),
            $.proxy(this._timerOnTick, this)
        );

        if (this._isDebug) {
            this._makeDebugTarget();
        }

        this._makeActions();
        this._update();
    };

    /**
     * Makes the component actions.
     *
     * @return {Void}
     */
    Slider.prototype._makeActions = function () {
        var self = this;

        this._container
            .on('mouseenter', function (event) {
                if (!self._isRunning) {
                    return;
                }

                if (self._timer.getState() == 'pause') {
                    self._play(event.pageX, event.pageY);
                }
            })
            .on('mouseleave blur', function () {
                if (!self._isRunning) {
                    return;
                }

                if (self._isDebug) {
                    self._debugTarget.hide();
                }

                self._timer.pause();
            })
            .on('mousemove', function (event) {
                if (!self._isRunning) {
                    return;
                }

                // saves the mouse position
                self._mousePosition = {x: event.pageX, y: event.pageY};

                // if the distance is greater than the radius then
                // moves the origin in the direction of the mouse position
                var d = self._getDistance();
                if (d > self._radius) {
                    var c = (1 - self._radius / d);
                    var direction = self._getDirection();
                    var v = {x: c * direction.x, y: c * direction.y};

                    self._origin = {x: self._origin.x + v.x , y: self._origin.y + v.y};
                }

                // if the slider has reached the horizontal limits then
                // moves the origin to the horizontal mouse position
                var hasReachedHorizontalLimits =
                    self._mousePosition.x > self._origin.x && self._scroll.x > self._getMaxScrollX() - 1
                    || self._mousePosition.x < self._origin.x && self._scroll.x < 1;
                if (hasReachedHorizontalLimits) {
                    self._origin.x = event.pageX;
                }

                // if the slider has reached the vertical limits then
                // moves the origin to the vertical mouse position
                var hasReachedVerticalLimits =
                    self._mousePosition.y > self._origin.y && self._scroll.y > self._getMaxScrollY() - 1
                    || self._mousePosition.y < self._origin.y && self._scroll.y < 1;
                if (hasReachedVerticalLimits) {
                    self._origin.y = event.pageY;
                }

                if (self._isDebug) {
                    self._updateDebugPosition();
                }
            });
    };

    /**
     * Makes the debug target.
     *
     * @return {Void}
     */
    Slider.prototype._makeDebugTarget = function () {
        var self = this;
        var color = 'red';
        var shadowColor = 'black';
        var lineWidth = 2;
        var offset = this._radius + lineWidth / 2;
        var canvasWidth = 2 * this._radius + 3/2 * lineWidth;

        this._debugTarget = $('<canvas />')
            .attr({width: canvasWidth, height: canvasWidth})
            .css({
                'position': 'absolute',
                'top': 0,
                'left': 0,
                'pointer-events': 'none',
                'margin': (-offset) + 'px 0 0 ' + (-offset) + 'px'
            })
            .hide()
            .appendTo('body');

        var canvas = this._debugTarget[0];
        var context = canvas.getContext('2d');

        // draws a circle and its shadow
        $.each([shadowColor, color], function (index, color) {
            var delta = (1 - index) * lineWidth / 2;

            // draws a circle
            context.beginPath();
            context.arc(offset + delta, offset + delta, self._radius, 0, 2 * Math.PI);
            context.lineWidth = lineWidth;
            context.strokeStyle = color;
            context.stroke();

            // draws a dot in the center of the circle
            context.beginPath();
            context.arc(offset + delta, offset + delta, lineWidth, 0, 2 * Math.PI);
            context.fillStyle = color;
            context.fill();
            context.stroke();
        });
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    Slider.prototype._update = function () {
        this._container.width(this._size.width).height(this._size.height);
        this._updateScroll();
    };

    /**
     * Updates the scroll.
     *
     * @return {Void}
     */
    Slider.prototype._updateScroll = function () {
        if (this._type == 'horizontal') {
            // moves the scroll horizontally
            this._container.scrollLeft(this._scroll.x);
        } else
        if (this._type == 'vertical') {
            // moves the scroll vertically
            this._container.scrollTop(this._scroll.y);
        } else {
            // moves the scroll in all directions
            this._container
                .scrollLeft(this._scroll.x)
                .scrollTop(this._scroll.y);
        }
    };

    /**
     * Updates debug position.
     *
     * @return {Void}
     */
    Slider.prototype._updateDebugPosition = function () {
        var offset = this._container.offset();
        this._debugTarget.css({left: this._origin.x, top: this._origin.y});
    };

    /**
     * Starts or resumes the slider and place the origin at the position (x, y)
     *
     * @param {Number} pageX X coordinate on the page
     * @param {Number} pageY Y coordinate on the page
     *
     * @return {Void}
     */
    Slider.prototype._play = function (pageX, pageY) {
        // captures the mouse position and current scroll and starts the slider
        this._origin = {x: pageX, y: pageY};
        this._scroll = {x: this._container.scrollLeft(), y: this._container.scrollTop()};
        this._timer.play();

        if (this._isDebug) {
            this._debugTarget.show();
            this._updateDebugPosition();
        }

        this._isRunning = true;
    };

    /**
     * Gets the direction of the movement.
     *
     * @return {{x:Number, y:Number}}
     */
    Slider.prototype._getDirection = function () {
        return {
            x: this._mousePosition.x - this._origin.x,
            y: this._mousePosition.y - this._origin.y
        };
    };

    /**
     * Gets the distance from the mouse position to the origin.
     *
     * @return {Number}
     */
    Slider.prototype._getDistance = function () {
        var direction = this._getDirection();
        return Math.sqrt(Math.pow(direction.x, 2) + Math.pow(direction.y, 2));
    };

    /**
     * Gets the current speed vector.
     *
     * The speed is calculated upon the distance from the origin.
     *
     * @return {{x:Number, y:Number}}
     */
    Slider.prototype._getSpeed = function () {
        var direction = this._getDirection();
        var signX = direction.x > 0? 1 : -1;
        var signY = direction.y > 0? 1 : -1;
        var speed = {
            x: signX * Math.abs(this._easeFunction(direction.x, 0, this._maxSpeed, this._radius)),
            y: signY * Math.abs(this._easeFunction(direction.y, 0, this._maxSpeed, this._radius))
        };

        return {
            x: Math.max(-this._maxSpeed, Math.min(this._maxSpeed, speed.x)),
            y: Math.max(-this._maxSpeed, Math.min(this._maxSpeed, speed.y))
        };
    };

    /**
     * Gets the maximum scroll X.
     *
     * @return {Number}
     */
    Slider.prototype._getMaxScrollX = function () {
        return this._container.prop('scrollWidth') - this._container.width();
    };

    /**
     * Gets the maximum scroll Y.
     *
     * @return {Number}
     */
    Slider.prototype._getMaxScrollY = function () {
        return this._container.prop('scrollHeight') - this._container.height();
    };

    /**
     * Handle 'tick' events.
     * This function is called by the timer every time it 'ticks'.
     *
     * @return {Void}
     */
    Slider.prototype._timerOnTick = function () {
        var speed = this._getSpeed();
        var scroll = {
            x: this._scroll.x + speed.x / this._framesPerSecond,
            y: this._scroll.y + speed.y / this._framesPerSecond
        };

        this._scroll = {
            x: Math.min(this._getMaxScrollX(), Math.max(0, scroll.x)),
            y: Math.min(this._getMaxScrollY(), Math.max(0, scroll.y))
        };

        this._updateScroll();
    };

    var methods = {
        /**
         * Initializes the plugin.
         *
         * Example:
         * ```JavaScript
         * // initializes the slider
         * container.spSlider({
         *     type: 'vertical'
         * });
         *
         * // starts the slider
         * $('div#draggable-object', container).on('mousedown', function (event) {
         *     container.spSlider('start');
         * });
         *
         * // stops the slider
         * $(window).on('mouseup', function () {
         *     container.spSlider('stop');
         * });
         * ```
         *
         * @param {String}   options.type            Slider type: 'vertical', 'horizontal', 'all' (
         *                                           default is 'all')
         * @param {Number}   options.radius          Slider radius (default is 100 pixels)
         * @param {Number}   options.maxSpeed        Maximum speed (default is 700 pixels per second)
         * @param {Number}   options.scrollLeft      Initial scroll left (not required)
         * @param {Number}   options.scrollTop       Initial scroll top (not required)
         * @param {Number}   options.framesPerSecond Number of times per second the slider is updated (
         *                                           default is 60 frames per second)
         * @param {Function} options.easeFunction    Ease function (not required)
         *
         * @return   {jQuery}
         * @name     spSlider#init
         * @function
         */
        'init': function (options) {
            return this.each(function () {
                var slider = new Slider(this);

                if (options !== undefined) {
                    // sets up the properties
                    $.each(options, function (key, value) {
                        var methodName = 'set' + $.spText('capitalize', key);
                        var method = slider[methodName];

                        if (method === undefined) {
                            $.error('Invalid parameter: ' + key);
                        }

                        $.proxy(method, slider)(value);
                    });
                }

                slider.make();
            });
        },

        /**
         * Starts the slider.
         *
         * @return   {jQuery}
         * @name     spSlider#start
         * @function
         */
        'start': function () {
            return this.each(function () {
                var slider = Slider.retrieveInstance(this);
                slider.start();
            });
        },

        /**
         * Stops the slider.
         *
         * @return   {jQuery}
         * @name     spSlider#stop
         * @function
         */
        'stop': function () {
            return this.each(function () {
                var slider = Slider.retrieveInstance(this);
                slider.stop();
            });
        }
    };

    /**
     * Creates the plugin.
     *
     * @param {String} methodName Method name
     *
     * @return {Slider}
     */
    $.fn.spSlider = function (methodName) {
        var method = methods['init'];
        var args = arguments;

        if ($.type(methodName) == 'string') {
            method = methods[methodName];
            args = Array.prototype.slice.call(arguments, 1);
        }

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
