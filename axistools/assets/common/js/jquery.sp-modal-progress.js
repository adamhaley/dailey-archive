/**
 * jQuery.spModalProgress - Modal Progress Dialog.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   1. jQuery.spHtml
 *   2. jQuery.spText
 *   2. jQuery.spModal
 *
 * @namespace
 * @name      spModalProgress
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Constructor.
     *
     * Example:
     * ```JavaScript
     * var progress = $.spModalProgress(title);
     * progress.onAbort(function () {
     *     this.close();
     *     if (req != null) {
     *         req.abort();
     *     }
     * });
     * ```
     *
     * @param {String} title   Title
     * @param {String} message Message (not required)
     *
     * @name     spModalProgress#ProgressDialog
     * @function
     */
    function ProgressDialog(title, message) {
        var text = $.spText('empty', message)? '' : '<p>' + $.spHtml('encode', message) + '</p>';
        var target = this;
        var html =
            '<div class="jquery-modal-progress">' +
                text +
                '<div class="jquery-modal-progress-bar">' +
                    '<div class="jquery-modal-indicator">&nbsp;</div>' +
                    '<div class="jquery-modal-percent">0%</div>' +
                '</div>' +
            '</div>';

        this._message = $.spModal('message', title, html, {
            html: true,
            buttons: [
                {
                    label: 'Cancel',
                    handler: function () {
                        $(target).trigger('abort');
                    }
                }
            ]
        });
    }

    /**
     * Sets progress.
     *
     * @param {Number} value Value (A number between 0 and 1)
     *
     * @return   {Void}
     * @name     spModalProgress#ProgressDialog::setProgress
     * @function
     */
    ProgressDialog.prototype.setProgress = function (value) {
        var contents = $('#contents', this._message);
        var percent = 100 * value;

        $('.jquery-modal-indicator', contents).css('width', percent + '%');
        $('.jquery-modal-percent', contents).text(Math.floor(percent) + '%');
    };

    /**
     * Handles 'abort' events.
     *
     * The 'abort' event is triggered when the user clicks the 'Cancel' button.
     *
     * @param {Function} handler
     *
     * @return   Void}
     * @name     spModalProgress#ProgressDialog::onAbort
     * @function
     */
    ProgressDialog.prototype.onAbort = function (handler) {
        $(this).on('abort', handler);
    };

    /**
     * Closes the modal dialog.
     *
     * @return   {Void}
     * @name     spModalProgress#ProgressDialog::close
     * @function
     */
    ProgressDialog.prototype.close = function () {
        this._message.remove();
    };

    /**
     * Creates the plugin.
     *
     * @param {String} title Title
     * @param {String} message Message (not required)
     *
     * @return   {ProgressDialog}
     */
    $.spModalProgress = function (title, message) {
        return new ProgressDialog(title, message);
    };
})(jQuery);
