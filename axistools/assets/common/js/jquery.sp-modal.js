/**
 * jQuery.spModal - Modal Window System.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spArgs
 *   3. jQuery.spHtml
 *   5. jQuery.spModalProgress
 *   6. jQuery.spText
 *   7. jQuery.spUload
 *
 * Recommended plugins:
 *   1. jQuery UI Draggable >= 1.8.18
 *
 * @namespace
 * @name      spModal
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    /**
     * Global settings.
     * @var {Object}
     */
    var settings = {
        // default CSS styles
        style: {
            box: {
                'background': '#e8e8e8',
                'border-radius': '12.5pt',
                'box-shadow': '0 3.75pt 12.5pt 0 rgba(0, 0, 0, 0.4)'
            },
            title: {
                'font': 'bold 12.5pt Arial, Helvetica, sans-serif',
                'text-align': 'center',
                'white-space': 'nowrap',
                'line-height': '18.75pt',
                'height': '18.75pt',
                'background-color': '#790000',
                'color': '#FFFFFF',
                'padding': '0 12.5pt',
                'cursor': 'pointer'
            },
            text: {
                'font': '12.5pt Arial, Helvetica, sans-serif',
                'text-align': 'center',
                'color': '#404040',
                'white-space': 'pre-line'
            },
            buttons: {
                'font': '11pt Arial'
            }
        },

        // default labels
        labels: {
            // button labels
            accept: 'Accept',
            cancel: 'Cancel',
            selectText: 'Select text',

            // modal loading text
            loadingText: 'Loading info ...',

            // modal window error
            windowErrorTitle: 'Modal Window Error',
            windowErrorMessage:
                'There was a problem while loading the page.\n' +
                'Probably the page was not found or ' +
                'does not include the jQuery library.',

            // request messages
            requestErrorTitle: 'HTTP Request Error',
            requestErrorMessage: 'The request has failed',
            requestParserErrorTitle: 'The document is not well formed',

            // upload request messages
            uploadRequestTitle: 'Uploading...',
            uploadRequestErrorTitle: 'Upload Error'
        }
    };
    
    /**
     * Gets parent window.
     * 
     * @param {Element} win Window
     * 
     * @return {Element|null}
     */
    function getParentWindow(win) {
        var ret = null;
        
        win.parent.$('iframe').each(function () {
            var iframeWindow = this.contentWindow;
            if (iframeWindow == win) {
                ret = iframeWindow.parent;
            }
        });
        
        return ret;
    }
    
    /**
     * Gets body's zoom.
     * 
     * This function returns a number between 0 and 1.
     * 
     * @return {Number}
     */
    function getZoom(win) {
        if (win === undefined) {
            win = window;
        }
        
        var ret = 1;
        var doc = win.document;
        var cssTransform = $('body', doc).css('transform');
        var regexp = /^matrix\(([0-9\.]+), 0, 0, ([0-9\.]+), 0, 0\)$/;
        var matches = regexp.exec(cssTransform);
        
        if (matches != null && matches[1] == matches[2]) {
            ret = parseFloat(matches[1], 10);
        }
        
        var parent = getParentWindow(win);
        if (parent !== null) {
            ret *= getZoom(parent);
        }
        
        return ret;
    }

    /**
     * <p>Sets the user interface position.</p>
     *
     * <p>This function extracts the position from a given argument and sets up the
     * user interface position.</p>
     *
     * @param {jQuery} ui  User interface
     * @param {Object} arg Argument to parse
     *
     * @return {Void}
     */
    function setUIPosition(ui, arg) {
        var position = {};
        var zoom = getZoom();

        // extracts the position from arg
        $.each(arg, function (name) {
            if (['x', 'y', 'top', 'right', 'bottom', 'left'].indexOf(name) >= 0) {
                var value = $.isNumeric(this)? this + 'px' : '' + this;

                if (name == 'x' || name == 'left') {
                    position.left = value / zoom;
                } else
                if (name == 'y' || name == 'top') {
                    position.top = value / zoom;
                } else
                if (name == 'right') {
                    position.right = value / zoom;
                } else
                if (name == 'bottom') {
                    position.bottom = value / zoom;
                }
            }
        });

        // default left position
        if (position.left === undefined && position.right == undefined) {
            position.left = (($(window).width() - ui.width()) / 2 / zoom) + 'px';
        }

        //  default right position
        if (position.top === undefined && position.bottom == undefined) {
            position.top = (($(window).height() - ui.height()) / 8 / zoom) + 'px';
        }

        ui.css(position);
    }

    /**
     * Gets the iframe container of an User Interface.
     *
     * @param {jQuery} ui User Interface (not required)
     *
     * @return {jQuery|null} iFrame object
     */
    function getIFrameContainer(ui) {
        var ret = null;
        var win = ui ? ui[0].ownerDocument.defaultView : window;

        win.parent.$('iframe').each(function () {
            if (this.contentWindow == win) {
                ret = win.parent.$(this);
                return false;
            }
        });

        return ret;
    }

    /**
     * Selects the text of an HTML element.
     *
     * @param {jQuery} elem HTML Element
     *
     * @return {Void}
     */
    function selectText(elem) {
        var node = elem[0];

        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(node);
            range.select();
        } else
        if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(node);
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);
        }
    }

    /**
     * Sends a request method.
     *
     * The 'options' parameter can have any of the following attributes:
     * ```
     * // loading parameters
     * {String} options.login.text    : Loading text
     * {Object} options.login.buttons : Loading buttons
     *
     * // data type
     * {String} options.dataType : Expected data type
     * ```
     *
     * @param {String} method   Method name: get, post or ajax
     * @param {String} url      Url
     * @param {Object} params   Request parameters
     * @param {String} dataType Data type
     *
     * @return {jQuery.Promise}
     */
    function sendRequest(method, url, params, dataType) {
        var req = null;
        var deferred = new $.Deferred();

        // arguments definition
        var args = $.spArgs(arguments, {
            method: 'string',
            url: 'string',
            params: {
                type: 'object',
                required: false
            },
            dataType: {
                type: 'string',
                required: false
            }
        });

        // allowed methods are 'get' or 'post'
        if (['get', 'post'].indexOf(method) < 0) {
            $.error(
                'Allowed methods are \'get\' or \'post\'; \'' +
                method + '\' was given'
            );
        }

        var loading = $.spModal('loading', {
            buttons: [
                {
                    label: settings.labels.cancel,
                    handler: function () {
                        this.remove();
                        if (req != null) {
                            req.abort();
                        }
                    }
                }
            ]
        });

        // sends a request
        req = $[method].call(
            this,
            args.url,
            args.params,
            function() {},
            args.dataType
        ).always(function () {
            loading.remove();
        }).done(function (data, status, xhr) {
            deferred.resolve(data, status, xhr);
        }).fail(function (xhr, status) {
            // checks for server error
            var message = $.spText('ifEmpty', xhr.responseText, settings.labels.requestErrorMessage);
            var title = status == 'parsererror'
                ? settings.labels.requestParserErrorTitle
                : settings.labels.requestErrorTitle;

            $.spModal('error', title, message, {
                pre: true,
                buttons: [
                    {
                        label: settings.labels.accept,
                        handler: function () {
                            this.remove();
                            deferred.reject(xhr, status);
                        }
                    },
                    {
                        label: settings.labels.selectText,
                        handler: function () {
                            var text = $('.jquery-modal-text', this);
                            selectText(text);
                        }
                    }
                ]
            });
        });

        return deferred.promise();
    }

    /**
     * Sends an 'UPLOAD' request method.
     *
     * The 'options' parameter can have any of the following attributes:
     * ```
     * // loading parameters
     * {String} options.login.text    : Loading text
     * {Object} options.login.buttons : Loading buttons
     *
     * // data type
     * {String} options.dataType : Expected data type
     * ```
     *
     * @param {String} url      Url
     * @param {Object} params   Request parameters
     *
     * @return {jQuery.Promise}
     */
    function uploadRequest(url, params) {
        var req = null;
        var deferred = new $.Deferred();
        var args = $.spArgs(arguments, {
            url: 'string',
            params: {
                type: 'object',
                def: {}
            }
        });

        var progress = $.spModalProgress(settings.labels.uploadRequestTitle);
        progress.onAbort(function () {
            this.close();
            if (req != null) {
                req.abort();
            }
        });

        req = $.spUpload(args.url, args.params, {
            loadend: function () {
                progress.close();
            },
            progress: function (event) {
                var total = $.spText('ifEmpty', event.total, 100);
                var loaded = $.spText('ifEmpty', event.loaded, 0);

                progress.setProgress(loaded / total);
            },
            load: function () {
                deferred.resolve(req.responseText, 'sucess', req);
            },
            error: function () {
                var message = $.spText('ifEmpty', req.responseText, settings.labels.requestErrorMessage);
                var title = settings.labels.uploadRequestErrorTitle;

                $.spModal('error', title, message, {
                    pre: true,
                    buttons: [
                        {
                            label: settings.labels.accept,
                            handler: function () {
                                this.remove();
                                deferred.reject(req, 'error');
                            }
                        },
                        {
                            label: settings.labels.selectText,
                            handler: function () {
                                var text = $('.jquery-modal-text', this);
                                selectText(text);
                            }
                        }
                    ]
                });
            }
        });

        return deferred.promise();
    }

    /**
     * Creates dialog box buttons.
     *
     * This function appends buttons to the dialog footer.
     *
     * @param {jQuery} target  Dialog box
     * @param {Object} buttons Buttons definition
     *
     * @return {Void}
     */
    function makeButtons(target, buttons) {
        var footer = $('.jquery-modal-footer', target);

        $.each(buttons, function (text) {
            var isPlainObject = $.isPlainObject(this);
            var buttonLabel = isPlainObject? this.label : text;
            var buttonHandler = isPlainObject? this.handler : this;
            var buttonFocus = isPlainObject? this.focus : false;

            if (buttonLabel.match(/^\[.*\]$/)) {
                buttonFocus = true;
                buttonLabel = $.spText(
                    'rtrim',$.spText('ltrim', buttonLabel, '['),']'
                );
            }

            var button = $(
                '<input ' +
                    'type="button" ' +
                    'value="' + $.spHtml('encode', buttonLabel) + '" ' +
                '/>'
            );
            button.click($.proxy(buttonHandler, target));
            footer.append(button);

            if (buttonFocus) {
                button.focus();
            }
        });

        // sets up the focus on the first button
        if ($('input:focus', footer).length == 0) {
            $('input:nth(0)', footer).focus();
        }
    }

    /**
     * Applies CSS styles to the User Interface.
     *
     * @param {jQuery} ui User Interface
     *
     * @return {Void}
     */
    function applyStyles(ui) {
        // applies a CSS style to the box
        ui.css(settings.style.box);

        // applies a CSS style to the title
        settings.style.title['border-top-left-radius'] =
            ui.css('border-top-left-radius');
        settings.style.title['border-top-right-radius'] =
            ui.css('border-top-left-radius');
        $('.jquery-modal-title', ui).css(settings.style.title);

        // applies a CSS style to the text
        $('.jquery-modal-text', ui).css(settings.style.text);

        // applies a CSS style to the buttons
        $('.jquery-modal-footer input', ui).css(settings.style.buttons);
    }

    var methods = {
        /**
         * Sets up global settings.
         *
         * Example:
         * ```JavaScript
         * $.spModal('config', {
         *      // CSS styles
         *      style: {
         *          box: {
         *              'background': '#e8e8e8',
         *              'border-radius': '10pt',
         *              'box-shadow': '0 3pt 10pt 0 rgba(0, 0, 0, 0.4)'
         *          },
         *          title: {
         *              'font': 'bold 10pt Arial, Helvetica, sans-serif',
         *              'text-align': 'center',
         *              'white-space': 'nowrap',
         *              'line-height': '15pt',
         *              'height': '15pt',
         *              'background-color': '#790000',
         *              'color': '#FFFFFF',
         *              'padding': '0 10pt'
         *          },
         *          text: {
         *              'font': '10pt Arial, Helvetica, sans-serif',
         *              'text-align': 'center',
         *              'color': '#404040',
         *              'white-space': 'pre-line'
         *          },
         *          buttons: {
         *              'font': '8pt Arial'
         *          }
         *      },
         *
         *      // labels
         *      labels: {
         *          // button labels
         *          accept: 'Accept',
         *          cancel: 'Cancel',
         *          selectText: 'Select text',
         *
         *          // modal loading text
         *          loadingText: 'Loading info ...',
         *
         *          // modal window error
         *          windowErrorTitle: 'Modal Window Error',
         *          windowErrorMessage:
         *              'There was a problem while loading the page.\n' +
         *              'Probably the page was not found or ' +
         *              'does not include the jQuery library.',
         *
         *          // request error tittle
         *          requestErrorTitle: 'HTTP Request Error'
         *      }
         * });
         * ```
         *
         * @param {Object} options Options
         *
         * @return   {jQuery}
         * @name     spModal#config
         * @function
         */
        'config': function (options) {
            $.extend(true, settings, options);
            return this;
        },

        /**
         * Shows a modal window.
         *
         * The 'options' parameter can have any of the following attributes:
         * ```
         * // parameters
         * {Object} options.params : parameters
         *
         * // positioning
         * {number|string} options.x      : left position
         * {number|string} options.y      : top position
         * {number|string} options.top    : top position (equivalent to options.y)
         * {number|string} options.right  : right position
         * {number|string} options.bottom : bottom position
         * {number|string} options.left   : left position (equivalent to options.x)
         * ```
         *
         * Example:
         * ```JavaScript
         * // shows a modal window and pass some parameters to the user interface
         * var ui = $.spModal('window', 'test4.html', {
         *     params: {
         *         name: 'Bonifacio Rodrigues',
         *         age: 58,
         *         sex: 'male'
         *     }
         * });
         * ```
         *
         * @param {String} url     URL of the page
         * @param {Object} options Parameters (not required)
         *
         * @return   {jQuery}
         * @name     spModal#window
         * @function
         */
        'window': function (url, options) {
            // arguments definition
            var args = $.spArgs(arguments, {
                url: 'string',
                options: {
                    type: 'object',
                    def: {}
                }
            });

            // creates an invisible (opacity = 0) modal window
            var target = $(
                '<iframe ' +
                    'class="jquery-modal" ' +
                    'allowtransparency="yes" ' +
                    'scrolling="auto" ' +
                    'frameborder="0" ' +
                    'hspace="0" ' +
                    'vspace="0" ' +
                    'src="' + $.spHtml('encode', args.url) +
                '" />'
            ).css('opacity', 0);
            $('body').append(target);

            // sets up the parameters
            if (args.options.params !== undefined) {
                $.each(args.options.params, function (name, value) {
                    target.data(name, value);
                });
            }

            // shows a modal loading window
            var loading = $.spModal('loading', settings.labels.loadingText, {
                buttons: [
                    {
                        label: settings.labels.cancel,
                        handler: function () {
                            this.remove();
                            target.remove();
                        }
                    }
                ]
            });

            // removes the loading when the modal window is loaded
            target.load(function () {
                loading.remove();
                target.css('opacity', 1);
            });

            return target;
        },

        /**
         * Shows a modal message dialog.
         *
         * The 'options' parameter can have any of the following attributes:
         * ```
         * // text formating
         * {Boolean} options.html  : shows an HTML text
         * {Boolean} options.pre   : shows a preformatted text
         * {String}  options.align : text align. Permitted values are: 'left',
         *                           'center', 'justify' or 'right'
         *
         * // positioning
         * {number|string} options.x      : left position
         * {number|string} options.y      : top position
         * {number|string} options.top    : top position (equivalent to options.y)
         * {number|string} options.right  : right position
         * {number|string} options.bottom : bottom position
         * {number|string} options.left   : left position (equivalent to options.x)
         *
         * // actions handling
         * {Object|Array} options.buttons : buttons
         * ```
         *
         * Example 1:
         * ```JavaScript
         * $.spModal(
         *      'message',
         *      'Please Confirm',
         *      'Are you <strong>SURE</strong>?',
         *      {
         *          html: true,
         *          buttons: {
         *              'Yes please!': function () {
         *                  console.log('Formating hard drive...');
         *                  this.remove();
         *              },
         *              'No way!': function () {
         *                  console.log('I don't care. Formating hard drive...');
         *                  this.remove();
         *              },
         *              'I\'m not sure': function () {
         *                  console.log('Let me decide for you. Formating HD ...');
         *                  this.remove();
         *              }
         *          }
         *      }
         * );
         * ```
         *
         * Example 2: shows a preformatted text
         * ```JavaScript
         * $.spModal(
         *      'message',
         *      'MySQL error',
         *      'Warning: odbc_exec(): SQL error ... preformatted text ...',
         *      {pre: true}
         * );
         * ```
         *
         * @param {String} title   Title
         * @param {String} message Message
         * @param {Object} options Options (not required)
         *
         * @return   {jQuery}
         * @name     spModal#message
         * @function
         */
        'message': function (title, message, options) {
            // arguments definition
            var args = $.spArgs(arguments, {
                title: 'string',
                message: 'string',
                options: {
                    type: 'object',
                    def: {}
                }
            });

            // default values
            if (args.options.pre === undefined) {
                args.options.pre = false;
            }
            if (args.options.html === undefined) {
                args.options.html = false;
            }
            if (args.options.buttons === undefined) {
                var buttonLabel = settings.labels.accept;
                var buttonHandler = function () {this.remove();};
                args.options.buttons = [
                    {
                        label: buttonLabel,
                        handler: buttonHandler
                    }
                ];
            }

            // creates a new message dialog
            var text = args.options.html
                ? $('<div />').html(args.message).html()
                : $.spHtml('encode', args.message);
            var target = $(
                '<div class="jquery-modal">' +
                    '<div class="jquery-modal-ui jquery-modal-message">' +
                        '<div class="jquery-modal-title">' +
                            $.spHtml('encode', args.title) +
                        '</div>' +
                        '<div class="jquery-modal-content">' +
                            '<div class="jquery-modal-text" id="contents">' +
                                $.trim(text) +
                            '</div>' +
                        '</div>' +
                        '<div class="jquery-modal-footer"></div>' +
                    '</div>' +
                '</div>'
            );
            $('body').append(target);

            // sets up the position
            var ui = $('.jquery-modal-ui', target);
            setUIPosition(ui, args.options);

            // makes the dialog draggable
            $('.jquery-modal-message').draggable({handle: '.jquery-modal-title'});

            // pre-formatted text
            var text = $('.jquery-modal-text', target);
            if (args.options.pre) {
                if (!args.options.align) {
                    args.options.align = 'left';
                }
                text.addClass('jquery-modal-pre');
            }

            // set up align
            var text = $('.jquery-modal-text', target);
            if (args.options.align == 'left') {
                text.addClass('jquery-modal-align-left');
            } else
            if (args.options.align == 'center') {
                text.addClass('jquery-modal-align-center');
            } else
            if (args.options.align == 'right') {
                text.addClass('jquery-modal-align-right');
            } else
            if (args.options.align == 'justify') {
                text.addClass('jquery-modal-align-justify');
            } else
            if (args.options.align !== undefined) {
                console.log(
                    'Warning: invalid text align \'' + args.options.align + '\''
                );
            }

            makeButtons(target, args.options.buttons);
            applyStyles(ui);

            return target;
        },

        /**
         * Shows a modal error dialog.
         *
         * This function is identical to $.spModal('message'), except that it
         * also throws an Error.
         *
         * The 'options' parameter can have any of the following attributes:
         * ```
         * // text formating
         * {Boolean} options.html  : shows an HTML text
         * {Boolean} options.pre   : shows a preformatted text
         * {String}  options.align : text align. Permitted values are: 'left',
         *                           'center', 'justify' or 'right'
         *
         * // positioning
         * {number|string} options.x      : left position
         * {number|string} options.y      : top position
         * {number|string} options.top    : top position (equivalent to options.y)
         * {number|string} options.right  : right position
         * {number|string} options.bottom : bottom position
         * {number|string} options.left   : left position (equivalent to options.x)
         *
         * // buttons
         * {Object|Array} options.buttons : buttons
         * ```
         *
         * Example: shows an error message and throws an error
         * ```JavaScript
         * $.spModal('error', 'Error!', 'An error has occurred');
         * ```
         *
         * @param {String} title   Title
         * @param {String} message Message
         * @param {Object} options Options (not required)
         *
         * @return   {Void}
         * @name     spModal#error
         * @throws   Error
         * @function
         */
        'error': function (title, message, options) {
            $.spModal('message', title, message, options);
            $.error(message);
        },

        /**
         * <p>Shows a modal loading dialog.</p>
         *
         * The 'options' parameter can have any of the following attributes:
         * ```
         * // positioning
         * {number|string} options.x      : left position
         * {number|string} options.y      : top position
         * {number|string} options.top    : top position (equivalent to options.y)
         * {number|string} options.right  : right position
         * {number|string} options.bottom : bottom position
         * {number|string} options.left   : left position (equivalent to options.x)
         *
         * // buttons
         * {Object|Array} options.buttons : buttons
         * ```
         *
         * Example:
         * ```JavaScript
         * $.spModal('loading', 'Loading info ...', {
         *     buttons: {
         *         'Cancel': function () {
         *             // removes the loading window
         *             this.remove();
         *         }
         *     }
         * });
         * ```
         *
         * @param {String} text    Text (not required)
         * @param {Object} options Options (not required)
         *
         * @return   {jQuery}
         * @name     spModal#loading
         * @function
         */
        'loading': function (text, options) {
            // arguments definition
            var args = $.spArgs(arguments, {
                text: {
                    type: 'string',
                    def: settings.labels.loadingText
                },
                options: {
                    type: 'object',
                    def: {}
                }
            });

            // creates a new message dialog
            var target = $(
                '<div class="jquery-modal">' +
                    '<div class="jquery-modal-ui jquery-modal-loading">' +
                        '<div class="jquery-modal-icon"></div>' +
                    '</div>' +
                '</div>'
            ).appendTo('body');

            // user interface
            var ui = $('.jquery-modal-ui', target);

            // appends the text to the user interface
            if (args.text !== undefined) {
                ui.append(
                    '<div class="jquery-modal-text">' +
                        $.spHtml('encode', args.text) +
                    '</div>'
                )
            }

            // creates the footer and appends the buttons
            if (!$.isEmptyObject(args.options.buttons)) {
                ui.append(
                    '<div class="jquery-modal-footer"></div>'
                );
                makeButtons(target, args.options.buttons);
            }

            applyStyles(ui);
            setUIPosition(ui, args.options);

            return target;
        },

        /**
         * Gets the root modal window.
         *
         * This function can be used to store global variables. In the following
         * example, the username is accesible from any interface:
         *
         * ```JavaScript
         * var root = $.spModal('root');
         * root.data('username', 'john');
         * ```
         *
         * @return   {jQuery}
         * @name     spModal#root
         * @function
         */
        'root': function () {
            var ret = getIFrameContainer();

            while (iFrame = getIFrameContainer(ret)) {
                ret = iFrame;
            }

            return ret? ret : $();
        },

        /**
         * Gets the parent modal window.
         *
         * @return   {jQuery}
         * @name     spModal#parent
         * @function
         */
        'parent': function () {
            return (ret = getIFrameContainer(getIFrameContainer()))? ret : $();
        },

        /**
         * Gets the current modal window.
         *
         * Example:
         * ```JavaScript
         * // somewhere in the parent interface
         * var ui = $.spModal('window', 'my-custom-interface.html');
         * ui.on('save', function () {
         *     ui.remove();
         *     saveData();
         * }
         *
         * // in the current interface
         * var self = $.spModal('self');
         * $('#my-button-id').click(function () {
         *     self.trigger('save');
         * }
         * ```
         *
         * @return   {jQuery}
         * @name     spModal#self
         * @function
         */
        'self': function () {
            return (ret = getIFrameContainer())? ret : $();
        },

        /**
         * Sends an asynchronous GET request.
         *
         * This function sends an asynchronous GET request and shows a modal
         * loading dialog while waiting for the server response. The user can
         * interrupt the request at any time.
         *
         * Example:
         * ```JavaScript
         * $.spModal('get', 'page.php').done(function () {
         *     console.log('Your request was sent successfully');
         * }).fail(function () {
         *     console.log('Something went wrong');
         * }).always(function () {
         *     console.log('This is always executed');
         * });
         * ```
         *
         * @param {String} url      Url
         * @param {Object} params   Request parameters
         * @param {String} dataType Data Type
         *
         * @return   {jQuery.Promise}
         * @name     spModal#get
         * @function
         */
        'get': function (url, params, dataType) {
            return sendRequest('get', url, params, dataType);
        },

        /**
         * Sends an asynchronous POST request.
         *
         * This function sends an asynchronous POST request and shows a modal
         * loading dialog while waiting for the server response. The user can
         * interrupt the request at any time.
         *
         * Example:
         * ```JavaScript
         * $.spModal('post', 'page.php').done(function () {
         *     console.log('Your request was sent successfully');
         * }).fail(function () {
         *     console.log('Something went wrong');
         * }).always(function () {
         *     console.log('This is always executed');
         * });
         * ```
         *
         * @param {String} url      Url
         * @param {Object} params   Request parameters
         * @param {String} dataType Data Type
         *
         * @return   {jQuery.Promise}
         * @name     spModal#post
         * @function
         */
        'post': function (url, params, dataType) {
            return sendRequest('post', url, params, dataType);
        },

        /**
         * Sends an asynchronous UPLOAD request.
         *
         * This function sends an asynchronous UPLOAD request and shows a modal
         * loading dialog while waiting for the server response. The user can
         * interrupt the request at any time.
         *
         * Example:
         * ```JavaScript
         * $('#input-file').change(function () {
         *     $.spModal('upload', 'upload-page.php', {file: this}).done(function (data, status, xhr) {
         *         console.log('success!');
         *     }).fail(function (xhr, status) {
         *         console.log('The upload process has failed');
         *     }).always(function () {
         *         console.log('This method is always fired at the end');
         *     });
         * });
         * ```
         *
         * @param {String} url      Url
         * @param {Object} params   Request parameters
         * @param {String} dataType Data Type
         *
         * @return   {jQuery.Promise}
         * @name     spModal#upload
         * @function
         */
        'upload': function (url, params, dataType) {
            return uploadRequest(url, params);
        },

        /**
         * Redirects to a url.
         *
         * @param {String} url Url
         *
         * @return   {jQuery}
         * @name     spModal#redirect
         * @function
         */
        'redirect': function (url) {
            window.location.href = url;
            return this;
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.spModal = function (methodName) {
        var ret = null;

        if (typeof methodName == 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            var method = methods[methodName];

            if (typeof method == 'undefined') {
                $.error('Method not found: ' + methodName);
            }

            ret = method.apply(this, args);
        } else {
            ret = methods.init.apply(this, arguments);
        }

        return ret;
    };
    
    /**
     * Initializes the plugin.
     */
    $(function () {
        var self = $.spModal('self');
        var ui = $('.jquery-modal-ui');

        // makes the user interface draggable
        var handle = $('.jquery-modal-draggable', ui);
        if (handle.length > 0 && $.type($.fn.draggable) == 'function') {
            ui.draggable({handle: handle});
        }
    });
})(jQuery);
