/**
 * jQuery.spUri - Some useful uri functions.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spText
 *
 * @namespace
 * @name      spUri
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Plugin methods.
     * @var {Object}
     */
    var methods = {
        /**
         * Encodes an URL.
         *
         * This function is identical to encodeURI.
         *
         * @param {String} url URL
         *
         * @return   {String}
         * @name     spUri#encodeUri
         * @function
         */
        'encodeUri': function (url) {
            return encodeURI(url);
        },

        /**
         * Encodes a URI parameter.
         *
         * This function is identical to encodeURIComponent.
         *
         * Example:
         * ```JavaScript
         * $url = 'http://www.mysite.com/?param1=' + $.spUri('encode', 'my param 1');
         * ```
         *
         * @param {String} param Parameter to be encoded
         *
         * @return   {String}
         * @name     spUri#encode
         * @function
         */
        'encode': function (param) {
            return encodeURIComponent(param);
        },

        /**
         * Decodes an URI parameter.
         *
         * This function is identical to decodeURIComponent.
         *
         * @param {String} param Parameter to be decoded
         *
         * @return   {String}
         * @name     spUri#decode
         * @function
         */
        'decode': function (param) {
            return decodeURIComponent(param);
        },

        /**
         * Parses an URL.
         *
         * Example:
         * ```JavaScript
         * var info = $.spUri(
         *      'parse',
         *      'http://perico:palotes@test.domain.com:1574/path/to/somewhere' +
         *      '?param%201=first%20param&param%202=second%20param#tag'
         * );
         * console.log(info);
         *
         * // the above code shows the following info:
         * //
         * // protocol: "http"
         * // username: "perico"
         * // password: "palotes"
         * // hostname: "test.domain.com"
         * // port    : "1574"
         * // pathname: "/path/to/somewhere"
         * // params  : {param 1: "first param", param 2: "second param"}
         * // hash    : "tag"
         * ```
         *
         * @param {String} url URL
         *
         * @return {Object}
         * @name spUri#encodeUri
         * @function
         */
        'parse': function (url) {
            var parser = document.createElement('a');
            parser.href = url;

            return {
                protocol: $.spText('rtrim', parser.protocol, ':'),
                username: parser.username,
                password: parser.password,
                hostname: parser.hostname,
                port: parser.port, // no existe
                pathname: parser.pathname,
                params: $.spUri('parseQuery', parser.search),
                hash: $.spText('ltrim', parser.hash, '#')
            };
        },

        /**
         * Parses a query string.
         *
         * Example:
         * ```JavaScript
         * console.log($.spUri('parseQuery', location.search));
         * ```
         *
         * @param {String} query Query string
         *
         * @return   {Object}
         * @name     spUri#parseQuery
         * @function
         */
        'parseQuery': function (query) {
            var ret = {};

            query = $.spText('ltrim', query, '?');
            if (!$.spText('empty', query)) {
                var items = query.split('&');

                $.each(items, function () {
                    var items1 = this.split('=');
                    var name = $.spUri('decode', items1[0]);
                    var value = $.spUri(
                        'decode', $.spText('ifEmpty', items1[1], '')
                    );

                    if (!$.spText('empty', name)) {
                        ret[name] = value;
                    }
                });
            }

            return ret;
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.spUri = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
