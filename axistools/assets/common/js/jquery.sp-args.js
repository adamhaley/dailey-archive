/**
 * jQuery.spArgs - A plugin to define optional parameters based on their types.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      spArgs
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Is the value an scalar?
     *
     * @param {*} value Value
     *
     * @return {Boolean}
     */
    function isScalar(value) {
        return (/boolean|number|string/).test($.type(value));
    }

    /**
     * Gets the type of the argument definition.
     *
     * @param {Object} def Argument definition
     *
     * @return {String|Function}
     */
    function getType(def) {
        return $.type(def) == 'object'? def.type : def;
    }

    /**
     * Gets the default value of the argument definition.
     *
     * @param {Object} def Argument definition
     *
     * @return {*}
     */
    function getDefaultValue(def) {
        return  def.def;
    }

    /**
     * Does the type of the value match any of the following list of types?
     *
     * Allowed types are '*', 'scalar' or any of the types returned by the native
     * typeof function.
     *
     * @param {*}      value       Value
     * @param {String} listOfTypes List of types separated by |
     *
     * @return {Boolean}
     */
    function testAgainstListOfTypes(value, listOfTypes) {
        var ret = false;

        $.each(listOfTypes.split('|'), function () {
            var type = $.trim(this);

            if (   type == '*'
                || (type == 'scalar' && isScalar(value))
                || (type == $.type(value))
            ) {
                ret = true;
                return false;
            }
        });

        return ret;
    }

    /**
     * Does the type of the value match the argument definition?
     *
     * Is the value an "instance of" the argument definition?
     *
     * @param {*}             value Value
     * @param {Object|String} def   Argument definition
     *
     * @return {Boolean}
     */
    function test(value, def) {
        return testAgainstListOfTypes(value, getType(def));
    }

    /**
     * Is the argument definition required?
     *
     * @param {Object|String} def Argument definition
     *
     * @return {Boolean}
     */
    function isRequired(def) {
        var ret = true;

        if ($.type(def) == 'object') {
            if (def.required !== undefined) {
                ret = def.required;
            } else {
                ret = !def.hasOwnProperty('def');
            }
        }

        return ret;
    }

    /**
     * Defines 'optional parameters'.
     *
     * For example:
     * ```JavaScript
     * function test(title, position, options) {
     *      // title is a required string
     *      // position is a non-required number and the default value is 100
     *      // options is not required and can be either an object or a string
     *      var args = $.spArgs(arguments, function () {
     *          title: 'string',
     *          position: {
     *              type: 'number',
     *              def: 100
     *          },
     *          options: {
     *              type: 'object|string',
     *              required: false
     *          }
     *      });
     *
     *      // do not use the passed parameters
     *      // use the 'args' variable instead
     *      console.log(args.title, args.position, args.options);
     * }
     *
     * // throws an error because 'title' is required
     * test();
     *
     * // args.position is '100' and args.options is 'undefined'
     * test('Title...');
     *
     * // args.position is '100' and args.options is '{color: 'black'}'
     * test('Title...', {color: 'black'});
     *
     * // args.position is '100' and args.options is '#ff00ff'
     * test('Title...', '#ff00ff');
     *
     * // args.position is '202' and args.options is '{color: 'black'}'
     * test('Titlte...', 202, {color: 'black'});
     * ```
     *
     * @param {Object} params Parameters
     * @param {Object} defs   Arguments definition
     *
     * @return   {Object}
     * @name     spArgs#main
     * @function
     */
    $.spArgs = function (params, defs) {
        var ret = {};
        var args = Array.prototype.slice.call(params);

        // compares the arguments against the definitions
        var value = args.shift();
        if (value !== undefined) {
            var paramName = '';
            var paramValue = null;

            $.each(defs, function (name) {
                paramName = name;
                paramValue = this;

                if (test(value, this)) {
                    ret[name] = value;

                    // next value
                    value = args.shift();
                    if (value === undefined) {
                        return false;
                    }
                } else
                if (isRequired(this)) {
                    $.error(
                        'The parameter \'' + name + '\' was expected to be \'' +
                        getType(this) + '\', \'' + $.type(value) + '\' was given'
                    );
                }
            });
        }

        // required arguments
        $.each(defs, function (name) {
            if (ret[name] === undefined) {
                if (isRequired(this)) {
                    $.error('The parameter \'' + name + '\' is required');
                } else {
                    ret[name] = getDefaultValue(this);
                }
            }
        });

        return ret;
    };
})(jQuery);
