/**
 * Require plugin configuration.
 */
$.require('config', {
    libraries: {
        'html2canvas': 'vendor/html2canvas/build/html2canvas.min.js',
        'jquery.browser': 'vendor/jquery.browser/dist/jquery.browser.min.js',
        'jquery.axis-calendar': {
            sources: {
                js: ['assets/common/js/jquery.axis-calendar.js'],
                css: ['assets/common/css/jquery.axis-calendar.css']
            },
            requires: ['jquery.axis-sugar-pos', 'jquery.axis-date']
        },
        'jquery.axis-color-picker': {
            sources: {
                js: ['assets/common/js/jquery.axis-color-picker.js'],
                css: ['assets/common/css/jquery.axis-color-picker.css']
            },
            requires: ['jquery.sp-text']
        },
        'jquery.axis-date': 'assets/common/js/jquery.axis-date.js',
        'jquery.axis-garbage': {
            sources: ['assets/common/js/jquery.axis-garbage.js'],
            requires: ['jquery.axis', 'jquery.sp-text']
        },
        'jquery.axis-item-selector': {
            sources: {
                js: ['assets/common/js/jquery.axis-item-selector.js'],
                css: ['assets/common/css/jquery.axis-item-selector.css']
            },
            requires: ['jquery.axis-sugar-pos', 'jquery.sp-html']
        },
        'jquery.axis-prompt': {
            sources: ['assets/common/js/jquery.axis-prompt.js'],
            requires: ['jquery.sp-text', 'jquery.sp-args', 'jquery.sp-modal']
        },
        'jquery.axis-tree-navigator': {
            sources: {
                js: ['assets/common/js/jquery.axis-tree-navigator.js'],
                css: ['assets/common/css/jquery.axis-tree-navigator.css']
            },
            requires: ['jquery.axis', 'jquery.axis-sugar-pos']
        },
        'jquery.axis-sugar-pos': 'assets/common/js/jquery.axis.sugar-pos.js',
        'jquery.axis': {
            sources: {
                js: ['assets/common/js/jquery.axis.js'],
                css: ['assets/common/css/jquery.axis.css']
            },
            requires: ['jquery.axis-prompt', 'jquery.axis-garbage']
        },
        'jquery.sp-args': 'assets/common/js/jquery.sp-args.js',
        'jquery.sp-cookie': 'assets/common/js/jquery.sp-cookie.js',
        'jquery.sp-dragger': {
            sources: ['assets/common/js/jquery.sp-dragger.js'],
            requires: ['jquery.browser', 'jquery.sp-text', 'jquery.sp-text-selection']
        },
        'jquery.sp-html-parser': 'assets/common/js/jquery.sp-html-parser.js',
        'jquery.sp-html': {
            sources: ['assets/common/js/jquery.sp-html.js'],
            requires: ['jquery.sp-text', 'jquery.sp-html-parser']
        },
        'jquery.sp-modal-progress': {
            sources: ['assets/common/js/jquery.sp-modal-progress.js'],
            requires: ['jquery.sp-html', 'jquery.sp-text', 'jquery.sp-modal']
        },
        'jquery.sp-modal': {
            sources: {
                js: ['assets/common/js/jquery.sp-modal.js'],
                css: ['assets/common/css/jquery.sp-modal.css']
            },
            requires: [
                'jquery.sp-args',
                'jquery.sp-html',
                'jquery.sp-modal-progress',
                'jquery.sp-text',
                'jquery.sp-upload'
            ]
        },
        'jquery.sp-reg': 'assets/common/js/jquery.sp-reg.js',
        'jquery.sp-request': {
            sources: ['assets/common/js/jquery.sp-request.js'],
            requires: ['jquery.sp-uri']
        },
        'jquery.sp-slider': {
            sources: ['assets/common/js/jquery.sp-slider.js'],
            requires: ['jquery.browser', 'jquery.sp-text', 'jquery.sp-timer']
        },
        'jquery.sp-text-selection': 'assets/common/js/jquery.sp-text-selection.js',
        'jquery.sp-text': {
            sources: ['assets/common/js/jquery.sp-text.js'],
            requires: ['jquery.sp-reg']
        },
        'jquery.sp-timer': 'assets/common/js/jquery.sp-timer.js',
        'jquery.sp-upload': {
            sources: ['assets/common/js/jquery.sp-upload.js'],
            requires: ['jquery.sp-args', 'jquery.sp-timer']
        },
        'jquery.sp-uri': {
            sources: ['assets/common/js/jquery.sp-uri.js'],
            requires: ['jquery.sp-text']
        }
    }
});
