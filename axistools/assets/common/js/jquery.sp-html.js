/**
 * jQuery.spHtml - Some useful HTML functions.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spText
 *   3. jQuery.spHtmlParser
 *
 * @namespace
 * @name      spHtml
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Plugin methods.
     * @var {Object}
     */
    var methods = {
        /**
         * Converts special characters to HTML entities.
         *
         * @param {String} text Text
         *
         * @return   {String}
         * @name     spHtml#encode
         * @function
         */
        'encode': function (text) {
            var map = {'&': 'amp', '<': 'lt', '>': 'gt', '"': 'quot', '\'': '#39'};

            return $.spText('empty', text)? '': ('' + text).replace(/[&<>"']/g, function(match) {
                return '&' + map[match] + ';';
            });
        },

        /**
         * Converts HTML to plaintext.
         *
         * @param {String} html HTML text
         *
         * @return   {String}
         * @name     spHtml#decode
         * @function
         */
        'plainText': function (html) {
            return $('<div />').html(html).text();
        },

        /**
         * Fixes a bad-formed HTML document.
         *
         * Example:
         * ```JavaScript
         * $html = $.htmlParser('<p>Bad formed<br>; html document');
         * ```
         *
         * @param {String} html HTML text
         *
         * @return   {String}
         * @name     spHtml#fix
         * @function
         */
        'fix': function (html) {
            return $.spHtmlParser(html);
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.spHtml = function (methodName) {
        var method = methods[methodName];
        var args = Array.prototype.slice.call(arguments, 1);

        // check for required plugins
        if ($.spHtmlParser === undefined) {
            //$.error('$.spHtml requires the following plugins: $.spText, $.spHtmlParser');
        }

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
