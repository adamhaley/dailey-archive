/**
 * jQuery.spRequest - A plugin for getting URL parameters
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spUri
 *
 * @namespace
 * @name      spRequest
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Plugin methods.
     * @var {Object}
     */
    var methods = {
        /**
         * Gets a request parameter.
         *
         * @param {String} name Parameter name
         * @pram  {String} def  Default value
         *
         * @return   {String}
         * @name     spRequest#get
         * @function
         */
        'get': function (name, def) {
            var params = $.spUri('parseQuery', location.search);

            return params[name] !== undefined? params[name] : def;
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.spRequest = function (methodName) {
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // check for required plugins
        if ($.spUri === undefined) {
            $.error(
                '$.spRequest requires the following plugins: $.spUri'
            );
        }

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
