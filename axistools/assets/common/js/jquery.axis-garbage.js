/**
 * jQuery.axisGarbage - A garbage collector plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spText
 *
 * @namespace
 * @name      axisGarbage
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    var _filenames = [];

    var methods = {
        /**
         * Adds files.
         *
         * Example:
         * ```JavaScript
         * $.axisGarbage('add', '/path/to/temporary/file');
         * ```
         *
         * @param {String|Array.<String>} filename Relative path to file
         *
         * @return   {jQuery}
         * @name     axisGarbage#add
         * @function
         */
        'add': function (filename) {
            var filenames = $.type(filename) == 'array'? filename : [filename];

            $.each(filenames, function () {
                var filename = $.spText('ltrim', $.spText('trim', '' + this), '/');

                // removes parameters
                var pos = filename.indexOf('?');
                if (pos >= 0) {
                    filename = filename.substr(0, pos);
                }

                // adds filename
                if (!$.spText('empty', filename) && _filenames.indexOf(filename) < 0) {
                    _filenames.push(filename);
                }
            });

            return this;
        },

        /**
         * Removes all unused files.
         *
         * Example:
         * ```JavaScript
         * $.axisGarbage('purge', function () {
         *      console.log('all unused files were removed');
         * });
         * ```
         *
         * @param {Function} callback Callback function (not required)
         *
         * @return   {jQuery}
         * @name     axisGarbage#purge
         * @function
         */
        'purge': function (callback) {
            if (_filenames.length > 0) {
                $.post(
                    $.axis('absPath', 'file-delete.php'),
                    {files: _filenames}
                ).always(function () {
                    _filenames = [];

                    if (callback !== undefined) {
                        callback();
                    }
                });
            } else if (callback !== undefined) {
                callback();
            }

            return this;
        }
    };

    /**
     * Creates the plugin.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.axisGarbage = function (methodName) {
        var ret = null;
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        // executes an specific method
        return method.apply(this, args);
    };
})(jQuery);
