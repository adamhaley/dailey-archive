/**
 * jQuery.spCookie - A plugin for managing HTTP cookies.
 *
 * Kindly provided by TheSiteWizard and slightly modified by me:
 * [How to Use Cookies in JavaScript](http://www.thesitewizard.com/javascripts/cookies.shtml)
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spArgs
 *   3. jQuery.spReg
 *   4. jQuery.spUri
 *
 * @namespace
 * @name      spCookie
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    /**
     * Plugin methods.
     * @var {Object}
     */
    var methods = {
        /**
         * Gets a cookie.
         *
         * Kindly provided by TheSiteWizard and slightly modified by me:
         * [How to Use Cookies in JavaScript](http://www.thesitewizard.com/javascripts/cookies.shtml)
         *
         * @param {String} name Cookie name
         *
         * @return   {String}
         * @name     spCookie#get
         * @function
         */
        'get': function (name) {
            var ret = '';
            var cookies = document.cookie ;

            if (cookies.length != 0) {
                var value = cookies.match(
                    '(^|;)[\\s]*' +
                    $.spReg('encode', $.spUri('encode', name)) +
                    '=([^;]*)'
                );
                if (value !== null) {
                    ret = decodeURIComponent(value[2]);
                }
            }

            return ret;
        },

        /**
         * Sets a cookie.
         *
         * @param {String} name  Cookie name
         * @param {String} value Cookie value
         * @param {Number} days  Expiration in days (default is 30 days)
         *
         * @return   {jQuery}
         * @name     spCookie#set
         * @function
         */
        'set': function (name, value, days) {
            var args = $.spArgs(arguments, {
                name: 'string',
                value: '*',
                days: {
                    type: 'number',
                    def: 30
                }
            });

            document.cookie =
                $.spUri('encode', args.name) + "=" +
                $.spUri('encode', args.value) +
                "; max-age=" + (60 * 60 * 24 * args.days) +
                "; path=/";

            return this;
        },

        /**
         * Deletes a cookie.
         *
         * @param {String} name Cookie name
         *
         * @return   {jQuery}
         * @name     spCookie#del
         * @function
         */
        'del': function (name) {
            document.cookie = $.spUri('encode', name) + "=; max-age=0; path=/";
            return this;
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {jQuery}
     */
    $.spCookie = function (methodName) {
        var ret = null;
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[methodName];

        // does the method exist?
        if (method === undefined) {
            $.error('Method not found: ' + methodName);
        }

        // executes an specific method
        return method.apply(this, args);
    };
})(jQuery);
