/**
 * jQuery.spUpload - A plugin for uploading data.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spArgs
 *
 * @namespace
 * @name      spUpload
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @copyright AxisGraphics, Inc. All Right Reserved.
 * @license   Proprietary/Closed Source
 */
(function ($) {

    /**
     * Uploads data to the server.
     *
     * Example 1:
     * ```JavaScript
     * $('input#file').change(function () {
     *      var req = $.spUpload('/controller', {file: this, name: 'file.txt', id: 101}, function () {
     *          console.log('Upload success!');
     *      });
     * });
     * ```
     *
     * Example 2:
     * ```JavaScript
     * $('input#file').change(function () {
     *     var req = $.spUpload('/controller', {file: this, name: 'file.txt', id: 101}, {
     *         loadstart: function () {
     *             console.log('This handler is fired at the beggining');
     *         },
     *         progress: function () {
     *             console.log('Upload progress...');
     *         },
     *         load: function () {
     *             console.log('Upload success!');
     *         },
     *         abort: function () {
     *             console.log('The upload has been aborted');
     *         },
     *         error: function () {
     *             console.log('Upload error');
     *         },
     *         timeout: function () {
     *             console.log('Upload timeout');
     *         },
     *         loadend: function () {
     *             console.log('This handler is always fired at the end');
     *         }
     *     });
     * });
     * ```
     *
     * @param {String}          url      URL
     * @param {Object}          params   Parameters (not required)
     * @param {Object|Function} handlers Handlers (not required)
     *
     * @return {XMLHttpRequest}
     * @name   spUpload#main
     */
    $.spUpload = function (url, params, handlers) {
        // arguments
        var args = $.spArgs(arguments, {
            url: 'string',
            params: {
                type: 'object',
                def: {}
            },
            handlers: {
                type: 'object|function',
                required: false
            }
        });

        // initializes data object
        var data = new FormData();
        $.each(args.params, function (key, value) {
            if (value instanceof jQuery) {
                // gets the HTMLElement instance
                value = value[0];
            }

            if ($.type(value) == 'object'
                && $.type(value.tagName) == 'string'
                && value.tagName.toLowerCase() == 'input'
            ) {
                var files = value.files;

                if (files[0] !== undefined) {
                    // gets the File instance
                    value = files[0];
                }
            }

            data.append(key, value);
        });

        // sends data object
        var req = new XMLHttpRequest();
        if ($.type(args.handlers) == 'object') {
            $.each(args.handlers, function (key, handler) {
                req.upload.addEventListener(key, function (event) {
                    $.spTimer(0, function () {
                        this.setDelay(500);
                        if (req.readyState == 4) {
                            this.stop();
                            $.proxy(handler, req.upload, event)();
                        }
                    }).start();
                }, false);
            });
        } else
        if ($.type(args.handlers) == 'function') {
            req.upload.addEventListener('load', args.handlers, false);
        }
        req.open('POST', url, true);
        req.send(data);

        return req;
    };
})(jQuery);
