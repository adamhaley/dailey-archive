/**
 * jQuery.axisSugarPos - 'Sugar' position plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      axisSugarPos
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @copyright AxisGraphics, Inc. All Right Reserved.
 * @license   Proprietary/Closed Source
 */
(function ($) {
    /**
     * Aligns target relative to sugar.
     *
     * @param {jQuery} sugar    Sugar target
     * @param {jQuery} target   Target
     * @param {String} position Relative position
     *
     * @return {jQuery}
     */
    function SugarPos(sugar, target, position) {
        this._sugar = sugar;
        this._target = target;
        this._horSugarPosition = '';
        this._horTargetPosition = '';
        this._verSugarPosition = '';
        this._verTargetPosition = '';

        this._target.css('position', 'absolute');
        this._parsePosition(position);
        this._updatePosition();
    }

    /**
     * Parses position.
     *
     * @param {String} position Position
     *
     * @return {Void}
     */
    SugarPos.prototype._parsePosition = function (position) {
        // parses position
        var items = position.match(/^(left|right)\-(left|right)\s+(bottom|top)\-(bottom|top)$/);
        if (items == null) {
            $.error('Invalid position format. Valid formats are: ' +
                '\'(left|right)-(left|right) (bottom|top)-(bottom|top)\'');
        }

        this._horSugarPosition = items[1];
        this._horTargetPosition = items[2];
        this._verSugarPosition = items[3];
        this._verTargetPosition = items[4];
    };

    /**
     * Updates target position.
     *
     * @return {Void}
     */
    SugarPos.prototype._updatePosition = function () {
        var offset = this._sugar.offset();

        // vertical sugar line
        var verSugarLine = offset.left;
        if (this._horSugarPosition == 'right') {
            verSugarLine += this._sugar.outerWidth();
        }

        // horizontal sugar line
        var horSugarLine = offset.top;
        if (this._verSugarPosition == 'bottom') {
            horSugarLine += this._sugar.outerHeight();
        }

        // left target position
        var left = verSugarLine;
        if (this._horTargetPosition == 'right') {
            left -= this._target.outerWidth();
        }

        // top target position
        var top = horSugarLine;
        if (this._verTargetPosition == 'bottom') {
            top -= this._target.outerHeight();
        }

        this._target.css({left: left, top: top});
    };

    /**
     * Aligns target relative to sugar.
     *
     * The 'position' parameter has the following format:
     *
     * 'sugarHorizontalPos-targetHorizontalPos sugarVerticalPos-targetHorizontalPost'
     *
     * Where sugarHorizontalPos/targetHorizontalPos can be 'left' or 'right' and
     * sugarVerticalPos/targetHorizontalPos can be 'top' or 'buttom'. For example:
     *
     * ```JavaScript
     * $.axisSugarPos(sugar, target, 'left-left bottom-top');
     * ```
     *
     * In the previous example the left side of 'sugar' is aligned along the left side of 'target', and
     * the bottom side of 'sugar' is aligned along the top side of 'target'.
     *
     * @param {jQuery} sugar    Sugar target
     * @param {jQuery} target   Target
     * @param {String} position Relative position
     *
     * @return   {SugarPos}
     * @name     axisSugarPos#main
     * @function
     */
    $.axisSugarPos = function (sugar, target, position) {
        return new SugarPos(sugar, target, position);
    };
})(jQuery);
