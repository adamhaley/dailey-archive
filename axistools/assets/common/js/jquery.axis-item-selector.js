/**
 * jQuery.axisItemSelector plugin - Item selector plugin.
 *
 * This plugin creates a table with selectable items. The table is aligned relative to the target and
 * is showed anytime the user clicks on the target.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      axisItemSelector
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    /**
     * Default options.
     * @var {Object}
     */
    var _defaultOptions = {
        numCols: 7,
        numItems: 24,
        iterator: function (i) {
            return i;
        }
    }

    /**
     * Class ItemSelector.
     *
     * @param {HTMLElement} target   Target
     * @param {Number}      numCols  Number of columns
     * @param {Number}      numItems Number of items
     * @param {Function}    iterator Items callback
     */
    function ItemSelector(target, numCols, numItems, iterator) {
        /**
         * Target.
         * @var {jQuery}
         */
        this._target = ItemSelector.setInstance(target, this);

        /**
         * Selector object.
         * @var {jQuery}
         */
        this._component = null;

        /**
         * Number of columns.
         * @var {Number}
         */
        this._numCols = numCols;

        /**
         * Number of items.
         * @var {Number}
         */
        this._numItems = numItems;

        /**
         * Items iterator.
         * @var {Function}
         */
        this._iterator = iterator;

        /**
         * Gets the selected index.
         * @var {Number}
         */
        this._selectedIndex = -1;

        this._make();
    }

    /**
     * Gets instance.
     *
     * @param {HTMLElement} target Target
     *
     * @return {?Object}
     */
    ItemSelector.getInstance = function (target) {
        return $(target).data('itemSelectorInstance');
    };

    /**
     * Sets instance.
     *
     * @param {HTMLElement} target   Target
     * @param {Object}      instance Instance
     *
     * @return {jQuery}
     */
    ItemSelector.setInstance = function (target, instance) {
        return $(target).data('itemSelectorInstance', instance);
    };

    /**
     * Makes the component.
     *
     * @var {Void}
     */
    ItemSelector.prototype._make = function () {
        this._component = $('<table />').addClass('axis-item-selector').appendTo('body').hide();
        this._makeActions();
        this._update();
    };

    /**
     * Makes actions.
     *
     * @var {Void}
     */
    ItemSelector.prototype._makeActions = function () {
        var self = this;

        // hides the component when the user clicks outside
        $(window).on('blur mousedown', function () {
            if (!self._target.is(':hover') && !self._component.is(':hover')) {
                self._component.hide();
            }
        });
    };

    /**
     * Updates the component.
     *
     * @var {Void}
     */
    ItemSelector.prototype._update = function () {
        this._component.empty();

        // fills table
        var tr = $('<tr />').appendTo(this._component);
        var numRows = Math.floor(this._numItems / this._numCols);
        if (this._numItems % this._numCols) {
            numRows = numRows + 1;
        }

        for (var i = 0, cols = 1; i < numRows * this._numCols; i++, cols++) {
            var outOfBounds = i > this._numItems - 1;
            var text = outOfBounds? '&nbsp;' : $.spHtml('encode', $.proxy(this._iterator, this)(i));

            if (cols > this._numCols) {
                tr = $('<tr />').appendTo(this._component);
                cols = 1;
            }

            tr.append(
                $('<td />').append(
                    $('<a href="#" />')
                        .toggleClass('disabled', outOfBounds)
                        .data('index', i)
                        .html(text)
                        .click($.proxy(this._onItemClick, this))
                )
            );
        }

        // updates component position
        $.axisSugarPos(this._target, this._component, 'left-left bottom-top');
    };

    /**
     * Dispatched when the user 'clicks' an item.
     *
     * @param {Object} event Event
     *
     * @return {Void}
     */
    ItemSelector.prototype._onItemClick = function (event) {
        var target = $(event.target);

        if (!target.hasClass('disabled')) {
            this._selectedIndex = target.data('index');
            this._target.trigger('item-selector-change');
            this._component.hide();
        }

        target.blur();
        return false;
    };

    /**
     * Handles events.
     *
     * @param {String}   eventType Event name
     * @param {Function} handler   Event handler
     *
     * @return {jQuery}
     */
    ItemSelector.prototype.on = function (eventType, handler) {
        return this._target.on(eventType, handler);
    };

    /**
     * Gets the selected index.
     *
     * @return {Number}
     */
    ItemSelector.prototype.getSelectedIndex = function () {
        return this._selectedIndex;
    };

    /**
     * Is the component visible?
     *
     * @return {Boolean}
     */
    ItemSelector.prototype.isVisible = function () {
        return this._component.is(':visible');
    };

    /**
     * Sets component visibility.
     *
     * @param {Boolean} value Is the component visible?
     *
     * @return {Void}
     */
    ItemSelector.prototype.setVisible = function (value) {
        this._component.toggle(value);
        this._update();
    };

    var methods = {
        /**
         * Initializes the plugin.
         *
         * For example:
         * ```JavaScript
         * $('#button').axisItemSelector{numCols: 7, numItems: 24, iterator: function (value) {
         *    // returns a normalized value
         *    return $.spText('normalize', value, 2);
         * });
         * ```
         *
         * @param {Number}   options.numCols  Number of columns
         * @param {Number}   options.numItems Number of items
         * @param {Function} options.iterator Iterator function
         *
         * @return   {jQuery}
         * @name     axisItemSelector#init
         * @function
         */
        'init': function (options) {
            options = $.extend({}, _defaultOptions, options);

            return this.each(function () {
                new ItemSelector(this, options.numCols, options.numItems, options.iterator);
            });
        },

        /**
         * Dispatched when the user clicks an item.
         *
         * For example:
         * ```JavaScript
         * $('#button').axisItemSelector('change', function () {
         *      var target = $(this);
         *      var index = $.axisItemSelector('index');
         *
         *      console.log('The user has clicked the item ' + index);
         * });
         * ```
         *
         * @return   {jQuery}
         * @name     axisItemSelector#change
         * @function
         */
        'change': function () {
            var args = Array.prototype.slice.call(arguments);
            args.unshift('item-selector-change');

            return this.each(function () {
                var selector = ItemSelector.getInstance(this);

                selector.on.apply(selector, args);
            });
        },

        /**
         * Gets the selected index.
         *
         * @return   {Number}
         * @name     axisItemSelector#index
         * @function
         */
        'index': function () {
            var ret = 0;

            this.each(function () {
                var selector = ItemSelector.getInstance(this);

                ret = selector.getSelectedIndex();
                return false;
            });

            return ret;
        },

        /**
         * Hides or shows the component.
         *
         * @return   {jQuery}
         * @name     axisItemSelector#toggle
         * @function
         */
        'toggle': function () {
            return this.each(function () {
                var selector = ItemSelector.getInstance(this);

                selector.setVisible(!selector.isVisible());
            });
        }
    };

    /**
     * This plugin allows selecting an item from a popup table.
     *
     * @param {Number}   numCols  Number of columns
     * @param {Number}   numItems Number of items
     * @param {Function} items to show
     *
     * @return {jQuery}
     */
    $.fn.axisItemSelector = function (methodName) {
        var method = methods['init'];
        var args = arguments;

        if ($.type(methodName) == 'string') {
            method = methods[methodName];
            args = Array.prototype.slice.call(arguments, 1);
        }

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
