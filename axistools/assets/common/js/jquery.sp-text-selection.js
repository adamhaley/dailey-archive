/**
 * jQuery.spTextSelection - A plugin for disabling/restoring texts selection behaviour.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *
 * @namespace
 * @name      spTextSelection
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {
    /**
     * Class TextSelection.
     *
     * @param {HTMLElement} targetElement Element to be dragged
     */
    function TextSelection(targetElement) {
        /**
         * Target object.
         * @var {jQuery}
         */
        this._target = TextSelection.saveInstance(targetElement, this);

        this._make();
    }

    /**
     * Retrieves the instance from an HTML element.
     *
     * @param {HTMLElement} element Target
     *
     * @return {VerticalSlider}
     */
    TextSelection.retrieveInstance = function (element) {
        return $(element).data('text-selection-instance');
    };

    /**
     * Saves an instance into an HTML element.
     *
     * @param {HTMLElement} target   Target
     * @param {Dragger}     instance Dragger instance
     *
     * @return {jQuery}
     */
    TextSelection.saveInstance = function (target, instance) {
        return $(target).data('text-selection-instance', instance);
    };

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    TextSelection.prototype._make = function () {
        var self = this;
        var items = [this._target, $('*', this._target)];

        $.each(items, function () {
            // saves the attributes before changing them
            this.each(function () {
                var target = $(this);
                var attrs = {};

                $.each(this.attributes, function () {
                    if ($.type(this.value) == 'string') {
                        attrs[this.name] = this.value;
                    }
                });

                target.data('text-selection-attributes', attrs);
            });

            // disables text selection and default system dragging
            this
                .attr('unselectable', 'on')
                .css({'user-select': 'none', 'user-drag': 'none', 'cursor': 'default'})
                .on('selectstart', $.proxy(self._onSelectStart, self));
        });
    };

    /**
     * Disables 'selectstart' event.
     *
     * @return {Boolean}
     */
    TextSelection.prototype._onSelectStart = function () {
        return false;
    };

    /**
     * Restores text selection and system dragging.
     *
     * @return {Void}
     */
    TextSelection.prototype.restore = function () {
        var self = this;
        var items = [this._target, $('*', this._target)];

        $.each(items, function () {
            this.each(function () {
                var target = $(this);
                var attrs = target.data('text-selection-attributes');

                if (attrs !== undefined) {
                    // removes all attributes that are not present in 'attrs'
                    $.each($.grep(this.attributes, function (attr) {
                        return attrs[attr.name] === undefined;
                    }), function () {
                        target.removeAttr(this.name);
                    });

                    // restores old attributes
                    $.each(attrs, function (name, value) {
                        target.attr(name, value);
                    });

                    target.off('selectstart', self._onSelectStart);
                }
            });
        });
    };

    var methods = {
        /**
         * Disables text selection and system dragging.
         *
         * Example:
         * ```JavaScript
         * $('p').spTextSelection('disable');
         * ```
         *
         * @return   {jQuery}
         * @name     spTextSelection#disable
         * @function
         */
        'disable': function () {
            return this.each(function () {
                new TextSelection(this);
            });
        },

        /**
         * Restores previous behaviour.
         *
         * Example:
         * ```JavaScript
         * $('p').spTextSelection('restore');
         * ```
         *
         * @return   {jQuery}
         * @name     spTextSelection#restore
         * @function
         */
        'restore': function () {
            return this.each(function () {
                var textSelection = TextSelection.retrieveInstance(this);

                if (textSelection !== undefined) {
                    textSelection.restore();
                }
            });
        }
    };

    $.fn.spTextSelection = function (methodName) {
        var method = methods['init'];
        var args = arguments;

        if ($.type(methodName) == 'string') {
            method = methods[methodName];
            args = Array.prototype.slice.call(arguments, 1);
        }

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);
