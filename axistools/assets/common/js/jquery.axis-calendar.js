/**
 * jQuery.axisCalendar - Calendar plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.axisDate
 *   3. jQuery.axisSugarPos
 *
 * @namespace
 * @name      axisCalendar
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @copyright AxisGraphics, Inc. All Right Reserved.
 * @license   Proprietary/Closed Source
 */
(function ($) {

    /**
     * Constructor.
     *
     * The component is aligned against the 'sugar' target using
     * the 'position' argument (see the plugin $.axisSugarPos for more info).
     *
     * Example:
     * ```JavaScript
     * // creates a calendar and aligns it
     * var calendar = $.axisCalendar($(this), 'left-left bottom-top');
     * ```
     *
     * @param {jQuery} sugar Sugar target
     * @param {String} position
     *
     * @name     axisCalendar#Calendar
     * @function
     */
    function Calendar(sugar, position) {
        /**
         * Current target.
         * @var {jQuery}
         */
        this._target = null;

        /**
         * Sugar target.
         * @var {jQuery}
         */
        this._sugar = sugar;

        /**
         * 'Sugar' position (see the plugin $.axisSugarPos for more info)
         * @var {String}
         */
        this._position = position;

        /**
         * Selected date.
         * @var {Date}
         */
        this._selectedDate = null;

        /**
         * The date to show in the calendar.
         * @var {Date}
         */
        this._calendarDate = new Date();

        this._make();
    }

    /**
     * Makes the component.
     *
     * @return {Void}
     */
    Calendar.prototype._make = function () {
        this._target = $(
            '<div class="axis-calendar">' +
                '<div class="header">' +
                    '<a href="#" title="Previous month" class="prev"><span>Previous month</span></a>' +
                    '<a href="#" class="current"></a>' +
                    '<a href="#" title="Next month" class="next"><span>Next month</span></a>' +
                '</div>' +
                '<div class="days">' +
                    '<table>' +
                        '<thead>' +
                            '<tr>' +
                                '<td>Sun</td>' +
                                '<td>Mon</td>' +
                                '<td>Tue</td>' +
                                '<td>Wed</td>' +
                                '<td>Thu</td>' +
                                '<td>Fri</td>' +
                                '<td>Sat</td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>' +
                '</div>' +
                '<a href="#" class="select-date">Select this date</a>' +
            '</div>'
        ).appendTo('body');

        this._makeActions();
        this._update();
    };

    /**
     * Makes actions.
     *
     * @return {Void}
     */
    Calendar.prototype._makeActions = function () {
        var self = this;

        // shows or hides the component when the users clicks on the button
        $(this._sugar).mousedown(function () {
            self._target.toggle(self._target.is(':hidden'));
        });

        // closes the component when the users clicks outside it
        $(window).mousedown(function () {
            if (!self._sugar.is(':hover') && !self._target.is(':hover')) {
                self._target.hide();
            }
        });

        // goes to the previous month
        $('.prev', this._target).click(function () {
            self._calendarDate.setMonth(self._calendarDate.getMonth() - 1);
            self._update();
            this.blur();
            return false;
        });

        // goes to the current month
        $('.current', this._target).click(function () {
            self._calendarDate = new Date();
            self._update();
            this.blur();
            return false;
        });

        // goes to the next month
        $('.next', this._target).click(function () {
            self._calendarDate.setMonth(self._calendarDate.getMonth() + 1);
            self._update();
            this.blur();
            return false;
        });

        // closes the interface and fires a 'done' event
        $('.select-date', this._target).click(function () {
            $(self).trigger('done');
            self._target.hide();
            this.blur();
            return false;
        });
    };

    /**
     * Updates the component.
     *
     * @return {Void}
     */
    Calendar.prototype._update = function () {
        var self = this;

        // updates position
        $.axisSugarPos(this._sugar, this._target, this._position);

        // current date
        var currentDate = new Date();
        var currentYear = currentDate.getFullYear();
        var currentMonth = currentDate.getMonth();
        var currentDay = currentDate.getDate();

        // current calendar date
        var calendarYear = this._calendarDate.getFullYear();
        var calendarMonth = this._calendarDate.getMonth();

        // selected date
        var selectedYear = -1;
        var selectedMonth = -1;
        var selectedDay = -1;
        if (this._selectedDate != null) {
            selectedYear = this._selectedDate.getFullYear();
            selectedMonth = this._selectedDate.getMonth();
            selectedDay = this._selectedDate.getDate();
        }

        // updates title
        var title = $.axisDate('format', 'F Y', this._calendarDate);
        $('.current', this._target).text(title);

        // finds the closest previous Sunday.
        var date = new Date(this._calendarDate);
        date.setDate(1);
        while (date.getDay() > 0) {
            date.setDate(date.getDate() - 1);
        }

        // updates days
        var maxCols = 7;
        var maxRows = 6;
        var table = $('.days table tbody', this._target).empty();
        var tr = $('<tr />').appendTo(table);

        for (var i = 0, cols = 1; i < maxRows * maxCols; i++, cols++, date.setDate(date.getDate() + 1)) {
            var year = date.getFullYear();
            var month = date.getMonth();
            var day = date.getDate();
            var disabled = calendarYear != year || calendarMonth != month;
            var current = !disabled
                && currentYear == year && currentMonth == month && currentDay == day;
            var selected = !disabled
                && selectedYear == year && selectedMonth == month && selectedDay == day;

            if (cols > maxCols) {
                tr = $('<tr />').appendTo(table);
                cols = 1;
            }

            tr.append(
                $('<td />').append(
                    $('<a href="#" />')
                        .toggleClass('current', current)
                        .toggleClass('disabled', disabled)
                        .toggleClass('selected', selected)
                        .text(date.getDate())
                        .click($.proxy(self._onDayClick, self))
                )
            );
        }
    };

    /**
     * This function is called when the user clicks on a day.
     *
     * @return {Void}
     */
    Calendar.prototype._onDayClick = function (event) {
        var target = $(event.currentTarget);

        if (!target.hasClass('disabled')) {
            var year = this._calendarDate.getFullYear();
            var month = this._calendarDate.getMonth();
            var day = parseInt(target.text(), 10);
            var date = target.hasClass('selected')? null : new Date(year, month, day, 0, 0, 0, 0);

            this.setSelectedDate(date);
        }
    };

    /**
     * Gets selected date.
     *
     * @return   {Date}
     * @name     axisCalendar#Calendar::getSelectedDate
     * @function
     */
    Calendar.prototype.getSelectedDate = function () {
        return this._selectedDate;
    };

    /**
     * Sets selected date.
     *
     * @param {Date} value Date
     *
     * @return   {Void}
     * @name     axisCalendar#Calendar::setSelectedDate
     * @function
     */
    Calendar.prototype.setSelectedDate = function (value) {
        this._selectedDate = value;

        if (value != null) {
            this._calendarDate = new Date(value);
        }

        this._update();
    };

    /**
     * Creates the plugin.
     *
     * @param {jQuery} sugarTarget Sugar target
     * @param {String} relativePos Position relative to 'sugar target'
     *
     * @return {TreeNavigator}
     */
    $.axisCalendar = function (sugarTarget, relativePosition) {
        return new Calendar(sugarTarget, relativePosition);
    };
})(jQuery);
