/**
 * jQuery.spDragger - A plugin for dragging HTML elements.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spText
 *   3. jQuery.spTextSelection
 *
 * @namespace
 * @name      spDragger
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
if ($.fn.spDragger === undefined) {
    (function ($) {
        var _mouseOrigin = null;
        var _snapshot = null;
        var _target = null;
        var _draggedTarget = null;

        /**
         * Prepares the plugin.
         *
         * This function is called only one time, when the plugin is loaded.
         */
        (function () {
            $(window)
                .on('mouseup', function () {
                    if (_draggedTarget != null) {
                        var dragger = Dragger.retrieveInstance(_target);
                        dragger._target.trigger('dragger-stop');
                    }

                    _mouseOrigin = null;
                    _target = null;
                    _draggedTarget = null;

                    // removes the snapshot
                    if (_snapshot != null) {
                        _snapshot.remove();
                        _snapshot = null;
                    }

                    //$('body').spTextSelection('restore');
                })
                .on('mousemove', function (event) {
                    var dragger = Dragger.retrieveInstance(_target);
                    if (dragger === undefined) {
                        return;
                    }

                    if (_mouseOrigin != null) {
                        var mouseX = _mouseOrigin.x;
                        var mouseY = _mouseOrigin.y;
                        var distance = Math.sqrt(
                            Math.pow(mouseX - event.pageX, 2) +
                            Math.pow(mouseY - event.pageY, 2)
                        );

                        if (distance > dragger.getMouseSensitivity()) {
                            var offset = _target.offset();
                            var snapshotOffset = {x: mouseX - offset.left, y: mouseY - offset.top};
                            var canvas = $('<div />');
                            var snapshotStyle = {
                                'position': 'absolute',
                                'z-index': 1000000,
                                'opacity': dragger.getSnapshotOpacity(),
                                'pointer-events': 'none'
                            };

                            _draggedTarget = _target;
                            _mouseOrigin = null;
                            dragger._target.trigger('dragger-start');

                            if (!$.browser.opera) {
                                // takes a snapshot of the target and makes the snapshot
                                html2canvas(_target, {
                                    onrendered: function (canvas) {
                                        // makes the snapshot and appends it to the body
                                        _snapshot = $(canvas)
                                            .css(snapshotStyle)
                                            .data('offset', snapshotOffset)
                                            .appendTo('body');
                                        dragger.setSnapshotPosition(event.pageX, event.pageY);
                                    }
                                });
                            } else {
                                // in the case of Opera Browser,
                                // the snapshot is a tiny square with a shadow
                                snapshotStyle = $.extend({}, snapshotStyle, {
                                    'width': '32px',
                                    'height': '24px',
                                    'background-color': '#ffffff',
                                    'box-shadow': '3px 3px 0px 0px rgba(0, 0, 0, 0.75)'
                                });
                                snapshotOffset = {x: -5, y: -5};

                                // makes the snapshot and appends it to the body
                                _snapshot = $('<div />')
                                    .css(snapshotStyle)
                                    .data('offset', snapshotOffset)
                                    .appendTo('body');
                                dragger.setSnapshotPosition(event.pageX, event.pageY);
                            }
                        }
                    }

                    // moves the snapshot with the mouse
                    if (_snapshot != null) {
                        dragger.setSnapshotPosition(event.pageX, event.pageY);
                    }
                });
        })();

        /**
         * Class Dragger.
         *
         * @param {HTMLElement} targetElement Element to be dragged
         */
        function Dragger(targetElement) {
            /**
             * Draggable object
             * @var {jQuery}
             */
            this._target = Dragger.saveInstance(targetElement, this);

            /**
             * Snapshot.
             * @var {jQuery}
             */
            this._snapshot = null;

            /**
             * Snapshot opacity.
             * @var {Number}
             */
            this._snapshotOpacity = 0.5;

            /**
             * Handle button.
             * By default, the handle button is the same as the draggable object.
             * @var {jQuery}
             */
            this._handle = this._target;

            /**
             * The number of pixels before the dragging process occurs.
             * @var {Number}
             */
            this._mouseSensitivity = 5;

            /**
             * Draggable area.
             * By default, the draggable area is the whole window.
             * @var {jQuery|{x:Number, y:Number, width:Number, height:Number}}
             */
            this._bounds = {x: -Infinity, y: -Infinity, width: Infinity, height: Infinity};

            /**
             * The origin of the mouse when the user clicks the handle.
             * @var {{x:Number, y:Number}}
             */
            this._mouseOrigin = null;

            this._make();
        }

        /**
         * Retrieves the instance from an HTML element.
         *
         * @param {HTMLElement} element Target
         *
         * @return {VerticalSlider}
         */
        Dragger.retrieveInstance = function (element) {
            return $(element).data('dragger-instance');
        };

        /**
         * Saves an instance into an HTML element.
         *
         * @param {HTMLElement} target   Target
         * @param {Dragger}     instance Dragger instance
         *
         * @return {jQuery}
         */
        Dragger.saveInstance = function (target, instance) {
            return $(target).data('dragger-instance', instance);
        };

        /**
         * Gets handle button.
         *
         * @return {jQuery}
         */
        Dragger.prototype.getHandle = function () {
            return this._handle;
        };

        /**
         * Sets handle button.
         *
         * @param {jQuery|String} value Handle button or CSS selector
         *
         * @return {Void}
         */
        Dragger.prototype.setHandle = function (value) {
            var elem = $.type(value) == 'string'? $(value, this._target) : value;

            if (this._handle != null) {
                this._handle.off('mousedown', this._onHandleMouseDown);
                this._handle.spTextSelection('restore');
            }

            this._handle = elem;
            this._handle.spTextSelection('disable');
            this._handle.on('mousedown', $.proxy(this._handleOnMouseDown, this));
        };

        /**
         * Gets the snapshot opacity.
         *
         * @return {Number}
         */
        Dragger.prototype.getSnapshotOpacity = function () {
            return this._snapshotOpacity;
        };

        /**
         * Sets the snapshot opacity.
         *
         * @param {Number} value Opacity
         *
         * @return {Void}
         */
        Dragger.prototype.setSnapshotOpacity = function (value) {
            this._snapshotOpacity = value;
        };

        /**
         * Gets the number of pixels before the dragging process occurs.
         *
         * @return {Number}
         */
        Dragger.prototype.getMouseSensitivity = function () {
            return this._mouseSensitivity;
        };

        /**
         * Sets the number of pixels before the dragging process occurs.
         *
         * @param {Number} value Number of pixels
         *
         * @return {Void}
         */
        Dragger.prototype.setMouseSensitivity = function (value) {
            this._mouseSensitivity = value;
        };

        /**
         * Gets the draggable area.
         *
         * @return {{x:Number, y:Number, width:Number, height:Number}}
         */
        Dragger.prototype.getBounds = function () {
            var ret = this._bounds;

            if (this._bounds instanceof jQuery) {
                var offset = this._bounds.offset();

                ret = {
                    x: offset.left,
                    y: offset.top,
                    width: this._bounds.width(),
                    height: this._bounds.height()
                };
            }

            return ret;
        };

        /**
         * Sets the draggable area.
         *
         * @param {jQuery|{x, y, width, height}} value Draggable area
         *
         * @return {Void}
         */
        Dragger.prototype.setBounds = function (value) {
            this._bounds = value instanceof jQuery? value: $.extend({}, this._bounds, value);
        };

        /**
         * Sets the snapshot position.
         *
         * @param {Number} x X coordinate
         * @param {Number} y Y coordinate
         *
         * @return {Void}
         */
        Dragger.prototype.setSnapshotPosition = function (x, y) {
            var offset = _snapshot.data('offset');
            var snapshotX = x - offset.x;
            var snapshotY = y - offset.y;
            var bounds = this.getBounds();

            // X restriction
            var minX = bounds.x;
            var maxX = isFinite(bounds.width)
                ? bounds.x + bounds.width - _snapshot.width()
                : Infinity;
            snapshotX = Math.min(Math.max(snapshotX, minX), maxX);

            // Y restriction
            var minY = bounds.y;
            var maxY = isFinite(bounds.height)
                ? bounds.y + bounds.height - _snapshot.height()
                : Infinity;
            snapshotY = Math.min(Math.max(snapshotY, minY), maxY);

            _snapshot.css({
                left: snapshotX,
                top: snapshotY
            });
        };

        /**
         * Makes the component.
         *
         * @return {Void}
         */
        Dragger.prototype._make = function () {
            $(this._handle).spTextSelection('disable');
            this._makeActions();
        };

        /**
         * Makes actions.
         *
         * @return {Void}
         */
        Dragger.prototype._makeActions = function () {
            var self = this;

            this._handle.on('mousedown', $.proxy(this._handleOnMouseDown, this));
        };

        /**
         * Handles 'mousedown' events.
         *
         * @param {Object} event Event
         *
         * @return {Void}
         */
        Dragger.prototype._handleOnMouseDown = function (event) {
            _target = this._target;
            _mouseOrigin = {x: event.pageX, y: event.pageY};
            //$('body').spTextSelection('disable');
        };

        var methods = {
            /**
             * Initializes the component.
             *
             * Example 1:
             * ```JavaScript
             * // this is the most simple way to initialize the plugin
             * $('.draggable-object').spDragger();
             * ```
             *
             * Example 2:
             * ```JavaScript
             * // in this case the 'draggable area' is a rectangle
             * $('.draggable-object').spDragger({
             *      bounds: {x: 100, y: 150, width: 640, height: 480}
             * });
             * ```
             *
             * Example 3:
             * ```JavaScript
             * // in this case the 'draggable area' is a vertical band
             * $('.daggable-object').spDragger({
             *      bounds: {x: 100, width: 640}
             * });
             * ```
             *
             * Example 2:
             * ```JavaScript
             * // in this case the 'draggable area' is defined by an HTML element.
             * $('.draggable-object').spDragger({
             *      handle: $('.handle-button'),
             *      snapshotOpacity: 0.2,
             *      mouseSensitivity: 5,
             *      bounds: $('.draggable-area')
             * });
             * ```
             *
             *
             * Example 3:
             * ```JavaScript
             * // in this case the 'handle' is a button inside the 'draggable object'
             * $('.draggable-object').spDragger({
             *      handle: '.handle-button'
             * });
             * ```
             *
             * @param {jQuery|String} options.handle           Handle button or CSS selector (not
             *                                                 required)
             * @param {Number}        options.snapshotOpacity  Snapshot opacity (default is 0.5)
             * @param {Number}        options.mouseSensitivity Mouse sensitivity (default is 5 pixels)
             *                                                 The number of pixels before the dragging
             *                                                 process occurs.
             * @param {jQuery|Object} options.bounds Draggable area (not required)
             *
             * @return   {jQuery}
             * @name     spDragger#init
             * @function
             */
            'init': function (options) {
                return this.each(function () {
                    var dragger = new Dragger(this);

                    if (options !== undefined) {
                        // sets up the parameters
                        $.each(options, function (key, value) {
                            var methodName = 'set' + $.spText('capitalize', key);
                            var method = dragger[methodName];

                            if (method === undefined) {
                                $.error('Invalid parameter: ' + key);
                            }

                            $.proxy(method, dragger)(value);
                        });
                    }
                });
            },

            /**
             * Handles 'drop' events.
             *
             * This function is called when the user drops the snapshot.
             *
             * For example:
             * ```JavaScript
             * $('.destination-object').spDragger('drop', function (event) {
             *      // this is the object being dragged
             *      var draggedTarget = $(event.draggedTarget);
             *
             *      //... more code
             * });
             * ```
             *
             * @param {Function} handler EventHandler
             *
             * @return   {jQuery}
             * @name     spDragger#onDrop
             * @function
             */
            'onDrop': function (handler) {
                return this.each(function () {
                    var target = $(this);

                    target.on('mouseup', function (event) {
                        if (_draggedTarget != null) {
                            var evt = $.extend($.Event('dragger-drop'), {
                                draggedTarget: _draggedTarget[0],
                                clientX: event.clientX,
                                clientY: event.clientY,
                                pageX: event.pageX,
                                pageY: event.pageY,
                                screenX: event.screenX,
                                screenY: event.screenY
                            });
                            target.trigger(evt);
                        }
                    });

                    target.on('dragger-drop', handler);
                });
            },

            /**
             * Handles 'start' events.
             *
             * This function is called when the dragging starts.
             *
             * @param {Function} handler EventHandler
             *
             * @return   {jQuery}
             * @name     spDragger#onStart
             * @function
             */
            'onStart': function (handler) {
                return this.each(function () {
                    var target = $(this);

                    target.on('dragger-start', handler);
                });
            },

            /**
             * Handles 'stop' events.
             *
             * This function is called when the dragging stops.
             *
             * @param {Function} handler EventHandler
             *
             * @return   {jQuery}
             * @name     spDragger#onStop
             * @function
             */
            'onStop': function (handler) {
                return this.each(function () {
                    var target = $(this);

                    target.on('dragger-stop', handler);
                });
            }
        };

        var staticMethods = {
            /**
             * Is the dragging process started?
             *
             * @return   {Boolean}
             * @name     spDragger#isStarted
             * @function
             */
            'isStarted': function () {
                return _draggedTarget != null;
            }
        };

        $.fn.spDragger = function (methodName) {
            var method = methods['init'];
            var args = arguments;

            if ($.type(methodName) == 'string') {
                method = methods[methodName];
                args = Array.prototype.slice.call(arguments, 1);
            }

            if (typeof method == 'undefined') {
                $.error('Method not found: ' + methodName);
            }

            return method.apply(this, args);
        };

        $.spDragger = function (methodName) {
            var method = staticMethods[methodName];
            var args = Array.prototype.slice.call(arguments, 1);

            return method.apply(this, args);
        };
    })(jQuery);
}
