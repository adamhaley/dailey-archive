/**
 * jQuery.spText - Some useful text functions.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spReg
 *
 * @namespace
 * @name      spText
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @license   https://raw2.github.com/soloproyectos/js.common-libs/master/LICENSE BSD 2-Clause License
 * @link      https://github.com/soloproyectos/js.common-libs
 */
(function ($) {

    /**
     * Plugin methods.
     * @var {Object}
     */
    var methods = {
        /**
         * Is an empty value?
         *
         * An "empty value" is any of the following values: undefined, null or an
         * empty string.
         *
         * @param {*} val Mixed variable
         *
         * @return   {Boolean}
         * @name     spText#empty
         * @function
         */
        'empty': function (val) {
            return (
                   val === undefined
                || val === null
                || $.type(val) == 'string' && val.length == 0
            );
        },

        /**
         * Gets the a default value if the value is "empty".
         *
         * Examples:
         * ```JavaScript
         * // shows 101, because null is an empty value
         * console.log($.spText('ifEmpty', null, 101));
         *
         * // shows 'on two three', because the passed string is a non-empty value
         * console.log($.spText('ifEmpty', 'one two three', 'Hi there!'));
         * ```
         *
         * @param {*} value Value
         * @param {*} def   Default value
         *
         * @return   {*}
         * @name     spText#ifEmpty
         * @function
         */
        'ifEmpty': function (value, def) {
            return $.spText('empty', value)? def : value;
        },

        /**
         * Trims a text.
         *
         * This function is similar to jQuery.trim, except that it admits a
         * 'delimiter' parameter.
         *
         * Examples:
         * ```JavaScript
         * // shows 'hello there!'
         * console.log($.spText('trim', '   hello there!   '));
         *
         * // shows 'dir1/dir2'
         * console.log($.spText('trim', '/dir1/dir2/', '/'));
         * ```
         *
         * @param {String} str       Text
         * @param {String} delimiter Delimiter character (default is a space)
         *
         * @return   {String}
         * @name     spText#trim
         * @function
         */
        'trim': function(str, delimiter) {
            return $.spText('ltrim', $.spText('rtrim', str, delimiter), delimiter);
        },

        /**
         * Removes left spaces from a text.
         *
         * @param {String} str       Text
         * @param {String} delimiter Delimiter character (default is a space)
         *
         * @return   {String}
         * @name     spText#ltrim
         * @function
         */
        'ltrim': function(str, delimiter) {
            var re = /^\s+/;

            if (typeof delimiter != 'undefined') {
                re = new RegExp("^(" + $.spReg('encode', delimiter) + ")+");
            }

            return str.replace(re, '');
        },

        /**
         * Removes right spaces from a text.
         *
         * @param {String} str       Text
         * @param {String} delimiter Delimiter character (default is a space)
         *
         * @return  {String}
         * @name     spText#rtrim
         * @function
         */
        'rtrim': function(str, delimiter) {
            var re = /\s+$/;

            if (typeof delimiter != 'undefined') {
                re = new RegExp("(" + $.spReg('encode', delimiter) + ")+$");
            }

            return str.replace(re, '');
        },

        /**
         * Truncates a text.
         *
         * @param {String} str    Text
         * @param {Number} len    Length
         * @param {String} suffix Suffix (default is '...')
         *
         * @return   {String}
         * @name     spText#truncate
         * @function
         */
        'truncate': function (str, len, suffix) {
            var ret = str;

            if (typeof suffix == 'undefined') {
                suffix = '...';
            }

            if (str.length > len) {
                ret = str.substr(0, len - suffix.length) + suffix;
            }

            return ret;
        },

        /**
         * Concatenates strings.
         *
         * This function ignores the empty values (null, undefine and empty
         * strings)
         *
         * For example:
         * ```JavaScript
         * // the next command returns the string 'John, Maria, Peter'
         * $.spText('concat', ', ', 'John', '', 'Maria', null, 'Peter');
         *
         * // and the next command returns 'John'
         * $.spText('concat', ', ', 'John');
         * ```
         *
         * @param {String} glue 'Glue' character
         *
         * @return   {String}
         * @name     spText#concat
         * @function
         */
         'concat': function (glue/*, str...*/) {
            var ret = '';
            var len = arguments.length;

            for (var i = 1; i < len; i++) {
                var str = arguments[i];

                if (str !== undefined
                    && str !== null
                    && !$.spText('empty', str)
                ) {
                    if (ret.length > 0) {
                        ret += glue;
                    }

                    ret += str;
                }
            }

            return ret;
        },

        /**
         * Generates a unique id.
         *
         * @param {String} prefix Prefix (not required)
         *
         * @return   {String}
         * @name     spText#uniqid
         * @function
         */
        'uniqid': function (prefix) {
            var ret = '';
            var alpha = 'abcdefghijklmnopqrstuvwxyz';
            var alphanum = alpha.concat('0123456789');

            if (typeof prefix == 'undefined') {
                prefix = '';
            }

            // the first character must be alphabetic
            ret = alpha[Math.floor(Math.random() * alpha.length)];

            // the second chatacter can be an alphabetic or numeric
            for (var i = 0; i < 22; i++) {
                ret += alphanum[Math.floor(Math.random() * alphanum.length)];
            }

            return prefix + ret;
        },

        /**
         * Normalizes a number by adding characters to the left.
         *
         * For example:
         * ```JavaScript
         * $.spText('normalize', 125, 7); // returns '0000125'
         * $.spText('normalize', 28, 5, 'x'); // returns 'xxx28'
         * ```
         *
         * @param {Number} num    Integer number
         * @param {Number} length String length
         * @param {String} char   Character fill (default is '0')
         *
         * @return {String}
         */
        'normalize': function (str, length, char) {
            if (char === undefined) {
                char = '0';
            }

            return (Array(length + 1).join(char) + str).substr(-length);
        },

        /**
         * Capitalizes the first character of a string.
         *
         * @param {String} str String
         *
         * @return {String}
         * @name   spText#capitalize
         * @function
         */
        'capitalize': function (str) {
            return str.replace(/^./, function (chr) {
                return chr.toUpperCase();
            });
        }
    };

    /**
     * Main plugin function.
     *
     * @param {String} methodName Method name
     *
     * @return {*}
     */
    $.spText = function (methodName) {
        var method = methods[methodName];
        var args = Array.prototype.slice.call(arguments, 1);

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + methodName);
        }

        return method.apply(this, args);
    };
})(jQuery);

