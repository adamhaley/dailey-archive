/**
 * jQuery.axis - Axistools plugin.
 *
 * This plugin requires:
 *   1. jQuery >= 1.7
 *   2. jQuery.spArgs
 *   3. jQuery.spIntl
 *   4. jQuery.spModal
 *   5. jQuery.spText
 *
 * @namespace
 * @name      axis
 * @author    Gonzalo Chumillas <gchumillas@email.com>
 * @copyright AxisGraphics, Inc. All Right Reserved.
 * @license   Proprietary/Closed Source
 */
(function ($) {
    /**
     * Minimum container width.
     * @var {Number}
     */
    var minWidth = 960;

    /**
     * Is the given path an absolute url?
     *
     * @param {String} url
     *
     * @return {Boolean}
     */
    function isAbsolutePath(url) {
        return /^\/|\w+:/.test(url);
    }

    /**
     * Gets the application path.
     *
     * Example:
     * ```JavaScript
     * // If the application is installed under the 'axistools' folder,
     * // the below command returns '/axistools'
     * console.log(getApplicationPath());
     * ```
     *
     * @return {String}
     */
    function getApplicationPath() {
        var ret = '/';
        var pathname = $.spText('trim', window.location.pathname, '/');
        var splits = pathname.split('/');

        if (splits.length > 0) {
            ret += splits[0];
        }

        return ret;
    }

    /**
     * Gets the absolute path of a relative path.
     *
     * Example:
     * ```JavaScript
     * // If the application is installed under the 'axistools' folder,
     * // the below command returns '/axistools/login.php'
     * console.log(getAbsolutePath('login.php'));
     * ```
     *
     * @return {String}
     */
    function getAbsolutePath(path) {
        return isAbsolutePath(path)? path : getApplicationPath() + '/' + path;
    }

    /**
     * Sends a request method.
     *
     * @param {String}   method    Method name: get, post or ajax
     * @param {String}   url       Url
     * @param {Object}   params    Parameters (not required)
     * @param {String}   dataType  Content type (not required)
     *
     * @return {jQuery.Promise}
     */
    function sendRequest(method, url, params, dataType) {
        var deferred = new $.Deferred();
        var promise = deferred.promise();

        // allowed methods are 'get', 'post' or 'ajax'
        if (['get', 'post'].indexOf(method) < 0) {
            $.error(
                'Allowed methods are \'get\' and \'post\'; \'' +
                method + '\' was given'
            );
        }
        
        var disableModalMessage = params != null && params['disableModalMessage'] != undefined
            ? params['disableModalMessage']
            : '';
        var path = isAbsolutePath(url)
            ? url
            : $.spText('rtrim', SERVER_URI, '/') + '/' + url;
        $.spModal(method, path, params, dataType).done(function (data, status, xhr) {
            // checks for application errors
            var title = $.spText(
                'ifEmpty', $.trim($('root > error > title', data).text()), 'Application Error'
            );
            var type = $.trim($('root > error > type', data).text());
            var message = $.trim($('root > error > message', data).text());

            if (!$.spText('empty', message)) {
                if (disableModalMessage != type) {
                    $.spModal('error', title, message, {
                        pre: (message.match(/\n/g) || []).length > 3,
                        buttons: [
                            {
                                label: 'Accept',
                                handler: function () {
                                    this.remove();
                                    deferred.reject(data, status, xhr);
                                }
                            }
                        ]
                    });
                } else {
                    deferred.reject(data, status, xhr);
                }
            } else {
                deferred.resolve(data, status, xhr);
            }
        }).fail(function (xhr) {
            deferred.reject(xhr);
        });

        return promise;
    }

    /**
     * Sends a 'upload' request.
     *
     * @param {String}   url    Url
     * @param {Object}   params Parameters (not required)
     *
     * @return {jQuery.Promise}
     */
    function uploadRequest(url, params) {
        var deferred = new $.Deferred();
        var promise = deferred.promise();

        var path = isAbsolutePath(url)
            ? url
            : $.spText('rtrim', SERVER_URI, '/') + '/' + url;
        $.spModal('upload', path, params).done(function (data, status, xhr) {
            var title = 'Application Error';
            var message = 'The document is not well formed';

            // checks for errors
            try {
                var xml = $.parseXML(data);
                title = $.spText('ifEmpty', $.trim($('root > error > title', xml).text()), title);
                message = $.trim($('root > error > message', xml).text());
            } catch (error) {
                // ignores the exception
            }

            if (!$.spText('empty', message)) {
                $.spModal('error', title, message, {
                    pre: (message.match(/\n/g) || []).length > 3,
                    buttons: [
                        {
                            label: 'Accept',
                            handler: function () {
                                this.remove();
                                deferred.reject(data, status, xhr);
                            }
                        }
                    ]
                });
            } else {
                deferred.resolve(data, status, xhr);
            }
        }).fail(function (xhr) {
            deferred.reject(xhr);
        });

        return promise;
    }
    
    /**
     * Gets parent window.
     * 
     * @param {Element} win Window
     * 
     * @return {Element|null}
     */
    function getParentWindow(win) {
        var ret = null;
        
        win.parent.$('iframe').each(function () {
            var iframeWindow = this.contentWindow;
            if (iframeWindow == win) {
                ret = iframeWindow.parent;
            }
        });
        
        return ret;
    }
    
    var methods = {
        /**
         * Gets body's zoom.
         * 
         * This function returns a number between 0 and 1.
         * 
         * @param {Element} win Window (not required)
         * 
         * @return {Number}
         */
        'zoom': function (win) {
            if (win === undefined) {
                win = window;
            }
            
            var ret = 1;
            var doc = win.document;
            var cssTransform = $('body', doc).css('transform');
            var regexp = /^matrix\(([0-9\.]+), 0, 0, ([0-9\.]+), 0, 0\)$/;
            var matches = regexp.exec(cssTransform);
            
            if (matches != null && matches[1] == matches[2]) {
                ret = parseFloat(matches[1], 10);
            }
            
            var parent = getParentWindow(win);
            if (parent !== null) {
                ret *= $.axis('zoom', parent);
            }
            
            return ret;
        },
        
        /**
         * Centers user interface.
         * 
         * @param {jQuery.<HTMLElement>} ui User Interface
         * 
         * @return {Void}
         */
        'center': function (ui) {
            var zoom = $.axis('zoom');
            ui.css({
                left: Math.max(0, ($(window).width() / zoom - ui.width()) / 2),
                top: Math.max(25, ($(window).height() / zoom - 2.8 * ui.height() * zoom) / 2)
            });
        },
        /**
         * Shows a modal loading dialog.
         *
         * Example:
         * ```JavaScript
         * $.axis('loading', function () {
         *     // .. cancel something ...
         *     console.log('The operation was canceled by the user');
         * });
         * ```
         *
         * @param {String}   text     Loading text (not required)
         * @param {Function} onCancel Cancel event handler
         *
         * @return   {jQuery}
         * @name     axis#loading
         * @function
         */
        'loading': function (text, onCancel) {
            var args = $.spArgs(arguments, {
                text: {
                    type: 'string',
                    def: 'Loading info ...'
                },
                onCancel: 'function'
            });

            return $.spModal('loading', args.text, {
                buttons: [
                    {
                        label: 'Accept',
                        handler: function () {
                            this.remove();
                            $.proxy(args.onCancel, this)();
                        }
                    }
                ]
            });
        },

        /**
         * Shows a modal message dialog.
         *
         * This function is equivalent to $.spModal('message').
         *
         * @param {String} title   Title
         * @param {String} message Message
         * @param {Object} options Options
         *
         * @return   {jQuery}
         * @name     axis#message
         * @function
         */
        'message': function (title, message, options) {
            return $.spModal('message', title, message, options);
        },

        /**
         * Shows a modal alert dialog.
         *
         * Example:
         * ```JavaScript
         * $.axis('alert', 'Password is required', function () {
         *     $('#password').focus();
         * });
         * ```
         *
         * @param {String}   message  Message
         * @param {Function} onAccept Accept event handler (not required)
         *
         * @return   {jQuery}
         * @name     axis#alert
         * @function
         */
        'alert': function (message, onAccept) {
            var args = $.spArgs(arguments, {
                message: 'string',
                onAccept: {
                    type: 'function',
                    def: function () {
                        this.remove();
                    }
                }
            });

            return $.spModal(
                'message',
                'Alert',
                args.message,
                {
                    buttons: [
                        {
                            label: 'Accept',
                            handler: function () {
                                this.remove();
                                $.proxy(args.onAccept, this)();
                            }
                        }
                    ]
                }
            );
        },

        /**
         * Shows a modal confirm dialog.
         *
         * Example:
         * ```JavaScript
         * $.axis('confirm', 'Are you really sure?', function () {
         *      console.log('Formating hard drive...');
         * });
         * ```
         *
         * @param {String}   message  Message
         * @param {Function} onAccept Accept event handler
         *
         * @return   {jQuery}
         * @name     axis#confirm
         * @function
         */
        'confirm': function (message, onAccept) {
            var args = $.spArgs(arguments, {
                message: 'string',
                onAccept: 'function'
            });

            return $.spModal('message', 'Please confirm', message, {
                buttons: [
                    {
                        label: 'Accept',
                        handler: function () {
                            this.remove();
                            $.proxy(args.onAccept, this)();
                        }
                    },
                    {
                        label: 'Cancel',
                        focus: true,
                        handler: function () {
                            this.remove();
                        }
                    }
                ]
            });
        },

        /**
         * Shows a modal error dialog.
         *
         * Example:
         * ```JavaScript
         * $.axis('error', 'Something went terribly wrong');
         * ```
         *
         * @param {String}   message  Error message
         * @param {Function} onAccept Accept event handler (not required)
         *
         * @return   {Void}
         * @name     axis#error
         * @function
         */
        'error': function (message, onAccept) {
            var args = $.spArgs(arguments, {
                message: 'string',
                onAccept: {
                    type: 'function',
                    def: function () {
                        this.remove();
                    }
                }
            });

            $.spModal('error', 'Error', args.message, {
                buttons: [
                    {
                        label: 'Accept',
                        handler: function () {
                            this.remove();
                            $.proxy(args.onAccept, this)();
                        }
                    }
                ]
            });
        },

        /**
         * Shows a modal prompt dialog.
         *
         * Example 1:
         * ```JavaScript
         * $.axis('prompt', 'Enter your username', function (username) {
         *      if (!username) {
         *          $.axis('error', 'Username is required');
         *      }
         *      this.remove();
         * });
         * ```
         *
         * Example 2 (multiple fields):
         * ```JavaScript
         * $.axis('prompt', ['Username', 'Age'], function (username, age) {
         *      if (!username || !age) {
         *          $.axis('error', 'Username and fields are required');
         *      }
         *      this.remove();
         * });
         * ```
         *
         * Example 3 (full example):
         * ```JavaScript
         * $.axis(
         *      'prompt',
         *      'Please complete the fields',
         *      [
         *          {label: 'Username', type: 'text'},
         *          {label: 'Password', type: 'password'},
         *          {label: 'Age', type: 'select', options: {f: 'Female', m: 'Male'}, value: 'm'},
         *          {
         *              label: 'Profession',
         *              type: 'checkbox',
         *              options: {1: 'Engineer', 2: 'Lawyer', 3: 'Scientist', 4: 'Other'},
         *              value: [1, 4]
         *          },
         *          {
         *              label: 'Salary',
         *              type: 'radio',
         *              options: {0: 'Low', 1: 'Middle', 2: 'High'}, value: 1
         *          }
         *      ],
         *      function (username, password, age, profession, salary) {
         *          if (!username || !password) {
         *              $.axis('error', 'Username and password are required');
         *          }
         *          this.remove();
         *      }
         * );
         * ```
         *
         * @param {String}   title    Prompt title (not required)
         * @param {Array}    fields List of fields
         * @param {Function} onAccept Accept event handler
         *
         * @return   {Void}
         * @name     axis#prompt
         * @function
         */
        'prompt': function () {
            return $.axisPrompt.apply(this, arguments);
        },

        /**
         * Shows a modal window.
         *
         * @param {String} url    Url
         * @param {Object} params Parameters (not required)
         *
         * @return   {jQuery}
         * @name     axis#window
         * @function
         */
        'window': function (url, params) {
            return $.spModal('window', getAbsolutePath(url), {
                params: params
            });
        },

        /**
         * Sends a GET request.
         *
         * This function sends a GET request to the server and shows a modal
         * dialog while waiting the response. The request can be cancelled at any
         * time by the user.
         *
         * Example:
         * ```JavaScript
         * $.axis(
         *      'get', 'login.php', {user: 'anthony', pass: 'xxx'}
         * ).failed(function () {
         *      console.log('Something went wrong');
         * }).done(function () {
         *      console.log('Congrats, you have been chosen.');
         * }).always(function () {
         *      console.log('This command is always executed');
         * });
         * ```
         *
         * @param {String} url      Url
         * @param {Object} params   Parameters (not required)
         * @param {String} dataType Data type (not required)
         *
         * @return   {jQuery.Promise}
         * @name     axis#get
         * @function
         */
        'get': function (url, params, dataType) {
            return sendRequest('get', url, params, dataType);
        },

        /**
         * Sends a POST request.
         *
         * This function sends a POST request to the server and shows a modal
         * dialog while waiting the response. The request can be cancelled at any
         * time by the user.
         *
         * Example:
         * ```JavaScript
         * $.axis(
         *      'post', 'login.php', {user: 'anthony', pass: 'xxx'}
         * ).failed(function () {
         *      console.log('Something went wrong');
         * }).done(function () {
         *      console.log('Congrats, you have been chosen.');
         * }).always(function () {
         *      console.log('This command is always executed');
         * });
         * ```
         *
         * @param {String} url      Url
         * @param {Object} param    Parameters (not required)
         * @param {String} dataType Data type (not required)
         *
         * @return   {jQuery.Promise}
         * @name     axis#post
         * @function
         */
        'post': function (url, param, dataType) {
            return sendRequest('post', url, param, dataType);
        },

        /**
         * Sends a UPLOAD request.
         *
         * This function sends a UPLOAD request to the server and shows a modal
         * dialog while waiting the response. The request can be cancelled at any
         * time by the user.
         *
         * Example:
         * ```JavaScript
         * $('#input-file').change(function () {
         *     $.axis('upload', 'upload-page.php', {file: this}).done(function (data, status, xhr) {
         *         console.log('success!');
         *     }).fail(function (xhr, status) {
         *         console.log('The upload process has failed');
         *     }).always(function () {
         *         console.log('This method is always fired at the end');
         *     });
         * });
         * ```
         *
         * @param {String} url      Url
         * @param {Object} params   Request parameters (not required)
         * @param {String} dataType Data type (not required)
         *
         * @return   {jQuery.Promise}
         * @name     axis#post
         * @function
         */
        'upload': function (url, params) {
            return uploadRequest(url, params);
        },

        /**
         * Garbage operations.
         *
         * @return {jQuery}
         */
        'garbage': function () {
            return $.axisGarbage.apply(this, arguments);
        },

        /**
         * Redirects to a new url.
         *
         * @param {String} url Url
         *
         * @return   {jQuery}
         * @name     axis#redirect
         * @function
         */
        'redirect': function (url) {
            return $.spModal('redirect', getAbsolutePath(url));
        },

        /**
         * Opens a page in a new window.
         *
         * By default, this function opens a page in the same window (target = '_self').
         * See http://www.w3schools.com/jsref/met_win_open.asp for available parameters.
         *
         * Examples:
         * ```JavaScript
         * // opens a page in the same window
         * $.axis('open', 'newpage.php');
         *
         * // shows a page in a centered pop-up
         * $.axis('open', 'newpage.php', {width: 320, height: 240});
         *
         * // opens a page into the parent frame
         * $.axis('open', 'newpage.php', {target: '_parent'});
         * ```
         *
         * @param {String}   url URL
         * @param {String[]} params Parameters
         *
         * @return   {Object|null}
         * @name     axis#open
         * @function
         */
        'open': function (url, params) {
            var defaultParams = {
                resizable: 'yes',
                scrollbars: 'yes',
                target: '_self'
            };

            params = $.extend({}, defaultParams, params);

            // centers the window if it is a pop-up
            if (params.width !== undefined && params.left === undefined) {
                params.left = Math.round((window.screen.availWidth - parseInt(params.width, 10)) / 2);
            }
            if (params.height !== undefined && params.top === undefined) {
                params.top = Math.round((window.screen.availHeight - parseInt(params.height, 10)) / 2);
            }

            // concatenates the parameters
            var strParams = '';
            $.each(params, function (key, value) {
                if (key !== 'target') {
                    strParams = $.spText('concat', ', ', strParams, escape(key) + '=' + escape(value));
                }
            });

            return window.open(getAbsolutePath(url), params.target, strParams);
        },

        /**
         * Opens a page in a new tab.
         *
         * @param {String} url    URL
         * @param {String} target Window target (not required)
         *
         * @return   {Object|null}
         * @name     axis#openTab
         * @function
         */
        'openTab': function (url, target) {
            console.log(target);
            return window.open(getAbsolutePath(url), target);
        },

        /**
         * Gets the root interface.
         *
         * @return   {jQuery}
         * @name     axis#root
         * @function
         */
        'root': function () {
            return $.spModal('root');
        },


        /**
         * Gets the parent interface.
         *
         * @return   {jQuery}
         * @name     axis#parent
         * @function
         */
        'parent': function () {
            return $.spModal('parent');
        },


        /**
         * Gets the current interface.
         *
         * @return {jQuery}
         */
        'self': function () {
            return $.spModal('self');
        },

        /**
         * Gets the absolute path of a relative path.
         *
         * @param {String} path
         *
         * @return   {String}
         * @name     axis#absPath
         * @function
         */
        'absPath': function (path) {
            return getAbsolutePath(path);
        }
    };

    /**
     * Main plugin function.
     * 
     * @param {string} arg Command name
     *
     * @return {*}
     */
    $.axis = function (arg) {
        // check for required plugins
        if (
            $.spArgs === undefined ||
            $.spModal === undefined ||
            $.spText === undefined
        ) {
            $.error(
                '$.axis requires the following plugins: ' +
                '$.spArgs, $.spModal and $.spText'
            );
        }
        if (
            $.axisPrompt === undefined ||
            $.axisGarbage === undefined
        ) {
            $.error(
                '$.axis requires the following plugins: $.axisPrompt, $.axisGarbage'
            );
        }

        var ret = null;
        var args = Array.prototype.slice.call(arguments, 1);
        var method = methods[arg];

        if (typeof method == 'undefined') {
            $.error('Method not found: ' + arg);
        }

        return method.apply(this, args);
    };
})(jQuery);
