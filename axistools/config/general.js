/**
 * Server Side URI.
 * 
 * Typically, the server-side application is installed under the external folder, but it can be
 * installed anyplace. For example:
 * 
 *      SERVER_URI = 'http://<domain name>/axistools-server';
 *      SERVER_URI = 'axistools-server';
 */
var SERVER_URI = 'external/server';
