# External Components

This is a convenient directory to install all those components that are not part of the repository,
such as modules or even the server-side application.
