<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'soloproyectos\\' => array($vendorDir . '/soloproyectos-php/text/src', $vendorDir . '/soloproyectos-php/event/src', $vendorDir . '/soloproyectos-php/db/src', $vendorDir . '/soloproyectos-php/array/src', $vendorDir . '/soloproyectos-php/db-record/src'),
);
