<?php
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\text\TextHelper;

require_once "common/classes/core/sys/file/sys-file-helper.php";
require_once "common/classes/core/text/text-helper.php";

/**
 * Autoload function.
 *
 * This function loads automatically the required scripts, so we do not need
 * to add the 'required_once' command at the beggining of our scripts. The script
 * name is determined by the class name.
 *
 * @param string $class Class name
 *
 * @return void
 */
spl_autoload_register(
    function ($classname) {
        $filename = "";
        $subdirs = array(
            "common/classes/core", "common/classes", "common/fields"
        );

        foreach ($subdirs as $subdir) {
            if (preg_match_all("/[A-Z][a-z,0-9]*/", $classname, $matches)) {
                $dir = __DIR__ . "/$subdir";
                $name = "";
                $items = $matches[0];
                foreach ($items as $item) {
                    $item = strtolower($item);
                    $d = SysFileHelper::concat($dir, $item);
                    if (is_dir($d)) {
                        $dir = $d;
                    }
                    $name = TextHelper::concat("-", $name, $item);
                }
                $filename = SysFileHelper::concat($dir, "$name.php");

                if (is_file($filename)) {
                    include_once $filename;
                    break;
                }
            }
        }
    }
);
