# Axistools Server-Side

Highly configurable CMS.

## Installation

```bash
# clone the server-side project into the /server folder
git clone https://github.com/AxisStudios/axistools-server server

# copy the `config-sample` directory to `config` and edit the `config/general.php` file
$ cp -r server/config-sample server/config
```

Finally test the application from your browser:  
http://www.yourdomain.com/path/to/server
