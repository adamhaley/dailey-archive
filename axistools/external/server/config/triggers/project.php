<?php

    include_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/common.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/api/helpers/php-image-resize-master/src/ImageResize.php';

    $post = $_POST['new'];

    if (json_decode($post['values'][1], true) == 'video') {

        $image_field = 'image';

        if ($post['values'][2] == 'custom') {


        }

        if (json_decode($post['values'][2], true) == 'youtube' && strlen(json_decode($post['values'][8], true)) > 6) {

            $image_field .= '3';
            $source_type = 'youtube';
            $source = json_decode($post['values'][8], true);
            $img = 'https://img.youtube.com/vi/' . $source . '/0.jpg';
        }

        /*if (json_decode($post['values'][2], true) == 'vimeo' && strlen(json_decode($post['values'][9], true)) > 6) {

            $image_field .= '4';
            $source_type = 'vimeo';
            $source = json_decode($post['values'][9], true);
            $_json = file_get_contents('http://vimeo.com/api/v2/video/' . $source . '.json');
            $_json = json_decode($_json, true);
            $img = $_json[0]['thumbnail_large'];
        }*/

        $content = file_get_contents($img);

        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/data/images/' . $post['id'] . '-' . $source_type . '-' . basename($img), 'w');
        fwrite($fp, $content);
        fclose($fp);

        # get data image id
        $i = DB::dailey('select data_image_id from content where id = :id', ['id' => $post['id']]);

        // update existing record
        if ($i[0]['data_image_id'] > 0) {

            DB::dailey('update data_image set ' . $image_field . ' = :img where id = :id', [
                'id'  => $i[0]['data_image_id'],
                'img' => '/data/images/' . $post['id'] . '-' . $source_type . '-' . basename($img)
            ]);

        } else {

            // we create a new record
            $im = DB::dailey('insert into data_image ('. $image_field . ') values (:img)', ['img' => '/data/images/' . $post['id'] . '-' . $source_type . '-' . basename($img)]);

            // update content record with new image id
            DB::dailey('update content set data_image_id = :img where id = :id', ['id' => $post['id'], 'img' => $im]);
        }
    }

    $temp = DB::dailey('select di.id, di.image1 from content c join data_image di on di.id = c.data_image_id where c.id = :id', ['id' => $post['id']]);

    if (strlen($temp[0]['image1']) > 5) {

        $image  = new \Eventviva\ImageResize($_SERVER['DOCUMENT_ROOT'] . $temp[0]['image1']);
        $name   = explode('.', $temp[0]['image1']);

        if ($width < 620) {

            $image->resizeToHeight(430, $allow_enlarge = true);

        } else {

            $image->resizeToBestFit(620, 430, $allow_enlarge = true);
        }

        $image->crop(620, 430, $allow_enlarge = true);

        $name[count($name) - 2] .= '_thumb620x430';

        $name = implode('.', $name);

        $image->save($_SERVER['DOCUMENT_ROOT'] . $name);

        $bind['mid'] = $temp[0]['id'];
        $bind['img'] = $name;

        DB::dailey('update data_image set thumb = :img where id = :mid', $bind);
    }