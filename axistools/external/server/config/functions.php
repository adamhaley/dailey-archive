<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license http://axis.axis-studios.com/LICENSE MS-RSL License
 * @link    https://github.com/soloproyectos/axistools
 */
use soloproyectos\db\DbConnector;
use soloproyectos\db\DbSource;
use com\soloproyectos\common\text\TextHelper;

/**
 * Gets rows from interrelated tables.
 *
 * @param DbConnector  $db         Database connector
 * @param string       $main_table Main table
 * @param array|string $ids        List of identifiers
 * @param array|string $values     List of values
 * @param array|string $filters    List of filters (not required)
 * @param array|string $order_by   List of orders
 *
 * @return DbSource
 */
function get_list_from_table($db, $main_table, $ids, $values, $filters = array(), $order_by = array())
{
    $ret = array();

    $tables = array();
    $ids = is_array($ids)? $ids: array($ids);
    $values = is_array($values)? $values: array($values);
    $filters = is_array($filters)? $filters: array($filters);
    $order_by = is_array($order_by)? $order_by: array($order_by);

    // order
    $orders = array();
    foreach ($order_by as $order) {
        $fields = array_merge($ids, $values);
        $pos = 1;
        $found = false;
        foreach ($fields as $field) {
            if ($order == $field) {
                $found = true;
                break;
            }
            $pos++;
        }
        if ($found) {
            array_push($orders, $pos);
        }
    }


    // search for tables
    $tables = array($main_table => array("alias" => "join_0", "key" => "id"));
    $search = array_merge($ids, $values, $filters);
    $size = sizeof($search);
    $i = 1;
    foreach ($search as $search) {
        // preg_match("/(\w+)(?(?=\.)\.(\w+))/", $search, $matches);
        preg_match("/(\w+)(?(?=\[)\[(\w+)\])(?(?=\.)\.(\w+))/", $search, $matches);
        if (sizeof($matches) > 3) {
            $table_name = $matches[1];
            $hook = $matches[2];
            if (($table_name != $main_table) && (array_key_exists($table_name, $tables) === false)) {
                $tables[$table_name] = array("alias" => "join_".$i, "hook" => $hook);
                $i++;
            }
        }
    }

    // search for ids
    $_ids = array();
    $size = sizeof($ids);
    $i = 0;
    foreach ($ids as $id) {
        preg_match("/(\w+)(?(?=\.)\.(\w+))/", $id, $matches);
        if (sizeof($matches) > 2) {
            $table_name = $matches[1];
            $fieldName = $matches[2];
            $alias = "field_".$i;
            array_push(
                $_ids,
                array(
                    "table" => $tables[$table_name]["alias"],
                    "name" => $fieldName, "alias" => "id_".$i
                )
            );
        } else {
            array_push(
                $_ids,
                array("table" => $tables[$main_table]["alias"], "name" => $id, "alias" => "id_".$i)
            );
        }
        $i++;
    }
    $ids = $_ids;

    // search for values
    $_values = array();
    $size = sizeof($ids);
    $i = 0;
    foreach ($values as $value) {
        $field_info = false;
        $pos = strpos($value, "*");
        if (($pos !== false) && ($pos == 0)) {
            $value = substr($value, $pos + 1);
            $field_info = true;
        }

        $field_extra = false;
        if (preg_match("/^\[(.*)\]$/", $value, $matches)) {
            $value = $matches[1];
            $field_extra = true;
        }

        preg_match("/(\w+)(?(?=\[)\[(\w+)\])(?(?=\.)\.(\w+))/", $value, $matches);
        if (sizeof($matches) > 3) {
            $table_name = $matches[1];
            $fieldName = $matches[3];
            $alias = "field_".$i;
            array_push(
                $_values,
                array(
                    "table" => $tables[$table_name]["alias"],
                    "name" => $fieldName,
                    "alias" => "field".$i,
                    "field_info" => $field_info, "field_extra" => $field_extra
                )
            );
        } else {
            array_push(
                $_values,
                array(
                    "table" => $tables[$main_table]["alias"],
                    "name" => $value,
                    "alias" => "field".$i,
                    "field_info" => $field_info,
                    "field_extra" => $field_extra
                )
            );
        }
        $i++;
    }
    $values = $_values;

    // search for filters
    $_filters = array();
    $size = sizeof($ids);
    $i = 0;
    foreach ($filters as $filter) {
        preg_match("/(\w+)\s+\=\s+([\w\.]+)/", $filter, $matches);

        preg_match("/(\w+)(?(?=\.)\.(.+))/", $filter, $matches);
        if (sizeof($matches) > 2) {
            $table_name = $matches[1];
            $filter = $matches[2];
            array_push(
                $_filters,
                array("table" => $tables[$table_name]["alias"], "condition" => $filter)
            );
        } else {
            if (preg_match("/(\w+)\s+\=\s+([\w\.]+)/", $filter, $matches)) {
                $a = $matches[1];
                $b = $matches[2];
                $filter = $a." = ".$db->quote($b);
            }
            array_push(
                $_filters,
                array("table" => $tables[$main_table]["alias"], "condition" => $filter)
            );
        }
        $i++;
    }
    $filters = $_filters;

    $sql = null;

    // sql fields
    $sql_fields = null;
    foreach ($ids as $id) {
        $sql_fields = TextHelper::concat(
            ",\n", $sql_fields, "\t".$id["table"].".".$id["name"]." as ".$id["alias"]
        );
    }
    foreach ($values as $value) {
        $sql_fields = TextHelper::concat(
            ",\n", $sql_fields, "\t".$value["table"].".".$value["name"]." as ".$value["alias"]
        );
    }

    // sql tables
    $sql_tables = null;
    foreach ($tables as $table => $info) {
        if ($table == $main_table) {
            continue;
        }
        if (strlen($info["hook"]) > 0) {
            $sql_tables .= "left join ".$table." as ".$info["alias"]
                ." on ".$info["alias"].".id = join_0.".$info["hook"]."\n";
        } else {
            $sql_tables .= "left join ".$table." as ".$info["alias"]
                ." on ".$info["alias"].".id = join_0.".$table."_id\n";
        }
    }

    // sql filters
    $sql_filter = null;
    foreach ($filters as $filter) {
        $sql_filter .= "and ".$filter["table"].".".$filter["condition"]."\n";
    }

    // order
    $sql_order = null;
    if (sizeof($orders) > 0) {
        $sql_order = "order by ".implode(", ", $orders);
    }

    // sql
    $sql = "select\n"
        .$sql_fields."\n"
        ."from ".$main_table." as join_0\n"
        .$sql_tables
        ."where join_0.row_state = 'live'\n"
        .$sql_filter
        .$sql_order;

    $rows = $db->query($sql);
    foreach ($rows as $row) {
        // id
        $id = null;
        $size = sizeof($ids);
        for ($i = 0; $i < $size; $i++) {
            $id = TextHelper::concat(" ", $id, $row["id_$i"]);
        }

        // value
        $fields = array();
        $value = null;
        $size = sizeof($values);
        for ($i = 0; $i < $size; $i ++) {
            $fields["field$i"] = $row["field$i"];
            if ($values[$i]["field_extra"]) {
                continue;
            }

            if ($values[$i]["field_info"]) {
                $value = TextHelper::concat(" - ", $value, $row["field$i"]);
            } else {
                $value = TextHelper::concat(" ", $value, $row["field$i"]);
            }
        }

        if (TextHelper::isEmpty($value)) {
            continue;
        }

        $r = array(
            "id" => $id, "field" => $value, "title" => $value, "_lowercase_" => strtolower($value)
        );
        $r = array_merge($r, $fields);

        array_push($ret, $r);
    }

    // remove the duplicated rows
    $size = sizeof($ret);
    $i = 0;
    while ($i < $size - 1) {
        $j = $i + 1;
        while ($j < $size) {
            if (($ret[$i]["id"] === $ret[$j]["id"]) && ($ret[$i]["field"] === $ret[$j]["field"])) {
                array_splice($ret, $j, 1);
                $size --;
                continue;
            }
            $j ++;
        }
        $i ++;
    }

    // sort the array ascending by the '_lowercase_' attribute
    if (sizeof($orders) == 0) {
        usort(
            $ret,
            function ($a, $b) {
                return $b["_lowercase_"] < $a["_lowercase_"];
            }
        );
    }

    // remove the "lowercase" column
    $size = sizeof($ret);
    for ($i = 0; $i < $size; $i ++) {
        array_splice($ret[$i], 3, 1);
    }

    return $ret;
}

    function getProjectsByClient($db){
        $sql = "
    select
        dt.text as client,
        c.id
    from content as c
    inner join data_text as dt on dt.id = c.data_text_id
    where length(dt.text) > 0
    and c.section_id = 'projects'
    group by dt.text
    order by dt.text desc";
        return $db->query($sql);
    }

    function getProjectsByName($db, $dir, $client = 0){

        $clientQ = '';

        if ($client != 0) {

            $clientQ = 'join content c2 on c2.id = ' . $client;
        }

        $sql = "
    select
    dt.text as client,
        dt.text1 as name,
        c.id
    from content as c
    inner join data_text as dt on dt.id = c.data_text_id
    $clientQ
    where length(dt.text) > 0
    and c.section_id = 'projects'
    order by dt.text1 " . $dir;
        return $db->query($sql);
    }