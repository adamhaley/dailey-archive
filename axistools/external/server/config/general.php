<?php
/**
 * Background and logo images.
 */
define("BACKGROUND_IMAGE", "config/images/background.jpg");
define("BACKGROUND_COLOR", "#333333");
define("LOGO_IMAGE", "config/images/logo.gif");

    $name = 'dailey_master';
    $host = 'axervices-backend.cxmkn58rekqs.us-west-2.rds.amazonaws.com';

    if (preg_match('/dev/', $_SERVER['HTTP_HOST'])) {
    	//$host = 'localhost';
        $name = 'dailey_dev';
    }

    if (preg_match('/staging/', $_SERVER['HTTP_HOST'])) {

        $name = 'dailey_staging';
    }

/*
 * Database connection
 */
define("DBHOST", $host);
define("DBNAME", $name);
define("DBUSER", "dailey");
define("DBPASS", "dailey");
define("DBCHARSET", "utf8mb4_unicode_ci");

/**
 * Emails
 */
define("BUG_REPORT_MAIL", "gonzalo@axis-studios.com");
define("NOREPLY_MAIL", "noreply@axis-studios.com");

/**
 * Absolute path to the sitemap file.
 *
 * Use *.php extension for a dynamic sitemap.
 */
define("SITEMAP", __DIR__ . "/sitemap.xml");

/**
 * Session expiration time.
 */
$expTime = 10 * 60 * 60; // 10 hours
ini_set("session.gc_maxlifetime", $expTime);
ini_set("session.cookie_lifetime", $expTime);

/**
 * Temporary folder.
 */
define("TMP_DIR", sys_get_temp_dir());
