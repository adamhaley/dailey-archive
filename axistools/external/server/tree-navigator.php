<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license http://axis.axis-studios.com/LICENSE MS-RSL License
 * @link    https://github.com/soloproyectos/axistools
 */
require_once "config.php";
require_once rtrim(CONFIG_DIR, "/") . "/general.php";
require_once "autoload.php";
require_once "vendor/autoload.php";
use com\axisgraphics\axis\http\controller\AxisHttpControllerTreeNavigator;

header("Content-Type: text/xml; charset=utf-8");
$c = new AxisHttpControllerTreeNavigator();
echo $c->getView();
