<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
require_once "classes/thumbnail-controller.php";
use com\axisgraphics\axis\http\script\ThumbnailController;

//header("Content-Type: image/jpeg; charset=utf-8");
header("Content-Type: text/plain; charset=utf-8");
$c = new ThumbnailController();
