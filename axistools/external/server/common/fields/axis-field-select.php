<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use \StdClass;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\field\AxisField;

/**
 * Class AxisFieldSelect.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldSelect extends AxisField
{
    /**
     * Field options.
     * @var array of string
     */
    private $_options = array();

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isSortable = false;

        // retrieves the options from the 'options-list' node
        $options = $this->query("> *");
        if (count($options) == 0) {
            $optionsAttr = trim($this->attr("options"));

            if (preg_match("/^\w+$/", $optionsAttr)) {
                $root = $this->root();
                $optionsSrc = $root->query("> sources > item[name = $optionsAttr]");

                if (count($optionsSrc) == 0) {
                    throw new AxisException("Options list not found: $optionsAttr");
                }

                $options = $optionsSrc->query("> *");
            }
        }

        $this->_options = $this->_parseOptions($options);
    }

    /**
     * Gets options.
     *
     * @return array of string
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        return $this->_searchLabel($this->getValue(), $this->_options);
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $obj = new StdClass();
        $obj->value = $this->getValue();
        $obj->options = $this->_options;
        return $obj;
    }
    
    /**
     * Gets the string representation of a filter value.
     * 
     * @param string $value Filter value
     * 
     * @see AxisField::getFilter()
     * @return string
     */
    public function getFilter($value)
    {
        return $this->_searchLabel($value, $this->_options);
    }

    /**
     * Parses options.
     *
     * @param DomNode $options Options
     *
     * @return array
     */
    private function _parseOptions($options)
    {
        $ret = array();

        foreach ($options as $option) {
            $obj = new StdClass();

            if ($option->name() == "options") {
                $obj->label = $option->attr("label");
                $obj->value = $this->_parseOptions($option->query("> *"));
            } else {
                $obj = new StdClass();
                $obj->label = $option->text();
                $obj->value = $option->attr("value");
            }

            array_push($ret, $obj);
        }

        return $ret;
    }

    /**
     * Searches the label by value.
     *
     * @param string $value   Value
     * @param array  $options Options (not required)
     *
     * @return string
     */
    private function _searchLabel($value, $options)
    {
        $ret = "";

        foreach ($options as $option) {
            if (is_array($option->value)) {
                $ret = $this->_searchLabel($value, $option->value);
            } elseif ($option->value == $value) {
                $ret = $option->label;
            }

            if (strlen($ret) > 0) {
                return $ret;
            }
        }

        return "";
    }
}
