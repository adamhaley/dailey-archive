<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldVideo.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldVideo extends AxisField
{
    /**
     * Poster column.
     * @var AxisField
     */
    private $_posterColumn;

    /**
     * Thumbnail column.
     * @var AxisField
     */
    private $_thumbnailColumn;

    /**
     * Extra video columns.
     * @var array of AxisField
     */
    private $_extraVideoColumns;

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isFiltrable = false;
        $this->isSortable = false;
        $this->_posterColumn = $this->registerColumn("snapshot > poster");
        $this->_thumbnailColumn = $this->registerColumn("snapshot > thumbnail");
        $this->_extraVideoColumns = $this->registerColumns("extra-videos > item");
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        return TextHelper::ifEmpty(
            $this->_thumbnailColumn->getValue(), $this->_posterColumn->getValue()
        );
    }

    /**
     * This field is represented as an image in the 'table viewer'.
     *
     * @see AxisField::isTableImage()
     * @return boolean
     */
    public function isTableImage()
    {
        return true;
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        return array(
            "video" => array(
                "src" => $this->getValue(),
                "preset" => $this->attr("preset"),
                "width" => $this->attr("width"),
                "height" => $this->attr("height")
            ),
            "poster" => array(
                "src" => $this->_posterColumn->getValue(),
                "width" => $this->_posterColumn->attr("width"),
                "height" => $this->_posterColumn->attr("height")
            ),
            "thumbnail" => array(
                "src" => $this->_thumbnailColumn->getValue(),
                "width" => $this->_thumbnailColumn->attr("width"),
                "height" => $this->_thumbnailColumn->attr("height")
            ),
            "extraVideos" => array_map(
                function ($item) {
                    return array(
                        "src" => $item->getValue(),
                        "preset" => $item->attr("preset"),
                        "width" => $item->attr("width"),
                        "height" => $item->attr("height")
                    );
                },
                $this->_extraVideoColumns
            )
        );
    }

    /**
     * Sets the object.
     *
     * @param mixed $object Object
     *
     * @see AxisField::setEditObject()
     * @return void
     */
    public function setEditObject($object)
    {
        $this->setValue($object->video->src);
        $this->_posterColumn->setValue($object->poster->src);
        $this->_thumbnailColumn->setValue($object->thumbnail->src);

        // sets extra videos
        $items = $object->extraVideos;
        foreach ($this->_extraVideoColumns as $extraVideo) {
            $item = current($items);
            $extraVideo->setValue($item->src);
            next($items);
        }
    }
}
