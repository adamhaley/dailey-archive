<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldImage.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldImage extends AxisField
{
    /**
     * Original image column.
     * @var AxisField
     */
    private $_originalImageColumn;
    
    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isFiltrable = false;
        $this->isSortable = false;
        
        if (count($this->query("original-image")) > 0) {
            $this->_originalImageColumn = $this->registerColumn("original-image");
        }
    }
    
    /**
     * This field is represented as an image in the 'table viewer'.
     *
     * @see AxisField::isTableImage()
     * @return boolean
     */
    public function isTableImage()
    {
        return true;
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $originalImage = "";
        $hasOriginalImage = $this->_originalImageColumn !== null;
        if ($hasOriginalImage) {
            $originalImage = $this->_originalImageColumn->getValue();
        }
        return array(
            "value" => $this->getValue(),
            "hasOriginalImage" => $hasOriginalImage,
            "originalImage" => $originalImage,
            "width" => $this->attr("width"),
            "height" => $this->attr("height"),
            "watermark" => $this->hasAttr("watermark")? $this->attr("watermark"): null
        );
    }
    
    /**
     * Sets the object.
     *
     * @param StdClass $object Object
     *
     * @see AxisField::setEditObject()
     * @return void
     */
    public function setEditObject($object)
    {
        $this->setValue($object->image);

        if ($this->_originalImageColumn != null) {
            $this->_originalImageColumn->setValue($object->originalImage);
        }
    }
}
