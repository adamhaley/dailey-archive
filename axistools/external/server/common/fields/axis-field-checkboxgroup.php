<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use \StdClass;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldCheckboxgroup.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldCheckboxgroup extends AxisField
{
    /**
     * Field options.
     * @var array of string
     */
    private $_options = array();

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isSortable = false;
        $options = $this->query("> *");

        // retrieves the options from the 'options-list' node
        if (count($options) == 0) {
            $optionsAttr = trim($this->attr("options"));

            if (preg_match("/^\w+$/", $optionsAttr)) {
                $root = $this->root();
                $optionsSrc = $root->query("> sources > item[name = $optionsAttr]");

                if (count($optionsSrc) == 0) {
                    throw new AxisException("Options list not found: $optionsAttr");
                }

                $options = $optionsSrc->query("> *");
            }
        }

        $this->_options = $this->_parseOptions($options);
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        $ret = "";
        $items = preg_split('/\s*,\s*/', trim($this->getValue()));

        foreach ($items as $item) {
            // searches label by value
            $label = "";
            foreach ($this->_options as $option) {
                if ($option->value == $item) {
                    $label = $option->label;
                    break;
                }
            }

            $ret = TextHelper::concat(", ", $ret, $label);
        }

        return $ret;
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $obj = new StdClass();
        $obj->value = preg_split('/\s*,\s*/', trim($this->getValue()));
        $obj->options = $this->_options;
        return $obj;
    }

    /**
     * Sets the object.
     *
     * @param array $object Object
     *
     * @see AxisField::setEditObject()
     * @return void
     */
    public function setEditObject($object)
    {
        $this->setValue(implode(",", $object));
    }
    
    /**
     * Gets the string representation of a filter value.
     * 
     * @param mixed $value Filter value
     * 
     * @see AxisField::getFilter()
     * @return string
     */
    public function getFilter($value)
    {
        $ret = "";

        foreach ($value as $item) {
            // searches label by value
            $label = "";
            foreach ($this->_options as $option) {
                if ($option->value == $item) {
                    $label = $option->label;
                    break;
                }
            }

            $ret = TextHelper::concat(", ", $ret, $label);
        }

        return $ret;
    }
    
    /**
     * Filter records by value.
     * 
     * This method overrides AxisField::filter().
     * 
     * @param Traversable $records List of records
     * @param array       $value   Value
     * 
     * @return Traversable
     */
    public function filter($records, $value)
    {
        $ret = $records;
        if (count($value) > 0) {
            $filter = AxisDbFilterFactory::getInstance("contain");
            $ret = $filter->filter($records, $this->attr("column"), implode(",", $value));
        }
        return $ret;
    }
    
    /**
     * Parses options.
     *
     * @param DomNode $options Options
     *
     * @return array
     */
    private function _parseOptions($options)
    {
        $ret = array();

        foreach ($options as $option) {
            $obj = new StdClass();

            if ($option->name() == "options") {
                $obj->label = $option->attr("label");
                $obj->value = $this->_parseOptions($option->query("> *"));
            } else {
                $obj = new StdClass();
                $obj->label = $option->text();
                $obj->value = $option->attr("value");
            }

            array_push($ret, $obj);
        }

        return $ret;
    }
}
