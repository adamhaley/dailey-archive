<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldText.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldText extends AxisField
{
    /**
     * Filter records by value.
     * 
     * This method overrides AxisField::filter().
     * 
     * @param Traversable $records List of records
     * @param string      $value   Value
     * 
     * @return Traversable
     */
    public function filter($records, $value)
    {
        $ret = $records;
        if (!TextHelper::isEmpty(trim($value))) {
            $filter = AxisDbFilterFactory::getInstance("match");
            $ret = $filter->filter($records, $this->attr("column"), $value);
        }
        return $ret;
    }
}
