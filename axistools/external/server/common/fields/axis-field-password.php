<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldPassword.
 *
 * This field is used to store passwords. It uses both, password_hash() and hash() to encrypt your
 * password. When the 'method' attribute is 'bcrypt', it uses password_hash():
 * http://php.net/manual/en/function.password-hash.php
 *
 * Otherwise, this class uses hash():
 * http://php.net/manual/en/function.hash.php
 *
 * See hash_algos() for a complete list of available algorithms:
 * http://php.net/manual/en/function.hash-algos.php
 *
 * By default this class uses 'bcrypt' algorithm.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldPassword extends AxisField
{
    /**
     * Default algorithm.
     * When the 'method' attribute is not present, this class uses the default 'bcrypt' algorithm
     * and password_hash() to encrypt your password.
     * @var string
     */
    private $_bcryptAlgorithm = "bcrypt";

    /**
     * Minimum field length.
     * @var integer
     */
    private $_minLength = 0;

    /**
     * Method algorithm.
     * When using 'bcrypt' this class uses password_hash(), otherwise it uses hash().
     * @var string
     */
    private $_method = "";

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isFiltrable = false;
        $this->isSortable = false;
        $this->_minLength = intval($this->attr("min-length"));

        // algorithm
        $this->_method = TextHelper::ifEmpty($this->attr("method"), $this->_bcryptAlgorithm);
        if (!$this->_isValidAlgorithm($this->_method)) {
            throw new AxisException(
                "Invalid method algorithm: '{$this->_method}'"
            );
        }
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        return !TextHelper::isEmpty($this->getValue())? "*****" : "";
    }


    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return string
     */
    public function getEditObject()
    {
        return !TextHelper::isEmpty($this->getValue())? "*****" : "";
    }

    /**
     * Sets the object.
     *
     * @param mixed $object Object
     *
     * @see AxisField::setEditObject()
     * @return void
     */
    public function setEditObject($object)
    {
        if ($object->hasChanged) {
            if ($this->_minLength > 0 && strlen($object->value) < $this->_minLength) {
                throw new AxisException(
                    "The password must have at least {$this->_minLength} characters"
                );
            }

            $value = $object->value;
            if (!TextHelper::isEmpty($value)) {
                $value = $this->_getEncryptedString($object->value);
            }
            $this->setValue($value);
        }
    }

    /**
     * Gets the encrypted string.
     *
     * When using 'bcrypt', this method uses password_hash(). Otherwise, it uses hash();
     *
     * @param string $str String
     *
     * @return string
     */
    private function _getEncryptedString($str)
    {
        $ret = $this->_method == $this->_bcryptAlgorithm
            ? password_hash($str, PASSWORD_BCRYPT)
            : @hash($this->_method, $str);

        if ($ret === false) {
            throw new AxisException("Invalid algorithm '{$this->_method}'");
        }

        return $ret;
    }

    /**
     * Is a vailid algorithm?
     *
     * @param string $algo Algorithm method
     *
     * @return boolean
     */
    private function _isValidAlgorithm($algo)
    {
        return $this->_method == $this->_bcryptAlgorithm || !is_bool(@hash($this->_method, ""));
    }
}
