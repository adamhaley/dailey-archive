<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use \StdClass;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldTime.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldTime extends AxisField
{
    /**
     * MySQL time format.
     * @var string
     */
    private $_mySqlTimeFormat = "H:i:s";

    /**
     * Date format.
     * see: http://php.net/manual/en/function.date.php
     */
    private $_timeFormat = "24h";

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isAscending = false;
        $format = trim($this->attr("format"));
        if (!TextHelper::isEmpty($format)) {
            $this->_timeFormat = $format;
        }
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        $format = "H:i:s";
        $value = trim($this->getValue());
        $timestamp = strtotime($value);

        if ($timestamp !== false && $this->_timeFormat == "12h") {
            $dateInfo = getdate($timestamp);
            $hours = $dateInfo["hours"];
            $suffix = $hours < 12? "\\A\\M" : "\\P\\M";
            $format = "g:i:s $suffix";
        }

        return $timestamp !== false? date($format, $timestamp) : "";
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $value = $this->getValue();
        if ($value == "current-time") {
            $value = date($this->_mySqlTimeFormat);
        }

        $obj = new StdClass();
        $obj->value = $value;
        $obj->format = $this->_timeFormat;
        return $obj;
    }
    
    /**
     * Filter records by value.
     * 
     * This method overrides AxisField::filter().
     * 
     * @param Traversable $records List of records
     * @param string      $value   Value
     * 
     * @return Traversable
     */
    public function filter($records, $value)
    {
        $ret = $records;
        if (count($value) > 0) {
            $filter = AxisDbFilterFactory::getInstance("match");
            $ret = $filter->filter($records, $this->attr("column"), "/^$value/");
        }
        return $ret;
    }
}
