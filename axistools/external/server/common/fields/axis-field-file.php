<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\field\AxisField;

/**
 * Class AxisFieldFile.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldFile extends AxisField
{
    /**
     * Text column.
     * @var AxisField
     */
    private $_textColumn;

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    function init()
    {
        $this->isFiltrable = false;
        $this->isSortable = false;
        
        if (count($this->query("text")) > 0) {
            $this->_textColumn = $this->registerColumn("text");
        }
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        return $this->_textColumn != null && strlen($this->_textColumn->getValue()) > 0
            ? $this->_textColumn->getValue()
            : $this->getValue();
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $ret = array(
            "url" => $this->getValue(),
            "text" => "",
            "showText" => false,
            "showSelector" => $this->attr("show-selector") == "true"
        );

        if ($this->_textColumn != null) {
            $ret["showText"] = true;
            $ret["text"] = $this->_textColumn->getValue();
        }

        return $ret;
    }

    /**
     * Sets the object.
     *
     * @param StdClass $object Object
     *
     * @see AxisField::setEditObject()
     * @return void
     */
    public function setEditObject($object)
    {
        $this->setValue($object->url);

        if ($this->_textColumn != null) {
            $this->_textColumn->setValue($object->text);
        }
    }
}
