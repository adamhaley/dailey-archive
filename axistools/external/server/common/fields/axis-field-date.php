<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use \StdClass;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldDate.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldDate extends AxisField
{
    /**
     * MySQL date format.
     * @var string
     */
    private $_mySqlDateFormat = "Y-m-d";

    /**
     * Date format.
     * see: http://php.net/manual/en/function.date.php
     */
    private $_dateFormat = "F j, Y";

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isAscending = false;
        $format = trim($this->attr("format"));
        if (!TextHelper::isEmpty($format)) {
            $this->_dateFormat = $format;
        }
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        $value = trim($this->getValue());
        $timestamp = strtotime($value);
        return $timestamp !== false? date($this->_dateFormat, $timestamp) : "";
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $value = $this->getValue();
        if ($value == "now") {
            $value = date($this->_mySqlDateFormat);
        }

        $obj = new StdClass();
        $obj->value = $value;
        $obj->format = $this->_dateFormat;
        return $obj;
    }
    
    /**
     * Filter records by value.
     * 
     * This method overrides AxisField::filter().
     * 
     * @param Traversable $records List of records
     * @param string      $value   Value
     * 
     * @return Traversable
     */
    public function filter($records, $value)
    {
        $ret = $records;
        if (count($value) > 0) {
            $filter = AxisDbFilterFactory::getInstance("match");
            $ret = $filter->filter($records, $this->attr("column"), "/^$value/");
        }
        return $ret;
    }
}
