<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use \StdClass;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\field\AxisField;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisFieldTextarea.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldTextarea extends AxisField
{
    /**
     * Is plaintext?
     * @var boolean
     */
    private $_isPlainText = false;

    /**
     * Font family.
     * @var string
     */
    private $_fontFamily = "";

    /**
     * Font size.
     * @var string
     */
    private $_fontSize = "";

    /**
     * Text color.
     * @var string
     */
    private $_color = "";

    /**
     * Initialization.
     *
     * @see AxisField::init()
     * @return void
     */
    public function init()
    {
        $this->isFiltrable = false;
        $this->isSortable = false;
        $root = $this->root();
        $config = $root->query("> config > editors > textarea");

        $this->_isPlainText = TextHelper::isEmpty($this->attr("plaintext"))
            ? $config->query("plaintext")->text() == "true"
            : $this->attr("plaintext") == "true";
        $this->_fontFamily = $this->attr("font-family");
        $this->_fontSize = $this->attr("font-size");
        $this->_color = $this->attr("color");
    }

    /**
     * Gets the value used by the 'table viewer'.
     *
     * @see AxisField::getTableValue()
     * @return string
     */
    public function getTableValue()
    {
        return $this->_isPlainText? htmlspecialchars($this->getValue()) : $this->getValue();
    }

    /**
     * Gets the object used by the 'record editor'.
     *
     * @see AxisField::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        $obj = new StdClass();
        $obj->value = $this->getValue();
        $obj->isPlainText = $this->_isPlainText;
        $obj->fontFamily = $this->_fontFamily;
        $obj->fontSize = $this->_fontSize;
        $obj->color = $this->_color;
        return $obj;
    }
}
