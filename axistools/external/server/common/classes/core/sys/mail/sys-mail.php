<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\sys\mail;
use \PHPMailer;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachment;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachmentData;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachmentFile;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachmentImage;
use com\soloproyectos\common\sys\mail\exception\SysMailException;
use com\soloproyectos\common\text\TextHelper;

require_once "php-mailer/PHPMailerAutoload.php";

/**
 * Class SysMail.
 *
 * @package Sys\Mail
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class SysMail
{
    /**
     * Sender email address.
     * @var string
     */
    private $_sender;

    /**
     * Recipient email addresses.
     * @var array of string
     */
    private $_recipients;

    /**
     * Carbon copy email addresses.
     * @var array of string
     */
    private $_ccRecipients;

    /**
     * Blind carbon copy email addresses.
     * @var array of string
     */
    private $_bccRecipiens;

    /**
     * Attachment objects.
     * @var array of SysMailAttachment
     */
    private $_attachments;

    /**
     * Content-Type.
     * @var string
     */
    private $_contentType;

    /**
     * Character set.
     * @var string
     */
    private $_charset;

    /**
     * Email subject.
     * @var string
     */
    private $_subject;

    /**
     * Email body.
     * @var string
     */
    private $_body;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->_sender = "";
        $this->_recipients = array();
        $this->_ccRecipients = array();
        $this->_bccRecipients = array();
        $this->_attachments = array();
        $this->_contentType = "text/plain";
        $this->_charset = "iso-8859-1";
        $this->_subject = "";
        $this->_body = "";
    }

    /**
     * Gets the sender.
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->_sender;
    }

    /**
     * Sets the sender.
     *
     * @param string $email Email address
     *
     * @return void
     */
    public function setFrom($email)
    {
        $this->_sender = $email;
    }

    /**
     * Adds recipient email.
     *
     * @param string $email Email address
     * @param string $name  Name (not required)
     *
     * @return void
     */
    public function addTo($email, $name = "")
    {
        if (!TextHelper::isEmpty($name)) {
            $email = "$name <$email>";
        }

        array_push($this->_recipients, $email);
    }

    /**
     * Adds carbon copy email.
     *
     * @param string $email Email address
     *
     * @return void
     */
    public function addCc($email)
    {
        array_push($this->_ccRecipients, $email);
    }

    /**
     * Adds blind carbon copy email.
     *
     * @param string $email Email address
     *
     * @return void
     */
    public function addBcc($email)
    {
        array_push($this->_bccRecipients, $email);
    }

    /**
     * Adds an attachment.
     *
     * @param SysMailAttachment $attachment Attachment
     *
     * @return void
     */
    public function addAttachment($attachment)
    {
        array_push($this->_attachments, $attachment);
    }

    /**
     * Attaches a file.
     *
     * @param string $path Path to file
     * @param string $name Attachment name (not required)
     *
     * @return void
     */
    public function addFile($path, $name = "")
    {
        $attachment = new SysMailAttachmentFile($path);
        $attachment->setName($name);
        $this->addAttachment($attachment);
    }

    /**
     * Attaches embedded data.
     *
     * @param string $data Binary data
     * @param string $name Attachment name
     *
     * @return void
     */
    public function addData($data, $name)
    {
        $attachment = new SysMailAttachmentData($data, $name);
        $this->addAttachment($attachment);
    }

    /**
     * Attaches an embedded image.
     *
     * Example:
     * ```php
     * $mail = new SysMail();
     * $mail->setFrom("from@email.com");
     * $mail->addEmail("to@email.com");
     * $mail->addImage("path/to/image.jpg", "my-image-id");
     * $htmlBody = '...<img src="cid:my-image-id" alt="" />...';
     * $mail->send("Test", $htmlBody, "text/html");
     * ```
     *
     * @param string $path Path to image
     * @param string $cid  Content ID (referenced by the HTML of the message body)
     * @param string $name Attachment name (not required)
     *
     * @return void
     */
    public function addImage($path, $cid, $name = "")
    {
        $attachment = new SysMailAttachmentImage($path, $cid);
        $attachment->setName($name);
        $this->addAttachment($attachment);
    }

    /**
     * Gets the Content-Type.
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->_contentType;
    }

    /**
     * Sets the Content-Type.
     *
     * @param string $contentType Content-Type (text/plain, text/html, ...)
     *
     * @return void
     */
    public function setContentType($contentType)
    {
        $this->_contentType = $contentType;
    }

    /**
     * Gets character set.
     *
     * @return string
     */
    public function getCharset()
    {
        return $this->_charset;
    }

    /**
     * Sets character set.
     *
     * @param string $charset Character set (iso-8859-1, UTF-8, ...)
     *
     * @return void
     */
    public function setCharset($charset)
    {
        $this->_charset = $charset;
    }

    /**
     * Gets the subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * Sets the subject.
     *
     * @param string $text Subject
     *
     * @return void
     */
    public function setSubject($text)
    {
        $this->_subject = $text;
    }

    /**
     * Gets the body text.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * Sets the body text.
     *
     * @param string $text Body text
     *
     * @return void
     */
    public function setBody($text)
    {
        $this->_body = $text;
    }

    /**
     * Sends an email.
     *
     * @param string $subject     Email subject
     * @param string $body        Email body
     * @param string $contentType Content Type (default is 'text/plain')
     * @param string $charset     Character Set (default is 'ISO-8859-1')
     *
     * @throws SysEmailException
     * @return void
     */
    public function send($subject = "", $body = "", $contentType = "text/plain", $charset = "ISO-8859-1")
    {

        $mail = new PHPMailer();

        // sets the sender
        list($email, $name) = $this->_parseEmail($this->_sender);
        $mail->From = $email;
        $mail->FromName = $name;

        // adds recipients
        foreach ($this->_recipients as $recipient) {
            list($email, $name) = $this->_parseEmail($recipient);
            $mail->addAddress($email, $name);
        }

        // adds carbon copy recipients
        foreach ($this->_ccRecipients as $recipient) {
            list($email, $name) = $this->_parseEmail($recipient);
            $mail->addCC($email, $name);
        }

        // adds blind carbon copy recipients
        foreach ($this->_ccRecipients as $recipient) {
            list($email, $name) = $this->_parseEmail($recipient);
            $mail->addBCC($email, $name);
        }

        // adds attachments
        foreach ($this->_attachments as $attachment) {
            if ($attachment instanceof SysMailAttachmentImage) {
                $mail->addEmbeddedImage(
                    $attachment->getPath(),
                    $attachment->getCid(),
                    $attachment->getName(),
                    $attachment->getEncoding(),
                    $attachment->getMimeType(),
                    $attachment->getDisposition()
                );
            } elseif ($attachment instanceof SysMailAttachmentFile) {
                $mail->addAttachment(
                    $attachment->getPath(),
                    $attachment->getName(),
                    $attachment->getEncoding(),
                    $attachment->getMimeType(),
                    $attachment->getDisposition()
                );
            } else {
                $mail->addStringAttachment(
                    $attachment->getData(),
                    $attachment->getName(),
                    $attachment->getEncoding(),
                    $attachment->getMimeType(),
                    $attachment->getDisposition()
                );
            }
        }

        $mail->Subject = TextHelper::ifEmpty($subject, $this->_subject);
        $mail->Body = TextHelper::ifEmpty($body, $this->_body);
        $mail->ContentType = TextHelper::ifEmpty($contentType, $this->_contentType);
        $mail->CharSet = TextHelper::ifEmpty($charset, $this->_charset);

        if (!$mail->send()) {
            throw new SysMailException($mail->ErrorInfo);
        }
    }

    /**
     * Extracts name and email from an email address.
     *
     * @param string $email Email address
     *
     * @return array
     */
    private function _parseEmail($email)
    {
        $name = "";

        // Full Name <email@address.com>
        if (preg_match("/(.*)<(.*)>$/U", trim($email), $matches)) {
            $name = trim($matches[1]);
            $email = trim($matches[2]);
        }

        return array($email, $name);
    }
}
