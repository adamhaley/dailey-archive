<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\sys\mail\attachment;

/**
 * Class SysMailAttachment.
 *
 * @package Sys\Mail\Attachment
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
abstract class SysMailAttachment
{
    /**
     * Name.
     * @var string
     */
    protected $name;

    /**
     * Mime-type
     * @var string
     */
    protected $mimeType;

    /**
     * Encoding.
     * @var string
     */
    protected $encoding;

    /**
     * Disposition.
     * @var string
     */
    protected $disposition;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->name = "";
        $this->mimeType = "";
        $this->encoding = "base64";
        $this->disposition = "attachment";
    }

    /**
     * Gets the name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name.
     *
     * @param string $name Resource name
     *
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Gets the mime-type.
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Sets the mime-type.
     *
     * @param string $mimeType Mime-type
     *
     * @return void
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * Gets the encoding.
     *
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * Sets the encoding.
     *
     * @param string $encoding Encoding
     *
     * @return void
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /**
     * Gets the disposition.
     *
     * @return string
     */
    public function getDisposition()
    {
        return $this->disposition;
    }

    /**
     * Sets the disposition.
     *
     * @param string $disposition Resource dispostition
     *
     * @return void
     */
    public function setDisposition($disposition)
    {
        $this->disposition = $disposition;
    }
}
