<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\sys\mail\attachment;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachmentFile;

/**
 * Class SysMailAttachmentImage.
 *
 * @package Sys\Mail\Attachment
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class SysMailAttachmentImage extends SysMailAttachmentFile
{
    /**
     * Content ID (referenced by the HTML of the message body)
     * @var string
     */
    private $_cid;

    /**
     * Constructor.
     *
     * @param string $path Path to file
     * @param string $cid  Content ID (referenced by the HTML of the message body)
     */
    public function __construct($path, $cid)
    {
        parent::__construct($path);
        $this->_cid = $cid;
        $this->disposition = "inline";
    }

    /**
     * Retuns content ID.
     *
     * @return string
     */
    public function getCid()
    {
        return $this->_cid;
    }

    /**
     * Sets content ID.
     *
     * @param string $cid Content ID
     *
     * @return void
     */
    public function setCid($cid)
    {
        $this->_cid = $cid;
    }
}
