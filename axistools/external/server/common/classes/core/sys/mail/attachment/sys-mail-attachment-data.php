<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\sys\mail\attachment;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachment;

/**
 * Class SysMailAttachmentData.
 *
 * @package Sys\Mail\Attachment
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class SysMailAttachmentData extends SysMailAttachment
{
    /**
     * Binary data.
     * @var string
     */
    private $_data;

    /**
     * Name.
     * @var string
     */
    private $_name;

    /**
     * Is the data embedded?
     * @var boolean
     */
    private $_isEmbedded;

    /**
     * Constructor.
     *
     * @param string  $data       Binary data
     * @param string  $name       Name
     * @param boolean $isEmbedded Is the data embedded? (default is false)
     */
    public function __construct($data, $name, $isEmbedded = false)
    {
        parent::__construct();
        $this->_data = $data;
        $this->_name = $name;
        $this->_isEmbedded = $isEmbedded;
    }

    /**
     * Gets the data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * Sets the data.
     *
     * @param string $data Binary data
     *
     * @return void
     */
    public function setData($data)
    {
        $this->_data = $data;
    }

    /**
     * Gets the name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets the name.
     *
     * @param string $name Resource name
     *
     * @return void
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * Is the data embedded?
     *
     * @return boolean
     */
    public function isEmbedded()
    {
        return $this->_isEmbedded;
    }

    /**
     * Sets the data embedded.
     *
     * @param boolean $isEmbedded Is the data embedded?
     *
     * @return void
     */
    public function setEmbedded($isEmbedded)
    {
        $this->_isEmbedded = $isEmbedded;
    }
}
