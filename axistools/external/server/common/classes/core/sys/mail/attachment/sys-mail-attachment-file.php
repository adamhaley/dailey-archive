<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\sys\mail\attachment;
use com\soloproyectos\common\sys\mail\attachment\SysMailAttachment;

/**
 * Class SysMailAttachmentFile.
 *
 * @package Sys\Mail\Attachment
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class SysMailAttachmentFile extends SysMailAttachment
{
    /**
     * Path to file.
     * @var string
     */
    private $_path;

    /**
     * Constructor.
     *
     * @param string $path Path to file
     */
    public function __construct($path)
    {
        parent::__construct();
        $this->_path = $path;
    }

    /**
     * Gets the file path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * Sets the file path.
     *
     * @param string $path Path to file
     *
     * @return void
     */
    public function setPath($path)
    {
        $this->_path = $path;
    }
}
