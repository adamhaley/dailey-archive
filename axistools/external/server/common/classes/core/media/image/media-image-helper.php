<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\media\image;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;

/**
 * Class MediaImageAbstract.
 *
 * @package Media\Image
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class MediaImageHelper
{
    /**
     * Gets image geometry.
     *
     * @param string $input Input filename
     *
     * @return array of [width, height, offsetX, offsetY]
     */
    public static function getImageGeometry($input)
    {
        $command = SysCmdHelper::replaceArgs(
            "identify -format {geometry} {input}",
            array("%W %H %X %Y", $input)
        );
        $str = SysCmdHelper::exec($command);
        list($width, $height, $offsetX, $offsetY) = explode(" ", $str);

        $base = pow(2, 31);
        if ($offsetX > $base) {
            $offsetX -= 2 * $base;
        }
        if ($offsetY > $base) {
            $offsetY -= 2 * $base;
        }

        return array($width, $height, $offsetX, $offsetY);
    }
}
