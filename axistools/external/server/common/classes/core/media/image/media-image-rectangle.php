<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\media\image;
use com\soloproyectos\common\media\image\MediaImageShapeAbstract;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;

/**
 * Class MediaImageRectangle.
 *
 * @package Media\Image
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class MediaImageRectangle extends MediaImageShapeAbstract
{
    /**
     * Rectangle color.
     * @var string|integer
     */
    private $_color;

    /**
     * Constructor.
     *
     * @param float          $width  Width
     * @param float          $height Height
     * @param string|integer $color  Color
     */
    public function __construct($width, $height, $color)
    {
        $this->setSize($width, $height);
        $this->setColor($color);
    }

    /**
     * Gets color.
     *
     * @return string|integer
     */
    public function getColor()
    {
        return $this->_bgColor;
    }

    /**
     * Sets color.
     *
     * Complete list of color names:
     * http://www.imagemagick.org/script/color.php#color_names
     *
     * @param string|integer $value Background color
     *
     * @return void
     */
    public function setColor($value)
    {
        if (is_numeric($value)) {
            $value = "#" . substr("000000" . dechex($value), -6);
        }

        $this->_color = $value;
    }

    /**
     * Gets the string representation of the object.
     *
     * @return string
     */
    public function __toString()
    {
        return SysCmdHelper::replaceArgs(
            "-size {width}x{height} xc:{color} " .
            "-background {bgColor} -virtual-pixel background " .
            "+distort SRT '{offsetX},{offsetY} {scaleX},{scaleY} {rotation} {x},{y}'",
            array(
                $this->getWidth(),
                $this->getHeight(),
                $this->_color,
                $this->getBgColor(),
                $this->getOffsetX(),
                $this->getOffsetY(),
                $this->getScaleX(),
                $this->getScaleY(),
                180 * $this->getRotation() / M_PI,
                $this->getX(),
                $this->getY()
            )
        );
    }
}
