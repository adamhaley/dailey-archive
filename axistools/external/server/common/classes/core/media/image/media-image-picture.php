<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\media\image;
use com\soloproyectos\common\media\image\exception\MediaImageException;
use com\soloproyectos\common\media\image\MediaImageShapeAbstract;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;

/**
 * Class MediaImagePicture.
 *
 * @package Media\Image
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class MediaImagePicture extends MediaImageShapeAbstract
{
    /**
     * Image filename.
     * @var string
     */
    private $_src = "";

    /**
     * Original image width.
     * @var float
     */
    private $_originalWidth = 0;

    /**
     * Original image height.
     * @var float
     */
    private $_originalHeight = 0;

    /**
     * Constructor.
     *
     * @param string $src Image filename
     */
    public function __construct($src)
    {
        if (!is_file($src)) {
            throw new MediaImageException("Image not found: $src");
        }

        $this->_src = $src;
        list($this->_originalWidth, $this->_originalHeight) = getimagesize($src);
        $this->setSize($this->_originalWidth, $this->_originalHeight);
    }

    /**
     * Gets image filename.
     *
     * @return string
     */
    public function getSrc()
    {
        return $this->_src;
    }

    /**
     * Sets image filename.
     *
     * @param string $value Image filename
     *
     * @return void
     */
    public function setSrc($value)
    {
        $this->_src = $value;
    }

    /**
     * Gets the string representation of the object.
     *
     * @return string
     */
    public function __toString()
    {
        return SysCmdHelper::replaceArgs(
            "{image} " .
            "-background {bgColor} -virtual-pixel background " .
            "+distort SRT '{offsetX},{offsetY} {scaleX},{scaleY} {rotation} {x},{y}'",
            array(
                $this->_src,
                $this->getBgColor(),
                $this->getOffsetX(),
                $this->getOffsetY(),
                $this->getScaleX() * ($this->getWidth() / $this->_originalWidth),
                $this->getScaleY() * ($this->getHeight() / $this->_originalHeight),
                180 * $this->getRotation() / M_PI,
                $this->getX(),
                $this->getY()
            )
        );
    }
}
