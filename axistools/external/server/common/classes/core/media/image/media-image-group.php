<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\media\image;
use com\soloproyectos\common\media\image\MediaImageAbstract;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class MediaImageGroup.
 *
 * @package Media\Image
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class MediaImageGroup extends MediaImageAbstract
{
    /**
     * Children.
     * @var array of MediaImageAbstract
     */
    private $_items = array();

    /**
     * Add an image.
     *
     * @param MediaImageAbstract $image Image child
     *
     * @return void
     */
    public function add($image)
    {
        array_push($this->_items, $image);
    }

    /**
     * Gets the string representation of the object.
     *
     * @return string
     */
    public function __toString()
    {
        $ret = "";

        foreach ($this->_items as $item) {
            $ret = TextHelper::concat(" ", $ret, "\\( $item \\)");
        }

        $ret = TextHelper::concat(
            " ",
            $ret,
            SysCmdHelper::replaceArgs(
                "-background {bgColor} -virtual-pixel background +distort SRT " .
                "'{offsetX},{offsetY} {scaleX},{scaleY} {rotation} {x},{y}'",
                array(
                    $this->getBgColor(),
                    $this->getOffsetX(),
                    $this->getOffsetY(),
                    $this->getScaleX(),
                    $this->getScaleY(),
                    180 * $this->getRotation() / M_PI,
                    $this->getX(),
                    $this->getY()
                )
            )
        );

        return $ret;
    }
}
