<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\media\image;
use com\soloproyectos\common\media\image\MediaImageAbstract;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class MediaImageShapeAbstract.
 *
 * @package Media\Image
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
abstract class MediaImageShapeAbstract extends MediaImageAbstract
{
    /**
     * Sets the image size.
     *
     * @param float $width  Width
     * @param float $height Height
     *
     * @return void
     */
    public function setSize($width, $height)
    {
        $this->setWidth($width);
        $this->setHeight($height);
    }

    /**
     * Gets the width.
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * Sets the width.
     *
     * @param float $value Value
     *
     * @return void
     */
    public function setWidth($value)
    {
        $this->_width = $value;
    }

    /**
     * Gets the height.
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * Sets the height.
     *
     * @param float $value Height
     *
     * @return void
     */
    public function setHeight($value)
    {
        $this->_height = $value;
    }
}
