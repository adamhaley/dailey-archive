<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\media\image;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;
use com\soloproyectos\common\media\image\MediaImageHelper;

/**
 * Class MediaImageAbstract.
 *
 * @package Media\Image
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
abstract class MediaImageAbstract
{
    /**
     * Colors within this distance are considered equal.
     * @var string
     */
    private $_fuzz = "7%";

    /**
     * X coordinate.
     * @var float
     */
    private $_x = 0;

    /**
     * Y coordinate.
     * @var float
     */
    private $_y = 0;

    /**
     * Width.
     * @var float
     */
    private $_width = 0;

    /**
     * Height.
     * @var float
     */
    private $_height = 0;

    /**
     * X offset coordinate.
     * @var float
     */
    private $_offsetX = 0;

    /**
     * Y offset coordinate.
     * @var float
     */
    private $_offsetY = 0;

    /**
     * X scale.
     * @var float
     */
    private $_scaleX = 1;

    /**
     * Y scale.
     * @var float
     */
    private $_scaleY = 1;

    /**
     * Rotation.
     * @var float
     */
    private $_rotation = 0;

    /**
     * Background color.
     * @var string|integer
     */
    private $_bgColor = "transparent";

    /**
     * Gets the string representation of the object.
     *
     * @return string
     */
    abstract public function __toString();

    /**
     * Sets the image position.
     *
     * @param float $x X coordinate
     * @param float $y Y coordinate
     *
     * @return void
     */
    public function setPosition($x, $y)
    {
        $this->setX($x);
        $this->setY($y);
    }

    /**
     * Sets the offset coordinates.
     *
     * @param float $x X offset coordinate
     * @param float $y Y offset coordinate
     *
     * @return void
     */
    public function setOffset($x, $y)
    {
        $this->setOffsetX($x);
        $this->setOffsetY($y);
    }

    /**
     * Sets the scale.
     *
     * @param float $x X scale
     * @param float $y Y scale (not required)
     *
     * @return void
     */
    public function setScale($x, $y = 0)
    {
        if (func_num_args() < 2) {
            $y = $x;
        }

        $this->setScaleX($x);
        $this->setScaleY($y);
    }

    /**
     * Gets X coordinate.
     *
     * @return float
     */
    public function getX()
    {
        return $this->_x;
    }

    /**
     * Sets X coordinate.
     *
     * @param float $value X coordinate
     *
     * @return void
     */
    public function setX($value)
    {
        $this->_x = $value;
    }

    /**
     * Gets Y coordinate.
     *
     * @return float
     */
    public function getY()
    {
        return $this->_y;
    }

    /**
     * Sets Y coordinate.
     *
     * @param float $value Y coordinate
     *
     * @return void
     */
    public function setY($value)
    {
        $this->_y = $value;
    }

    /**
     * Gets the X offset coordinate.
     *
     * @return float
     */
    public function getOffsetX()
    {
        return $this->_offsetX;
    }

    /**
     * Sets the X offset coordinate.
     *
     * @param float $value X offset coordinate.
     *
     * @return void
     */
    public function setOffsetX($value)
    {
        $this->_offsetX = floatval($value);
    }

    /**
     * Gets the Y offset coordinate.
     *
     * @return float
     */
    public function getOffsetY()
    {
        return $this->_offsetY;
    }

    /**
     * Sets the Y offset coordinate.
     *
     * @param float $value Y offset coordinate
     *
     * @return void
     */
    public function setOffsetY($value)
    {
        $this->_offsetY = floatval($value);
    }

    /**
     * Gets the X scale.
     *
     * @return float
     */
    public function getScaleX()
    {
        return $this->_scaleX;
    }

    /**
     * Sets the Y scale.
     *
     * @param float $value Y scale
     *
     * @return void
     */
    public function setScaleX($value)
    {
        $this->_scaleX = floatval($value);
    }

    /**
     * Gets the Y scale.
     *
     * @return float
     */
    public function getScaleY()
    {
        return $this->_scaleY;
    }

    /**
     * Sets the Y scale.
     *
     * @param float $value Y scale
     *
     * @return void
     */
    public function setScaleY($value)
    {
        $this->_scaleY = floatval($value);
    }

    /**
     * Gets the rotation in radians.
     *
     * @return float
     */
    public function getRotation()
    {
        return $this->_rotation;
    }

    /**
     * Sets the rotation in radians.
     *
     * @param float $value Rotation in radians
     *
     * @return void
     */
    public function setRotation($value)
    {
        $this->_rotation = floatval($value);
    }

    /**
     * Gets background color.
     *
     * @return string|integer
     */
    public function getBgColor()
    {
        return $this->_bgColor;
    }

    /**
     * Sets background color.
     *
     * Complete list of color names:
     * http://www.imagemagick.org/script/color.php#color_names
     *
     * @param string|integer $value Background color
     *
     * @return void
     */
    public function setBgColor($value)
    {
        if (is_numeric($value)) {
            $value = "#" . substr("000000" . dechex($value), -6);
        }

        $this->_bgColor = $value;
    }

    /**
     * Saves image in a file or returns the data.
     *
     * @param string $output     Output filename
     * @param float  $cropWidth  Crop width (default is 0)
     * @param float  $cropHeight Crop height (default is 0)
     *
     * @return void
     */
    public function save($output, $cropWidth = 0, $cropHeight = 0)
    {
        // creates a temporary image output
        $tmpDir = sys_get_temp_dir();
        $tmpOutput = tempnam($tmpDir, "image");

        // merges and trims the images
        $command = SysCmdHelper::replaceArgs(
            "convert $this -layers merge -fuzz {fuzz} -trim png:{output}",
            array($this->_fuzz, $tmpOutput)
        );
        SysCmdHelper::exec($command);

        list($pageWidth, $pageHeight, $offsetX, $offsetY) = MediaImageHelper::getImageGeometry(
            $tmpOutput
        );

        // changes the sign of the offset X
        $x = "+0";
        if ($cropWidth > 0) {
            $x = ($offsetX > 0? "-" : "+") . abs($offsetX);
        }

        // changes the sign of the offset Y
        $y = "+0";
        if ($cropHeight > 0) {
            $y = ($offsetY > 0? "-" : "+") . abs($offsetY);
        }

        // crops image
        $command = SysCmdHelper::replaceArgs(
            "convert {input} -background {bgColor} -extent {geometry} +repage {output}",
            array($tmpOutput, $this->_bgColor, "{$cropWidth}x{$cropHeight}{$x}{$y}", $output)
        );
        SysCmdHelper::exec($command);

        unlink($tmpOutput);
    }
}
