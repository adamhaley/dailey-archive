<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\event;

/**
 * Class EventDispatcherInterface.
 *
 * @package Event
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
interface EventDispatcherInterface
{
    /**
     * Adds event listener.
     *
     * @param string  $eventType    Event type
     * @param Closure $listener     Event listener
     * @param boolean $highPriority Is high priority? (default is false)
     *
     * @return EventDisaptcherInterface
     */
    function on($eventType, $listener, $highPriority = false);

    /**
     * Deletes event listener.
     *
     * @param string  $eventType Event type
     * @param Closure $listener  Event listener (not required)
     *
     * @return EventDisaptcherInterface
     */
    function off($eventType, $listener = null);

    /**
     * Triggers event.
     *
     * @param string $eventType Event type
     * @param mixed  $data      Additional data
     *
     * @return EventDisaptcherInterface
     */
    function trigger($eventType, $data = null);
}
