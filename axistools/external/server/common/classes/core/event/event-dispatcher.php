<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\event;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\event\EventListener;

/**
 * Class EventDispatcher.
 *
 * @package Event
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class EventDispatcher
{
    /**
     * Event listeners.
     * @var array of EventListener
     */
    private $_eventListeners = array();

    /**
     * Is event workflow stopped?
     * @var boolean
     */
    private $_isStopped = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->_eventListeners = array();
        $this->_isStopped = false;
    }

    /**
     * Gets event listeners.
     *
     * @return array of EventListener
     */
    public function getEventListeners()
    {
        return $this->_eventListeners;
    }

    /**
     * Is the event handling stopped?
     *
     * @return boolean
     */
    public function isStopped()
    {
        return $this->_isStopped;
    }

    /**
     * Sets stop status.
     *
     * @param boolean $value Stop status
     *
     * @return void
     */
    public function setStopped($value)
    {
        $this->_isStopped = $value;
    }

    /**
     * Stops the event propagation.
     *
     * @return void
     */
    public function stopPropagation()
    {
        $this->_isStopped = true;
    }

    /**
     * Adds an event listener.
     *
     * @param EventListener $eventListener Event listener
     *
     * @return EventDispatcher
     */
    public function addEventListener($eventListener)
    {
        ArrHelper::add(
            $this->_eventListeners,
            $eventListener,
            $eventListener->isHighPriority()
        );

        return $this;
    }

    /**
     * Removes a listener.
     *
     * @param string   $type     Event type
     * @param Callable $listener Listener (not required)
     *
     * @return EventDispatcher
     */
    public function removeEventListener($type, $listener = null)
    {
        foreach ($this->_eventListeners as $i => $l) {
            if ($l->getType() == $type) {
                if ($listener === null || $l->getListener() === $listener) {
                    unset($this->_eventListeners[$i]);
                }
            }
        }

        return $this;
    }

    /**
     * Adds an event listener.
     *
     * @param string   $type           Event type
     * @param Callable $listener       Listener
     * @param boolean  $isHighPriority Is high priority? (default is false)
     *
     * @return EventDispatcher
     */
    public function on($type, $listener, $isHighPriority = false)
    {
        $eventListener = new EventListener($type, $listener);
        $eventListener->setHighPriority($isHighPriority);
        return $this->addEventListener($eventListener);
    }

    /**
     * Adds a 'one time' event listener.
     *
     * @param string   $type           Event type
     * @param Callable $listener       Listener
     * @param boolean  $isHighPriority Is high priority? (default is false)
     *
     * @return EventDispatcher
     */
    public function one($type, $listener, $isHighPriority = false)
    {
        $eventListener = new EventListener($type, $listener);
        $eventListener->setHighPriority($isHighPriority);
        $eventListener->setOneTime(true);
        return $this->addEventListener($eventListener);
    }

    /**
     * Removes an event listener.
     *
     * @param string   $type     Event type
     * @param Callable $listener Listener (not required)
     *
     * @return EventDispatcher
     */
    public function off($type, $listener = null)
    {
        return $this->removeEventListener($type, $listener);
    }

    /**
     * Triggers an event.
     *
     * @param string|array $type Event type or a list of event types
     * @param mixed        $data Additional data
     *
     * @return EventDisaptcherInterface
     */
    public function trigger($type, $data = null)
    {
        $this->_isStopped = false;
        $types = is_array($type)? $type : array($type);

        foreach ($types as $type) {
            foreach ($this->_eventListeners as $eventListener) {
                if ($eventListener->getType() == $type) {
                    $args = array_slice(func_get_args(), 1);
                    call_user_func_array(array($eventListener, "exec"), $args);
                }

                if ($this->_isStopped) {
                    return $this;
                }
            }
        }

        return $this;
    }
}
