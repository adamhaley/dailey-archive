<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\event;

/**
 * Class EventListener.
 *
 * @package Event
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class EventListener
{
    /**
     * Event type.
     * @var string
     */
    private $_type;

    /**
     * Event listener.
     * @var Closure
     */
    private $_listener;

    /**
     * Is high priority event?
     * @var boolean
     */
    private $_isHighPriority;

    /**
     * Is 'one time' event listener?
     * @var boolean
     */
    private $_isOneTime;

    /**
     * Has the listener been fired?
     * @var boolean
     */
    private $_isFired;

    /**
     * Constructor.
     *
     * @param string  $type     Event type
     * @param Closure $listener Event listener
     *
     * @return void
     */
    public function __construct($type, $listener)
    {
        $this->_type = $type;
        $this->_listener = $listener;
        $this->_isHighPriority = false;
        $this->_isOneTime = false;
        $this->_isFired = false;
    }

    /**
     * Gets the event type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Gets the listener.
     *
     * @return Closure
     */
    public function getListener()
    {
        return $this->_listener;
    }

    /**
     * Is high priority event listener?
     *
     * @return boolean
     */
    public function isHighPriority()
    {
        return $this->_isHighPriority;
    }

    /**
     * Sets priority.
     *
     * @param boolean $isHighPriority Is the listener high priority?
     *
     * @return void
     */
    public function setHighPriority($isHighPriority)
    {
        $this->_isHighPriority = $isHighPriority;
    }

    /**
     * Is 'one time' event listener?
     *
     * @return boolean
     */
    public function isOneTime()
    {
        return $this->_isOneTime;
    }

    /**
     * Sets 'one time' event listener
     *
     * @param boolean $isOneTime Is one time event listener?
     *
     * @return boolean
     */
    public function setOneTime($isOneTime)
    {
        $this->_isOneTime = $isOneTime;
    }

    /**
     * Calls the event listener.
     *
     * @param mixed $data Additional info (not required)
     *
     * @return HttpControllerEventHandler
     */
    public function exec($data = null)
    {
        if (!$this->_isOneTime || !$this->_isFired) {
            $this->_isFired = true;
            call_user_func_array($this->_listener, func_get_args());
        }
        return $this;
    }
}
