<?php
/**
 * This file contains the SecurityPermissionsInterface interface.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\security\permissions;

/**
 * Interface SecurityPermissionsInterface.
 *
 * @package Security\Permissions
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
interface SecurityPermissionsInterface
{
    /**
     * Is the used allowed to do an action?
     *
     * @param string $action Action
     *
     * @return boolean
     */
    function isAllowed($action);

    /**
     * Sets the users to be able to do an action.
     *
     * @param string           $action    Action
     * @param boolean|Callable $condition Condition
     *
     * @return void
     */
    function setAllowed($action, $condition);
}
