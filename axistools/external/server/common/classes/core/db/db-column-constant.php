<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db;

/**
 * Class DbColumnConstant.
 *
 * This class represents a 'constant column'.
 *
 * @package Db
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class DbColumnConstant extends DbColumn
{
    /**
     * Constant.
     * @var scalar
     */
    private $_constant = "";

    /**
     * Constructor.
     *
     * @param scalar $constant Constant
     */
    public function __construct($constant)
    {
        $this->_constant = $constant;
    }

    /**
     * Gets the original value.
     *
     * This function overrides DbColumn::getOriginalValue()
     *
     * @return string
     */
    public function getOriginalValue()
    {
        return $this->_constant;
    }

    /**
     * Gets the column value.
     *
     * This function overrides DbColumn::getValue()
     *
     * @return string
     */
    public function getValue()
    {
        return $this->_constant;
    }
}
