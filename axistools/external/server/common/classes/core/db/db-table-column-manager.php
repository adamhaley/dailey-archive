<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db;

/**
 * Trait DbTableColumnManager.
 *
 * This "trait" is part of com\soloproyectos\db\DbTable. It is responsible for registering and searching
 * table columns.
 *
 * @package Db
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
trait DbTableColumnManager
{
    /**
     * Gets a column value.
     *
     * @param string $exp Column expression
     *
     * @return mixed
     */
    public function get($exp)
    {
        $column = $this->_regColumn($exp);
        return $column->getValue();
    }

    /**
     * Sets a column value.
     *
     * @param string      $exp   Column expressiona
     * @param scalar|null $value Value
     *
     * @return void
     */
    public function set($exp, $value)
    {
        $column = $this->_regColumn($exp);
        $column->setValue($value);
    }

    /**
     * Searches a column.
     *
     * @param string $columnName Column name
     *
     * @return DbColumn|null
     */
    private function _searchColumn($columnName)
    {
        foreach ($this->_columns as $column) {
            if ($column->getName() == $columnName) {
                return $column;
            }
        }

        return null;
    }

    /**
     * Searches a table.
     *
     * @param string   $tableName  Table name
     * @param string   $columnName Column name
     * @param DbColumn $leftColumn Left linked column
     *
     * @return DbTable|null
     */
    private function _searchTable($tableName, $columnName, $leftColumn)
    {
        if ($this->_name == $tableName) {
            $column = $this->_searchColumn($columnName);
            if ($column != null && $column->getLeftLinkedColumn() === $leftColumn) {
                return $this;
            }
        }

        foreach ($this->_columns as $column) {
            $rightColumn = $column->getRightLinkedColumn();
            if ($rightColumn != null) {
                $rightTable = $rightColumn->getTable();
                $table = $rightTable->_searchTable($tableName, $columnName, $leftColumn);
                if ($table != null) {
                    return $table;
                }
            }
        }

        return null;
    }

    /**
     * Registers a table.
     *
     * This function creates or searches a table.
     *
     * @param string $exp Table expression
     *
     * @return DbTable
     */
    private function _regTable($exp)
    {
        $tableName = $exp;
        $columnName = "id";
        $leftColumnName = $tableName . "_id";

        if (preg_match("/(.*)\s*\[(.*)\]$/U", $exp, $matches)) {
            $tableName = $matches[1];
            $leftColumnName = trim($matches[2]);

            $pos = strpos($leftColumnName, "=");
            if ($pos !== false) {
                $columnName = trim(substr($leftColumnName, 0, $pos));
                $leftColumnName = trim(substr($leftColumnName, $pos + 1));
            }
        }

        $leftColumn = $this->_regColumn($leftColumnName);
        $table = $this->_searchTable($tableName, $columnName, $leftColumn);
        if ($table == null) {
            $table = new DbTable($this->_db, $tableName, array($columnName => $leftColumn));
        }

        return $table;
    }

    /**
     * Registers a column.
     *
     * This function adds or searches a column.
     *
     * @param string $exp Column expression
     *
     * @return DbColumn
     */
    private function _regColumn($exp)
    {
        $table = $this;
        $columnName = $exp;

        $pos = strrpos($exp, ".");
        if ($pos !== false) {
            $tableExp = trim(substr($exp, 0, $pos));
            $columnName = trim(substr($exp, $pos + 1));
            $table = $this->_regTable($tableExp);
        }

        $column = $table->_searchColumn($columnName);
        if ($column == null) {
            $column = new DbColumn($table, $columnName);
            array_push($table->_columns, $column);
            $table->_isUpdated = false;
        }

        return $column;
    }
}
