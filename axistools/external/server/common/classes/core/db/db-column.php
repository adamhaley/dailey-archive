<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db;

/**
 * Class DbColumn.
 *
 * This class represents a table column.
 *
 * @package Db
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class DbColumn
{
    /**
     * Column table.
     * @var DbTable
     */
    private $_table = null;

    /**
     * Column name.
     * @var string
     */
    private $_name = "";

    /**
     * Column value.
     * @var string
     */
    private $_value = "";

    /**
     * Has the column changed?
     * @var boolean
     */
    private $_hasChanged = false;

    /**
     * Left linked column
     * @var DbColumn
     */
    private $_leftLinkedColumn = null;

    /**
     * List of right linked columns.
     * @var array of DbColumn
     */
    private $_rightLinkedColumn = null;

    /**
     * Constructor.
     *
     * @param DbTable $table      Table
     * @param string  $columnName Column name
     */
    public function __construct($table, $columnName)
    {
        $this->_table = $table;
        $this->_name = strtolower(trim($columnName));
    }

    /**
     * Gets the column name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Gets the column table.
     *
     * @return DbTable
     */
    public function getTable()
    {
        return $this->_table;
    }

    /**
     * Gets the original value.
     *
     * @return string
     */
    public function getOriginalValue()
    {
        $ret = "";

        if ($this->_leftLinkedColumn != null) {
            $ret = $this->_leftLinkedColumn->getOriginalValue();
        } else {
            $ret = $this->_table->getColumnValue($this->getName());
        }

        return $ret;
    }

    /**
     * Gets the column value.
     *
     * @return string
     */
    public function getValue()
    {
        $ret = "";

        if ($this->_hasChanged) {
            $ret = $this->_value;
        } elseif ($this->_leftLinkedColumn != null) {
            $ret = $this->_leftLinkedColumn->getValue();
        } else {
            $ret = $this->_table->getColumnValue($this->getName());
        }

        return $ret;
    }

    /**
     * Sets column value.
     *
     * @param scalar $value Value
     *
     * @return void
     */
    public function setValue($value)
    {
        $this->_value = $value;

        if ($this->_leftLinkedColumn != null) {
            $this->_leftLinkedColumn->setValue($value);
        }

        $this->_hasChanged = true;
    }

    /**
     * Has the column changed?
     *
     * @return boolean
     */
    public function hasChanged()
    {
        return $this->_hasChanged;
    }

    /**
     * Gets left linked column.
     *
     * @return @DbColumn
     */
    public function getLeftLinkedColumn()
    {
        return $this->_leftLinkedColumn;
    }

    /**
     * Sets left linked column.
     *
     * @param DbColumn $column Left linked column
     *
     * @return void
     */
    public function setLeftLinkedColumn($column)
    {
        $this->_leftLinkedColumn = $column;
    }

    /**
     * Gets right linked column.
     *
     * @return @DbColumn
     */
    public function getRightLinkedColumn()
    {
        return $this->_rightLinkedColumn;
    }

    /**
     * Sets right linked column.
     *
     * @param DbColumn $column Right linked column
     *
     * @return void
     */
    public function setRightLinkedColumn($column)
    {
        $this->_rightLinkedColumn = $column;
    }

    /**
     * Resets column state.
     *
     * @return void
     */
    public function reset()
    {
        $this->_value = "";
        $this->_hasChanged = false;
    }
}
