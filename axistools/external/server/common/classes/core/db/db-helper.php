<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db;

/**
 * Class DbHelper.
 *
 * @package Db
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class DbHelper
{
    /**
     * Escapes and quotes an identifier to be used in a SQL sentence.
     *
     * @param string $identifier Identifier
     *
     * @return string
     */
    public static function quoteId($identifier)
    {
        return "`" . str_replace("`", "``", $identifier) . "`";
    }
}
