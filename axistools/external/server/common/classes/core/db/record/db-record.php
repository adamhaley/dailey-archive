<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db\record;
use \ArrayAccess;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\db\DbConnector;
use com\soloproyectos\common\db\exception\DbException;
use com\soloproyectos\common\db\record\DbRecordAbstract;
use com\soloproyectos\common\db\record\DbRecordInsert;
use com\soloproyectos\common\db\record\DbRecordUpdate;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class DbRecord.
 *
 * @package Db\Record
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos-php/db-record/blob/master/LICENSE The MIT License (MIT)
 * @link    https://github.com/soloproyectos-php/db-record
 */
class DbRecord implements ArrayAccess
{
    /**
     * Database connector.
     * @var DbConnector
     */
    protected $db;

    /**
     * Record ID.
     * @var string
     */
    protected $id;

    /**
     * Record.
     * @var DbRecordAbstract
     */
    protected $record;

    /**
     * Table name.
     * @var string
     */
    private $_tableName = "";

    /**
     * Is the record updated?
     * @var boolean
     */
    private $_isUpdated = true;

    /**
     * Constructor.
     *
     * @param DbConnector  $db        Database connector
     * @param string       $tableName Table name
     * @param scalar|array $id        Record ID (not required)
     */
    public function __construct($db, $tableName, $id = null)
    {
        $this->db = $db;
        $this->_tableName = $tableName;
        $this->id = $id;
        $this->record = TextHelper::isEmpty($id)
            ? new DbRecordInsert($db, $tableName)
            : new DbRecordUpdate($db, $tableName, $id);
    }

    /**
     * Inserts or updates the record.
     *
     * @return void
     */
    public function save()
    {
        $this->record->save();

        // retrieves the inserted record
        if (TextHelper::isEmpty($this->id)) {
            $row = $this->db->query("select last_insert_id() as id");
            $this->id = $row["id"];
        }

        $this->_isUpdated = false;
    }

    /**
     * Deletes the record.
     *
     * @return integer
     */
    public function delete()
    {
        if (TextHelper::isEmpty($this->id)) {
            throw new DbException("The record is still not saved");
        }
        $this->record->delete();
        $this->_isUpdated = false;
    }

    /**
     * Refreshes the record from the database.
     *
     * @return void
     */
    public function refresh()
    {
        if (TextHelper::isEmpty($this->id)) {
            throw new DbException("The record is still not saved");
        }
        $this->record = new DbRecordUpdate($this->db, $this->_tableName, $this->id);
        $this->_isUpdated = true;
    }

    /**
     * Does the column exist?
     *
     * This function implements ArrayAccess::offsetExists().
     *
     * @param string $columnName Column name
     *
     * @return boolean
     */
    public function offsetExists($columnName)
    {
        return isset($this->record[$columnName]);
    }

    /**
     * Gets the column value.
     *
     * This function implements ArrayAccess::offsetGet().
     *
     * @param string $columnName Column name
     *
     * @return string|null
     */
    public function offsetGet($columnName)
    {
        if (!$this->_isUpdated) {
            $this->refresh();
        }
        return $this->record[$columnName];
    }

    /**
     * Sets the column value.
     *
     * This function implements ArrayAccess::offsetSet().
     *
     * @param string $columnName Column name
     * @param mixed  $value      Value
     *
     * @return void
     */
    public function offsetSet($columnName, $value)
    {
        if (!$this->_isUpdated) {
            $this->refresh();
        }
        $this->record[$columnName] = $value;
    }

    /**
     * Removes a column.
     *
     * This function implements ArrayAccess::offsetUnset().
     *
     * @param string $columnName Column name
     *
     * @return void
     */
    public function offsetUnset($columnName)
    {
        unset($this->record[$columnName]);
    }
}
