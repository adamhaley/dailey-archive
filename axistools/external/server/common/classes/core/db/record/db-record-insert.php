<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db\record;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\db\DbHelper;
use com\soloproyectos\common\db\exception\DbException;
use com\soloproyectos\common\db\record\DbRecordAbstract;
use com\soloproyectos\common\db\record\DbRecordCell;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class DbRecordInsert.
 *
 * @package Db\Record
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos-php/db-record/blob/master/LICENSE The MIT License (MIT)
 * @link    https://github.com/soloproyectos-php/db-record
 */
class DbRecordInsert extends DbRecordAbstract
{
    /**
     * Inserts a record into the table.
     *
     * @return void
     */
    public function save()
    {
        // sql columns and values
        $sqlColumns = "";
        $sqlValues = "";
        foreach ($this->record as $columnName => $cell) {
            if (in_array($columnName, ["created_on", "updated_on"]) || $cell->hasChanged()) {
                $key = DbHelper::quoteId($columnName);
                $value = $cell->hasChanged()? $this->db->quote($cell->getValue()): "utc_timestamp()";
                $sqlColumns = TextHelper::concat(", ", $sqlColumns, $key);
                $sqlValues = TextHelper::concat(", ", $sqlValues, $value);
            }
        }

        $this->db->exec(
            "insert into " . DbHelper::quoteId($this->tableName) . " ($sqlColumns) values ($sqlValues)"
        );
    }
}
