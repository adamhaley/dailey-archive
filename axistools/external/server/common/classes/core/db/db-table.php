<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\db;
use \ArrayIterator;
use \Exception;
use \Countable;
use \IteratorAggregate;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class DbTable.
 *
 * This class represents a database table. We can traverse, edit, insert or delete records.
 *
 * @package Db
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class DbTable implements IteratorAggregate, Countable
{
    use DbTableColumnManager;

    /**
     * Database connector.
     * @var Db
     */
    private $_db = null;

    /**
     * Datasource.
     * @var DbDataSource
     */
    private $_dataSource = null;

    /**
     * List of columns.
     * @var array of DbColumn
     */
    private $_columns = array();

    /**
     * Table name.
     * @var string
     */
    private $_name = "";

    /**
     * List of filters.
     * @var array of string
     */
    private $_filters = array();

    /**
     * Order by.
     * @var string|null
     */
    private $_order = null;

    /**
     * Offset.
     * This property is used to define the 'limit' clause in a 'select' statement.
     * @var integer
     */
    private $_offset = 0;

    /**
     * 'Row count'.
     * This property is used to define the 'limit' clause in a 'select' statement.
     * @var integer
     */
    private $_rowCount = -1;

    /**
     * Is the table updated?
     * @var boolean
     */
    private $_isUpdated = false;

    /**
     * Insertion is in progress.
     * @var boolean
     */
    private $_isInsertMode = false;

    /**
     * Primary key column.
     * @var DbColumn
     */
    private $_primaryKey = null;

    /**
     * Constructor.
     *
     * @param DbConnector           $db          Database connector
     * @param string                $tableName   Table name
     * @param scalar|DbColumn|array $constraints List of constraints (not required)
     * @param string                $pkName      Primary key name (default is 'id')
     */
    public function __construct($db, $tableName, $constraints = null, $pkName = "id")
    {
        $this->_db = $db;
        $this->_name = strtolower(trim($tableName));
        $this->_primaryKey = $this->_regColumn($pkName);

        if (func_num_args() > 2) {
            if (!is_array($constraints)) {
                $constraints = array($pkName => $constraints);
            }

            foreach ($constraints as $colName => $colValue) {
                $this->addConstraint($colName, $colValue);
            }
        }
    }

    /**
     * Implements IteratorAggregate::getIterator()
     *
     * @return Traversable
     */
    public function getIterator()
    {
        $items = array();

        if (!$this->_isUpdated) {
            $this->refresh();
        }

        foreach ($this->_dataSource as $row) {
            $pkName = $this->_primaryKey->getName();
            $pkValue = $row[$pkName];
            array_push($items, new DbTable($this->_db, $this->_name, array($pkName => $pkValue)));
        }

        return new ArrayIterator($items);
    }

    /**
     * Implements Countable::count()
     *
     * @return integer
     */
    public function count()
    {
        $row = new DbDataSource($this->_db, $this->_getSelectCountStatement());
        return $row[0];
    }

    /**
     * Inserts a new row.
     *
     * @return void
     */
    public function insert()
    {
        $this->_isInsertMode = true;

        // previously, inserts all linked tables
        foreach ($this->_columns as $column) {
            $rightColumn = $column->getRightLinkedColumn();
            if ($rightColumn != null) {
                $tableLink = $rightColumn->getTable();

                if ($tableLink->_hasChanged()) {
                    $tableLink->insert();
                }
            }
        }

        $this->_db->exec($this->_getInsertStatement());

        // gets the last inserted id
        $insertId = $this->_primaryKey->hasChanged()
            ? $this->_primaryKey->getValue()
            : $this->_db->getLastInsertId();

        $firstLeftColumn = $this->_primaryKey;
        while (
            $firstLeftColumn->getLeftLinkedColumn() != null
            && !($firstLeftColumn->getLeftLinkedColumn() instanceof DbColumnConstant)
        ) {
            $firstLeftColumn = $firstLeftColumn->getLeftLinkedColumn();
        }
        $firstLeftColumn->setLeftLinkedColumn(new DbColumnConstant($insertId));
        $firstLeftColumn->setValue($insertId);

        $this->_resetColumns();
        $this->_isInsertMode = false;
    }

    /**
     * Updates the current row.
     *
     * @return void
     */
    public function update()
    {
        // previously, saves all linked tables
        foreach ($this->_columns as $column) {
            $rightColumn = $column->getRightLinkedColumn();
            if ($rightColumn != null) {
                $tableLink = $rightColumn->getTable();

                // inserts or updates
                if ($tableLink->_hasChanged()) {
                    if ($tableLink->_primaryKey->getOriginalValue() > 0) {
                        $tableLink->update();
                    } else {
                        $tableLink->insert();
                    }
                }
            }
        }

        // gets modified columns
        $columns = array_filter(
            $this->_columns, function ($column) {
                return $column->hasChanged();
            }
        );

        if (count($columns) > 0) {
            $this->_db->exec($this->_getUpdateStatement());
            $this->_resetColumns();
        }
    }

    /**
     * Deletes the current row.
     *
     * @return void
     */
    public function delete()
    {
        $this->_db->exec($this->_getDeleteStatement());
        $this->_resetColumns();
    }

    /**
     * Refreshes the table columns.
     *
     * @return void
     */
    public function refresh()
    {
        $this->_resetColumns();
        $this->_fetchRows();
    }

    /**
     * Adds a filter.
     *
     * @param string $filter Filter or column
     *
     * @return void
     */
    public function addFilter($filter)
    {
        array_push($this->_filters, $filter);
        $this->_isUpdated = false;
    }

    /**
     * Adds a constraint.
     *
     * @param string          $columnName Column name
     * @param scalar|DbColumn $value      Value
     *
     * @return void
     */
    public function addConstraint($columnName, $value)
    {
        $column = $this->_regColumn($columnName);
        $leftColumn = $value instanceof DbColumn? $value : new DbColumnConstant($value);

        // links columns in both directions
        $column->setLeftLinkedColumn($leftColumn);
        $leftColumn->setRightLinkedColumn($column);
    }

    /**
     * Gets the 'order by'.
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Sets an 'order by'.
     *
     * @param string $value 'Order by' expression
     *
     * @return void
     */
    public function setOrder($value)
    {
        $this->_order = $value;
        $this->_isUpdated = false;
    }

    /**
     * Gets the offset.
     *
     * @return integer
     */
    public function getOffset()
    {
        return $this->_offset;
    }

    /**
     * Sets the offset.
     *
     * This property is used to define the 'limit' clause:
     *
     * select * from table limit offset, rowCount
     *
     * @param integer $value Offset
     *
     * @return void
     */
    public function setOffset($value)
    {
        $this->_offset = $value;
        $this->_isUpdated = false;
    }

    /**
     * Gets the 'row count'.
     *
     * @return integer
     */
    public function getRowCount()
    {
        return $this->_rowCount;
    }

    /**
     * Sets the 'row count'.
     *
     * This property is used to define the 'limit' clause:
     *
     * select * from table limit offset, rowCount
     *
     * @param integer $value "Row count"
     *
     * @return void
     */
    public function setRowCount($value)
    {
        $this->_rowCount = $value;
        $this->_isUpdated = false;
    }

    /**
     * Gets a column value by its name.
     *
     * @param string $columnName Column name
     *
     * @return string
     */
    public function getColumnValue($columnName)
    {
        $ret = null;

        if (!$this->_isInsertMode) {
            if (!$this->_isUpdated) {
                $this->_fetchRows();
            }

            $ret = $this->_dataSource[$columnName];
        }

        return $ret;
    }

    /**
     * Fetches rows from database.
     *
     * @return void
     */
    private function _fetchRows()
    {
        $this->_dataSource = new DbDataSource($this->_db, $this->_getSelectStatement());
        $this->_isUpdated = true;
    }

    /**
     * Resets the table columns.
     *
     * @return void
     */
    private function _resetColumns()
    {
        $this->_isUpdated = false;

        foreach ($this->_columns as $column) {
            $column->reset();
            $rightColumn = $column->getRightLinkedColumn();

            if ($rightColumn != null) {
                $rightTable = $rightColumn->getTable();
                $rightTable->_resetColumns();
            }
        }
    }

    /**
     * Has the table changed?
     *
     * The table has changed if any of its columns (or right-linked columns) has changed.
     *
     * @return boolean
     */
    private function _hasChanged()
    {
        foreach ($this->_columns as $column) {
            if ($column->hasChanged()) {
                return true;
            }

            $rightColumn = $column->getRightLinkedColumn();
            if ($rightColumn != null) {
                $rightTable = $rightColumn->getTable();
                if ($rightTable->_hasChanged()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Gets the 'select' statement.
     *
     * @return string
     */
    private function _getSelectStatement()
    {
        // list of columns separated by commas
        $columnsList = implode(
            ", ",
            array_map(
                function ($column) {
                    return DbHelper::quoteId($this->_name) . "." . DbHelper::quoteId($column->getName());
                },
                $this->_columns
            )
        );

        // filters
        $filters = array();
        foreach ($this->_filters as $filter) {
            $tableName = "";
            if (!preg_match('/^([a-z]+\w*)\.[a-z]+\w*/', trim($filter), $matches)) {
                $tableName = DbHelper::quoteId($this->_name) . ".";
            }
            array_push($filters, $tableName . $filter);
        }
        foreach ($this->_columns as $column) {
            $leftColumn = $column->getLeftLinkedColumn();
            if ($leftColumn != null) {
                $col = DbHelper::quoteId($this->_name) . "." . DbHelper::quoteId($column->getName());
                $val = $this->_db->quote($leftColumn->getOriginalValue());
                array_push($filters, "$col = $val");
            }
        }
        $filtersList = implode(" and ", $filters);

        // left joins
        $leftJoins = array();
        $filters = array_merge($this->_filters, array($this->_order));
        foreach ($filters as $filter) {
            if (preg_match('/^([a-z]+\w*)\.[a-z]+\w*/', trim($filter), $matches)) {
                $joinName = $matches[1];
                if (!in_array($joinName, $leftJoins)) {
                    array_push($leftJoins, $joinName);
                }
            }
        }
        $leftJoinsList = implode(
            "\n",
            array_map(
                function ($join) {
                    return "left join `$join` on `$join`.id = `{$this->_name}`.`{$join}_id`";
                },
                $leftJoins
            )
        );

        // creates the 'select' statement
        $tableName = DbHelper::quoteId($this->_name);
        $sql = "select $columnsList from $tableName $leftJoinsList";
        if (strlen($filtersList) > 0) {
            $sql .= " where $filtersList";
        }
        if ($this->_order != null) {
            $orderTable = preg_match('/^([a-z]+\w*)\.([a-z]+\w*)/', trim($this->_order))
                ? ""
                : DbHelper::quoteId($this->_name) . ".";
            $sql .= " order by $orderTable{$this->_order}";
        }
        if ($this->_offset > 0 || $this->_rowCount > 0) {
            $rowCount = $this->_rowCount > 0? $this->_rowCount : "18446744073709551610";
            $sql .= " limit {$this->_offset}, $rowCount";
        }

        return $sql;
    }

    /**
     * Gets the 'select count(*)' statement.
     *
     * @return string
     */
    private function _getSelectCountStatement()
    {
        // filters
        $filters = array();
        foreach ($this->_filters as $filter) {
            $tableName = "";
            if (!preg_match('/^([a-z]+\w*)\.[a-z]+\w*/', trim($filter), $matches)) {
                $tableName = DbHelper::quoteId($this->_name) . ".";
            }
            array_push($filters, $tableName . $filter);
        }
        foreach ($this->_columns as $column) {
            $leftColumn = $column->getLeftLinkedColumn();
            if ($leftColumn != null) {
                $col = DbHelper::quoteId($this->_name) . "." . DbHelper::quoteId($column->getName());
                $val = $this->_db->quote($leftColumn->getOriginalValue());
                array_push($filters, "$col = $val");
            }
        }
        $filtersList = implode(" and ", $filters);

        // left joins
        $leftJoins = array();
        foreach ($this->_filters as $filter) {
            if (preg_match('/^([a-z]+\w*)\.([a-z]+\w*)/', trim($filter), $matches)) {
                $joinName = $matches[1];
                $joinColumn = $matches[2];
                if (!in_array($joinName, $leftJoins)) {
                    array_push($leftJoins, $joinName);
                }
            }
        }
        $leftJoinsList = implode(
            "\n",
            array_map(
                function ($join) {
                    return "left join `$join` on `$join`.id = `{$this->_name}`.`{$join}_id`";
                },
                $leftJoins
            )
        );

        // creates the 'select count(*)' statement
        $tableName = DbHelper::quoteId($this->_name);
        $sql = "select 1 from $tableName $leftJoinsList";
        if (strlen($filtersList) > 0) {
            $sql .= " where $filtersList";
        }
        if ($this->_offset > 0 || $this->_rowCount > 0) {
            $rowCount = $this->_rowCount > 0? $this->_rowCount : "18446744073709551610";
            $sql .= " limit {$this->_offset}, $rowCount";
        }

        return "select count(*) from ($sql) t";
    }

    /**
     * Gets the 'insert' statement.
     *
     * @return string
     */
    private function _getInsertStatement()
    {
        // list of columns that have changed or have 'left linked' columns
        $columns = array_filter(
            $this->_columns,
            function ($column) {
                return $column->getLeftLinkedColumn() != null || $column->hasChanged();
            }
        );

        // list of columns separated by commas
        $columnsList = implode(
            ", ",
            array_map(
                function ($column) {
                    return DbHelper::quoteId($column->getName());
                },
                $columns
            )
        );

        // list of values separated by commas
        $valuesList = implode(
            ", ",
            array_map(
                function ($column) {
                    return $this->_db->quote($column->getValue());
                },
                $columns
            )
        );

        // creates the 'insert' statement
        $tableName = DbHelper::quoteId($this->_name);
        $sql = "insert into $tableName ($columnsList) values ($valuesList)";

        return $sql;
    }

    /**
     * Gets the 'update' statement.
     *
     * @return string
     */
    private function _getUpdateStatement()
    {
        // gets modified columns
        $columns = array_filter(
            $this->_columns, function ($column) {
                return $column->hasChanged();
            }
        );

        // pairs of columns and values
        $columnsValuesList = implode(
            ", ",
            array_map(
                function ($column) {
                    $name = DbHelper::quoteId($column->getName());
                    $value = $this->_db->quote($column->getValue());
                    return "$name = $value";
                },
                $columns
            )
        );

        // creates the 'update' statement
        $tableName = DbHelper::quoteId($this->_name);
        $pkName = DbHelper::quoteId($this->_primaryKey->getName());
        $pkValue = $this->_db->quote($this->_primaryKey->getOriginalValue());
        $sql = "update $tableName set $columnsValuesList where $pkName = $pkValue";

        return $sql;
    }

    /**
     * Gets the 'delete' statement.
     *
     * @return string
     */
    private function _getDeleteStatement()
    {
        // creates the 'delete' statement
        $tableName = DbHelper::quoteId($this->_name);
        $pkName = DbHelper::quoteId($this->_primaryKey->getName());
        $pkValue = $this->_db->quote($this->_primaryKey->getOriginalValue());
        $sql = "delete from $tableName where $pkName = $pkValue";

        return $sql;
    }
}
