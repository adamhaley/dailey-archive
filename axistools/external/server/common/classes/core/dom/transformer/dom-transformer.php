<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\dom\transformer;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\dom\DomHelper;
use com\soloproyectos\common\dom\exception\DomException;

/**
 * Class DomTransformer.
 *
 * This class transforms an XML document into another XML document.
 *
 * @package Dom\Transformer
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class DomTransformer
{
    /**
     * Output XML document.
     * This is the transformed XML document.
     * @var string
     */
    private $_output = "";

    /**
     * Nodes info.
     * @var array of {tagName: String, attrs: Array}
     */
    private $_nodes = array();

    /**
     * Start handler functions.
     * @var array
     */
    private $_startHandlers = array();

    /**
     * End handler functions.
     * @var array
     */
    private $_endHandlers = array();

    /**
     * Data handler functions.
     * @var array
     */
    private $_dataHandlers = array();

    /**
     * Current line number.
     * @var integer
     */
    private $_currentLine = 0;

    /**
     * Current column number.
     * @vare integer
     */
    private $_currentColumn = 0;

    /**
     * Gets the current line number.
     *
     * @return integer
     */
    public function getCurrentLine()
    {
        return $this->_currentLine;
    }

    /**
     * Gets the current column number.
     *
     * @return integer
     */
    public function getCurrentColumn()
    {
        return $this->_currentColumn;
    }

    /**
     * Transform an XML document.
     *
     * This function transform a well formed XML document into another string.
     *
     * @param string $input A well formed XML document
     *
     * @return string
     */
    public function transform($input)
    {
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, false);
        xml_set_element_handler(
            $parser, array(&$this, "_defaultStartHandler"), array(&$this, "_defaultEndHandler")
        );
        xml_set_character_data_handler($parser, array(&$this, "_defaultDataHandler"));

        if (xml_parse($parser, $input) == 0) {
            $code = xml_get_error_code($parser);
            $text = xml_error_string($code);
            xml_parser_free($parser);
            throw new DomException("$text (error no. $code)");
        }

        xml_parser_free($parser);
        return $this->_output;
    }

    /**
     * Stets the start tag handler.
     *
     * @param string|array $tag     Tagname
     * @param string|array $handler Handlers
     *
     * @return void
     */
    public function startHandler($tag, $handler)
    {
        $tags = is_array($tag)? $tag: array($tag);
        foreach ($tags as $tag) {
            $this->_startHandlers[$tag] = $handler;
        }
    }

    /**
     * Sets the end tag handler.
     *
     * @param string|array $tag     Tagname
     * @param string|array $handler Handlers
     *
     * @return void
     */
    public function endHandler($tag, $handler)
    {
        $tags = is_array($tag)? $tag: array($tag);
        foreach ($tags as $tag) {
            $this->_endHandlers[$tag] = $handler;
        }
    }

    /**
     * Sets the data tag handler.
     *
     * @param string|array $tag     Tagname
     * @param string|array $handler Handlers
     *
     * @return void
     */
    public function dataHandler($tag, $handler)
    {
        $tags = is_array($tag)? $tag: array($tag);
        foreach ($tags as $tag) {
            $this->_dataHandlers[$tag] = $handler;
        }
    }

    /**
     * Default start tag handler.
     *
     * @param resource $parser XML parser
     * @param string   $name   Handler name
     * @param array    $attrs  Attributes
     *
     * @return void
     */
    private function _defaultStartHandler($parser, $name, $attrs)
    {
        array_push($this->_nodes, array("tagName" => $name, "attrs" => $attrs));
        $this->_currentLine = xml_get_current_line_number($parser);
        $this->_currentColumn = xml_get_current_column_number($parser);

        if (ArrHelper::is($this->_startHandlers, $name)) {
            $this->_output .= call_user_func($this->_startHandlers[$name], $name, $attrs);
        } else {
            $attrsStr = null;
            if (count($attrs) > 0) {
                foreach ($attrs as $key => $value) {
                    $attrsStr .= ' ' . $key . '="' . DomHelper::escape($value) . '"';
                }
            }
            $this->_output .=  "<$name$attrsStr>";
        }
    }

    /**
     * Default end tag handler
     *
     * @param resource $parser XML parser
     *
     * @return void
     */
    private function _defaultEndHandler($parser)
    {
        $nodeInfo = array_pop($this->_nodes);

        if (ArrHelper::is($this->_endHandlers, $nodeInfo["tagName"])) {
            $this->_output .= call_user_func(
                $this->_endHandlers[$nodeInfo["tagName"]], $nodeInfo["tagName"], $nodeInfo["attrs"]
            );
        } else {
            $this->_output .= "</$nodeInfo[tagName]>";
        }
    }

    /**
     * Default data tag handler.
     *
     * @param resource $parser XML parser
     * @param string   $data   Data
     *
     * @return void
     */
    private function _defaultDataHandler($parser, $data)
    {
        $nodeInfo = $this->_nodes[count($this->_nodes) - 1];

        if (ArrHelper::is($this->_dataHandlers, $nodeInfo["tagName"])) {
            $this->_output .= call_user_func(
                $this->_dataHandlers[$nodeInfo["tagName"]],
                $nodeInfo["tagName"],
                $nodeInfo["attrs"],
                $data
            );
        } else {
            $this->_output .= DomHelper::escape($data);
        }
    }
}
