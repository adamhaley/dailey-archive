<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\debug;
use \Exception;

/**
 * Class DebugCapable.
 *
 * Any class that implements this 'treat' is debuggable.
 *
 * @package Debug
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
trait DebugCapable
{
    /**
     * Is the class in 'debug-mode'?
     * @var boolean
     */
    private $_isDebugMode = false;

    /**
     * Debug handler.
     *
     * @param mixed $object Object to debug
     *
     * @return void
     */
    abstract protected function debugger($object);

    /**
     * Debugs an object.
     *
     * @param mixed $object Object to debug
     *
     * @return void
     */
    protected function debug($object)
    {
        if ($this->_isDebugMode) {
            $this->debugger($object);
        }
    }

    /**
     * Is the class in 'debug-mode'?
     *
     * @return boolean
     */
    public function isDebugMode()
    {
        return $this->_isDebugMode;
    }

    /**
     * Sets the 'debug-mode' property.
     *
     * @param boolean $value Value
     *
     * @return void
     */
    public function setDebugMode($value)
    {
        $this->_isDebugMode = $value;
    }
}
