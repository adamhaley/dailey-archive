<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\debug;

/**
 * Class DebugHelper.
 *
 * This class is used for debugging purpose.
 *
 * @package Debug
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class DebugHelper
{
    /**
     * Sends a message to the web server's error log.
     *
     * @param mixed $var An arbitrary variable
     *
     * @return void
     */
    public static function log($var)
    {
        $message = "";

        if (is_scalar($var)) {
            $message = $var;
        } else {
            $message = print_r($var, true);
            $message = preg_replace(
                array('/(\W)\n\s*/', '/\n\s*/'),
                array('$1', ' '),
                $message
            );
        }

        error_log("DEBUG: $message");
    }
}
