<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\http\request;
use \Finfo;
use com\soloproyectos\common\http\request\exception\HttpRequestException;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class HttpRequestFormFile.
 *
 * @package Http
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class HttpRequestFormFile extends HttpRequestFormData
{
    /**
     * Constructor.
     *
     * @param string $path     Path to file
     * @param string $mimeType MIME type (not required)
     * @param string $filename Filename (not required)
     */
    public function __construct($path, $mimeType = "", $filename = "")
    {
        $contents = file_get_contents($path);
        parent::__construct($contents);

        // sets mimetype
        if (TextHelper::isEmpty($mimeType)) {
            $result = new Finfo(FILEINFO_MIME);
            $mimeType = $result->buffer($contents);
            if ($mimeType === false) {
                throw new HttpRequestException("Error detecting MIME type");
            }
        }
        $this->setMimeType($mimeType);

        // sets filename
        if (TextHelper::isEmpty($filename)) {
            $filename = basename($path);
        }
        $this->setFilename($filename);
    }
}
