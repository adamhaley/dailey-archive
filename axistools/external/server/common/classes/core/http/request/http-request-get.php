<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\http\request;
use com\soloproyectos\common\http\HttpHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class HttpRequestGet.
 *
 * This class is used to send GET requests.
 *
 * @package Http
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class HttpRequestGet extends HttpRequestAbstract
{
    /**
     * Prepares the request.
     *
     * This function implements HttpRequestAbstract::prepare().
     *
     * @param HttpRequestConfig $config Configuration instance
     *
     * @return void
     */
    protected function prepare($config)
    {
        $config->setOption("method", "GET");
        $config->setContentType("application/x-www-form-urlencoded");
    }
}
