<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\http\request;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class HttpRequestPost.
 *
 * This class is used to send POST requests.
 *
 * @package Http
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class HttpRequestPost extends HttpRequestAbstract
{
    /**
     * Prepares the request.
     *
     * This function implements HttpRequestAbstract::prepare().
     *
     * @param HttpRequestConfig $config Configuration instance
     *
     * @return void
     */
    protected function prepare($config)
    {
        $config->setOption("method", "POST");
        $config->setContentType("multipart/form-data");
    }
}
