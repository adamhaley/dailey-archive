<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\http;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class HttpHelper.
 *
 * This class is used to send POST requests.
 *
 * @package Http
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class HttpHelper
{
    /**
     * Appends parameters to a given url.
     *
     * For example:
     * ```php
     * echo HttpHelper::addParams("http://www.mysite.php", array("username" => "John", "id" => 101));
     * ```
     *
     * @param string $url    URL
     * @param array  $params Associative array of parameters
     *
     * @return strings.
     */
    static public function addParams($url, $params)
    {
        $query = parse_url($url, PHP_URL_QUERY);
        $separator = (TextHelper::isEmpty($query)? "?" : "&");
        return TextHelper::concat($separator, $url, http_build_query($params));
    }
}
