<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */

namespace com\soloproyectos\common\http\session;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\http\exception\HttpException;

/**
 * Class HttpSessionHelper.
 *
 * This class is used to access the session variables.
 *
 * @package Http\Session
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class HttpSessionHelper
{
    /**
     * Starts a session, if not already started.
     *
     * @return void
     */
    public static function start()
    {
        if (!session_id()) {
            session_start();
        }
    }

    /**
     * Gets a session attribute.
     *
     * @param string $name    Session attribute.
     * @param mixed  $default Default value (default is "")
     *
     * @return mixed
     */
    public static function get($name, $default = "")
    {
        HttpSessionHelper::start();
        return ArrHelper::get($_SESSION, $name, $default);
    }

    /**
     * Sets a session attribute.
     *
     * @param string $name  Session attribute.
     * @param mixed  $value Value attribute.
     *
     * @return void
     */
    public static function set($name, $value)
    {
        HttpSessionHelper::start();

        if (!preg_match("/^[\_a-z]/i", $name)) {
            throw new HttpException("Invalid session attribute: $name");
        }

        $_SESSION[$name] = $value;
    }

    /**
     * Does the session attribute exist?
     *
     * @param string $name Session attribute.
     *
     * @return boolean
     */
    public static function is($name)
    {
        HttpSessionHelper::start();
        return ArrHelper::is($_SESSION, $name);
    }

    /**
     * Deletes a session attribute.
     *
     * @param string $name Session attribute.
     *
     * @return void
     */
    public static function del($name)
    {
        HttpSessionHelper::start();
        ArrHelper::del($_SESSION, $name);
    }

    /**
     * Deletes all session variables.
     *
     * @return void
     */
    public static function clear()
    {
        HttpSessionHelper::start();
        session_unset();
    }

    /**
     * Saves data and closes the current session.
     *
     * @return void
     */
    public static function close()
    {
        HttpSessionHelper::start();
        session_write_close();
    }
}
