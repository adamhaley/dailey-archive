<?php
/**
 * This file is part of Soloproyectos common library.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\soloproyectos\common\exception;
use \Exception;

/**
 * Class Exception.
 *
 * This class provides a more readable exception message.
 *
 * @package Xml\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
class ExceptionBase extends Exception
{

    /**
     * Redefines the exception message.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage() . "\n\n" . $this->getTraceAsString();
    }
}
