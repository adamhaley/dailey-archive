<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\exception;
use com\axisgraphics\axis\exception\AxisException;

/**
 * Class AxisDomException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Dom\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomException extends AxisException
{

}
