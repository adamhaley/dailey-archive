<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\transformer;
use com\axisgraphics\axis\dom\exception\AxisDomException;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\dom\DomHelper;
use com\soloproyectos\common\dom\transformer\DomTransformer;
use com\soloproyectos\common\sys\cmd\exception\SysCmdException;
use com\soloproyectos\common\text\TextHelper;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;

/**
 * Class AxisDomTransformer.
 *
 * @package Axis\Dom\Transformer
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomTransformer
{
    /**
     * Section ids.
     * @var array of strings
     */
    private $_sectionIds = array();

    /**
     * Ids.
     * @var array of strings
     */
    private $_ids = array();

    /**
     * Transforms the sitemap into another sitemap.
     *
     * @param string $input Well formed XML document
     *
     * @return string
     */
    public function transform($input)
    {
        $status = 0;
        $input = $this->_transformXmlToPhpScript($input);
        $input = $this->_executePhpScript($input, $status);
        if ($status == 0) {
            $input = $this->_createSectionIds($input);
        }
        return $input;
    }

    /**
     * Transforms an XML document into a PHP script.
     *
     * @param string $input Well-formed XML document
     *
     * @return string
     */
    private function _transformXmlToPhpScript($input)
    {
        // creates the PHP script
        $t = new DomTransformer();

        // sitemap handlers
        $t->startHandler(
            "sitemap",
            function ($name) {
                $configDir = rtrim(CONFIG_DIR, "/");
                $cmsDir = rtrim(CMS_DIR, "/");
                return "<?php\n"
                    . "header('Content-Type: text/xml; charset=utf-8');\n"
                    . (array_key_exists("ENV", $_SERVER)? "\$_SERVER['ENV'] = '$_SERVER[ENV]';": "")
                    . (array_key_exists("HTTP_HOST", $_SERVER)
                        ? "\$_SERVER['HTTP_HOST'] = '$_SERVER[HTTP_HOST]';": "")
                    . "require_once '$configDir/general.php';\n"
                    . "require_once '$cmsDir/autoload.php';\n"
                    . "require_once '$cmsDir/vendor/autoload.php';\n"
                    . "require_once '$configDir/functions.php';\n"
                    . "use soloproyectos\db\DbConnector;\n"
                    . "use com\soloproyectos\common\dom\DomHelper;\n"
                    . "\$db = new DbConnector(DBNAME, DBUSER, DBPASS, DBHOST, DBCHARSET);\n"
                    . "?><$name>";
            }
        );
        $t->endHandler(
            "sitemap",
            function ($name) {
                return "<?php \$db->close() ?></$name>";
            }
        );

        // section handlers
        $t->startHandler(
            array("section", "options", "option"),
            function ($name, $attrs) use ($t) {
                $inlinePhpCode = "";

                if (ArrHelper::is($attrs, "repeat")) {
                    $inlinePhpCode = $this->_parseRepeatAttribute(
                        $attrs["repeat"], $t->getCurrentLine()
                    );
                    ArrHelper::del($attrs, "repeat");
                }

                $str = "$inlinePhpCode<$name";
                foreach ($attrs as $key => $attr) {
                    $str = TextHelper::concat(" ", $str, "$key=\"" . DomHelper::escape($attr) . "\"");
                }
                $str .= ">";

                return $str;
            }
        );
        $t->endHandler(
            array("section", "options", "option"),
            function ($name, $attrs) {
                $inlinePhpCode = ArrHelper::is($attrs, "repeat")? "<?php endforeach ?>" : "";
                return "</$name>$inlinePhpCode";
            }
        );
        
        // 'if' condition
        $t->startHandler(
            array("if"),
            function ($name, $attr) use ($t) {
                $condition = $this->_parsesVariables($attr["condition"]);
                return "<?php if (" . $condition . "): ?>";
            }
        );
        $t->endHandler(
            array("if"),
            function ($name, $attr) use ($t) {
                return "<?php endif ?>";
            }
        );

        // notes handler
        $t->dataHandler(
            "notes",
            function ($name, $attrs, $data) {
                return DomHelper::escape($data);
            }
        );

        // 'select' field handlers
        $t->startHandler(
            array('select', 'checkboxgroup'),
            function ($name, $attrs) use ($t) {
                $inlinePhpCode = "";

                $str = "<$name";
                foreach ($attrs as $key => $attr) {
                    $str = TextHelper::concat(" ", $str, "$key=\"" . DomHelper::escape($attr) . "\"");
                }
                $str .= ">$inlinePhpCode";

                return $str;
            }
        );

        $input = $t->transform($input);

        // replaces variables
        $input = preg_replace_callback(
            '/\{([a-z]\w*)\.([a-z]\w*)\}/',
            function ($matches) {
                $varName = '$' . $matches[1] . '["' . $matches[2] . '"]';
                return "<?php echo DomHelper::escape($varName) ?>";
            },
            $input
        );

        return $input;
    }

    /**
     * Executes a PHP script.
     *
     * This function compiles the sitemap and returns the contents.
     *
     * @param string $input  PHP source
     * @param string $status Status code (passed as reference)
     *
     * @return string
     */
    private function _executePhpScript($input, &$status)
    {
        $output = array();
        $filename = tempnam(TMP_DIR, "sitemap-");
        file_put_contents($filename, $input);
        exec("php $filename 2>&1", $output, $status);
        $input = implode("\n", $output);
        if ($status == 0) {
            unlink($filename);
        }
        return $input;
    }

    /**
     * Adds unique and 'human readable' identifiers to each section.
     *
     * @param string $input Well formed XML document
     *
     * @return string
     */
    private function _createSectionIds($input)
    {
        $t = new DomTransformer();
        $t->startHandler(
            array("section"),
            function ($name, $attrs) {
                $title = ArrHelper::get($attrs, "seo-title");
                if (TextHelper::isEmpty($title)) {
                    $title = ArrHelper::get($attrs, "title");
                }
                list($seoId, $id) = $this->_getUniqueId($title);
                array_push($this->_sectionIds, $seoId);

                $str = "<$name id=\"" . DomHelper::escape($id) . "\"";
                foreach ($attrs as $key => $attr) {
                    $str = TextHelper::concat(" ", $str, "$key=\"" . DomHelper::escape($attr) . "\"");
                }
                $str .= ">";

                return $str;
            }
        );
        $t->endHandler(
            array("section"),
            function ($name, $attrs) {
                array_pop($this->_sectionIds);
                return "</$name>";
            }
        );
        $input = $t->transform($input);

        return $input;
    }

    /**
     * Parses the 'repeat' attribute of the sections.
     *
     * This function parses the 'repeat' attribute of the sections nodes and returns the inline PHP
     * code. The 'repeat' attribute has the following form:
     * ```php
     * $row = function(paramters...)
     * ```
     *
     * @param string  $str         Assignment
     * @param integer $currentLine Current line
     *
     * @return string
     */
    private function _parseRepeatAttribute($str, $currentLine)
    {
        $formattedError = "Line %d: Expected 'row = function(...)', found: %s";

        // extracts left and right side
        $pos = strpos($str, "=");
        if ($pos === false) {
            throw new AxisDomException(sprintf($formattedError, $currentLine, $str));
        }
        $leftSide = substr($str, 0, $pos);
        $rightSide = substr($str, $pos + 1);

        // creates the PHP code
        $pos = strpos($rightSide, "(");
        if ($pos === false) {
            throw new AxisDomException(sprintf($formattedError, $currentLine, $str));
        }
        $varName = "\$" . trim($leftSide);
        $functionName = trim(substr($rightSide, 0, $pos));

        // validates $varName
        if (!preg_match('/^\$[a-z]\w*$/', $varName)) {
            throw new AxisDomException(sprintf($formattedError, $currentLine, $str));
        }

        // validates $functionName
        if (!preg_match('/^[a-z]\w*$/', $functionName)) {
            throw new AxisDomException(sprintf($formattedError, $currentLine, $str));
        }

        $parameters = trim(substr($rightSide, $pos + 1));
        $function = "$functionName(\$db" . (strpos($parameters, ")") === 0? ")" : ", $parameters");
        $inlinePhpCode = "<?php foreach ($function as $varName): ?>";

        return $this->_parsesVariables($inlinePhpCode);
    }
    
    /**
     * Parses variables.
     * 
     * @param string $str String
     * 
     * @return string
     */
    private function _parsesVariables($str)
    {
        return preg_replace_callback(
            '/\{([a-z]\w*)\.([a-z]\w*)\}/',
            function ($matches) {
                $varName = '$' . $matches[1] . '["' . $matches[2] . '"]';
                return "$varName";
            },
            $str
        );
    }

    /**
     * Gets an human readable identifier.
     *
     * @param string $str A string
     *
     * @return string
     */
    private function _getHumanReadableId($str)
    {
        $id = preg_replace(
            array("/[\s]/", "/[^a-z0-9\_\*\@\.\/\+\-]/i", "/\-+/"),
            array("-", "", "-"),
            strtolower($str)
        );

        return $id;
    }

    /**
     * Gets an unique human readable ID.
     *
     * @param string $str A string
     *
     * @return array of two values, a friendly ID and a unique ID.
     */
    private function _getUniqueId($str)
    {
        $prefix = implode("/", $this->_sectionIds);
        $seoId = $this->_getHumanReadableId($str);
        $id = TextHelper::concat("/", $prefix, $seoId);
        if (array_search($id, $this->_ids) !== false) {
            list($seoId, $id) = $this->_getUniqueId("_$str");
        }
        array_push($this->_ids, $id);
        return array($seoId, $id);
    }
}
