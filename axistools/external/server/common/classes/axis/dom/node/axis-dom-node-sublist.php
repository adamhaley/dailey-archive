<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\node;
use com\axisgraphics\axis\dom\node\AxisDomNodeList;

/**
 * Class AxisDomNodeSublist.
 *
 * @package Axis\Dom\Node
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomNodeSublist extends AxisDomNodeList
{
}
