<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\node;
use com\axisgraphics\axis\dom\node\AxisDomNodeSublist;
use com\axisgraphics\axis\field\AxisFieldFactory;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisDomNodeList.
 *
 * @package Axis\Dom\Node
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomNodeList extends DomNode
{
    /**
     * Table fields.
     * @var array of AxisField
     */
    private $_fields;

    /**
     * Overrides DomNode::createFromNode().
     *
     * @param DomNode $node Node
     *
     * @return DomNode
     */
    public static function createFromNode($node)
    {
        $node = parent::createFromNode($node);

        $node->_fields = array();
        $fields = $node->query("> :not(notes, sublist, key, filter, order-by)");
        foreach ($fields as $field) {
            array_push($node->_fields, AxisFieldFactory::getInstance($field));
        }

        return $node;
    }

    /**
     * Gets a sublist by its index.
     *
     * @param integer $index Position
     *
     * @return AxisDomNodeList
     */
    public function getSublistByIndex($index)
    {
        $node = $this->query("sublist:eq('$index')");
        return AxisDomNodeSublist::createFromNode($node);
    }

    /**
     * Gets the notes.
     *
     * @return string
     */
    public function getNotes()
    {
        return TextHelper::trimText($this->query("notes")->text());
    }

    /**
     * Gets the fields.
     *
     * @return array of AxisField objects
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * Gets only 'listable' fields.
     *
     * @return array of AxisField objects
     */
    public function getListableFields()
    {
        $ret = array();

        foreach ($this->_fields as $field) {
            if ($field->attr('listable') != 'false'
                && TextHelper::isEmpty($field->attr('field-name'))
            ) {
                array_push($ret, $field);
            }
        }

        return $ret;
    }

    /**
     * Gets only 'editable' fields.
     *
     * @return array of AxisField objects
     */
    public function getEditableFields()
    {
        $ret = array();

        foreach ($this->_fields as $field) {
            if ($field->attr('editable') != 'false') {
                array_push($ret, $field);
            }
        }

        return $ret;
    }

    /**
     * Gets filters.
     *
     * @return DomNode
     */
    public function getFilters()
    {
        return $this->query("> filter");
    }
    
    /**
     * Gets keys.
     * 
     * @return DomNode[]
     */
    public function getKeys()
    {
        $ret = [];
        return array_filter(
            iterator_to_array($this->getFilters()),
            function ($filter) {
                $operator = $filter->attr("operator");
                return TextHelper::isEmpty($operator) || $operator == "eq";
            }
        );
    }
    
    /**
     * Gets 'order-by' subnodes.
     * 
     * @return DomNode
     */
    public function getOrders()
    {
        return $this->query("> order-by");
    }
}
