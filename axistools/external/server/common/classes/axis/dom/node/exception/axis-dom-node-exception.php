<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\node\exception;
use com\axisgraphics\axis\dom\exception\AxisDomException;

/**
 * Class AxisDomNodeException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Dom\Node\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomNodeException extends AxisDomException
{

}
