<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\node;
use com\axisgraphics\axis\dom\node\AxisDomNodeSection;
use com\axisgraphics\axis\dom\node\exception\AxisDomNodeException;
use com\axisgraphics\axis\dom\transformer\AxisDomTransformer;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\text\TextHelper;
use com\soloproyectos\common\http\cookie\HttpCookieHelper;

/**
 * Class AxisDomNodeSitemap.
 *
 * @package Axis\Dom\Node
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomNodeSitemap extends DomNode
{
    /**
     * Gets the sections.
     *
     * @return DomNode
     */
    public function getSections()
    {
        return $this->query("> section");
    }

    /**
     * Gets the notes.
     *
     * @return DomNode
     */
    public function getNotes()
    {
        return TextHelper::trimText($this->query("> notes")->text());
    }

    /**
     * Gets a section by its id.
     *
     * <p>This function returns null if not found.</p>
     *
     * @param string $sectionId Section id
     *
     * @return AxisDomNodeSection|null
     */
    public function getSectionById($sectionId)
    {
        $node = $this->xpath("//section[@id='$sectionId']");
        return AxisDomNodeSection::createFromNode($node);
    }

    /**
     * Gets config node.
     *
     * @return DomNode
     */
    public function getConfig()
    {
        return $this->query("> config");
    }

    /**
     * Validates the document against an XSD schema.
     *
     * @param string $schema XSD schema
     *
     * @return void
     */
    public function validateXsd($schema)
    {
        $useInternalErrors = libxml_use_internal_errors(true);
        $isValidDocument = @$this->document->schemaValidate($schema);

        // retrieves the errors
        $text = "";
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
            $message = trim($error->message);
            $text = TextHelper::concat(
                "\n", $text, "$message on line {$error->line}, column {$error->column}"
            );
        }
        libxml_clear_errors();

        libxml_use_internal_errors($useInternalErrors);

        if (!$isValidDocument) {
            throw new AxisDomNodeException($text);
        };
    }
}
