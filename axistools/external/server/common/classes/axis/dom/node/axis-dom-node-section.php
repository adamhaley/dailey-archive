<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\dom\node;
use com\axisgraphics\axis\dom\node\AxisDomNodeList;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisDomNodeSection.
 *
 * @package Axis\Dom\Node
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDomNodeSection extends DomNode
{
    /**
     * Gets the number of lists.
     *
     * @return integer
     */
    public function getNumLists()
    {
        return count($this->query("> list"));
    }

    /**
     * Gets a list by its index.
     *
     * @param integer $index Position
     *
     * @return AxisDomNodeList
     */
    public function getListByIndex($index)
    {
        $node = $this->query("> list:eq('$index')");
        return AxisDomNodeList::createFromNode($node);
    }

    /**
     * Gets the notes of the section.
     *
     * @return string
     */
    public function getNotes()
    {
        return TextHelper::trimText($this->query("> notes")->text());
    }
    
    public function isAdmin()
    {
        $ret = false;
        $item = $this;
        while ($item !== null) {
            if ($item->attr("admin-access") == "true") {
                $ret = true;
                break;
            }
            $item = $item->parent();
        }
        return $ret;
    }
}
