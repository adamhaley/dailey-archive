<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\filter;
use com\axisgraphics\axis\db\filter\AxisDbFilter;

/**
 * Class AxisDbFilterContain.
 *
 * @package Axis\Db\Filter
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbFilterContain extends AxisDbFilter
{
    /**
     * Filters records whose column contains any of the given values.
     * 
     * For example:
     * ```php
     * // "column" contains 1, 2 or 3 (the intersection is not an empty set)
     * $f->filter($rows, "column", "1, 2, 3");
     * ```
     * 
     * This method implements AxisDbFilter::filter().
     * 
     * @param AxisDbRecord[] $rows   List of records
     * @param string           $column Column name
     * @param string           $value  Value
     * 
     * @return AxisDbRecord[]
     */
    public function filter($rows, $column, $value)
    {
        $values = $this->_parseList($value);
        return array_filter(
            $rows,
            function ($row) use ($column, $values) {
                return count(array_intersect($this->_parseList($row->{$column}), $values)) > 0;
            }
        );
    }
    
    /**
     * Parses a list of items separated by commas.
     * 
     * @param string $value List of items
     * 
     * @return array
     */
    private function _parseList($value)
    {
        return array_filter(preg_split('/\s*,\s*/', trim($value)));
    }
}
