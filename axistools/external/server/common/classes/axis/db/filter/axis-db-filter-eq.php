<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\filter;
use com\axisgraphics\axis\db\filter\AxisDbFilter;

/**
 * Class AxisDbFilterEq.
 *
 * @package Axis\Db\Filter
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbFilterEq extends AxisDbFilter
{
    /**
     * Filters records whose column are equals to a given value.
     * 
     * This method implements AxisDbFilter::filter().
     * 
     * @param AxisDbRecord[] $rows   List of records
     * @param string           $column Column name
     * @param string           $value  Value
     * 
     * @return AxisDbRecord[]
     */
    public function filter($rows, $column, $value)
    {
        return array_filter(
            $rows,
            function ($row) use ($column, $value) {
                return $row->{$column} == $value;
            }
        );
    }
}
