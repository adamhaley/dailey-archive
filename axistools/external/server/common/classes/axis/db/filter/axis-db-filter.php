<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\filter;

/**
 * Class AxisDbFilter.
 *
 * @package Axis\Db\Filter
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
abstract class AxisDbFilter
{
    /**
     * Filters records by column and value.
     * 
     * @param AxisDbRecord[] $rows   List of records
     * @param string           $column Column name
     * @param string           $value  Value
     * 
     * @return AxisDbRecord[]
     */
    abstract function filter($rows, $column, $value);
}
