<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\filter;
use com\axisgraphics\axis\exception\AxisException;

/**
 * Class AxisDbFilterFactory.
 *
 * @package Axis\Db\Filter
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbFilterFactory
{
    /**
     * List of available operators.
     * @var array
     */
    private static $_operators = ["eq", "lt", "gt", "match", "contain"];
    
    /**
     * Gets a filter instance by operator.
     * 
     * @param string $operator Operator
     * 
     * @return AxisDbFilter
     */
    public static function getInstance($operator)
    {
        if (array_search($operator, AxisDbFilterFactory::$_operators) === false) {
            throw new AxisException("Invalid operator '$operator'");
        }
        
        $className = __NAMESPACE__ . "\\AxisDbFilter" . ucfirst($operator);
        return new $className();
    }
}
