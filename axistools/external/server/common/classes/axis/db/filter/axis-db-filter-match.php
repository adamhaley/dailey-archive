<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\filter;
use com\axisgraphics\axis\db\filter\AxisDbFilter;

/**
 * Class AxisDbFilterMatch.
 *
 * @package Axis\Db\Filter
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbFilterMatch extends AxisDbFilter
{
    /**
     * Filters records whose column are equals to a given value.
     * 
     * It is not necessary to quote the regular expression. For example:
     * ```php
     * $f->filter($rows, "column", "^aaa");
     * 
     * // is equivalent to
     * $f->filter($rows, "column", "/^aaa/i");
     * ```
     * 
     * Also, by default the function is 'case-insensitive'.
     * 
     * This method implements AxisDbFilter::filter().
     * 
     * @param AxisDbRecord[] $rows   List of records
     * @param string           $column Column name
     * @param string           $value  Value
     * 
     * @return AxisDbRecord[]
     */
    public function filter($rows, $column, $value)
    {
        // quotes (if necessary) the regular expression
        if ((@preg_match($value, null) === false)) {
            $value = "/" . preg_quote($value, "/") . "/i";
        }
        
        return array_filter(
            $rows,
            function ($row) use ($column, $value) {
                return preg_match($value, $row->{$column});
            }
        );
    }
}
