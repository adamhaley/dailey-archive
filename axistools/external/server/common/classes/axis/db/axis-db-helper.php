<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db;
use soloproyectos\db\Db;

/**
 * Class AxisDbHelper.
 *
 * @package Axis\Db
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbHelper
{
    /**
     * Gets the next 'ord' value from a table.
     *
     * @param Database $db    Database connector
     * @param string   $table Table name
     *
     * @return integer
     */
    public static function getNextOrd($db, $table)
    {
        $row = $db->query("select ifnull(max(ord) + 1, 1) as ord from " . Db::quoteId($table));
        return intval($row["ord"]);
    }
}
