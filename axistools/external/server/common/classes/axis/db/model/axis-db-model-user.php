<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\model;
use com\axisgraphics\axis\db\model\AxisDbModelInterface;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\security\authentication\exception\AxisSecurityAuthenticationException;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\event\EventDispatcherInterface;
use com\soloproyectos\common\event\EventListener;
use com\soloproyectos\common\http\cookie\HttpCookieHelper;
use com\soloproyectos\common\http\session\HttpSessionHelper;
use com\soloproyectos\common\security\authentication\SecurityAuthenticationInterface;
use com\soloproyectos\common\security\permissions\SecurityPermissionsInterface;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisDbModelUser.
 *
 * @package Axis\Db\Model
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbModelUser {
    /**
     * Database connection.
     * @var DbConnector
     */
    private $_db;

    /**
     * User id.
     * @var string
     */
    private $_id;

    /**
     * Full name.
     * @var string
     */
    private $_fullName;

    /**
     * Email.
     * @var string
     */
    private $_email;

    /**
     * Has admin privileges?
     * @var boolean
     */
    private $_isAdmin;

    /**
     * Constructor.
     *
     * @param DbConnector $database Database connection
     * @param string      $id       User ID
     *
     * @return void
     */
    public function __construct($database, $id)
    {
        $this->_db = $database;
        $this->_id = $id;
        $this->_fullName = "";
        $this->_email = "";

        // fetches the user
        $this->_refresh();
    }

    /**
     * Gets record ID.
     *
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Gets full name.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->_fullName;
    }

    /**
     * Gets email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * Has the user admin privileges?
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->_isAdmin;
    }
    
    /**
     * Refreshes the user from the database.
     *
     * @return void
     */
    private function _refresh()
    {
        $sql = "
        select
            username,
            email,
            name,
            sys_admin
        from users
        where id = ?";
        $row = $this->_db->query($sql, $this->_id);
        $this->_username = $row["username"];
        $this->_email = $row["email"];
        $this->_fullName = $row["name"];
        $this->_isAdmin = $row["sys_admin"] > 0;
    }
}
