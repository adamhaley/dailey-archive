<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\model;
use com\axisgraphics\axis\db\model\AxisDbModelInterface;
use com\axisgraphics\axis\exception\AxisException;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisDbModelUserQuestion.
 *
 * @package Axis\Db\Model
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbModelUserQuestion implements AxisDbModelInterface
{
    /**
     * Database connection.
     * @var DbConnector
     */
    private $_db;

    /**
     * Record id.
     * @var string
     */
    private $_id;

    /**
     * Question text.
     * @var string
     */
    private $_text;

    /**
     * Constructor.
     *
     * @param DbConnector $db Database connection
     * @param string      $id Record ID (not required)
     */
    public function __construct($db, $id = "")
    {
        $this->_db = $db;
        $this->_id = $id;
        $this->_text = "";

        if (!TextHelper::isEmpty($this->_id)) {
            $this->refresh();
        }
    }

    /**
     * Refreshes the record from the database.
     *
     * @return void
     */
    public function refresh()
    {
        $this->_text = "";

        $sql = "
        select
            text
        from users_question
        where id = ?";
        $row = $this->_db->query($sql, $this->_id);

        if (count($row) > 0) {
            $this->_text = $row["text"];
        }
    }

    /**
     * Saves the record into the database.
     *
     * @return void
     */
    public function save()
    {
        throw new AxisException("Not implemented");
    }

    /**
     * Deletes a record from the database.
     *
     * @return void
     */
    public function delete()
    {
        throw new AxisException("Not implemented");
    }

    /**
     * Gets record ID.
     *
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Gets the question text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * Sets the question text.
     *
     * @param string $text Question text
     *
     * @return void
     */
    public function setText($text)
    {
        $this->_text = $text;
    }
}
