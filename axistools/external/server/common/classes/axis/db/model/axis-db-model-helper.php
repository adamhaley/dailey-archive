<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\db\model;
use com\axisgraphics\axis\db\model\AxisDbModelUser;
use com\axisgraphics\axis\db\model\AxisDbModelUserQuestion;

/**
 * Class AxisDbModelHelper.
 *
 * @package Axis\Db\Model
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisDbModelHelper
{
    /**
     * Searches an user by email, question and answer.
     *
     * @param DbConnector $db         Database connection
     * @param string      $email      Email address
     * @param integer     $questionId Question ID
     * @param string      $answer     Question answer
     *
     * @return AxisModelUser
     */
    public static function searchUser($db, $email, $questionId, $answer)
    {
        $ret = null;

        $sql = "
        select
            id
        from users
        where email = ?
        and question_id = ?
        and answer = ?";
        $row = $db->query($sql, array($email, $questionId, sha1($answer)));

        if (count($row) > 0) {
            $ret = new AxisDbModelUser($db, $row["id"]);
        }

        return $ret;
    }

    /**
     * Gets the list of user questions.
     *
     * @param DbConnector $db Database connection
     *
     * @return array of AxisDbModelUserQuestion
     */
    public static function getUserQuestions($db)
    {
        $ret = array();

        $sql = "
        select
            id
        from users_question
        order by text";
        $rows = $db->query($sql);
        foreach ($rows as $row) {
            array_push($ret, new AxisDbModelUserQuestion($db, $row["id"]));
        }

        return $ret;
    }

    /**
     * Generates a new aleatory password.
     *
     * @return string
     */
    public static function generatePassword()
    {
        $ret = "";
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";

        for ($i = 0; $i < 10; $i++) {
            $ret .= $chars[rand(0, strlen($chars) - 1)];
        }

        return $ret;
    }
}
