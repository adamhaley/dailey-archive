<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
namespace com\axisgraphics\axis\db\model;

/**
 * Interface AxisDbModelInterface.
 *
 * @package Axis\Db\Model
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license https://github.com/soloproyectos/php.common-libs/blob/master/LICENSE BSD 2-Clause License
 * @link    https://github.com/soloproyectos/php.common-libs
 */
interface AxisDbModelInterface
{
    /**
     * Refreshes the record from the database.
     *
     * @return void
     */
    function refresh();

    /**
     * Saves the record into the database.
     *
     * @return void
     */
    function save();


    /**
     * Deletes the record from the database.
     *
     * @return void
     */
    function delete();
}
