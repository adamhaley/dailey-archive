<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\security;
use com\axisgraphics\axis\db\model\AxisDbModelUser;
use com\axisgraphics\axis\security\exception\AxisSecurityException;
use com\soloproyectos\common\http\cookie\HttpCookieHelper;
use com\soloproyectos\common\http\session\HttpSessionHelper;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\record\DbRecordTable;

/**
 * Class AxisSecuritySystem.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Security
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisSecuritySystem
{
    /**
     * Expiration time: one month;
     * @var integer
     */
    private $_expirationTime = 30 * 24 * 60 * 60;
    
    /**
     * ID length.
     * @var integer
     */
    private $_idLength = 20;
    
    /**
     * Maximum number of tokens per user.
     * @var integer
     */
    private $_maxTokens = 10;
    
    /**
     * Database connector.
     * @var DbConnector
     */
    private $_db;
    
    /**
     * Constructor.
     * 
     * @param DbConnector $db Database connector
     */
    public function __construct($db)
    {
        $this->_db = $db;
    }
    
    /**
     * Logs into the system.
     * 
     * @param string  $username    User name
     * @param string  $password    Password
     * @param boolean $isPersisten Long-Term persistence (remember me on this computer)
     * 
     * @return AxisDbModelUser
     */
    public function login($username, $password, $isPersistent)
    {
        // searches user by name
        $sql = "
        select
            id,
            password
        from users
        where login_enable
        and username = ?";
        $row = $this->_db->query($sql, [$username, sha1($password)]);
        if (count($row) == 0) {
            throw new AxisSecurityException("Invalid username or password");
        }
        
        // verifies password
        if (!password_verify($password, $row["password"])) {
            throw new AxisSecurityException("Invalid username or password");
        }
        
        // creates a new token
        $selector = $this->_getRandomId();
        $token = $this->_getRandomId();
        $r = new DbRecordTable($this->_db, "users_token");
        $id = $r->insert(
            [
                "user_id" => $row["id"],
                "selector" => $selector,
                "token" => sha1($token)
            ]
        );
        
        $this->_delOldTokens($row["id"]);
        
        // saves credentials
        $this->_setStoredCredentials(
            "com\soloproyectos\common\http\session\HttpSessionHelper", $selector, $token
        );
        if ($isPersistent) {
            $this->_setStoredCredentials(
                "com\soloproyectos\common\http\cookie\HttpCookieHelper", $selector, $token
            );
        }
        
        return new AxisDbModelUser($this->_db, $row["id"]);
    }
    
    /**
     * Logs out from the system.
     * 
     * This function deletes credentials from the current session and the cookies.
     * 
     * @return void
     */
    public function logout()
    {
        $helpers = [
            "com\soloproyectos\common\http\session\HttpSessionHelper",
            "com\soloproyectos\common\http\cookie\HttpCookieHelper"
        ];
        foreach ($helpers as $helper) {
            $this->_delStoredCredentials($helper);
        }
    }
    
    /**
     * Gets the current user.
     * 
     * @retun AxisDbModelUser|null
     */
    public function getUser()
    {
        $ret = null;
        
        // recovers credentials
        $helpers = [
            "com\soloproyectos\common\http\session\HttpSessionHelper",
            "com\soloproyectos\common\http\cookie\HttpCookieHelper"
        ];
        foreach ($helpers as $helper) {
            list($selector, $token) = $this->_getStoredCredentials($helper);
            if (!TextHelper::isEmpty($selector)) {
                break;
            }
        }
        
        // verifies credentials
        if (!TextHelper::isEmpty($selector)) {
            $sql = "
            select
                user_id,
                token
            from users_token
            where selector = ?";
            $rows = $this->_db->query($sql, $selector);
            foreach ($rows as $row) {
                if ($row["token"] == sha1($token)) {
                    $ret = new AxisDbModelUser($this->_db, $row["user_id"]);
                    break;
                }
            }
        }
        
        return $ret;
    }
    
    /**
     * Gets stored credentials.
     * 
     * @return [selector:string, token:string]
     */
    public function getCredentials()
    {
        $helpers = [
            "com\soloproyectos\common\http\session\HttpSessionHelper",
            "com\soloproyectos\common\http\cookie\HttpCookieHelper"
        ];
        foreach ($helpers as $helper) {
            list($selector, $token) = $this->_getStoredCredentials($helper);
            if (!TextHelper::isEmpty($selector)) {
                break;
            }
        }
        return [$selector, $token];
    }
    
    /**
     * Gets stored credentials.
     * 
     * @param string $helper Helper class
     * 
     * @return [selector:string, token:string]
     */
    private function _getStoredCredentials($helper)
    {
        $ret = ["", ""];
        $str = $helper::get("token");
        if (!TextHelper::isEmpty($str)) {
            $pos = strpos($str, ":");
            if ($pos !== false) {
                $selector = substr($str, 0, $pos);
                $token = substr($str, $pos + 1);
                $ret = [$selector, $token];
            }
        }
        return $ret;
    }
    
    /**
     * Saves credentials in the current session.
     * 
     * @param string $class    Helper class
     * @param string $selector Credential selector
     * @param string $token    Credential token
     * 
     * @return void
     */
    private function _setStoredCredentials($helper, $selector, $token) {
        $helper::set("token", "$selector:$token", $this->_expirationTime);
    }
    
    /**
     * Removes credentials from the current session.
     * 
     * @param string $helper Helper class
     * 
     * @return void
     */
    private function _delStoredCredentials($helper)
    {
        $helper::del("token");
    }
    
    /**
     * Generates a random token.
     * 
     * @return string
     */
    private function _getRandomId()
    {
        return bin2hex(mcrypt_create_iv($this->_idLength, MCRYPT_DEV_URANDOM));
    }
    
    /**
     * Deletes old tokens from database.
     * 
     * @param string $userId User ID
     * 
     * @return void
     */
    private function _delOldTokens($userId)
    {
        $sql = "
        select
            id
        from (
            select
                id
            from users_token
            where user_id = ?
            order by id desc
            limit {$this->_maxTokens}
        ) as A
        order by id
        limit 1";
        $row = $this->_db->query($sql, $userId);
        if (count($row) > 0) {
            $this->_db->exec("
                delete
                from users_token
                where user_id = ?
                and id < ?",
                [$userId, $row["id"]]
            );
        }
    }
}
