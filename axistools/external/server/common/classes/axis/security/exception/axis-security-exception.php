<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\security\exception;
use com\axisgraphics\axis\exception\AxisException;

/**
 * Class AxisSecurityException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Security\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisSecurityException extends AxisException
{

}
