<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\security\authentication\exception;
use com\axisgraphics\axis\security\exception\AxisSecurityException;

/**
 * Class AxisSecurityPermissionsException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Security\Permissions\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisSecurityPermissionsException extends AxisSecurityException
{

}
