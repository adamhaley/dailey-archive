<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\text\parser;
use com\axisgraphics\axis\text\parser\exception\AxisTextParserException;
use com\soloproyectos\common\text\parser\TextParser;

/**
 * Class AxisTextParserList.
 *
 * @package Axis\Text\Parser
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisTextParserList extends TextParser
{
    /**
     * Is the next thing a value?
     *
     * @return boolean|array of a single string
     */
    protected function value()
    {
        if (   (list($value) = $this->number())
            || (list($value) = $this->str())
            || (list($value) = $this->match('[\w\-]+'))
        ) {
            return array($value);
        }

        return false;
    }
    
    /**
     * Parses an expression.
     *
     * @return boolean|array
     */
    public function evaluate()
    {
        $ret = false;
        
        if (list($value) = $this->is("value")) {
            $ret = [];
            array_push($ret, $value);
            while ($this->eq(",")) {
                if (!list($value) = $this->is("value")) {
                    throw new AxisTextParserException("Invalid value", $this);
                }
                array_push($ret, $value);
            }
        }
        
        return $ret;
    }
}
