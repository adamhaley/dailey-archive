<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\text\parser\exception;
use com\axisgraphics\axis\text\exception\AxisTextException;
use com\soloproyectos\common\text\parser\exception\TextParserException;

/**
 * Class AxisTextParserException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Text\Parser\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisTextParserException extends AxisTextException
{
    /**
     * Constructor.
     *
     * @param string     $message The exception message
     * @param TextParser $parser  The parser object (default is null)
     */
    public function __construct($message, $parser = null)
    {
        parent::__construct(new TextParserException($message, $parser));
    }
}
