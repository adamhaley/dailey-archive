<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\text\parser;
use com\axisgraphics\axis\text\parser\exception\AxisAxisTextParserException;
use com\soloproyectos\common\text\parser\TextParser;

/**
 * Class AxisTextParserField.
 *
 * @package Axis\Text\Parser
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisTextParserField extends TextParser
{
    /**
     * Table name.
     * @var string
     */
    private $_tableName;

    /**
     * Constructor.
     *
     * @param string $str       Arbitrary string
     * @param string $tableName Table name
     */
    public function __construct($str, $tableName)
    {
        $this->_tableName = $tableName;
        parent::__construct($str);
    }

    /**
     * Is the next thing a join?
     *
     * @return boolean|array Array(string 'table_id', array 'join')
     */
    public function join()
    {
        $ret = false;

        if ((list($tableId) = $this->id())
            && $this->eq("=")
            && ($join = $this->is("field"))
        ) {
            $ret = array($tableId, $join);
        }

        return $ret;
    }

    /**
     * Is the next thing a field?
     *
     * @return boolean|array Array(
     *                          string 'table_name', string 'table_id',
     *                          array 'join', string 'field_name'
     *                       )
     */
    protected function field()
    {
        $ret = false;
        $tableName = "";
        $tableId = "";
        $fieldName = "";
        $join = array();

        if (list($tableName) = $this->id()) {
            if ($this->eq("[")) {
                if (!list($tableId, $join) = $this->is("join")) {
                    if (!$join = $this->is("field")) {
                        throw new AxisTextParserException("Invalid field", $this);
                    }

                    $tableId = "id";
                }

                if (!$this->eq("]")) {
                    throw new AxisTextParserException("Missing ]", $this);
                }

                if (!$this->eq(".")) {
                    throw new AxisTextParserException("Missing .", $this);
                }

                if (!list($fieldName) = $this->id()) {
                    throw new AxisTextParserException("Invalid identifier", $this);
                }
            } elseif ($this->eq(".")) {
                if (!list($fieldName) = $this->id()) {
                    throw new AxisTextParserException("Invalid identifier", $this);
                }

                $tableId = "id";
                if ($this->_tableName != $tableName) {
                    $join = array(
                        $this->_tableName,
                        "id",
                        null,
                        "{$tableName}_id"
                    );
                }
            } else {
                $fieldName = $tableName;
                $tableName = $this->_tableName;
                $tableId = "id";
            }

            $ret = array($tableName, $tableId, $join, $fieldName);
        }

        return $ret;
    }

    /**
     * Parses an expression.
     *
     * @return boolean|array Array(
     *                          string 'table_name', string 'table_id',
     *                          array 'join', string 'field_name'
     *                       )
     */
    protected function evaluate()
    {
        return $this->is("field");
    }
}
