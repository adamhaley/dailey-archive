<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\text\parser;
use com\axisgraphics\axis\text\parser\exception\AxisAxisTextParserException;
use com\axisgraphics\axis\text\parser\AxisTextParserField;

/**
 * Class AxisTextParserFilter.
 *
 * @package Axis\Text\Parser
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisTextParserFilter extends AxisTextParserField
{
    /**
     * Operators
     * @var array of strings
     */
    private $_operators = array("=", "<", ">", "<=", ">=", "!=", "in");

    /**
     * Is the next thing an operator?
     *
     * @return boolean|array of a single string
     */
    protected function operator()
    {
        return $this->in($this->_operators);
    }

    /**
     * Is the next thing a value?
     *
     * @return boolean|array of a single string
     */
    protected function value()
    {
        if (   (list($value) = $this->number())
            || (list($value) = $this->str())
            || (list($value) = $this->match("[\w\.]+|."))
        ) {
            return array($value);
        }

        return false;
    }

    /**
     * Parses an expression.
     *
     * @return boolean|array Array(
     *                          string 'table_name', string 'table_id',
     *                          array 'join', string 'field_name',
     *                          string 'operator', string value
     *                      )
     */
    protected function evaluate()
    {
        if (!list($tableName, $tableId, $join, $fieldName) = $this->is("field")) {
            return false;
        }

        if (!list($op) = $this->is("operator")) {
            throw new AxisTextParserException("Invalid operator", $this);
        }

        if (!list($value) = $this->is("value")) {
            throw new AxisTextParserException("Invalid value", $this);
        }

        return array($tableName, $tableId, $join, $fieldName, $op, $value);
    }
}
