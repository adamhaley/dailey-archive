<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\text\parser;
use com\axisgraphics\axis\text\parser\exception\AxisAxisTextParserException;
use com\axisgraphics\axis\text\parser\AxisTextParserField;

/**
 * Class AxisTextParserOrder.
 *
 * @package Axis\Text\Parser
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisTextParserOrder extends AxisTextParserField
{
    /**
     * Default sort type.
     * @var string
     */
    private $_defaultSortType = "asc";

    /**
     * Available sort types.
     * @var array of strings
     */
    private $_sortTypes = array("asc", "desc");

    /**
     * Is the next thing a sort type?
     *
     * @return boolean|array of a single string
     */
    protected function sortType()
    {
        return $this->in($this->_sortTypes);
    }

    /**
     * Parses an expression.
     *
     * @return boolean|array Array(
     *                          string 'table_name', string 'table_id',
     *                          array 'join', string 'field_name',
     *                          string 'sort_type'
     *                      )
     */
    protected function evaluate()
    {
        $ret = array();

        if (!list($tableName, $tableId, $join, $fieldName) = $this->is("field")) {
            return false;
        }

        if (!list($sortType) = $this->is("sortType")) {
            $sortType = $this->_defaultSortType;
        }

        array_push(
            $ret,
            array(
                $tableName,
                $tableId,
                $join,
                $fieldName,
                $sortType
            )
        );

        while ($this->eq(",")) {
            $list = list(
                $tableName,
                $tableId,
                $join,
                $fieldName
            ) = $this->is("field");

            if (!$list) {
                throw new AxisTextParserException("Invalid field", $this);
            }

            if (!list($sortType) = $this->is("sortType")) {
                $sortType = $this->_defaultSortType;
            }

            array_push(
                $ret,
                array(
                    $tableName,
                    $tableId,
                    $join,
                    $fieldName,
                    $sortType
                )
            );
        }

        return $ret;
    }
}
