<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\text\exception;
use com\axisgraphics\axis\exception\AxisException;

/**
 * Class AxisTextException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Text\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisTextException extends AxisException
{

}
