<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;

/**
 * Class AxisFieldEditInterface.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
interface AxisFieldEditInterface
{
    /**
     * Gets the object.
     *
     * The object returned by this method is used in the 'record editor'.
     *
     * @return mixed
     */
    function getEditObject();

    /**
     * Sets the object.
     *
     * The 'record editor' calls this method and passes an object.
     *
     * @param mixed $object Object
     *
     * @return void
     */
    function setEditObject($object);
}
