<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\exception\AxisException;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisField.
 *
 * This class represents a field. A field is declared in the sitemap.xml file as
 * follows: &lt;field column="name" type="type" ... /&gt;
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisField extends DomNode implements AxisFieldTableInterface, AxisFieldEditInterface
{
    /**
     * List of columns.
     * @var array AxisField
     */
    private $_columns = array();

    /**
     * Field value.
     * @var string
     */
    private $_value = "";
    
    /**
     * Can this field used as filter?
     * @var boolean
     */
    protected $isFiltrable = true;
    
    /**
     * Can this field used to sort the results?
     * @var boolean
     */
    protected $isSortable = true;
    
    /**
     * Default sorting order.
     * @var boolean
     */
    protected $isAscending = true;

    /**
     * Overrides DomNode::createFromNode().
     *
     * @param DomNode $node Node
     *
     * @return DomNode
     */
    public static function createFromNode($node)
    {
        $node = parent::createFromNode($node);

        array_push($node->_columns, $node);
        $node->init();

        return $node;
    }

    /**
     * Initialization.
     *
     * Override this method in the inherited classes to initialize variables.
     *
     * @return void
     */
    public function init()
    {
    }
    
    /**
     * Can this field used as filter?
     * 
     * @return boolean
     */
    public function isFiltrable()
    {
        return $this->attr("readonly") !== "true" && $this->isFiltrable;
    }

    /**
     * Is the field sortable?
     * 
     * @return boolean
     */
    public function isSortable()
    {
        return $this->attr("sortable") !== "false" && $this->isSortable;
    }
    
    /**
     * Is the field ascending?
     * 
     * This method returns the default sorting order.
     * 
     * @return boolean
     */
    public function isAscending()
    {
        return $this->isAscending;
    }

    /**
     * Is the field represented as an image in the 'table viewer'?
     *
     * @see AxisFieldTableInterface::isTableImage()
     * @return boolean
     */
    public function isTableImage()
    {
        return false;
    }

    /**
     * Gets the value.
     *
     * The value returned by this method is used in the 'table viewer'. It can represent a text or an
     * image, depending of the 'isTableImage' method.
     *
     * @see AxisFieldTableInterface::isTableValue()
     * @return string
     */
    public function getTableValue()
    {
        return $this->_value;
    }

    /**
     * Gets the object.
     *
     * The object returned by this method is used in the 'record editor'.
     *
     * @see AxisFieldEditInterface::getEditObject()
     * @return mixed
     */
    public function getEditObject()
    {
        return $this->_value;
    }

    /**
     * Sets the object.
     *
     * The 'record editor' calls this method and passes an object.
     *
     * @param mixed $object Object
     *
     * @see AxisFieldEditInterface::setEditObject()
     * @return void
     */
    public function setEditObject($object)
    {
        $this->setValue($object);
    }

    /**
     * Registers a database column.
     *
     * @param string $selector CSS selector
     *
     * @return AxisField
     */
    public function registerColumn($selector)
    {
        $item = $this->query($selector);
        if (count($item) == 0) {
            throw new AxisException("[AxisField] Column not found: $selector");
        }

        $column = AxisField::createFromNode($this->query($selector));
        array_push($this->_columns, $column);
        return $column;
    }

    /**
     * Registers one or more database columns.
     *
     * @param string $selector CSS selector
     *
     * @return array of AxisField
     */
    public function registerColumns($selector)
    {
        $ret = array();
        $items = $this->query($selector);
        foreach ($items as $item) {
            $column = AxisField::createFromNode($item);
            array_push($ret, $column);
            array_push($this->_columns, $column);
        }
        return $ret;
    }

    /**
     * Gets columns by name.
     *
     * If no name is provided, this function returns all columns, including the instance itself.
     *
     * @return array of AxisField
     */
    public function getColumns()
    {
        return $this->_columns;
    }

    /**
     * Gets the field value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Sets the value.
     *
     * @param string $value Value
     *
     * @return void
     */
    public function setValue($value)
    {
        if ($this->attr("required") == "true" && TextHelper::isEmpty($value)) {
            $title = TextHelper::ifEmpty($this->attr("title"), $this->attr("column"));
            throw new AxisException("`$title` is required.");
        }

        $this->_value = $value;
    }

    /**
     * Sets the original value.
     *
     * @param string $value Value
     *
     * @return void
     */
    public function setOriginalValue($value)
    {
        $this->_value = $value;
    }
    
    /**
     * Gets the string representation of a filter value.
     * 
     * @param mixed $value Filter value
     * 
     * @return string
     */
    public function getFilter($value)
    {
        return print_r($value, true);
    }
    
    /**
     * Filter records by value.
     * 
     * @param Traversable $records List of records
     * @param mixed       $value   Value
     * 
     * @return Traversable
     */
    public function filter($records, $value)
    {
        $ret = $records;
        if (!TextHelper::isEmpty($value)) {
            $filter = AxisDbFilterFactory::getInstance("eq");
            $ret = $filter->filter($records, $this->attr("column"), $value);
        }
        return $ret;
    }
}
