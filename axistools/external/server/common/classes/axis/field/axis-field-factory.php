<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\field\AxisField;

/**
 * Class AxisFieldFactory.
 *
 * This is a factory class.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldFactory
{
    /**
     * Class prefix.
     * @var string
     */
    const CLASSPREFIX = "AxisField";

    /**
     * Gets the AxisField instance.
     *
     * @param DomNode $field A field
     *
     * @return AxisField
     */
    public static function getInstance($field)
    {
        $ret = null;
        $type = $field->name();
        $className = __NAMESPACE__ .
            "\\" . AxisFieldFactory::CLASSPREFIX . ucfirst($type);

        if (!class_exists($className)) {
            throw new AxisException("Invalid field <$type />");
        }

        return $className::createFromNode($field);
    }
}
