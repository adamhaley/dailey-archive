<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;

/**
 * Class AxisFieldPosition.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisFieldPosition
{
    /**
     * Column.
     * @var AxisField
     */
    private $_column = null;

    /**
     * Position of the column.
     * @var integer
     */
    private $_position = 0;

    /**
     * Constructor.
     *
     * @param AxisField $column Column
     */
    public function __construct($column)
    {
        $this->_column = $column;
    }

    /**
     * Gets the column.
     *
     * @return AxisField
     */
    public function getColumn()
    {
        return $this->_column;
    }

    /**
     * Gets the position of the column.
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->_position;
    }

    /**
     * Sets the position of the column.
     *
     * @param integer $position Field position
     *
     * @return void
     */
    public function setPosition($position)
    {
        $this->_position = $position;
    }
}
