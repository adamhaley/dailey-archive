<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\field;

/**
 * Class AxisFieldTableInterface.
 *
 * @package Axis\Field
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
interface AxisFieldTableInterface
{
    /**
     * Is the field represented as an image in the 'table viewer'?
     *
     * @return boolean
     */
    function isTableImage();

    /**
     * Gets the value.
     *
     * The value returned by this method is used in the 'table viewer'. It can represent a text or an
     * image, depending of the 'isTableImage' method.
     *
     * @return string
     */
    function getTableValue();
}
