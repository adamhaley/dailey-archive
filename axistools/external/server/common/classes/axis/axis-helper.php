<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis;
use com\axisgraphics\axis\exception\AxisException;
use com\soloproyectos\common\sys\file\SysFileHelper;
use soloproyectos\db\Db;

/**
 * Class AxisHelper.
 *
 * @package Axis
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHelper
{
    /**
     * Gets an available filename.
     *
     * This function gets an available filename under a given directory.
     *
     * @param string $dir     Directory
     * @param string $refname Reference name
     * @param string $refext  Reference extension (default is "")
     *
     * @return string
     */
    public static function getAvailName($dir, $refname, $refext)
    {
        return AxisHelper::getRelPath(
            SysFileHelper::getAvailName($dir, $refname, $refext)
        );
    }

    /**
     * Gets the relative path.
     *
     * This function gets the relative path of a given path. The relative path is
     * calculated from the document root.
     *
     * For example:
     * ```php
     * AxisHelper::getRelPath("/path/to/my/proyect/folder/test.txt");
     * // the above command prints
     * // "folder/test.txt"
     * ```
     *
     * @param string $absolutePath An absolute path
     *
     * @throws AxisException if the path is not under the document root
     * @return string
     */
    public static function getRelPath($absolutePath)
    {
        $docRoot = rtrim($_SERVER["DOCUMENT_ROOT"], "/");

        if (strpos($absolutePath, $docRoot) !== 0) {
            throw new AxisException("$absolutePath is not under document root");
        }

        return "/" . ltrim(substr($absolutePath, strlen($docRoot)), "/");
    }

    /**
     * Gets the absolute path.
     *
     * This function gets the absolute path of a given path. The absolute path is
     * calculated from the document root. For example:
     *
     * For example:
     * ```php
     * echo AxisHelper::getAbsPath("folder/test.txt");
     * // the above command prints
     * // "/path/to/my/proyect/folder/test.txt"
     * ```
     *
     * @param string $relativePath A relative path
     *
     * @return string
     */
    public static function getAbsPath($relativePath)
    {
        return SysFileHelper::concat($_SERVER["DOCUMENT_ROOT"], $relativePath);
    }

    /**
     * Gets the CMS url.
     *
     * @return string
     */
    public static function getCmsUrl()
    {
        $protocol = preg_match('/^https/i', $_SERVER["SERVER_PROTOCOL"])? "https" : "http";
        return "$protocol://" . CMS_HOST;
    }

    /**
     * Gets the 'next' ord value.
     *
     * @param DbConnector $db    Database connector
     * @param string      $table Table name
     *
     * @return integer
     */
    public static function getNextOrd($db, $table)
    {
        $row = $db->query(
            "select ifnull(max(ord), 0) as ord from " . Db::quoteId($table)
        );
        return $row["ord"] + 1;
    }
}
