<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\sys\ffmpeg\SysFfmpeg;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerVideoEditorInfo.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerVideoEditorInfo extends AxisHttpController
{
    /**
     * Video width.
     * @var integet
     */
    private $_width = 0;

    /**
     * Video height.
     * @var integer
     */
    private $_height = 0;

    /**
     * Frames per second.
     * @var integer
     */
    private $_fps = 0;

    /**
     * Video size in bytes.
     * @var integer
     */
    private $_size = 0;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("get", array($this, "onGet"));
        $this->done();
    }

    /**
     * Handles 'get' requests.
     *
     * @return void
     */
    public function onGet()
    {
        $src = $this->request->get("src");

        if (!TextHelper::isEmpty($src)) {
            $filename = AxisHelper::getAbsPath($src);

            if (!is_file($filename)) {
                throw new AxisException("Video not found: $src");
            }

            $ffmpeg = new SysFfmpeg($filename);
            $this->_width = $ffmpeg->getWidth();
            $this->_height = $ffmpeg->getHeight();
            $this->_fps = $ffmpeg->getFramesPerSecond();
            $this->_size = SysFileHelper::getHumanSize($ffmpeg->getSize());
        }
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $xml = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode('width', $this->_width));
                $target->append(new DomNode('height', $this->_height));
                $target->append(new DomNode('frames-per-second', $this->_fps));
                $target->append(new DomNode('human-size', $this->_size));
            }
        );

        return $xml->html();
    }
}
