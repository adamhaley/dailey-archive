<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\exception\AxisExceptionExpiredSession;
use com\axisgraphics\axis\dom\transformer\AxisDomTransformer;
use com\axisgraphics\axis\http\controller\AxisHttpControllerBase;
use com\soloproyectos\common\dom\exception\DomException;
use com\soloproyectos\common\sys\cmd\SysCmdHelper;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerLogin.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerSitemap extends AxisHttpControllerBase
{
    /**
     * Sitemap contents.
     * @var string
     */
    private $_contents = "";

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * Handles START requests.
     *
     * @return void
     */
    public function onStart()
    {
        // is any user logged?
        $this->user = $this->system->getUser();
        if ($this->user === null) {
            throw new AxisExceptionExpiredSession("Your session has expired");
        }

        // does the SITEMAP exist?
        if (!is_file(SITEMAP)) {
            throw new AxisException(
                "The sitemap file was not found.\n" .
                "Please check the general.php config file"
            );
        }

        // gets the sitemap contents
        $extension = SysFileHelper::getExtension(SITEMAP);
        if ($extension == "php") {
            $keys = preg_replace('/\s+/', ' ', var_export($_SERVER, true));
            $this->_contents = SysCmdHelper::exec(
                // changes to the sitemap directory
                'cd ' . dirname(SITEMAP) . '; ' .
                // executes sitemap
                'php -r ' . SysCmdHelper::escape(
                    // exports the $_SERVER variable
                    'foreach (' . $keys . ' as $key => $value) $_SERVER[$key] = $value; ' .
                    // includes the sitemap
                    'require_once "' . basename(SITEMAP) . '";'
                )
            );
        } else {
            // loads the contents from file
            $this->_contents = file_get_contents(SITEMAP);
        }

        // transforms and compiles the sitemap
        $t = new AxisDomTransformer();
        try {
            $this->_contents = $t->transform($this->_contents);
        } catch (DomException $e) {
            header("HTTP/1.0 500 The sitemap is not well formed: {$e->getMessage()}");
        }
    }

    /**
     * Gets the sitemap contents.
     *
     * @return string
     */
    public function getContents()
    {
        return $this->_contents;
    }
}
