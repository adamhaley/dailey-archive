<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\db\AxisDbHelper;
use com\axisgraphics\axis\db\AxisDbRecord;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\DomHelper;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\Db;
use soloproyectos\db\record\DbRecordTable;

/**
 * Class AxisHttpControllerList.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerOrderList extends AxisHttpController
{
    /**
     * Section name identifier.
     * @var string
     */
    private $_sectionId;

    /**
     * Current table position.
     * @var integer
     */
    private $_tablePos;

    /**
     * Table node.
     * @var AxisDomNodeList
     */
    private $_table;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles START requests.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $this->_sectionId = $this->request->get("section");
        $this->_tablePos = $this->request->get("table");

        // section node
        $section = $this->sitemap->getSectionById($this->_sectionId);
        if (count($section) == 0 || $section->isAdmin() && !$this->user->isAdmin()) {
            throw new AxisException(
                "Section not found: " . $this->_sectionId
            );
        }

        // table node
        $this->_table = $section->getListByIndex($this->_tablePos);
        if (count($this->_table) == 0) {
            throw new AxisException(
                "Table position not found: " . $this->_tablePos
            );
        }
        
        // checks if the list is sortable
        $orders = $this->_table->getOrders();
        if (count($orders) > 0) {
            $this->_orders = [];
            foreach ($orders as $order) {
                $column = $order->attr("column");
                $desc = $order->attr("desc") == "true";
                if ($column !== "ord" || $desc) {
                    throw new AxisException("The list is not sortable");
                }
            }
        }
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        $action = $this->request->get("action");

        if ($action == "sort") {
            $ids = $this->request->get("ids");
            $tableName = $this->_table->attr("table");
            $t = new DbRecordTable($this->database, $tableName);
            $nextOrd = AxisDbHelper::getNextOrd($this->database, $tableName);
            
            // sets non-duplicate ord's
            $ords = [];
            foreach ($ids as $i => $id) {
                // saves 'ord' into a variable
                list($ord) = $t->select(["ord"], $id);
                array_push($ords, $ord);
                // sets a temporary 'ord'
                $t->update(["ord" => $nextOrd + $i], $id);
            }
            sort($ords, SORT_NUMERIC);

            // rearranges records
            foreach ($ids as $i => $id) {
                $t->update(["ord" => $ords[$i]], $id);
            }
        }
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    protected function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode('title', $this->_table->attr("title")));
                $target->append(new DomNode('section', $this->_sectionId));
                $target->append(new DomNode('position', $this->_tablePos));

                // columns
                $fields = $this->_table->getListableFields();
                foreach ($fields as $field) {
                    $target->append(
                        new DomNode(
                            'column',
                            array(
                                "title" => $field->attr("title"),
                                "type" => $field->isTableImage()? "image" : "text"
                            )
                        )
                    );
                }

                // rows
                $rows = $this->_getRows();
                foreach ($rows as $row) {
                    $target->append(
                        new DomNode(
                            'row',
                            array("id" => $row->id, "ord" => $row->ord),
                            function ($target) use ($fields, $row) {
                                foreach ($fields as $field) {
                                    // fills columns
                                    $columns = $field->getColumns();
                                    foreach ($columns as $column) {
                                        $column->setOriginalValue($row->{$column->attr("column")});
                                    }

                                    $item = new DomNode('item');
                                    $item->append(DomHelper::cdata($field->getTableValue()));
                                    $target->append($item);
                                }
                            }
                        )
                    );
                }
            }
        );

        return $root->html();
    }

    /**
     * Gets the rows of the list.
     *
     * @return AxisDbRecord[]
     */
    private function _getRows()
    {
        $ret = [];
        
        // get records
        $fields = $this->_table->getListableFields();
        $tableName = $this->_table->attr("table");
        $rows = iterator_to_array($this->database->query($this->_getSelectStatement()));
        foreach ($rows as $row) {
            array_push($ret, new AxisDbRecord($this->database, $tableName, $row["id"]));
        }
        
        // sorts records by 'ord' ascending
        $orders = $this->_table->getOrders();
        usort(
            $ret,
            function ($row1, $row2) {
                $ret = 0;
                return strnatcasecmp($row1->ord, $row2->ord);
            }
        );
        
        // filters records
        $filters = $this->_table->getFilters();
        foreach ($filters as $filter) {
            $column = $filter->attr("column");
            $operator = TextHelper::ifEmpty($filter->attr("operator"), "eq");
            $value = $filter->attr("value");
            $f = AxisDbFilterFactory::getInstance($operator);
            $ret = $f->filter($ret, $column, $value);
        }
        
        return $ret;
    }
    
    /**
     * Gets the SELECT statement
     * 
     * @return string
     */
    private function _getSelectStatement()
    {
        $sqlWhere = "";
        
        // gets simple filters
        $keys = array_filter(
            $this->_table->getKeys(),
            function ($key) {
                $column = trim($key->attr("column"));
                $operator = TextHelper::ifEmpty($key->attr("operator"), "eq");
                return $operator == "eq" && preg_match('/^\w+$/', $column);
            }
        );
        
        // sql where
        if (count($keys) > 0) {
            $sqlWhere = "\nwhere " . implode(
                "\nand ",
                array_map(
                    function ($key) {
                        $column = Db::quoteId($key->attr("column"));
                        $value = $this->database->quote($key->attr("value"));
                        return "$column = $value";
                    },
                    $keys
                )
            );
        }
        
        return "select id from " . Db::quoteId($this->_table->attr("table")) . $sqlWhere;
    }
}
