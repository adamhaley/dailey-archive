<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\exception\AxisExceptionExpiredSession;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\dom\node\AxisDomNodeSitemap;
use com\axisgraphics\axis\http\controller\AxisHttpControllerBase;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\dom\node\exception\DomNodeException;
use com\soloproyectos\common\http\request\HttpRequestConfig;
use com\soloproyectos\common\http\request\HttpRequestGet;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpController.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
abstract class AxisHttpController extends AxisHttpControllerBase
{
    /**
     * Sitemap object.
     * @var AxisDomNodeSitemap
     */
    protected $sitemap;

    /**
     * Configuration file.
     * @var DomNode
     */
    protected $config;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onAxisStartRequest"));
    }

    /**
     * Handles 'start' events.
     *
     * @return void
     */
    public final function onAxisStartRequest()
    {
        // parameters
        $forzeCompilation = $this->request->get("compile-sitemap");

        // is any user logged?
        $this->user = $this->system->getUser();
        if ($this->user === null) {
            throw new AxisExceptionExpiredSession("Your session has expired");
        }

        // does the SITEMAP exist?
        if (!is_file(SITEMAP)) {
            throw new AxisException(
                "The SITEMAP constant contains an invalid filename.\n" .
                "Please check the general.php config file"
            );
        }

        // gets the compiled sitemap (one per session)
        $compiledSitemap = $this->session->get("COMPILED_SITEMAP");
        if (TextHelper::isEmpty($compiledSitemap)) {
            $compiledSitemap = tempnam(TMP_DIR, "cms-");
            $this->session->set("COMPILED_SITEMAP", $compiledSitemap);
        }

        // loads the sitemap
        $contents = "";
        $sitemapHasChanged = !$this->cookies->is("sitemap-last-change")
            || $this->cookies->get("sitemap-last-change") < date("Y-m-d H:i:s", filemtime(SITEMAP));
        $recompile = $forzeCompilation == "true" || $sitemapHasChanged
            || !is_file($compiledSitemap) || filesize($compiledSitemap) == 0;
        if ($recompile) {
            $contents = $this->_getSitemapContents();
            file_put_contents($compiledSitemap, $contents);
        } else {
            $contents = file_get_contents($compiledSitemap);
        }

        // loads and validates the sitemap
        try {
            $this->sitemap = AxisDomNodeSitemap::createFromString($contents);
        } catch (DomNodeException $e) {
            throw new DomNodeException(
                "The sitemap is not well formed. Probably it could not be compiled successfully.\n" .
                "Please visit the following link to see the errors:\n\n" .
                AxisHelper::getCmsUrl() . "/sitemap.php\n\n"
                . $e->getMessage(),
                $e->getCode(),
                $e
            );
            die();
        }

        // checks for errors
        $error = $this->sitemap->query("> error > message")->text();
        if (!TextHelper::isEmpty($error)) {
            throw new AxisException($error);
        }

        // validates the sitemap against an XSD schema
        if ($recompile) {
            $this->sitemap->validateXsd(SCHEMA);
        }

        // loads the config file
        $this->config = $this->_loadConfigFile();

        // saves the last time the sitemap was changed
        if ($recompile) {
            $this->cookies->set("sitemap-last-change", date("Y-m-d H:i:s", filemtime(SITEMAP)));
        }
    }

    /**
     * Loads the configuration file.
     *
     * @return void
     */
    private function _loadConfigFile()
    {
        $ret = new DomNode();
        $configPath = "config.xml";
        $xsdPath = "config.xsd";

        if (is_file($configPath)) {
            $configContents = file_get_contents($configPath);
            $ret = DomNode::createFromString($configContents);

            // validates the config file against the XSD file
            if (is_file($xsdPath)) {
                $useInternalErrors = libxml_use_internal_errors(true);
                $doc = $ret->document();
                $isValidDocument = @$doc->schemaValidate($xsdPath);

                // retrieves the errors
                $text = "";
                $errors = libxml_get_errors();
                foreach ($errors as $error) {
                    $message = trim($error->message);
                    $text = TextHelper::concat(
                        "\n", $text, "$message on line {$error->line}, column {$error->column}"
                    );
                }
                libxml_clear_errors();

                libxml_use_internal_errors($useInternalErrors);

                if (!$isValidDocument) {
                    throw new AxisException($text);
                }
            }
        }

        return $ret;
    }

    /**
     * Gets the sitemap contents.
     *
     * @return string
     */
    private function _getSitemapContents()
    {
        list($selector, $token) = $this->system->getCredentials();
        $url = AxisHelper::getCmsUrl() . "/sitemap.php";

        // sets credentials
        $config = new HttpRequestConfig();
        $config->setHeaderKey("Cookie", "token=$selector:$token");

        // sends a GET request and returns the contents
        $req = new HttpRequestGet($config);
        return $req->send($url);
    }
}
