<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;
use com\soloproyectos\common\media\image\MediaImageGroup;
use com\soloproyectos\common\media\image\MediaImagePicture;

/**
 * Class AxisHttpControllerImageEditor.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerImageEditor extends AxisHttpController
{
    /**
     * User interface width.
     * @var integer
     */
    private $_width = 0;

    /**
     * User interface height.
     * @var integer
     */
    private $_height = 0;
    
    /**
     * Watermark.
     * @var array [
     *      text: string,
     *      opacity: number between 0 and 1,
     *      font: string,
     *      font: integer,
     *      color:string,
     *      halign: string (left, center, right),
     *      valign: string (top, center, bottom),
     *      hpadding: number|string (positive number or percentage),
     *      vpadding: number|string (positive number or percentage)
     * ]
     */
    private $_watermark = [
        "text" => "",
        "opacity" => 1,
        "font" => "",
        "size" => 24,
        "color" => "black",
        "halign" => "left",
        "valign" => "top",
        "hpadding" => 0,
        "vpadding" => 0
    ];

    /**
     * Background color.
     * @var string
     */
    private $_bgColor = "";

    /**
     * Upload directory.
     * @var string
     */
    private $_uploadDir = "";
    
    /**
     * Image path.
     * @var string
     */
    private $_src = "";

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * This function is called at the beginning of the request.
     *
     * @return void
     */
    public function onStart()
    {
        $config = $this->sitemap->getConfig();
        $interface = $config->query("editors > image");
        $this->_width = intval($interface->query("width")->text());
        $this->_height = intval($interface->query("height")->text());
        $this->_bgColor = TextHelper::ifEmpty(
            $interface->query("background-color")->text(), "transparent"
        );
        $this->_uploadDir = $config->query("directories > image")->text();
        
        // parses the watermark node
        if (count($interface->query("watermark")) > 0) {
            $this->_parseWatermark();
        }
    }

    /**
     * Processes the POST request.
     *
     * @return void
     */
    public function onPost()
    {
        // parameters
        $src = $this->request->get("src");
        $x = floatval($this->request->get("x"));
        $y = floatval($this->request->get("y"));
        $width = floatval($this->request->get("width"));
        $height = floatval($this->request->get("height"));
        $cropWidth = floatval($this->request->get("crop_width"));
        $cropHeight = floatval($this->request->get("crop_height"));
        $rotation = floatval($this->request->get("rotation"));
        $preserveImage = $this->request->get("preserve_image") == "true";
        $preserveTransparency = $this->request->get("preserve_transparency") == "true";
        $bgColor = $preserveTransparency? "transparent" : $this->_bgColor;
        
        // required fields
        if (TextHelper::isEmpty($src)) {
            throw new AxisExceptionAlert("The following fields are required: src");
        }
        
        // input / output
        $input = AxisHelper::getAbsPath($src);
        $output = $input;
        if ($preserveImage) {
            $output = SysFileHelper::getAvailName(
                AxisHelper::getAbsPath($this->_uploadDir), $src
            );
        }
        
        // checks input
        if (!is_file($input)) {
            throw new AxisExceptionAlert("File not found: $src");
        }

        // applies transformations to the image
        $group = new MediaImageGroup();
        $group->setBgColor($bgColor);
        $group->setPosition($cropWidth / 2, $cropHeight / 2);

        $img = new MediaImagePicture($input);
        $img->setOffset($img->getWidth() / 2, $img->getHeight() / 2);
        $img->setPosition($x, $y);
        $img->setSize($width, $height);
        $img->setRotation($rotation);
        $group->add($img);

        // save image
        $group->save($output, $cropWidth, $cropHeight);
        $this->_src = AxisHelper::getRelPath($output);
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode("width", $this->_width));
                $target->append(new DomNode("height", $this->_height));
                $target->append(new DomNode("directory", $this->_uploadDir));
                $target->append(new DomNode("src", $this->_src));
                $target->append(
                    new DomNode(
                        "watermark",
                        function ($target) {
                            $target->append(new DomNode("text", $this->_watermark["text"]));
                            $target->append(new DomNode("opacity", $this->_watermark["opacity"]));
                            $target->append(new DomNode("font-file", $this->_watermark["font"]));
                            $target->append(new DomNode("font-size", $this->_watermark["size"]));
                            $target->append(new DomNode("color", $this->_watermark["color"]));
                            $target->append(
                                new DomNode(
                                    "align",
                                    function ($target) {
                                        $target->append(
                                            new DomNode("horizontal", $this->_watermark["halign"])
                                        );
                                        $target->append(
                                            new DomNode("vertical", $this->_watermark["valign"])
                                        );
                                    }
                                )
                            );
                            $target->append(
                                new DomNode(
                                    "padding",
                                    function ($target) {
                                        $target->append(
                                            new DomNode("horizontal", $this->_watermark["hpadding"])
                                        );
                                        $target->append(
                                            new DomNode("vertical", $this->_watermark["vpadding"])
                                        );
                                    }
                                )
                            );
                        }
                    )
                );
            }
        );

        return $root->html();
    }
    
    /**
     * Parses 'watermark' node.
     * 
     * @return void
     */
    private function _parseWatermark()
    {
        $config = $this->sitemap->getConfig();
        $watermark = $config->query("editors > image > watermark");
        $this->_watermark["text"] = $watermark->query("> text")->text();
        $this->_watermark["opacity"] = floatval($watermark->query("> opacity")->text());
        $this->_watermark["font"] = $watermark->query("> font-file")->text();
        $this->_watermark["size"] = intval($watermark->query("> font-size")->text());
        $this->_watermark["color"] = $watermark->query("> color")->text();
        
        // parses horizontal align
        $this->_watermark["halign"] = $watermark->query("> align > horizontal")->text();
        if (!in_array($this->_watermark["halign"], ["left", "center", "right"])) {
            throw new AxisExceptionAlert(
                "Watermark: Horizontal align must be 'left', 'center' or 'right'"
            );
        }
        
        // parses vertical align
        $this->_watermark["valign"] = $watermark->query("> align > vertical")->text();
        if (!in_array($this->_watermark["valign"], ["top", "center", "bottom"])) {
            throw new AxisExceptionAlert(
                "Watermark: Vertical align must be 'top', 'center' or 'bottom'"
            );
        }
        
        // parses horizontal padding
        $this->_watermark["hpadding"] = $watermark->query("> padding > horizontal")->text();
        if (!is_numeric($this->_watermark["hpadding"])
            && !preg_match("/^\d+%$/", $this->_watermark["hpadding"])
        ) {
            throw new AxisExceptionAlert(
                "Watermark: padding must be a positive number or a percentage"
            );
        }
        
        // parses vertical padding
        $this->_watermark["vpadding"] = $watermark->query("> padding > vertical")->text();
        if (!is_numeric($this->_watermark["vpadding"])
            && !preg_match("/^\d+%$/", $this->_watermark["vpadding"])
        ) {
            throw new AxisExceptionAlert(
                "Watermark: padding must be a positive number or a percentage"
            );
        }
    }
}
