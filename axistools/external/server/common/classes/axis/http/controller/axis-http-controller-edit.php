<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\db\AxisDbRecord;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\axisgraphics\axis\http\controller\AxisHttpControllerSublist;
use com\axisgraphics\axis\text\parser\AxisTextParserList;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\Db;

/**
 * Class AxisHttpControllerEdit.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerEdit extends AxisHttpController
{
    /**
     * Files directory.
     * @var string
     */
    private $_filesDir = "";

    /**
     * Images directory.
     * @var string
     */
    private $_imagesDir;

    /**
     * Videos directory.
     * @var string
     */
    private $_videosDir;
    
    /**
     * List of available servers.
     * @var Traversable
     */
    private $_servers = null;

    /**
     * List node.
     * @var AxisDomNodeList
     */
    private $_list;
    
    /**
     * Are insertions allowed?
     * @var boolean
     */
    private $_isInsertable;
    
    /**
     * Are editions allowed?
     * @var boolean
     */
    private $_isEditable;

    /**
     * Table id.
     * @var string
     */
    private $_id = "";
    
    /**
     * User defined filter.
     * @var array
     */
    private $_filter = [];

    /**
     * Current row.
     * @var AxisDbRecord
     */
    private $_currentRow;

    /**
     * List of sublist controllers.
     * @var array of AxisHttpControllerSublist
     */
    private $_sublistControllers = array();

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * This function is called at the beginning of the request.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $sectionId = $this->request->get("section");
        $tablePos = $this->request->get("table");
        $this->_id = $this->request->get("id");
        $this->_filter = json_decode($this->request->get("filter"), true);

        // config
        $config = $this->sitemap->getConfig();
        $directories = $config->query("directories");
        $this->_filesDir = $directories->query("file")->text();
        $this->_imagesDir = $directories->query("image")->text();
        $this->_videosDir = $directories->query("video")->text();
        $this->_servers = $config->query("> servers");

        // gets the section
        $section = $this->sitemap->getSectionById($sectionId);
        if (count($section) == 0 || $section->isAdmin() && !$this->user->isAdmin()) {
            throw new AxisException("Section not found: $sectionId");
        }

        // gets the table
        $this->_list = $section->getListByIndex($tablePos);
        if (count($this->_list) == 0) {
            throw new AxisException("Table position not found: $tablePos");
        }

        $this->_update();

        // sublists controllers
        $this->_sublistControllers = array();
        $sublists = $this->_list->query("sublist");
        foreach ($sublists as $i => $sublist) {
            $c = new AxisHttpControllerSublist($this->_currentRow, $sectionId, $tablePos, $i);
            array_push($this->_sublistControllers, $c);
        }
        
        // list attributes
        $this->_isInsertable = $this->_list->attr("allow-insert") != "false";
        $this->_isEditable = $this->_list->attr("allow-edit") != "false";
    }

    /**
     * This function is called on POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        // are insertions allowed?
        if (TextHelper::isEmpty($this->_id) && !$this->_isInsertable) {
            throw new AxisException("Insertions are not allowed");
        }
        
        // are editions allowed?
        if (!TextHelper::isEmpty($this->_id) && !$this->_isEditable) {
            throw new AxisException("Editions are not allowed");
        }
        
        // parameters
        $values = $this->request->get("values", []);
        $sublistValues = $this->request->get("sublistValues");
        $server = $this->request->get("server");
        
        // sets keys
        $keys = $this->_list->getKeys();
        foreach ($keys as $key) {
            $colName = $key->attr("column");
            $value = $key->attr("value");
            $this->_currentRow->{$colName} = $value;
        }

        // sets column values
        $fields = $this->_list->getEditableFields();
        foreach ($fields as $i => $field) {
            // exclude 'readonly' fields
            if ($field->attr("readonly") == "true") {
                continue;
            }
            
            // exclude missing fields (except for required fields)
            if (!array_key_exists($i, $values) && $field->attr("required") != "true") {
                continue;
            }
            
            // required fields
            if (!array_key_exists($i, $values)) {
                throw new AxisException("`{$field->attr("title")}` is required.");
            }
            
            // fills columns
            $columns = $field->getColumns();
            foreach ($columns as $column) {
                $column->setOriginalValue($this->_currentRow->{$column->attr("column")});
            }

            $field->setEditObject(json_decode($values[$i]));
            $columns = $field->getColumns();
            foreach ($columns as $column) {
                $value = TextHelper::ifEmpty($column->getValue(), null);
                $this->_currentRow->{$column->attr("column")} = $value;
            }
        }
        
        // sets ord
        $tables = array_merge([$this->_currentRow], $this->_getLeftJoinTables($this->_currentRow));
        foreach ($tables as $table) {
            if (TextHelper::isEmpty($table->id) && $this->_hasColumn($table, "ord")) {
                $table->ord = $this->_getNextOrd($table);
            }
        }
        
        // sets row_state
        if ($this->_hasColumn($this->_currentRow, "row_state")) {
            $this->_currentRow->row_state = $server;
        }
        
        // sets 'created_on' and 'updated_on' fields
        $currentDate = date("Y-m-d H:i:s");
        if (TextHelper::isEmpty($this->_id) && $this->_hasColumn($this->_currentRow, "created_on")) {
            $this->_currentRow->created_on = $currentDate;
        }
        if ($this->_hasColumn($this->_currentRow, "updated_on")) {
            $this->_currentRow->updated_on = $currentDate;
        }

        // inserts or updates the record
        $this->_currentRow->save();
        $this->_id = $this->_currentRow->id;

        // updates sublists
        foreach ($this->_sublistControllers as $i => $controller) {
            $rowValues = json_decode($sublistValues[$i]);
            $controller->save($rowValues, $this->_currentRow);
        }
        
        $this->_update();
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $fields = $this->_list->getEditableFields();

                $target->append(new DomNode('id', $this->_id));
                $target->append(new DomNode('title', $this->_list->attr("title")));
                $target->append(new DomNode('refresh', $this->_list->attr("refresh")));
                $target->append(
                    new DomNode(
                        'directories',
                        function ($target) {
                            $target->append(new DomNode('file', $this->_filesDir));
                            $target->append(new DomNode('image', $this->_imagesDir));
                            $target->append(new DomNode('video', $this->_videosDir));
                        }
                    )
                );
                
                // appends servers
                if ($this->_hasColumn($this->_currentRow, "row_state")) {
                    $value = TextHelper::ifEmpty(
                        $this->_currentRow->{"row_state"},
                        $this->_servers->query("option:first-child")->attr("value")
                    );
                    
                    $target->append(
                        new DomNode(
                            "servers",
                            ["value" => $value],
                            function ($target) {
                                $options = $this->_servers->query("> option");
                                foreach ($options as $option) {
                                    $target->append($option);
                                }
                            }
                        )
                    );
                }
                
                // triggers
                $target->append(
                    new DomNode(
                        "triggers",
                        function ($target) {
                            $target->append(
                                new DomNode("before-post", trim($this->_list->attr("before-post")))
                            );
                            $target->append(
                                new DomNode("after-post", trim($this->_list->attr("after-post")))
                            );
                        }
                    )
                );

                // fields
                $target->append(
                    new DomNode(
                        "fields",
                        function ($target) use ($fields) {
                            foreach ($fields as $i => $field) {
                                // fills columns
                                $columns = $field->getColumns();
                                foreach ($columns as $column) {
                                    $columnValue = TextHelper::isEmpty($this->_id)
                                        ? $column->attr("default")
                                        : $this->_currentRow->{$column->attr("column")};
                                    $column->setOriginalValue($columnValue);
                                }
                                
                                // filters
                                if (TextHelper::isEmpty($this->_id) && isset($this->_filter[$i])) {
                                    $field->setEditObject(json_decode($this->_filter[$i]));
                                }

                                $value = $field->getEditObject();
                                $type = $field->name();

                                if ($field->attr("readonly") == "true") {
                                    $value = $field->getTableValue();
                                    $type = "readonly";
                                }
                                
                                // parses the 'field-values' attribute
                                $fieldValues = $field->attr("field-values");
                                $values = json_encode([]);
                                if (!TextHelper::isEmpty($fieldValues)) {
                                    $p = new AxisTextParserList($fieldValues);
                                    if (($items = $p->parse()) === false) {
                                        throw new AxisException("Invalid 'field-values' attribute:\n\n$field");
                                    }
                                    $values = json_encode($items);
                                }
                                
                                $target->append(
                                    new DomNode(
                                        'item',
                                        array(
                                            "name" => $field->attr("name"),
                                            "type" => $type,
                                            "title" => $field->attr("title"),
                                            "required" => $field->attr("required"),
                                            "filtrable" => $field->isFiltrable()? "true": "false",
                                            "value" => json_encode($value),
                                            "field-name" => $field->attr("field-name"),
                                            "field-values" => $values
                                        )
                                    )
                                );
                            }
                        }
                    )
                );

                // sublists
                foreach ($this->_sublistControllers as $controller) {
                    $target->append(
                        new DomNode(
                            'sublist',
                            function ($target) use ($controller) {
                                $target->append($controller->getBody());
                            }
                        )
                    );
                }
            }
        );

        return $root->html();
    }
    
    /**
     * Updates the class properties.
     * 
     * @return void
     */     
    private function _update()
    {
        // current row
        $this->_currentRow = new AxisDbRecord(
            $this->database, $this->_list->attr("table"), $this->_id
        );
    }

    /**
     * Gets the current row.
     *
     * @return AxisDbRecord
     */
    private function _getCurrentRow()
    {
        return new AxisDbRecord($this->database, $this->_list->attr("table"), $this->_id);
    }

    /**
     * Gets the next `ord` value.
     * 
     * @param AxisDbRecord $row Record
     *
     * @return integer
     */
    private function _getNextOrd($row)
    {
        $row = $this->database->query(
            "select max(ord) + 1 from " . Db::quoteId($row->getTableName())
        );
        return intval($row[0]);
    }
    
    /**
     * Has the table a given column?
     * 
     * @param AxisDbRecord $row     Record
     * @param string         $colName Column name
     * 
     * @return boolean
     */
    private function _hasColumn($row, $colName)
    {
        $tableName = $row->getTableName();
        $r = $this->database->query(
            "show columns from $tableName like " . $this->database->quote($colName)
        );
        return count($r) > 0;
    }
    
    /**
     * Gets the list of 'left join' tables of a given record.
     * 
     * @return AxisDbRecord[]
     */
    private function _getLeftJoinTables($record)
    {
        $ret = [];
        $leftJoins = $record->getLeftJoins();
        foreach ($leftJoins as $leftJoin) {
            $r = $leftJoin->getRecord();
            $ret = array_merge($ret, [$r], $this->_getLeftJoinTables($r));
        }
        return $ret;
    }
}
