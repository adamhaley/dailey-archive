<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller\exception;
use com\axisgraphics\axis\http\exception\AxisHttpException;

/**
 * Class AxisHttpControllerException.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Http\Controller\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerException extends AxisHttpException
{

}
