<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\sys\ffmpeg\SysFfmpeg;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerVideoEditorConverter.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerVideoEditorConverter extends AxisHttpController
{
    /**
     * Video folder.
     * @var string
     */
    private $_videoFolder = '';

    /**
     * Available presets.
     * @var array
     */
    private $_presets = array();

    /**
     * Output video file.
     * @var string
     */
    private $_output = "";

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles 'start' requests.
     *
     * @return void
     */
    public function onStart()
    {
        $config = $this->sitemap->getConfig();
        $interface = $config->query("editors > video");
        $this->_videoFolder = AxisHelper::getAbsPath($config->query("directories video")->text());
        $this->_presets = $interface->query("presets item");
    }

    /**
     * Handles 'post' requests.
     *
     * @return void
     */
    public function onPost()
    {
        // parameters
        $src = $this->request->get("src");
        $presetName = $this->request->get("preset");
        $width = $this->request->get("width");
        $height = $this->request->get("height");

        // required parameters
        if (   TextHelper::isEmpty($src)
            || TextHelper::isEmpty($presetName)
            || TextHelper::isEmpty($width)
            || TextHelper::isEmpty($height)
        ) {
            throw new AxisExceptionAlert(
                "The parameter 'src', 'preset', 'width' and 'height' are required"
            );
        }

        // searches the preset
        $preset = $this->_searchPreset($presetName);
        if ($preset == null) {
            throw new AxisExceptionAlert("Preset not found: $presetName");
        }

        // checks parameters
        $input = AxisHelper::getAbsPath($src);
        if (!is_file($input)) {
            throw new AxisExceptionAlert("File not found: $src");
        }

        // converts video
        $extension = $preset->query("extension")->text();
        $args = $preset->query("ffmpeg-args")->text();
        $output = SysFileHelper::getAvailName($this->_videoFolder, $input, $extension);
        $ffmpeg = new SysFfmpeg($input, $output);
        $ffmpeg->setArguments($args);
        $ffmpeg->scaleInside($width, $height);
        $ffmpeg->exec();

        $this->_output = AxisHelper::getRelPath($output);
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $xml = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode('video', $this->_output));
            }
        );

        return $xml->html();
    }

    /**
     * Searches a preset by name.
     *
     * @param string $name Preset name
     *
     * @return DomNode|null
     */
    private function _searchPreset($name)
    {
        $ret = null;

        foreach ($this->_presets as $preset) {
            if ($preset->attr("name") == $name) {
                $ret = $preset;
                break;
            }
        }

        return $ret;
    }
}
