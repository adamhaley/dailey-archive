<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\sys\mail\SysMail;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerList.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerBugReport extends AxisHttpController
{
    /**
     * Message.
     * @var string
     */
    private $_message = "";

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        $summary = $this->request->get("summary");
        $description = $this->request->get("description");

        if (TextHelper::isEmpty($description)) {
            throw new AxisExceptionAlert("Description is required");
        }

        // sends an email to technical team
        $fullname = $this->user->getFullName();
        $email = TextHelper::isEmpty($fullname)
            ? $this->user->getEmail()
            : "$fullname <{$this->user->getEmail()}>";
        $mail = new SysMail();
        $mail->setFrom($email);
        $mail->addTo(BUG_REPORT_MAIL);
        try {
            $mail->send(
                "Axistool Bug Report",
                "Site: {$_SERVER["HTTP_HOST"]}\n" .
                "User name: {$this->user->getFullName()}\n" .
                "Summary: $summary\n\n" .
                "$description"
            );
        } catch (SysMailException $e) {
            throw new AxisExceptionAlert(
                "The email could not be sent.\nPlease contact the technical support."
            );
        }

        $this->_message = "The incidence has been reported successfully. Thank you.";
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    protected function getBody()
    {
        $root = new DomNode(
            "root",
            function ($target) {
                $target->append(new DomNode("message", $this->_message));
            }
        );

        return $root->html();
    }
}
