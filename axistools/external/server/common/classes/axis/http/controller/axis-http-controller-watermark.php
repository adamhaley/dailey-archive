<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerHome.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerWatermark extends AxisHttpController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Processes the POST request.
     *
     * @return void
     */
    public function onPost()
    {
        // parameters
        $src = AxisHelper::getAbsPath($this->request->get("src"));
        $text = $this->request->get("text");
        $x = $this->request->get("x");
        $y = $this->request->get("y");
        $font = $this->request->get("font");
        $size = $this->request->get("size");
        $color = $this->request->get("color");
        $opacity = $this->request->get("opacity");
        
        // required parameters
        if (   TextHelper::isEmpty($src)
            || TextHelper::isEmpty($text)
            || TextHelper::isEmpty($x)
            || TextHelper::isEmpty($y)
            || TextHelper::isEmpty($font)
            || TextHelper::isEmpty($size)
            || TextHelper::isEmpty($color)
            || TextHelper::isEmpty($opacity)
        ) {
            throw new AxisExceptionAlert(
                "The following fields are required: src, text, x, y, font, size, color, opacity"
            );
        }
        
        // checks image parameter
        if (!is_file($src)) {
            throw new AxisExceptionAlert("Image not found");
        }
        
        // checks coordinates parameters
        if (!is_numeric($x) || !is_numeric($y)) {
            throw new AxisExceptionAlert("Invalid coordinates");
        }
        $x = floatval($x);
        $y = floatval($y);
        
        // checks font parameter
        $source = AxisHelper::getAbsPath($font);
        if (!is_file($source)) {
            throw new AxisExceptionAlert("Font not found: $font");
        }
        $font = $source;
        
        // checks size parameter
        if (!(is_numeric($size) && !is_double(1 * $size)) || ($size < 1)) {
            throw new AxisExceptionAlert("Invalid font size");
        }
        $size = intval($size);
        
        // checks color
        if (!preg_match('/^\#[0-9a-f]{6}$/i', $color)) {
            throw new AxisExceptionAlert("Invalid color");
        }
        $color = hexdec(ltrim($color, "#"));
        
        // checks opacity
        if (!is_numeric($opacity) || $opacity < 0 || $opacity > 1) {
            throw new AxisExceptionAlert("Invalid opacity");
        }
        
        // prints the watermark
        $data = file_get_contents($src);
        $img = imagecreatefromstring($data);
        $img = $this->_printText($img, $text, $x, $y, $font, $size, $color, $opacity);
        
        // saves the image
        $this->_saveImage($img, $src);
        imagedestroy($img);
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        return "";
    }
    
    /**
     * Saves the image depending on the type.
     * 
     * @param string $src Image path
     * 
     * @return void
     */
    private function _saveImage($img, $src) {
        $info = getimagesize($src);
        $mimeType = $info["mime"];
        if ($mimeType == "image/jpeg") {
            imagejpeg($img, $src, 90);
        } elseif ($mimeType == "image/png") {
            imagepng($img, $src, 9);
        } elseif ($mimeType == "image/gif") {
            imagegif($img, $src);
        }
    }
    
    /**
     * Prints text on an image.
     * 
     * @param resource $img     Image
     * @param string   $text    Text
     * @param float    $x       X coordinate
     * @param float    $y       Y coordinate
     * @param string   $font    Path to font
     * @param integer  $size    Font size in pixels
     * @param string   $color   Hexadecimal color
     * @param float    $opacity Opacity (0..1)
     * 
     * @return void
     */
    private function _printText($img, $text, $x, $y, $font, $size, $color, $opacity) {
        // image size
        $width = imagesx($img);
        $height = imagesy($img);
        
        // allocates color
        $red = ($color & 0xFF0000) >> 16;
        $green = ($color & 0x00FF00) >> 8;
        $blue = $color & 0x0000FF;
        $alpha = 127 * (1 - $opacity);
        $color = imagecolorallocatealpha($img, $red, $green, $blue, $alpha);
        
        // converts pixels to points  (pixels = 3/4 * points + 1, approx)
        $points = round(3 * $size / 2) / 2 + 1;
        
        // prints the text
        $box = imagettfbbox($size, 0, $font, $text);
        $lowerLeftCorner = array($box[0], $box[1]);
        $lowerRightCorner = array($box[2], $box[3]);
        $topRightCorner = array($box[4], $box[5]);
        $topLeftCorner = array($box[6], $box[7]);
        $x = $x - $lowerLeftCorner[0];
        $y = $y - $topRightCorner[1];
        imagettftext($img, $points, 0, $x, $y, $color, $font, $text);
        
        return $img;
    }
}
