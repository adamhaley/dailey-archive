<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\db\AxisDbRecord;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\field\AxisFieldSelect;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\DomHelper;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\Db;
use soloproyectos\db\record\DbRecordTable;

/**
 * Class AxisHttpControllerSublist.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerSublist extends AxisHttpController
{
    /**
     * Sublist node.
     * @var AxisXmlQuerySublist
     */
    private $_sublist;

    /**
     * The record to which this sublist is attached.
     * @var AxisDbRecord
     */
    private $_parentRow;

    /**
     * Section identifier.
     * @var string
     */
    private $_sectionId = "";

    /**
     * Table position.
     * @var string
     */
    private $_tablePos = "";

    /**
     * Sublist position.
     * @var string
     */
    private $_sublistPos = "";

    /**
     * Constructor.
     *
     * @param AxisDbRecord $parentRow  The record to which this sublist is attached (oh yeah)
     * @param string         $sectionId  Section identifier
     * @param string         $tablePos   Table position
     * @param string         $sublistPos Sublist position
     */
    public function __construct($parentRow = null, $sectionId = "", $tablePos = "", $sublistPos = "")
    {
        parent::__construct();
        $this->_parentRow = $parentRow;
        $this->_sectionId = $sectionId;
        $this->_tablePos = $tablePos;
        $this->_sublistPos = $sublistPos;

        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * This function is called at the beginning of the request.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $sectionId = TextHelper::ifEmpty($this->_sectionId, $this->request->get("section"));
        $tablePos = intval(TextHelper::ifEmpty($this->_tablePos, $this->request->get("table")));
        $sublistPos = intval(TextHelper::ifEmpty($this->_sublistPos, $this->request->get("sublist")));

        // gets the section
        $section = $this->sitemap->getSectionById($sectionId);
        if (count($section) == 0 || $section->isAdmin() && !$this->user->isAdmin()) {
            throw new AxisException("Section not found: $sectionId");
        }

        // gets the table
        $table = $section->getListByIndex($tablePos);
        if (count($table) == 0) {
            throw new AxisException("Table position not found: $tablePos");
        }

        // gets the sublist
        $this->_sublist = $table->getSublistByIndex($sublistPos);
        if (count($this->_sublist) == 0) {
            throw new AxisException("Sublist position not found: $sublistPos");
        }
    }

    /**
     * Saves the new values into the table.
     *
     * @param array $rowValues Two-dimensional array of string
     * @param array $parentRow Parent row
     *
     * @return void
     */
    public function save($rowValues, $parentRow)
    {
        $rows = $this->_getRows($parentRow);
        
        // deletes all rows
        foreach ($rows as $row) {
            $row->delete();
        }
        
        // inserts new records
        $t = new DbRecordTable($this->database, $this->_sublist->attr("table"));
        foreach ($rowValues as $rowValue) {
            // sets field values
            $values = [];
            $fields = $this->_sublist->getFields();
            foreach ($fields as $i => $field) {
                $values[$field->attr("column")] = $rowValue[$i];
            }
            
            // sets constraints
            $keys = $this->_sublist->getKeys();
            foreach ($keys as $key) {
                $colName = $key->attr("column");
                $value = $key->attr("value");
                $values[$colName] = $this->_replaceVar($value, $parentRow);
            }
            
            $t->insert($values);
        }
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $fields = $this->_sublist->getFields();
                $target->append(new DomNode('title', $this->_sublist->attr('title')));

                // columns
                foreach ($fields as $field) {
                    $options = [];
                    if ($field instanceof AxisFieldSelect) {
                        $options = $field->getOptions();
                    }

                    $target->append(
                        new DomNode(
                            'column',
                            array(
                                "type" => $field->name(),
                                "title" => $field->attr("title"),
                                "required" => $field->attr("required"),
                                "default" => $field->attr("default"),
                                "options" => json_encode($options)
                            )
                        )
                    );
                }

                // rows
                $rows = $this->_getRows();
                foreach ($rows as $row) {
                    $target->append(
                        new DomNode(
                            'row',
                            function ($target) use ($fields, $row) {
                                foreach ($fields as $field) {
                                    // fills columns
                                    $columns = $field->getColumns();
                                    foreach ($columns as $column) {
                                        $column->setOriginalValue($row->{$column->attr("column")});
                                    }

                                    $item = new DomNode('item');
                                    $item->append(DomHelper::cdata($field->getValue()));
                                    $target->append($item);
                                }
                            }
                        )
                    );
                }
            }
        );

        return $root->html();
    }

    /**
     * Gets the rows of the list.
     *
     * @param AxisDbRecord $parentRow Parent row
     *
     * @return AxisDbRecord[]
     */
    private function _getRows($parentRow = null)
    {
        $ret = [];
        
        if ($parentRow == null) {
            $parentRow = $this->_parentRow;
        }
        
        // get records
        $fields = $this->_sublist->getFields();
        $tableName = $this->_sublist->attr("table");
        $rows = iterator_to_array($this->database->query($this->_getSelectStatement($parentRow)));
        foreach ($rows as $row) {
            array_push($ret, new AxisDbRecord($this->database, $tableName, $row["id"]));
        }
        
        // sorts records by id
        usort(
            $ret,
            function ($row1, $row2) {
                return strnatcasecmp($row1->id, $row2->id);
            }
        );
        
        // filters records
        $filters = $this->_sublist->getFilters();
        foreach ($filters as $filter) {
            $column = $filter->attr("column");
            $operator = TextHelper::ifEmpty($filter->attr("operator"), "eq");
            $value = $this->_replaceVar($filter->attr("value"), $parentRow);
            $f = AxisDbFilterFactory::getInstance($operator);
            $ret = $f->filter($ret, $column, $value);
        }
        
        return $ret;
    }
    
    /**
     * Replaces variables.
     * 
     * For example, the following expression:
     * section_id = [id]
     * 
     * is replaced by:
     * section_id = {$parentRow->id}
     * 
     * @param string         $str       String
     * @param AxisDbRecord $parentRow Parent row
     * @param callable       $quoter    Quoter function (not required)
     * 
     * @return string
     */
    private function _replaceValues($str, $parentRow, $quoter = null)
    {
        return preg_replace_callback(
            '/\[(.*)\]/U',
            function ($matches) use ($parentRow, $quoter) {
                $columnName = $matches[1];
                $value = $parentRow->{$columnName};
                return $quoter !== null? $quoter($value): $value;
            },
            $str
        );
    }
    
    /**
     * Replaces vars (if necessary).
     * 
     * @param string         $value     Value
     * @param AxisDbRecord $parentRow Parent row
     * 
     * @return string
     */
    private function _replaceVar($value, $parentRow)
    {
        return TextHelper::ifEmpty(
            preg_replace_callback(
                '/^\[([^]]*)]$/',
                function ($matches) use ($parentRow) {
                    $colName = $matches[1];
                    return $parentRow->{$colName};
                },
                $value
            ),
            null
        );
    }
    
    /**
     * Gets the SELECT statement.
     * 
     * @param AxisDbRecord $parentRow Parent record
     * 
     * @return string
     */
    private function _getSelectStatement($parentRow)
    {
        $sqlWhere = "";
        $keys = $this->_sublist->getKeys();
        if (count($keys) > 0) {
            $sqlWhere = "\nwhere " . implode(
                "\nand ",
                array_map(
                    function ($key) use ($parentRow) {
                        $colName = $key->attr("column");
                        $value = $this->_replaceVar($key->attr("value"), $parentRow);
                        return Db::quoteId($colName) . " = " . $this->database->quote($value);
                    },
                    $keys
                )
            );
        }
        
        return "select id from " . Db::quoteId($this->_sublist->attr("table")) . $sqlWhere;
    }
}
