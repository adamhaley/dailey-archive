<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerHome.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerTextareaEditor extends AxisHttpController
{
    /**
     * User interface width
     * @var integer
     */
    private $_width = 0;

    /**
     * User interface height.
     * @var integer
     */
    private $_height = 0;

    /**
     * Available fonts.
     * @var DomNode
     */
    private $_fonts = array();

    /**
     * Available font sizes.
     * @var DomNode
     */
    private $_sizes = array();

    /**
     * Font family.
     * @var string
     */
    private $_fontFamily = "";


    /**
     * Font size.
     * @var string
     */
    private $_fontSize = "";

    /**
     * Text color.
     * @var string
     */
    private $_color = "";
    
    /**
     * TinyMCE toolbar1 buttons.
     * @var string
     */
    private $_toolbar1 = "";
    
    /**
     * TinyMCE toolbar2 buttons.
     * @var string
     */
    private $_toolbar2 = "";

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * This function is called at the beginning of the request.
     *
     * @return void
     */
    public function onStart()
    {
        $config = $this->sitemap->getConfig();
        $config = $config->query("editors > textarea");

        // gets image size
        $this->_width = intval($config->query("width")->text());
        $this->_height = intval($config->query("height")->text());
        $this->_fontFamily = $config->query("font-family")->text();
        $this->_fontSize = $config->query("font-size")->text();
        $this->_color = $config->query("color")->text();
        $this->_fonts = $config->query("fonts > item");
        $this->_sizes = $config->query("sizes > item");
        $this->_toolbar1 = $config->query("buttons > toolbar1")->text();
        $this->_toolbar2 = $config->query("buttons > toolbar2")->text();
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode("width", $this->_width));
                $target->append(new DomNode("height", $this->_height));
                $target->append(new DomNode("font-family", $this->_fontFamily));
                $target->append(new DomNode("font-size", $this->_fontSize));
                $target->append(new DomNode("color", $this->_color));
                $target->append(
                    new DomNode(
                        "buttons",
                        function ($target) {
                            $target->append(
                                new DomNode("toolbar1", $this->_toolbar1)
                            );
                            $target->append(
                                new DomNode("toolbar2", $this->_toolbar2)
                            );
                        }
                    )
                );
                $target->append(
                    new DomNode(
                        "sizes",
                        function ($target) {
                            $target->append($this->_sizes);
                        }
                    )
                );
                $target->append(
                    new DomNode(
                        "fonts",
                        function ($target) {
                            $target->append($this->_fonts);
                        }
                    )
                );
            }
        );

        return $root->html();
    }
}
