<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;

/**
 * Class AxisHttpControllerTreeNavigator.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerTreeNavigator extends AxisHttpController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * This function is called at the beginning of the request.
     *
     * @return void
     */
    public function onStart()
    {
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append($this->_getMainMenu());
            }
        );

        return $root->html();
    }

    /**
     * Gets the main menu.
     *
     * @param DomNode $parent Parent node
     *
     * @return DomNode
     */
    private function _getMainMenu($parent = null)
    {
        $ret = new DomNode("menu");
        $sections = $parent == null ? $this->sitemap->getSections() : $parent->query("> section");

        foreach ($sections as $section) {
            $ret->append(
                new DomNode(
                    "item",
                    array(
                        "url" => "/" . $section->attr("id"),
                        "title" => $section->attr("title")
                    ),
                    function ($target) use ($section) {
                        $items = $this->_getMainMenu($section)->query("> item");
                        foreach ($items as $item) {
                            $target->append($item);
                        }
                    }
                )
            );
        }

        return $ret;
    }
}
