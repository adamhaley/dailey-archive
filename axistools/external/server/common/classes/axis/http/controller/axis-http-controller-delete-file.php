<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerDeleteFile.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerDeleteFile extends AxisHttpController
{
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        $files = $this->request->get("files");

        if (!is_array($files)) {
            $files = array($files);
        }

        $error = "";
        foreach ($files as $file) {
            $file = trim($file);
            $dir = dirname($file);

            if (!TextHelper::isEmpty($file)) {
                $filename = AxisHelper::getAbsPath($file);
                if (!is_file($filename)) {
                    $error = TextHelper::concat("\n", $error, "File not found: $file");
                    continue;
                }

                if (!@unlink($filename)) {
                    $info = error_get_last();
                    $message = array_key_exists("message", $info)
                        ? $info["message"]
                        : "Could not remove the file";
                    $error = TextHelper::concat("\n", $error, $message);
                }
            }
        }

        if (strlen($error) > 0) {
            throw new AxisExceptionAlert($error);
        }
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        return '';
    }
}
