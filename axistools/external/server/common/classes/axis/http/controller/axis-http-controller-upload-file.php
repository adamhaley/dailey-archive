<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\http\upload\HttpUpload;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerUploadFile.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerUploadFile extends AxisHttpController
{
    /**
     * Destination path.
     * @var string
     */
    private $_path = "";

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        $dir = $this->request->get("dir");

        // required parameters
        if (TextHelper::isEmpty($dir)) {
            throw new AxisExceptionAlert("'dir' is required");
        }
        
        $absDir = AxisHelper::getAbsPath($dir);
        if (!is_dir($absDir)) {
            throw new AxisExceptionAlert("Directory not found: $dir");
        }

        // moves the uploaded file
        $upload = new HttpUpload("file");
        $this->_path = AxisHelper::getRelPath($upload->move($absDir));
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $self = $this;
        $root = new DomNode(
            'root',
            function ($target) use ($self) {
                $target->append(new DomNode('path', $self->_path));
            }
        );

        return $root->html();
    }
}
