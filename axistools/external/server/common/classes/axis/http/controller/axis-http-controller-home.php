<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerHome.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerHome extends AxisHttpController
{
    /**
     * Number of tables.
     * @var integer
     */
    private $_numTables;

    /**
     * Homepage title.
     * @var string
     */
    private $_title;

    /**
     * Footer text.
     * @var string
     */
    private $_footerText;

    /**
     * Section notes.
     * @var string
     */
    private $_sectionNotes;

    /**
     * External module source.
     * @var string
     */
    private $_externalModuleSource = "";

    /**
     * Refresh menu?
     * @var boolean
     */
    private $_refreshMenu = false;

    /**
     * Help documentation.
     * @var string
     */
    private $_help = "";

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * Handles START requests.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $sectionId = $this->request->get("section");

        $config = $this->sitemap->getConfig();
        $this->_title = $config->query("> title")->text();
        $this->_footerText = $config->query("> footer-text")->text();
        $this->_help = $config->query("> help")->text();
        $this->_refreshMenu = $config->query("> refresh-sitemap")->text() != "false";

        // current section
        if (!TextHelper::isEmpty($sectionId)) {
            // section page
            $section = $this->sitemap->getSectionById($sectionId);
            if (count($section) == 0 || $section->isAdmin() && !$this->user->isAdmin()) {
                throw new AxisException("Section not found: $sectionId");
            }
            
            if ($section->hasAttr("refresh")) {
                $this->_refreshMenu = $section->attr("refresh") != "false";
            }

            $this->_externalModuleSource = $section->attr("external-module");
            $this->_numTables = $section->getNumLists();
            $this->_sectionNotes = $section->getNotes();
        } else {
            // home page
            $this->_numTables = 0;
            $this->_sectionNotes = $this->sitemap->getNotes();
        }
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    protected function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode("title", $this->_title));
                $target->append(new DomNode("footer-text", $this->_footerText));
                $target->append(new DomNode("help", $this->_help));
                $target->append(new DomNode("num_tables", $this->_numTables));
                $target->append(new DomNode("notes", $this->_sectionNotes));
                $target->append(new DomNode("refresh", $this->_refreshMenu? "true" : "false"));
                $target->append(new DomNode("external-module-source", $this->_externalModuleSource));
                $target->append(
                    new DomNode(
                        "background",
                        ["image" => BACKGROUND_IMAGE, "color" => BACKGROUND_COLOR]
                    )
                );
                $target->append(new DomNode("logo", ["image" => LOGO_IMAGE]));
                $target->append(
                    new DomNode(
                        "menu",
                        function ($target) {
                            $items = $this->_getMenuItems($this->sitemap);
                            foreach ($items as $item) {
                                $target->append($item);
                            }
                        }
                    )
                );
            }
        );

        return $root->html();
    }
    
    /**
     * Gets menu itmes.
     * 
     * @param DomNode $parent Parent node
     * 
     * @return array of DomNode
     */
    private function _getMenuItems($parent)
    {
        $ret = [];
        $sections = $parent->xpath("section");
        foreach ($sections as $section) {
            // discards non-authorized sections
            if ($section->attr("admin-access") == "true" && !$this->user->isAdmin()) {
                continue;
            }
            
            $item = new DomNode("item", [
                "id" => $section->attr("id"),
                "title" => $section->attr("title"),
                "align" => $section->attr("align"),
                "is_empty" => false
            ]);
            
            // appends subitems
            $subitems = $this->_getMenuItems($section);
            foreach ($subitems as $subitem) {
                $item->append($subitem);
            }
            
            // is the item empty?
            $isEmpty = count($subitems) == 0
                && strlen($section->attr("external-module")) == 0
                && count($section->xpath("list")) == 0;
            $item->attr("is_empty", $isEmpty? "true": "false");
            
            array_push($ret, $item);
        }
        return $ret;
    }
}
