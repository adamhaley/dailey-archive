<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;

/**
 * Class AxisHttpControllerVideoEditor.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerVideoEditor extends AxisHttpController
{
    /**
     * User interface width.
     * @var integer
     */
    private $_width = 0;

    /**
     * User interface height.
     * @var integer
     */
    private $_height = 0;

    /**
     * Video folder.
     * @var string
     */
    private $_uploadDir = '';

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * Handles 'start' requests.
     *
     * @return void
     */
    public function onStart()
    {
        $config = $this->sitemap->getConfig();
        $interface = $config->query("editors > video");
        $this->_width = intval($interface->query("width")->text());
        $this->_height = intval($interface->query("height")->text());
        $this->_uploadDir = $config->query("directories > video")->text();
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode('width', $this->_width));
                $target->append(new DomNode('height', $this->_height));
                $target->append(new DomNode('upload-dir', $this->_uploadDir));
            }
        );

        return $root->html();
    }
}
