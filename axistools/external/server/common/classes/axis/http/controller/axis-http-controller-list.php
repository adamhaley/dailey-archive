<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\db\filter\AxisDbFilterFactory;
use com\axisgraphics\axis\db\AxisDbRecord;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\DomHelper;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\http\request\HttpRequestConfig;
use com\soloproyectos\common\http\request\HttpRequestPost;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\Db;

/**
 * Class AxisHttpControllerList.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerList extends AxisHttpController
{
    /**
     * Section name identifier.
     * @var string
     */
    private $_sectionId;

    /**
     * Current table position.
     * @var integer
     */
    private $_tablePos;
    
    /**
     * User defined filter.
     * @var array
     */
    private $_filter = [];

    /**
     * Current record ID.
     * @var string
     */
    private $_currentId;

    /**
     * Number of rows per page.
     * @var integer
     */
    private $_rowsPerPage;

    /**
     * Maximum number of records.
     * @var integer
     */
    private $_maxRows;
    
    /**
     * Refreshes main menu?
     * @var boolean
     */
    private $_refreshMenu = false;
    
    /**
     * Are insertions allowed?
     * @var boolean
     */
    private $_isInsertable;
    
    /**
     * Are editions allowed?
     * @var boolean
     */
    private $_isEditable;
    
    /**
     * Are deletions allowed?
     * @var boolean
     */
    private $_isDeletable;

    /**
     * List node.
     * @var AxisDomNodeList
     */
    private $_list;
    
    /**
     * List of 'order-by'.
     * @var array of the form [column:string, asc:boolean]
     */
    private $_orders;
    
    /**
     * List of rows.
     * @var AxisDbRecord[]
     */
    private $_rows;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles START requests.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $this->_sectionId = $this->request->get("section");
        $this->_tablePos = $this->request->get("table");
        $this->_filter = json_decode($this->request->get("filter"), true);
        $order = json_decode($this->request->get("order"), true);

        // rows per page
        $config = $this->sitemap->getConfig();
        $this->_rowsPerPage = max(1, intval($config->query("> rows-per-page")->text()));
        $this->_refreshMenu = $config->query("> refresh-sitemap")->text() != "false";

        // section node
        $section = $this->sitemap->getSectionById($this->_sectionId);
        if (count($section) == 0 || $section->isAdmin() && !$this->user->isAdmin()) {
            throw new AxisException(
                "Section not found: " . $this->_sectionId
            );
        }

        // table node
        $this->_list = $section->getListByIndex($this->_tablePos);
        if (count($this->_list) == 0) {
            throw new AxisException(
                "Table position not found: " . $this->_tablePos
            );
        }
        
        if ($this->_list->hasAttr("refresh")) {
            $this->_refreshMenu = $this->_list->attr("refresh") != "false";
        }

        $this->_maxRows = intval(TextHelper::ifEmpty($this->_list->attr("max-rows"), "0"));
        $this->_isInsertable = $this->_list->attr("allow-insert") != "false";
        $this->_isEditable = $this->_list->attr("allow-edit") != "false";
        $this->_isDeletable = $this->_list->attr("allow-delete") != "false";

        // does the 'ord' column exist?
        $r = $this->database->query(
            "show columns from " . Db::quoteId($this->_list->attr("table")) . " like 'ord'"
        );
        $hasOrdColumn = count($r) > 0;
        
        // parses 'order' parameter
        if ($order !== null && is_array($order)) {
            $this->_orders = [];
            $fields = $this->_list->getListableFields();
            foreach ($order as $item) {
                if (!array_key_exists($item["index"], $fields)) {
                    throw new AxisException("Field index not found: $item[index]");
                }
                $field = $fields[$item["index"]];
                $isAsc = $field->isAscending();
                if (!TextHelper::isEmpty($item["asc"])) {
                    $isAsc = $item["asc"] === "true";
                }
                array_push($this->_orders, ["column" => $field->attr("column"), "asc" => $isAsc]);
            }
        }
        
        if (count($this->_orders) == 0) {
            // default order
            $this->_orders = [
                ["column" => $hasOrdColumn? "ord": "id", "asc" => true]
            ];
            
            // sets order (if any)
            $orders = $this->_list->getOrders();
            if (count($orders) > 0) {
                $this->_orders = [];
                foreach ($orders as $order) {
                    array_push(
                        $this->_orders,
                        [
                            "column" => $order->attr("column"),
                            "asc" => $order->attr("desc") != "true"
                        ]
                    );
                }
            }
        }
        
        $this->_update();
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        $action = $this->request->get("action");
        $id = $this->request->get("id");

        if ($action == "up") {
            $this->_rowUp($id);
        } elseif ($action == "down") {
            $this->_rowDown($id);
        } elseif ($action == "delete") {
            $this->_deleteRow($id);
        }
        
        $this->_update();
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    protected function getBody()
    {
        // parameters
        $currentId = $this->request->get("search_id", $this->_currentId);
        $pageNumber = TextHelper::isEmpty($currentId)
            ? $this->_getCurrentPage()
            : $this->_getPageById($currentId);
        $numPages = $this->_getNumPages();
        $numRows = count($this->_getRows());
        $rows = $this->_getRows($pageNumber);
        $fields = $this->_list->getListableFields();

        $root = new DomNode(
            'root',
            function ($target) use ($pageNumber, $numRows, $numPages, $fields, $rows) {
                // is the list 'sortable'?
                $isSortable = false;
                if (count($this->_filter) == 0 && count($this->_orders) == 1) {
                    $order = $this->_orders[0];
                    $isSortable = $order["column"] == "ord" && $order["asc"];
                }
                
                $target->append(new DomNode('title', $this->_list->attr("title")));
                $target->append(new DomNode('section', $this->_sectionId));
                $target->append(new DomNode('position', $this->_tablePos));
                $target->append(new DomNode('notes', $this->_list->getNotes()));
                $target->append(new DomNode('sortable', $isSortable? "true" : "false"));
                $target->append(new DomNode('max-rows', $this->_maxRows));
                $target->append(new DomNode('num-rows', $numRows));
                $target->append(new DomNode('page', $pageNumber));
                $target->append(new DomNode('num-pages', $numPages));
                $target->append(new DomNode('refresh', $this->_refreshMenu? "true": "false"));
                $target->append(new DomNode('allow-insert', $this->_isInsertable? "true": "false"));
                $target->append(new DomNode('allow-edit', $this->_isEditable? "true": "false"));
                $target->append(new DomNode('allow-delete', $this->_isDeletable? "true": "false"));
                $target->append(new DomNode('external-module', $this->_list->attr("external-module")));
                
                // orders
                $target->append(
                    new DomNode(
                        "order",
                        function ($target) use ($fields) {
                            foreach ($this->_orders as $order) {
                                $field = $this->_searchFieldByColumn($order["column"]);
                                if ($field !== null) {
                                    $index = array_search($field, $fields, true);
                                    $target->append(
                                        new DomNode(
                                            "field",
                                            [
                                                "index" => $index,
                                                "asc" => $order["asc"]? "true": "false"
                                            ]
                                        )
                                    );
                                }
                            }
                        }
                    )
                );
                
                // filters
                $target->append(
                    new DomNode(
                        "filter",
                        function ($target) {
                            if (count($this->_filter) > 0) {
                                $fields = $this->_getFilterFields();
                                foreach ($fields as $i => $field) {
                                    $value = json_decode($this->_filter[$i]);
                                    $filter = $field->getFilter($value);
                                    if (!TextHelper::isEmpty($filter)) {
                                        $target->append(
                                            new DomNode(
                                                "field",
                                                [
                                                    "position" => $i,
                                                    "title" => $field->attr("title"),
                                                    "value" => $this->_filter[$i],
                                                ],
                                                $filter
                                            )
                                        );
                                    }
                                }
                            }
                        }
                    )
                );
                
                // triggers
                $target->append(
                    new DomNode(
                        "triggers",
                        function ($target) {
                            $target->append(
                                new DomNode("before-delete", trim($this->_list->attr("before-delete")))
                            );
                            $target->append(
                                new DomNode("after-delete", trim($this->_list->attr("after-delete")))
                            );
                        }
                    )
                );

                // columns
                foreach ($fields as $field) {
                    $target->append(
                        new DomNode(
                            'column',
                            array(
                                "title" => $field->attr("title"),
                                "type" => $field->isTableImage()? "image" : "text",
                                "filtrable" => $field->isFiltrable()? "true": "false",
                                "sortable" => $field->isSortable()? "true": "false"
                            )
                        )
                    );
                }

                // rows
                foreach ($rows as $row) {
                    $target->append(
                        new DomNode(
                            'row',
                            array("id" => $row->id),
                            function ($target) use ($fields, $row) {
                                foreach ($fields as $field) {
                                    $columns = $field->getColumns();
                                    foreach ($columns as $column) {
                                        $column->setOriginalValue(
                                            $row->{$column->attr("column")}
                                        );
                                    }

                                    $item = new DomNode('item');
                                    $item->append(DomHelper::cdata($field->getTableValue()));
                                    $target->append($item);
                                }
                            }
                        )
                    );
                }
            }
        );

        return $root->html();
    }
    
    /**
     * Updates the properties.
     * 
     * @return void
     */
    private function _update()
    {
        $this->_rows = $this->_fetchRows();
    }
    
    /**
     * Fetches the rows from database.
     * 
     * @return AxisDbRecord[]
     */
    private function _fetchRows()
    {
        $ret = [];
        
        // get records
        $fields = $this->_list->getListableFields();
        $tableName = $this->_list->attr("table");
        $rows = iterator_to_array($this->database->query($this->_getSelectStatement()));
        foreach ($rows as $row) {
            array_push($ret, new AxisDbRecord($this->database, $tableName, $row["id"]));
        }
        
        // sorts records
        $orders = $this->_list->getOrders();
        usort(
            $ret,
            function ($row1, $row2) {
                $ret = 0;
                foreach ($this->_orders as $order) {
                    $asc = $order["asc"]? +1: -1;
                    $ret = $asc * strnatcasecmp($row1->{$order["column"]}, $row2->{$order["column"]});
                    if ($ret != 0) {
                        break;
                    }
                }
                return $ret;
            }
        );
        
        // filters records
        $filters = $this->_list->getFilters();
        foreach ($filters as $filter) {
            $column = $filter->attr("column");
            $operator = TextHelper::ifEmpty($filter->attr("operator"), "eq");
            $value = $filter->attr("value");
            $f = AxisDbFilterFactory::getInstance($operator);
            $ret = $f->filter($ret, $column, $value);
        }
        
        // user defined filters
        if (count($this->_filter) > 0) {
            $fields = $this->_getFilterFields();
            
            // filters records
            foreach ($fields as $i => $field) {
                $value = json_decode($this->_filter[$i]);
                $ret = $field->filter($ret, $value);
            }
        }
        
        return $ret;
    }

    /**
     * Gets the rows of the list.
     *
     * @param integer $pageNumber Page number (not required)
     *
     * @return AxisDbRecord[]
     */
    private function _getRows($pageNumber = null)
    {
        $ret = $this->_rows;
        
        // filters records
        if ($pageNumber !== null) {
            $offset = $this->_rowsPerPage * $pageNumber;
            $ret = array_slice($ret, $offset, $this->_rowsPerPage);
        }
        
        return $ret;
    }
    
    /**
     * Gets the SELECT statement
     * 
     * @return string
     */
    private function _getSelectStatement()
    {
        $sqlWhere = "";
        
        // gets simple filters
        $keys = array_filter(
            $this->_list->getKeys(),
            function ($key) {
                $column = trim($key->attr("column"));
                $operator = TextHelper::ifEmpty($key->attr("operator"), "eq");
                return $operator == "eq" && preg_match('/^\w+$/', $column);
            }
        );
        
        // sql where
        if (count($keys) > 0) {
            $sqlWhere = "\nwhere " . implode(
                "\nand ",
                array_map(
                    function ($key) {
                        $column = Db::quoteId($key->attr("column"));
                        $value = $this->database->quote($key->attr("value"));
                        return "$column = $value";
                    },
                    $keys
                )
            );
        }
        
        return "select id from " . Db::quoteId($this->_list->attr("table")) . $sqlWhere;
    }
    
    /**
     * Searches field by column.
     * 
     * @param string $column Column
     * 
     * @return DomNode|null
     */
    private function _searchFieldByColumn($column)
    {
        $ret = null;
        $fields = $this->_list->getListableFields();
        foreach ($fields as $field) {
            if ($field->attr("column") == $column) {
                $ret = $field;
                break;
            }
        }
        return $ret;
    }

    /**
     * Gets row by id.
     *
     * @param string $id Record id
     *
     * @return AxisDbRecord|null
     */
    private function _searchRowById($id)
    {
        $ret = null;
        $rows = $this->_getRows();
        foreach ($rows as $row) {
            if ($id == $row->id) {
                $ret = $row;
                break;
            }
        }
        return $ret;
    }

    /**
     * Gets previous the previous row.
     *
     * @param AxisDbRecord $row Record
     *
     * @return AxisDbRecord|null
     */
    private function _searchPreviousRow($row)
    {
        $ret = null;
        $rows = $this->_getRows();
        $ord1 = $row->ord;
        
        // searches the 'previous' record
        foreach (array_reverse($rows) as $row) {
            $ord2 = $row->ord;
            if ($ord1 > $ord2) {
                $ret = $row;
                break;
            }
        }
        
        return $ret;
    }

    /**
     * Gets previous the previous row.
     *
     * @param AxisDbRecord $row Record
     *
     * @return AxisDbRecord|null
     */
    private function _searchNextRow($row)
    {
        $ret = null;
        $rows = $this->_getRows();
        $ord1 = $row->ord;
        
        // searches the 'previous' record
        foreach ($rows as $row) {
            $ord2 = $row->ord;
            if ($ord1 < $ord2) {
                $ret = $row;
                break;
            }
        }
        
        return $ret;
    }

    /**
     * Gets the number of pages.
     *
     * @return integer
     */
    private function _getNumPages()
    {
        $rows = $this->_getRows();
        $numRows = count($rows);
        $numPages = floor($numRows / $this->_rowsPerPage);

        if ($numRows % $this->_rowsPerPage > 0) {
            $numPages++;
        }

        return $numPages;
    }

    /**
     * Gets the current page.
     *
     * @return integer
     */
    private function _getCurrentPage()
    {
        $pageNumber = $this->request->get("page");
        $numPages = $this->_getNumPages();

        return max(0, min($numPages - 1, $pageNumber));
    }

    /**
     * Moves the record up.
     *
     * @param string $id Record id
     *
     * @return void
     */
    private function _rowUp($id)
    {
        $row1 = $this->_searchRowById($id);
        if ($row1 === null) {
            throw new AxisException("Record not found");
        }
        
        // swaps records
        $row2 = $this->_searchPreviousRow($row1);
        if ($row2 !== null) {
            $this->_swapRows($row1, $row2);
        }
        
        $this->_currentId = $row1->id;
    }

    /**
     * Moves the record down.
     *
     * @param string $id Record id
     *
     * @return void
     */
    private function _rowDown($id)
    {
        $row1 = $this->_searchRowById($id);
        if ($row1 === null) {
            throw new AxisException("Record not found");
        }
        
        // swaps records
        $row2 = $this->_searchNextRow($row1);
        if ($row2 !== null) {
            $this->_swapRows($row1, $row2);
        }
        
        $this->_currentId = $row1->id;
    }
    
    /**
     * Swaps row positions.
     * 
     * @param AxisDbRecord $row1 First record
     * @param AxisDbRecord $row2 Second record
     * 
     * @return void
     */
    private function _swapRows($row1, $row2)
    {
        $ord1 = $row1->ord;
        $ord2 = $row2->ord;
        
        // temporary save a 'fake' ord to avoid 'unique constraint' errors
        $row2->ord = -1;
        $row2->save();
        
        $row1->ord = $ord2;
        $row1->save();
        
        $row2->ord = $ord1;
        $row2->save();
    }

    /**
     * Deletes a record.
     *
     * @param string $id Record id
     *
     * @return void
     */
    private function _deleteRow($id)
    {
        if (!$this->_isDeletable) {
            throw new AxisException("Deletions are not allowed");
        }
        
        $row = $this->_searchRowById($id);
        if ($row === null) {
            throw new AxisException("Record not found");
        }
        
        $row->delete();
    }

    /**
     * Gets the row position by id.
     *
     * @param string $id Record id
     *
     * @return integer
     */
    private function _getRowPosById($id)
    {
        $pos = 0;
        
        $rows = $this->_getRows();
        $len = count($rows);
        reset($rows);
        $row = current($rows);
        for ($i = 0; $i < $len; $i++) {
            if ($row->id == $id) {
                $pos = $i;
                break;
            }
            $row = next($rows);
        }
        
        return $pos;
    }

    /**
     * Gets the page by id.
     *
     * @param string $id Record id
     *
     * @return integer
     */
    private function _getPageById($id)
    {
        $pos = $this->_getRowPosById($id);
        $pageNumber = floor($pos / $this->_rowsPerPage);
        return $pageNumber;
    }
    
    /**
     * Gets the field used as filter.
     * 
     * @return Traversable
     */
    private function _getFilterFields()
    {
        $ret = [];
        
        $fields = $this->_list->getEditableFields();
        foreach ($this->_filter as $i => $filter) {
            if (array_key_exists($i, $fields)) {
                $ret[$i] = $fields[$i];
            }
        }
        
        return $ret;
    }
}
