<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerMenu.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerMenu extends AxisHttpController
{
    /**
     * Help documentation.
     * @var string
     */
    private $_help = "";

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * Handles START requests.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $config = $this->sitemap->getConfig();
        $this->_help = $config->query("> help")->text();
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    protected function getBody()
    {
        $root = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode("help", $this->_help));
                $target->append(
                    new DomNode(
                        "menu",
                        function ($target) {
                            $items = $this->_getMenuItems($this->sitemap);
                            foreach ($items as $item) {
                                $target->append($item);
                            }
                        }
                    )
                );
            }
        );

        return $root->html();
    }
    
    /**
     * Gets menu itmes.
     * 
     * @param DomNode $parent Parent node
     * 
     * @return array of DomNode
     */
    private function _getMenuItems($parent)
    {
        $ret = [];
        $sections = $parent->xpath("section");
        foreach ($sections as $section) {
            // discards non-authorized sections
            if ($section->attr("admin-access") == "true" && !$this->user->isAdmin()) {
                continue;
            }
            
            $item = new DomNode("item", [
                "id" => $section->attr("id"),
                "title" => $section->attr("title"),
                "align" => $section->attr("align"),
                "is_empty" => false
            ]);
            
            // appends subitems
            $subitems = $this->_getMenuItems($section);
            foreach ($subitems as $subitem) {
                $item->append($subitem);
            }
            
            // is the item empty?
            $isEmpty = count($subitems) == 0
                && strlen($section->attr("external-module")) == 0
                && count($section->xpath("list")) == 0;
            $item->attr("is_empty", $isEmpty? "true": "false");
            
            array_push($ret, $item);
        }
        return $ret;
    }
}
