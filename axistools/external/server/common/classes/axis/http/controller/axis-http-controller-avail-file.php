<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerAvailFile.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerAvailFile extends AxisHttpController
{
    /**
     * New path.
     * @var string
     */
    private $_newPath = "";

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * Handles START requests.
     *
     * @return void
     */
    public function onStart()
    {
        // paremeters
        $pathref = $this->request->get("pathref");

        // required parameters
        if (TextHelper::isEmpty($pathref)) {
            throw new AxisExceptionAlert("'pathref' is required");
        }

        // validates parameters
        $filename = AxisHelper::getAbsPath($pathref);
        if (!is_file($filename)) {
            throw new AxisExceptionAlert("File not found: $pathref");
        }

        $this->_newPath = SysFileHelper::getAvailName(dirname($filename), $filename);
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $self = $this;
        $root = new DomNode(
            'root',
            function ($target) use ($self) {
                $target->append(new DomNode('path', $self->_newPath));
            }
        );

        return $root->html();
    }
}
