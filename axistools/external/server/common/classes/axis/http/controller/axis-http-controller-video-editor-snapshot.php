<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\AxisHelper;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\http\controller\AxisHttpController;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\media\image\MediaImagePicture;
use com\soloproyectos\common\sys\ffmpeg\SysFfmpeg;
use com\soloproyectos\common\sys\file\SysFileHelper;
use com\soloproyectos\common\text\TextHelper;

/**
 * Class AxisHttpControllerVideoEditorSnapshot.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerVideoEditorSnapshot extends AxisHttpController
{
    /**
     * Snapshot padding.
     * @var integer
     */
    private $_snapshotPadding = 6;

    /**
     * Image folder.
     * @var string
     */
    private $_imageDir = '';

    /**
     * Thumbnail path.
     * @var string
     */
    private $_thumbnail = '';

    /**
     * Thumbnail width.
     * @var integer
     */
    private $_thumbnailWidth = 0;

    /**
     * Thumbnail height.
     * @var integer
     */
    private $_thumbnailHeight = 0;

    /**
     * Poster path.
     * @var string
     */
    private $_poster = '';

    /**
     * Poster width.
     * @var integer
     */
    private $_posterWidth = 0;

    /**
     * Poster height.
     * @var integer
     */
    private $_posterHeight = 0;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles 'start' requests.
     *
     * @return void
     */
    public function onStart()
    {
         $config = $this->sitemap->getConfig();
        $this->_imageDir = AxisHelper::getAbsPath(
            $config->query("directories image")->text()
        );
    }

    /**
     * Handles 'post' requests.
     *
     * @return void
     */
    public function onPost()
    {
        $src = $this->request->get("src");
        $time = floatval($this->request->get("time"));
        $this->_thumbnailWidth = intval($this->request->get("thumbnail-width"));
        $this->_thumbnailHeight = intval($this->request->get("thumbnail-height"));
        $this->_posterWidth = intval($this->request->get("poster-width"));
        $this->_posterHeight = intval($this->request->get("poster-height"));

        // required parameters
        if (TextHelper::isEmpty($src)) {
            throw new AxisExceptionAlert("The paramter 'src' is required");
        }

        // checks paramters
        $filename = AxisHelper::getAbsPath($src);
        if (!is_file($filename)) {
            throw new AxisExceptionAlert("File not found: $src");
        }

        // takes a picture
        $filename = AxisHelper::getAbsPath($src);
        $ffmpeg = new SysFfmpeg($filename);
        $snapshot = $ffmpeg->takePicture($this->_imageDir, $time);

        // creates thumbnail and poster
        $poster = $this->_saveImage($snapshot, $this->_posterWidth, $this->_posterHeight);
        $thumbnail = $this->_saveImage($snapshot, $this->_thumbnailWidth, $this->_thumbnailHeight);

        // removes original image
        unlink($snapshot);

        $this->_poster = AxisHelper::getRelPath($poster);
        $this->_thumbnail = AxisHelper::getRelPath($thumbnail);
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $xml = new DomNode(
            'root',
            function ($target) {
                $target->append(new DomNode('poster', $this->_poster));
                $target->append(new DomNode('thumbnail', $this->_thumbnail));
            }
        );

        return $xml->html();
    }

    /**
     * Saves and resize proportionaly an image.
     *
     * @param string  $input  Input image path
     * @param integer $width  Output image width
     * @param integer $height Output image height
     *
     * @return string Output path
     */
    private function _saveImage($input, $width, $height)
    {
        $w = $width + $this->_snapshotPadding;
        $h = $height + $this->_snapshotPadding;
        $output = SysFileHelper::getAvailName($this->_imageDir, $input);

        $img = new MediaImagePicture($input);
        $this->_resizeImage($img, $w, $h);
        $img->setPosition(($w - $img->getWidth()) / 2, ($h - $img->getHeight()) / 2);
        $img->save($output, $width, $height);

        return $output;
    }

    /**
     * Resize the image proportionally.
     *
     * @param MediaImagePicture $image  Image object
     * @param integer           $width  Rectangle width (default is INF)
     * @param integer           $height Rectangle height (default is INF)
     *
     * @return void
     */
    private function _resizeImage($image, $width = INF, $height = INF)
    {
        $width = intval($width);
        if ($width < 1) {
            $width = INF;
        }

        $height = intval($height);
        if ($height < 1) {
            $height = INF;
        }

        if ($width < INF && $height < INF) {
            $w0 = $width;
            $h0 = $height;
            $a0 = $h0 / $w0;
            $w1 = $image->getWidth();
            $h1 = $image->getHeight();
            $a1 = $h1 / $w1;
            $w2 = 0;
            $h2 = 0;

            if ($a0 < $a1) {
                $w2 = $w0;
                $h2 = ceil($w0 * $a1);
            } else {
                $w2 = ceil($h0 / $a1);
                $h2 = $h0;
            }

            $image->setSize($w2, $h2);
        }
    }
}
