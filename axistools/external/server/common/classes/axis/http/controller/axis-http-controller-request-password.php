<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\http\controller\AxisHttpControllerBase;
use com\axisgraphics\axis\db\model\AxisDbModelHelper;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\sys\mail\exception\SysMailException;
use com\soloproyectos\common\sys\mail\SysMail;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\record\DbRecordTable;

/**
 * Class AxisHttpControllerRequestPassword.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerRequestPassword extends AxisHttpControllerBase
{
    /**
     * Message.
     * @var string
     */
    private $_message;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("get", array($this, "onGet"));
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles GET requests.
     *
     * @return void
     */
    public function onGet()
    {
        $this->system->logout();
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        // parameters
        $email = $this->request->get("email");
        $questionId = $this->request->get("question_id");
        $answer = $this->request->get("answer");

        // required fields
        if (TextHelper::isEmpty($email)
            || TextHelper::isEmpty($questionId)
            || TextHelper::isEmpty($answer)
        ) {
            throw new AxisExceptionAlert("'Email', 'Question ID' and 'Answer' are required fields");
        }

        // searchs user by email and security answer
        $user = AxisDbModelHelper::searchUser($this->database, $email, $questionId, $answer);
        if ($user == null) {
            throw new AxisExceptionAlert("The email address or the security answer are incorrect.");
        }
        
        // generates a new password and saves the user
        $password = AxisDbModelHelper::generatePassword();
        $r = new DbRecordTable($this->database, "users");
        $r->update(["password" => sha1($password)], $user->getId());
        
        // deletes user's tokens
        $this->database->exec("delete from users_token where user_id = ?", $user->getId());

        // sends an email to the user
        $mail = new SysMail();
        $mail->setFrom(NOREPLY_MAIL);
        $mail->addTo($email);
        try {
            $mail->send(
                "Axistools: Requested Password", "Your new password is: $password"
            );
        } catch (SysMailException $e) {
            throw new AxisExceptionAlert(
                "The email could not be sent.\nPlease contact the technical support."
            );
        }

        $this->_message = "Thank you.\nYour username and password has been sent to your email address.";
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    public function getBody()
    {
        $questions = AxisDbModelHelper::getUserQuestions($this->database);
        $message = $this->_message;

        $root = new DomNode(
            "root",
            function ($target) use ($message, $questions) {
                $target->append(
                    new DomNode(
                        "background",
                        ["image" => BACKGROUND_IMAGE, "color" => BACKGROUND_COLOR]
                    )
                );
                $target->append(new DomNode("logo", ["image" => LOGO_IMAGE]));
                $target->append(new DomNode("message", $message));

                foreach ($questions as $question) {
                    $target->append(
                        new DomNode("question", array("id" => $question->getId()), $question->getText())
                    );
                }
            }
        );
        return $root->html();
    }
}
