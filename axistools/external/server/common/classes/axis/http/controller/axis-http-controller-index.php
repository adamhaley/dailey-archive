<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisException;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\http\controller\HttpController;
use com\soloproyectos\common\request\request;

/**
 * Class AxisHttpControllerIndex.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerIndex extends HttpController
{
    /**
     * Indicates where to find the different resources.
     * @var array of strings
     */
    private $_resourcePaths = array(
        "script" => "server/common/scripts/%path",
        "module" => "server/common/modules/%path",
        "external-module" => "external-modules/%path/server",
        "trigger" => "server/config/triggers/%path"
    );

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->onError(
            'com\axisgraphics\axis\exception\AxisException',
            array($this, "onAxisException")
        );

        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * On start request.
     *
     * @return void
     */
    public function onStart()
    {
        // parameters
        $type = $this->request->get("__type__");
        $path = rtrim($this->request->get("__path__"), "/");

        // loads the file
        $file = $this->_getServerFile($type, $path);
        $this->_includeFile($file);
    }

    /**
     * Prints a 404 error page on errors.
     *
     * @param AxisException $e Exception object
     *
     * @return void
     */
    public function onAxisException($e)
    {
        header("HTTP/1.1 404 Not found");
        header("Content-Type: text/html; charset=utf-8");
        echo
            '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">' .
            '<html><head>' .
            '<title>404 Not found</title>' .
            '</head><body>' .
            '<h1>Not Found</h1>' .
            '<p>' . $e->getMessage() . '</p>' .
            '</body></html>';
        die();
    }

    /**
     * Includes a file and ends the script.
     *
     * @param string $file Path to file
     *
     * @return void
     */
    private function _includeFile($file)
    {
        include $file;
        die();
    }

    /**
     * Gets a server side file.
     *
     * @param string $type Resource type
     * @param string $path Resource path
     *
     * @return string
     */
    private function _getServerFile($type, $path)
    {
        if (!array_key_exists($type, $this->_resourcePaths)) {
            throw new AxisException("Invalid resource type: $type");
        }

        // gest the module and the submodule
        $module = $path;
        $submodule = "";
        $pos = strpos($path, "/");
        if ($pos !== false) {
            $module = substr($path, 0, $pos);
            $submodule = substr($path, $pos);
        }

        // gets the directory
        $basedir = rtrim(str_replace('%path', $module, $this->_resourcePaths[$type]), "/");
        $dirname = $basedir . $submodule;
        if (!is_dir($dirname)) {
            throw new AxisException("Module not found: $path");
        }


        $indexname = "$dirname/index.php";
        if (!is_file($indexname)) {
            throw new AxisException("The module '$path' must contain a file called 'index.php'");
        }

        return $indexname;
    }
}
