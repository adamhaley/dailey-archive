<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\http\controller\AxisHttpControllerBase;
use com\axisgraphics\axis\security\exception\AxisSecurityException;
use com\soloproyectos\common\dom\node\DomNode;

/**
 * Class AxisHttpControllerLogin.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpControllerLogin extends AxisHttpControllerBase
{
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("post", array($this, "onPost"));
        $this->done();
    }

    /**
     * Handles POST requests.
     *
     * @return void
     */
    public function onPost()
    {
        // params
        $action = $this->request->get("action");

        if ($action == "logout") {
            // logs out from the system
            $this->system->logout();
        } else {
            $username = $this->request->get("username");
            $password = $this->request->get("password");
            $isPersistent = $this->request->get("remember") == "true";
            // logs into the system
            try {
                $this->user = $this->system->login($username, $password, $isPersistent);
            } catch (AxisSecurityException $e) {
                throw new AxisExceptionAlert($e->getMessage());
            }
        }
    }

    /**
     * Gets the body of the view.
     *
     * @see AxisHttpControllerBase::getBody()
     * @return string
     */
    protected function getBody()
    {
        $ret = new DomNode(
            "root",
            function ($target) {
                $target->append(
                    new DomNode(
                        "background",
                        ["image" => BACKGROUND_IMAGE, "color" => BACKGROUND_COLOR]
                    )
                );
                $target->append(new DomNode("logo", ["image" => LOGO_IMAGE]));
            }
        );
        return $ret->html();
    }
}
