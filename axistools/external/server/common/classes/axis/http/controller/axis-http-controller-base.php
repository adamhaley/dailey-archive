<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\controller;
use \Exception;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\exception\AxisExceptionAlert;
use com\axisgraphics\axis\db\model\AxisDbModelUser;
use com\axisgraphics\axis\security\AxisSecuritySystem;
use com\soloproyectos\common\dom\DomHelper;
use com\soloproyectos\common\dom\node\DomNode;
use com\soloproyectos\common\dom\node\exception\DomNodeException;
use com\soloproyectos\common\http\controller\HttpController;
use com\soloproyectos\common\text\TextHelper;
use soloproyectos\db\DbConnector;
use soloproyectos\db\exception\DbException;

/**
 * Class AxisHttpControllerBase.
 *
 * @package Axis\Http\Controller
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
abstract class AxisHttpControllerBase extends HttpController
{
    /**
     * Error type.
     * @var string
     */
    private $_errorType;
    
    /**
     * Error message.
     * @var string
     */
    private $_errorMessage;

    /**
     * Body of the view.
     * @var string
     */
    private $_body;

    /**
     * Database connection.
     * @var DbConnector
     */
    protected $database;

    /**
     * Current user.
     * @var AxisDbModelUser
     */
    protected $user;
    
    /**
     * System.
     * @var AxisSecuritySystem
     */
    protected $system;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_errorType = "";
        $this->_errorMessage = "";

        // alert error handlers
        $this->onError(
            'com\axisgraphics\axis\exception\AxisExceptionAlert',
            array($this, "onAxisExceptionAlert")
        );

        // fatal error handlers
        $this->onError(
            'com\axisgraphics\axis\exception\AxisExceptionExpiredSession',
            array($this, "onAxisExceptionExpiredSession")
        );
        $this->onError(
            'soloproyectos\db\exception\DbException',
            array($this, "onDbException")
        );
        $this->onError(
            'com\soloproyectos\common\dom\node\exception\DomNodeException',
            array($this, "onDomNodeException")
        );
        $this->onError(
            'com\axisgraphics\axis\exception\AxisException',
            array($this, "onAxisException")
        );
        $this->onError('Exception', array($this, "onException"));

        // request handlers
        $this->on("start", array($this, "onBaseStartRequest"));
        $this->on("end", array($this, "onBaseEndRequest"));
    }

    /**
     * Gets the body of the view.
     *
     * <p>This function is used by AxisHttpControllerBase::getView() to mount the view.<p>
     *
     * @return String
     */
    protected function getBody()
    {
        return "";
    }

    /**
     * Handles 'start' requests.
     *
     * @return void
     */
    public final function onBaseStartRequest()
    {
        $this->database = new DbConnector(DBNAME, DBUSER, DBPASS, DBHOST, DBCHARSET);
        $this->system = new AxisSecuritySystem($this->database);
    }

    /**
     * Handles 'end' requests.
     *
     * @return void
     */
    public final function onBaseEndRequest()
    {
        $this->_body = $this->getBody();
        if (!DomHelper::isValidXmlFragment("<root>{$this->_body}</root>")) {
            throw new AxisException("Body is not a valid XML fragment:\n\n{$this->_body}");
        }
    }

    /**
     * Gets the view.
     *
     * The 'view' is an XML document and looks as follows:
     * ```xml
     * <root>
     *      <error>
     *          <title>Error title, if applicable</title>
     *          <message>Error, if applicable</message>
     *      </error>
     *      <!-- contents here -->
     * </root>
     * ```
     *
     * @return string
     */
    public function getView()
    {
        try {
            $root = new DomNode('root');
            $root->append(
                new DomNode(
                    'error',
                    function ($target) {
                        $target->append(new DomNode('type', $this->_errorType));
                        $target->append(
                            new DomNode(
                                'title',
                                TextHelper::isEmpty($this->_errorMessage)? "" : "Alert"
                            )
                        );
                        $target->append(new DomNode('message', $this->_errorMessage));
                    }
                )
            );
            $root->append($this->_body);
        } catch (Exception $e) {
            $this->_exit("Body Error", "$e");
        }

        return "$root";
    }
    
    public function onAxisExceptionExpiredSession($e)
    {
        $this->_exit("Session Error", $e->getMessage(), "expired_session");
    }

    /**
     * Handles DbException events.
     *
     * @param DbException $e Exception
     *
     * @return void
     */
    public function onDbException($e)
    {
        $this->_exit("Database Error", $e->getMessage());
    }

    /**
     * Handles DomNodeException events.
     *
     * @param DomNodeException $e Exception
     *
     * @return void
     */
    public function onDomNodeException($e)
    {
        $this->_exit("DOM Error", $e->getMessage());
    }

    /**
     * Handles AxisException events.
     *
     * @param AxisException $e Exception object
     *
     * @return void
     */
    public function onAxisException($e)
    {
        $this->_exit("Application Error", $e->getMessage());
    }

    /**
     * Handles generic exceptions.
     *
     * @param Exception $e Exception
     *
     * @return void
     */
    public function onException($e)
    {
        $this->_exit("Fatal Error", $e);
    }

    /**
     * Handles AxisExceptionAlert events.
     *
     * @param AxisException $e Exception object
     *
     * @return void
     */
    public function onAxisExceptionAlert($e)
    {
        $this->_errorType = "client";
        $this->_errorMessage = $e->getMessage();
        $this->stopErrorPropagation();
    }

    /**
     * Terminates the program immediately and shows a message.
     *
     * @param string $title   Error title
     * @param string $message Error message
     * @param string $type    Error type (Valid values are 'client' or 'application'.
     *                        Default is application)
     *
     * @return void
     */
    private function _exit($title, $message, $type = "application")
    {
        die(
            '<root>' .
                '<error>' .
                    '<type>' . DomHelper::escape($type) . '</type>' .
                    '<title>' . DomHelper::escape($title) . '</title>' .
                    '<message>' . DomHelper::escape($message) . '</message>' .
                '</error>' .
            '</root>'
        );
    }

    /**
     * Closes resources.
     *
     * @return void
     */
    public function __destruct()
    {
        if ($this->database != null) {
            $this->database->close();
        }
    }
}
