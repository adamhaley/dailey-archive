<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\http\script;
use com\axisgraphics\axis\exception\AxisException;
use com\axisgraphics\axis\AxisHelper;
use com\soloproyectos\common\arr\ArrHelper;
use com\soloproyectos\common\http\controller\HttpController;

/**
 * Class AxisHttpScriptThumbnail.
 *
 * @package Axis\Http\Scripts
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisHttpScriptThumbnail extends HttpController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->on("start", array($this, "onStart"));
        $this->done();
    }

    /**
     * Handles 'start' requests.
     *
     * @return void
     */
    public function onStart()
    {
        $path = $this->request->get("path");
        $w0 = intval($this->request->get("width"));
        $h0 = intval($this->request->get("height"));

        $this->_printThumbnail($path, $w0, $h0);
    }

    /**
     * Resamples and prints an image.
     *
     * @param string  $path Image path
     * @param integer $w0   Output image width
     * @param integer $h0   Output image height
     *
     * @return void
     */
    private function _printThumbnail($path, $w0, $h0)
    {
        $filename = AxisHelper::getAbsPath($path);
        if (!is_file($filename)) {
            $filename = CMS_DIR . "/assets/images/broken-image.png";
        }

        // soource image
        $contents = file_get_contents($filename);
        $src = imagecreatefromstring($contents);
        $w1 = imagesx($src);
        $h1 = imagesy($src);
        $t1 = $h1 / $w1;

        // fixes the crop size
        if ($w0 == 0 && $h0 == 0) {
            $w0 = $w1;
            $h0 = $h1;
        } elseif ($w0 == 0 || $h0 == 0) {
            if ($w0 > 0) {
                $h0 = $w0 * $t1;
            } elseif ($h0 > 0) {
                $w0 = $h0 / $t1;
            }
        }

        $etag = md5_file($filename) . ".$w0.$h0";
        $httpIfNoneMatch = ArrHelper::get($_SERVER, "HTTP_IF_NONE_MATCH");
        header("Etag: $etag");

        if (trim($httpIfNoneMatch) == $etag) {
            // the thumbnail has not changed
            header("HTTP/1.1 304 Not Modified");
        } else {
            // calculates the resampled image size
            $t0 = $h0 / $w0;
            $w2 = 0;
            $h2 = 0;
            $x2 = 0;
            $y2 = 0;
            if ($t0 < $t1) {
                $w2 = $w1;
                $h2 = $w1 * $t0;
                $x2 = 0;
                $y2 = ($h1 - $h2) / 2;
            } else {
                $w2 = $h1 / $t0;
                $h2 = $h1;
                $x2 = ($w1 - $w2) / 2;
                $y2 = 0;
            }

            // resamples and prints the image
            $dst = imagecreatetruecolor($w0, $h0);
            imagecopyresampled($dst, $src, 0, 0, $x2, $y2, $w0, $h0, $w2, $h2);
            imagejpeg($dst, null, 100);
            imagedestroy($dst);
        }

        imagedestroy($src);
    }
}
