<?php
/**
 * This file is part of Axistools project.
 *
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
namespace com\axisgraphics\axis\exception;
use com\axisgraphics\axis\exception\AxisException;

/**
 * Class AxisExceptionAlert.
 *
 * This exception describes a fatal error.
 *
 * @package Axis\Exception
 * @author  Gonzalo Chumillas <gchumillas@email.com>
 * @license Proprietary License
 * @link    https://github.com/AxisStudios/axistools-server
 */
class AxisExceptionAlert extends AxisException
{

}
