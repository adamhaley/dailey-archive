<?php
/**
 * These constants are for internal use only and you do not need to change them.
 */
define("DOCUMENT_ROOT", rtrim($_SERVER["DOCUMENT_ROOT"], "/"));
define("CMS_DIR", __DIR__);
define("SCHEMA", CMS_DIR . "/common/documents/schema.xsd");
define("FIELDPATH", CMS_DIR . "/common/fields/");

/**
 * Configuration directory.
 *
 * This constant can be overridden by the AXIS_CONFIG_DIR environment variable.
 * Just enable 'mod_env' for Apache2 (a2enmod env) and add the following line
 * to the .htaccess file:
 *
 * SetEnv AXIS_CONFIG_DIR config/axistools/server
 */
$configDir = rtrim(getenv("AXIS_CONFIG_DIR"), "/");
define(
  "CONFIG_DIR",
  strlen($configDir) > 0
    ?  implode(array_filter([DOCUMENT_ROOT, $configDir]), "/") . "/"
    : "config/"
);

/**
 * Content Management System hostname.
 *
 * The application assumes that it is installed under DOCUMENT_ROOT.
 * Change this constant manually if that is not the case. For example:
 *
 * define("CMS_HOST", "my-domain.com/cmsfolder/server");
 */
$cmsFolder = getenv("AXIS_DIR");
if (strlen($cmsFolder) == 0) {
    $cmsFolder = substr(CMS_DIR, strlen(DOCUMENT_ROOT) + 1);
}
define("CMS_HOST", rtrim($_SERVER["HTTP_HOST"], "/") . "/" . rtrim($cmsFolder, "/"));

/**
 * Backward compatibility
 */
$_SERVER["HOST"] = "('live')";
define("OWNER_ID", 55);
define('ENV', $_SERVER["HOST"]);
