var webpack = require('webpack');


module.exports = {
	entry: './src/main',
	output: {
		path: __dirname,
		filename: './bundle.js'
	},
	resolve: {
		extensions: ['.js', '.css', '.styl'],
		alias: {
			vue: 'vue/dist/vue.js',
			ScrollMagicGSAP: 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap',
			ScrollToPlugin: 'gsap/src/uncompressed/plugins/ScrollToPlugin',
			jquery: 'jquery/src/jquery',
			scrollspeed: 'scroll-speed/index'
		},
        modules: [
            'node_modules'
        ]
	},
	module: {
		rules: [
			{test: /\.js$/, use: 'babel-loader',exclude: /node_modules/},
			{test: /\.styl$/, use: 'style-loader!css-loader!stylus-loader'},
			{test: /\.css$/, use: 'style-loader!css-loader!'},
			{test: /\.tpl$/, use: 'raw-loader'},
			{test: /\.(png|woff|woff2|eot|ttf|otf|svg)$/, use: 'url-loader?limit=100000'}
		]
	},
	externals: {
        'TweenLite': 'TweenLite'
	},
	devServer: {
		port:8080,
		historyApiFallback: true
	},
	plugins: [
		new webpack.DefinePlugin({
			ON_TEST: process.env.NODE_ENV === 'test'
		})
	]
}
