/*
 Navicat Premium Data Transfer

 Source Server         : axervices
 Source Server Type    : MySQL
 Source Server Version : 50623
 Source Host           : axervices-backend.cxmkn58rekqs.us-west-2.rds.amazonaws.com
 Source Database       : dailey_master

 Target Server Type    : MySQL
 Target Server Version : 50623
 File Encoding         : utf-8

 Date: 11/16/2016 12:17:21 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `content`
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `section_id` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `data_image_id` int(11) DEFAULT NULL,
  `data_text_id` int(11) DEFAULT NULL,
  `data_video_credit_id` int(11) DEFAULT NULL,
  `data_video_credit_category_id` int(11) DEFAULT NULL,
  `data_video_credit_reel_id` int(11) DEFAULT NULL,
  `data_desktop_id` int(11) DEFAULT NULL,
  `data_download_id` int(11) DEFAULT NULL,
  `data_link_id` int(11) DEFAULT NULL,
  `data_file1_id` int(11) DEFAULT NULL,
  `data_file2_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT '0',
  `hook` varchar(200) DEFAULT NULL,
  `row_state` varchar(55) NOT NULL DEFAULT 'production',
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`id`) USING BTREE,
  KEY `idx2` (`data_text_id`) USING BTREE,
  KEY `idx3` (`data_image_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2058 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `content`
-- ----------------------------
BEGIN;
INSERT INTO `content` VALUES ('2051', null, 'projects', null, null, '2051', null, null, null, null, null, null, null, null, null, '0', null, '', '2016-11-02 11:30:06', '2016-11-02 11:30:06'), ('2052', null, 'projects', null, null, '2052', null, null, null, null, null, null, null, null, null, '1', null, '', '2016-11-02 11:30:14', '2016-11-15 10:16:54'), ('2056', null, 'projects.2051', null, '1169', '2055', null, null, null, null, null, null, null, null, null, '3', null, '', '2016-11-04 12:56:25', '2016-11-10 20:25:36'), ('2055', null, 'about', null, null, '2054', null, null, null, null, null, null, null, null, null, '2', null, '', '2016-11-03 09:22:48', '2016-11-15 10:08:51'), ('2057', null, 'projects.2052', null, '1170', '2056', null, null, null, null, null, null, null, null, null, '4', null, '', '2016-11-10 20:34:28', '2016-11-15 10:17:11');
COMMIT;

-- ----------------------------
--  Table structure for `data_image`
-- ----------------------------
DROP TABLE IF EXISTS `data_image`;
CREATE TABLE `data_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `data_video_credit_id` int(11) DEFAULT NULL,
  `original` varchar(200) DEFAULT NULL,
  `thumb` varchar(200) DEFAULT NULL,
  `thumb1` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `image1` varchar(200) DEFAULT NULL,
  `image2` varchar(200) DEFAULT NULL,
  `image3` varchar(200) DEFAULT NULL,
  `image4` varchar(255) DEFAULT NULL,
  `image5` varchar(255) DEFAULT NULL,
  `image6` varchar(255) DEFAULT NULL,
  `image7` varchar(255) DEFAULT NULL,
  `image8` varchar(255) DEFAULT NULL,
  `image9` varchar(255) DEFAULT NULL,
  `image10` varchar(255) DEFAULT NULL,
  `image11` varchar(255) DEFAULT NULL,
  `image12` varchar(255) DEFAULT NULL,
  `image13` varchar(255) DEFAULT NULL,
  `image14` varchar(255) DEFAULT NULL,
  `original_image` varchar(255) DEFAULT NULL,
  `gallery_image` enum('yes','no') NOT NULL DEFAULT 'no',
  `gallery_name` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `row_state` varchar(55) NOT NULL DEFAULT 'live',
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1171 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `data_image`
-- ----------------------------
BEGIN;
INSERT INTO `data_image` VALUES ('1169', null, null, null, null, null, null, '/data/images/arches.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', ''), ('1170', null, null, null, null, null, null, '/data/images/Screen Shot 2016-11-15 at 10.09.51 AM.png', null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'no', null, null, 'live', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `data_link`
-- ----------------------------
DROP TABLE IF EXISTS `data_link`;
CREATE TABLE `data_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `text` text,
  `url` varchar(200) DEFAULT NULL,
  `open_mode` enum('normal','window','popup','special') NOT NULL DEFAULT 'window',
  `popup_width` int(11) DEFAULT NULL,
  `popup_height` int(11) DEFAULT NULL,
  `bg_color` varchar(255) DEFAULT NULL,
  `special_page` varchar(255) DEFAULT NULL,
  `special_title` varchar(255) DEFAULT NULL,
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `data_text`
-- ----------------------------
DROP TABLE IF EXISTS `data_text`;
CREATE TABLE `data_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `heading` text,
  `text` text,
  `text1` text,
  `text2` text,
  `text3` text,
  `text4` text,
  `text5` text,
  `text6` text,
  `text7` text,
  `text8` text,
  `text9` text,
  `text10` text,
  `text11` text,
  `text12` text,
  `text13` text,
  `text14` text,
  `text15` text,
  `text16` text,
  `text17` text,
  `text18` text,
  `bg_color` varchar(255) DEFAULT NULL,
  `bg_image` varchar(255) DEFAULT NULL,
  `row_state` varchar(55) NOT NULL DEFAULT 'live',
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`id`) USING BTREE,
  FULLTEXT KEY `ft_text` (`text`),
  FULLTEXT KEY `ft_text1` (`text1`),
  FULLTEXT KEY `ft_text2` (`text2`),
  FULLTEXT KEY `ft_text3` (`text3`),
  FULLTEXT KEY `ft_text4` (`text4`),
  FULLTEXT KEY `ft_text5` (`text5`),
  FULLTEXT KEY `ft_text6` (`text6`),
  FULLTEXT KEY `ft_text7` (`text7`),
  FULLTEXT KEY `ft_text8` (`text8`),
  FULLTEXT KEY `ft_text9` (`text9`),
  FULLTEXT KEY `ft_text10` (`text10`),
  FULLTEXT KEY `ft_text11` (`text11`),
  FULLTEXT KEY `ft_text12` (`text12`)
) ENGINE=MyISAM AUTO_INCREMENT=2057 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `data_text`
-- ----------------------------
BEGIN;
INSERT INTO `data_text` VALUES ('2051', null, null, 'abc', 'xyz', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2052', null, null, 'Honda Pioneer 1000', 'Crazy Story', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2053', null, null, null, '8687 Melrose Ave. Suite G300', null, 'West Hollywood', 'CA', '90069', '310.360.3100', 'hello@daileyideas.com', null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2054', null, null, 'We Tell Brand Stories', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\"><br />Dailey is a full-service, creative ad agency. We\'re based in Los Angeles where we\'ve been telling memorable brand stories for almost 50 years.<br /><br />We believe a good idea can live anywhere, which is why we engage consumers across all mediums. Whether it&rsquo;s experiential, digital, broadcast, or print, we specialize in smart executions with proven results.<br /><br />The only thing we love more than the work is the clients we work with, so give us a call. We can&rsquo;t wait to meet you. (310) 360-3100 or hello@daileyideas.com.</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2055', null, null, 'image', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">abc details</span></p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', ''), ('2056', null, null, 'video', '<p><span style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 14pt; color: #333333;\">aergaerg</span></p>', null, 'https://vimeo.com/176784080/d7b271b4f1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'live', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `data_video_credit`
-- ----------------------------
DROP TABLE IF EXISTS `data_video_credit`;
CREATE TABLE `data_video_credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `studio` varchar(200) DEFAULT NULL,
  `client` varchar(200) DEFAULT NULL,
  `director` varchar(200) DEFAULT NULL,
  `work_type` varchar(200) DEFAULT NULL,
  `show_in_works` enum('yes','no') DEFAULT 'yes',
  `release` varchar(10) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `link_type` enum('external','internal') NOT NULL DEFAULT 'external',
  `usage` varchar(200) DEFAULT NULL,
  `data_image_id` int(11) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `high_video` varchar(200) DEFAULT NULL,
  `snapshot` varchar(200) DEFAULT NULL,
  `description` text,
  `ord` int(11) NOT NULL DEFAULT '0',
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `data_video_credit_category`
-- ----------------------------
DROP TABLE IF EXISTS `data_video_credit_category`;
CREATE TABLE `data_video_credit_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `site_section_id` int(11) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT '0',
  `row_state` enum('tentative','active','updated','deleted','live','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `import`
-- ----------------------------
DROP TABLE IF EXISTS `import`;
CREATE TABLE `import` (
  `import_id` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` text,
  `SUBTITLE` text,
  `ACTIVE` varchar(4) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `EXTERNAL_URL` text,
  `EDITED` datetime DEFAULT NULL,
  `OVERVIEW` text,
  `CATEGORY` varchar(255) DEFAULT NULL,
  `Related People` text,
  `Related Practices` text,
  `Related File` text,
  `Overriding File` text,
  PRIMARY KEY (`import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `site_section`
-- ----------------------------
DROP TABLE IF EXISTS `site_section`;
CREATE TABLE `site_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT '0',
  `hook` varchar(255) DEFAULT NULL,
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `canonical_link` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `type` enum('generic','form','image_gallery','video_gallery','custom','popup','temporary') NOT NULL DEFAULT 'generic',
  `metadata_id` int(11) DEFAULT NULL,
  `visible` enum('yes','no') NOT NULL DEFAULT 'yes',
  `top` enum('yes','no') NOT NULL DEFAULT 'no',
  `bottom` enum('yes','no') NOT NULL DEFAULT 'no',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subdomain` enum('main','sexy') NOT NULL,
  `page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `site_section_path`
-- ----------------------------
DROP TABLE IF EXISTS `site_section_path`;
CREATE TABLE `site_section_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `path_by_id` text,
  `url` text,
  `path_by_index` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `section_id` (`section_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `sys_admin` tinyint(1) NOT NULL DEFAULT '0',
  `login_enable` tinyint(4) NOT NULL DEFAULT '1',
  `ord` int(11) NOT NULL DEFAULT '0',
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('51', null, 'don', 'don', '$2y$10$V9GsPlo19PQwckdQJJDzxedCje/C5cez0ofVtPM2QWfdOsG1B6QCC', 'DLupo@daileyideas.com', '1', '1', '3', '', '2016-11-02 12:45:40', '2016-11-02 13:33:34', '0'), ('49', '55', 'jeffrey', 'jeffrey', '$2y$10$PDGq88G/MwSRry8hMUjoWOIlUXqYxpjVd05uHDHRmyhLBG2UAUTR.', 'jeffrey@axis-studios.com', '1', '1', '2', 'live', '2010-06-08 00:00:00', '2013-05-06 13:38:50', '0'), ('52', null, 'guest', 'guest', '$2y$10$ILPTJN7iD57zn2LgycjSoe/auMcl25Xr8NaJTu.ST12uL.8eI2ZZ.', 'rudy@axis-studios.com', '0', '1', '4', '', '2016-11-02 13:15:25', '2016-11-02 13:33:41', '0');
COMMIT;

-- ----------------------------
--  Table structure for `users_role`
-- ----------------------------
DROP TABLE IF EXISTS `users_role`;
CREATE TABLE `users_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `row_state` enum('live','dev','test','none','tentative','active','updated','deleted','pending') NOT NULL DEFAULT 'live',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `users_token`
-- ----------------------------
DROP TABLE IF EXISTS `users_token`;
CREATE TABLE `users_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `selector` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`selector`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `users_token`
-- ----------------------------
BEGIN;
INSERT INTO `users_token` VALUES ('111', '49', '', '6e7e4ca093048d26b7114e9ba459f0fd1344ceca', '444fd36fecd5c302b6f06009438422e42c383d5f'), ('112', '49', '', '66a79ca36fefb8a11d926b66605017973839ff16', 'ce923e0ca50198e6b1ce6ce78d562c93f50736d0'), ('113', '49', '', '3b17b9a29f3c472af399f6c8024f72fc39cc3671', 'ade2a07197a9dea9968315fe24ce0578761cfd5a'), ('114', '49', '', '0783f4493e5e8dda321bfbf5dcfbe571fefb9017', '7698670a6101a2be554f6b9126f592ac48d6c1c5'), ('115', '51', '', 'fa06f429aae11cb20f786525479e05ff08204de9', 'ce2063ec92f5abe55ff310b7ac2dd104e0c3e514'), ('116', '51', '', '299163d09021683bdc5b15033064c7f1c8cdefaf', 'f06b2675c82b5fbdedcb59ec3d6a6908d3566d7a'), ('117', '49', '', '4125f01b88f1bb2ef763ef75123f9d389234720f', '8d593579820507ddaff9a372d800d7bc22f78a3f'), ('118', '49', '', 'c296417e407a9f8243b6c4c8e885d6fdd954f9df', 'aeea8c3678a815af00f5be76c7e21e8c853dd58e'), ('119', '49', '', '1321b636128b9956cf983868f8530d41671edb52', 'de83fc8e0e5d9d5bca7d07c2c812779d6e845314'), ('120', '49', '', '4027ba061f0e92d0add080f8ccc3b5e62cc38417', '8a5fd9f7d1345ca51e8e084c3bd1ef30d67b989c'), ('121', '51', '', '9a4971fdc790e4861c82168bc4ad9f471ed47411', '0a81dd6282d05e55c411852ba51c08a22712e144'), ('122', '51', '', '8cd5533a4b574b3982dba3c9555858310bf41d2a', '609be9488abacd06569f7996ddda74b259d5c066'), ('123', '51', '', 'e88b04795b4e028c5f8f1b7e4b286c012dc19532', 'e954be8cd89d49184daa2b8ec71ac27bb363ed66'), ('124', '51', '', '2a194cdf66af87a55f05776b625289db4aeedc22', '0de9d884ad5f5804fc40ea332339e4032ba414e8'), ('125', '49', '', '2050b7c4a405cbd50f69782fc5ea034acfccad3d', '1f455ee02d4726766fe603dea1b8a372aec5e5f8'), ('126', '51', '', 'eb34fe4bef9c69536450abdae45653f37f4aaf32', 'f37fa0f5110b3d164c29338de96adccefd3f3b2f'), ('127', '51', '', '773eec6c6ea4fe5b56b5efa28a414fe38bea9058', '61c9fe28c00bbc37f3ac0fe6341f6af4f6a5709e'), ('128', '49', '', '9f4685abfc349b4568e4c693149345c7cd615e13', 'dcfb84ff2889fd21e92255ac62bf7293e239796c'), ('129', '51', '', '2c05394b4e09aa0c17efc60fb2a8fe3bd766bd96', '5c0e6a200c518ae2253cd18bb9073f981d7818c3');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
